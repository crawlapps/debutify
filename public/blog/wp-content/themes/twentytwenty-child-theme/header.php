<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0" >

<link rel="profile" href="https://gmpg.org/xfn/11">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/assets/css/bootstrap.min.css">
<script src="<?php echo get_stylesheet_directory_uri();?>/assets/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<?php wp_head(); ?>


<script type="text/javascript">
    $(document).ready(function() {

    var getMax = function() {
        return $(document).height() - $(window).height();
    }

    var getValue = function() {
        return $(window).scrollTop();
    }

    if ('max' in document.createElement('progress')) {
        var progressBar = $('progress');
        
        progressBar.attr({
            max: getMax()
        });

        $(document).on('scroll', function() {
            progressBar.attr({
                value: getValue()
            });
        });

        $(window).resize(function() {
            
            progressBar.attr({
                max: getMax(),
                value: getValue()
            });
        });

    } else {

        var progressBar = $('.progress-bar'),
            max = getMax(),
            value, width;

        var getWidth = function() {
            
            value = getValue();
            width = (value / max) * 100;
            width = width + '%';
            return width;
        }

        var setWidth = function() {
            progressBar.css({
                width: getWidth()
            });
        }

        $(document).on('scroll', setWidth);
        $(window).on('resize', function() {
            
            max = getMax();
            setWidth();
        });
    }
	
	$('.sp-smartbar__close').on('click',function(){
		
		$('.annocement_bar').css('display','none');
		$('.header-inner .toggle').css('top','0px');
		$('.site-logo').css('top','0px');
		
	});
});
</script>   
	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

		<header id="site-header" class="header-footer-group" role="banner">
		
		
			<?php
				// Check whether the header search is activated in the customizer.
					$headerTopText = get_theme_mod( 'theme_text_change', true );
					$headerTopURL = get_theme_mod( 'annocement_bar_link', true );
					
					$theme_text_change2 = get_theme_mod( 'theme_text_change2', true );
					$call_to_action_link = get_theme_mod( 'call_to_action_link', true );
					
					if(!empty($headerTopURL)){
						
						$headerTopURL=$headerTopURL;
					}else{
						
						$headerTopURL='javascript:void(0);';
					}	
					
					
					if(!empty($call_to_action_link)){
						
						$call_to_action_link=$call_to_action_link;
					}else{
						
						$call_to_action_link='javascript:void(0);';
					}

					if ( !empty($headerTopText) ) {
						
					?>

					
						
							<div class="annocement_bar" style="background: <?php echo get_theme_mod('background_color_annoucement',true); ?>;color: <?php echo get_theme_mod('text_color',true); ?>">
								
								<div class="sp-smartbar__body">
									
									


								<a href="<?php echo $headerTopURL;?>" class="text_area" >
									<span>
									<?php echo get_theme_mod('theme_text_change',true);?>
									</span>
								</a>
							<?php if(!empty($theme_text_change2)){?>
								<div class="sp-smartbar__button">
										<a href="<?php echo $call_to_action_link;?>"><?php echo $theme_text_change2;?></a>
								</div>
							<?php } ?>
							<button title="close" class="sp-smartbar__close" style="
										top: 20%;color:#fff;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 22.6 22.6" style="height: 1em; width: 1em; xml:space=;float: right;" preserve"="" preserveAspectRatio="none">
										<rect fill="currentColor" x="8.3" y="-1.7" transform="matrix(0.7071 0.7071 -0.7071 0.7071 11.3137 -4.6863)" width="6" height="26"></rect>
										<rect fill="currentColor" x="8.3" y="-1.7" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 27.3137 11.3137)" width="6" height="26"></rect>
										</svg>
										</button>
								</div>
							</div>
						
				  <?php } ?>
			<div class="header-inner section-inner">

				<div class="header-titles-wrapper">

					<?php

					// Check whether the header search is activated in the customizer.
					$enable_header_search = get_theme_mod( 'enable_header_search', true );

					if ( true === $enable_header_search ) {

						?>

						<button class="toggle search-toggle mobile-search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
							<span class="toggle-inner">
								<span class="toggle-icon">
									<?php twentytwenty_the_theme_svg( 'search' ); ?>
								</span>
								<span class="toggle-text"><?php _e( 'Search', 'twentytwenty' ); ?></span>
							</span>
						</button><!-- .search-toggle -->

					<?php } ?>

					<div class="header-titles">

						<?php
							// Site title or logo.
							twentytwenty_site_logo();

							// Site description.
							twentytwenty_site_description();
						?>

					</div><!-- .header-titles -->

					<button class="toggle nav-toggle mobile-nav-toggle" data-toggle-target=".menu-modal"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
						<span class="toggle-inner">
							<span class="toggle-icon">
								<?php twentytwenty_the_theme_svg( 'ellipsis' ); ?>
							</span>
							<span class="toggle-text"><?php _e( 'Menu', 'twentytwenty' ); ?></span>
						</span>
					</button><!-- .nav-toggle -->

				</div><!-- .header-titles-wrapper -->

				<div class="header-navigation-wrapper">

					<?php
					if ( has_nav_menu( 'primary' ) || ! has_nav_menu( 'expanded' ) ) {
						?>

							<nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e( 'Horizontal', 'twentytwenty' ); ?>" role="navigation">

								<ul class="primary-menu reset-list-style">

								<?php
								if ( has_nav_menu( 'primary' ) ) {

									wp_nav_menu(
										array(
											'container'  => '',
											'items_wrap' => '%3$s',
											'theme_location' => 'primary',
										)
									);

								} elseif ( ! has_nav_menu( 'expanded' ) ) {

									wp_list_pages(
										array(
											'match_menu_classes' => true,
											'show_sub_menu_icons' => true,
											'title_li' => false,
											'walker'   => new TwentyTwenty_Walker_Page(),
										)
									);

								}
								?>

								</ul>

							</nav><!-- .primary-menu-wrapper -->

						<?php
					}

					if ( true === $enable_header_search || has_nav_menu( 'expanded' ) ) {
						?>

						<div class="header-toggles hide-no-js">

						<?php
						if ( has_nav_menu( 'expanded' ) ) {
							?>

							<div class="toggle-wrapper nav-toggle-wrapper has-expanded-menu">

								<button class="toggle nav-toggle desktop-nav-toggle" data-toggle-target=".menu-modal" data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
									<span class="toggle-inner">
										<span class="toggle-text"><?php _e( 'Menu', 'twentytwenty' ); ?></span>
										<span class="toggle-icon">
											<?php twentytwenty_the_theme_svg( 'ellipsis' ); ?>
										</span>
									</span>
								</button><!-- .nav-toggle -->

							</div><!-- .nav-toggle-wrapper -->

							<?php
						}

						if ( true === $enable_header_search ) {
							?>

							<div class="toggle-wrapper search-toggle-wrapper">

								<button class="toggle search-toggle desktop-search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
									<span class="toggle-inner">
										<?php twentytwenty_the_theme_svg( 'search' ); ?>
										<span class="toggle-text"><?php _e( 'Search', 'twentytwenty' ); ?></span>
									</span>
								</button><!-- .search-toggle -->

							</div>

							<?php
						}
						?>

						</div><!-- .header-toggles -->
						<?php
					}
					?>

				</div><!-- .header-navigation-wrapper -->

			</div><!-- .header-inner -->

			<?php
			// Output the search modal (if it is activated in the customizer).
			if ( true === $enable_header_search ) {
				get_template_part( 'template-parts/modal-search' );
			}
			?>

		</header><!-- #site-header -->

<!-- 
        The progress bar 
            - Value is 0 so it doesn't have any size to start with
            - Max is set to 1 in case of an older browser that doesn't support progress bars.
        -->
<progress value="0" max="1">
            <!-- Older browsers look to the old div style of progress bars -->
            <!-- Newer browsers ignore this stuff but it allows older browsers to 
                    still have a progress bar. -->
            <div class="progress-container">
                <span class="progress-bar"></span>    
            </div>
</progress>
		<?php
		// Output the menu modal.
		get_template_part( 'template-parts/modal-menu' );