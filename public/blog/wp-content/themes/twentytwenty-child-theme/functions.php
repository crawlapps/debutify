<?php
function mychildtheme_enqueue_styles() {
wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}
add_action( 'wp_enqueue_scripts', 'mychildtheme_enqueue_styles' ); 

// Changing excerpt more
   function new_excerpt_more($more) {
   global $post;
   return '… <br> <a class="btn more-link" href="'. get_permalink($post->ID) . '">' . 'Continue reading &raquo;' . '</a>';
   }
   add_filter('excerpt_more', 'new_excerpt_more');
   
   
   function twentytwenty_sidebar_registration_custom() {

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
		'after_title'   => '</h2>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget'  => '</div></div>',
	);


		// Footer #4.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Footer #4', 'twentytwenty' ),
				'id'          => 'sidebar-44',
				'description' => __( 'Widgets in this area will be displayed in the second column in the footer.', 'twentytwenty' ),
			)
		)
	);	
	
	
	
	
	// BLog sidebar #2.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Blog SIdebar #4', 'twentytwenty' ),
				'id'          => 'sidebar-4',
				'description' => __( 'Widgets in this area will be displayed in the second column in the footer.', 'twentytwenty' ),
			)
		)
	);

}

add_action( 'widgets_init', 'twentytwenty_sidebar_registration_custom' );

function child_theme_mytheme_customize_register( $wp_customize ) {
   //All our sections, settings, and controls will be added here

/* Section to change the color of the text / background */
$wp_customize->add_section( 'cd_colors' , array(
    'title'      => 'Annocement Bar Colors',
    'priority'   => 30,
) );


$wp_customize->add_setting( 'text_color' , array(
    'default'     => '#43C6E4',
    'transport'   => 'refresh',
) );

$wp_customize->add_setting( 'background_color_annoucement' , array(
    'default'     => '#43C6E4',
    'transport'   => 'refresh',
) );



$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color_annoucement', array(
	'label'        => 'Background Color',
	'section'    => 'cd_colors',
	'settings'   => 'background_color_annoucement',
) ) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'text_color', array(
	'label'        => 'Text  Color ',
	'section'    => 'cd_colors',
	'settings'   => 'text_color',
) ) );



/* Section to change the text */

$wp_customize->add_section( 'custom_section' , array(
    'title'      => 'Annocement Bar Text',
    'priority'   => 31,
) );
$wp_customize->add_section( 'custom_section_2' , array(
    'title'      => 'Call to action  Text',
    'priority'   => 32,
) );


$wp_customize->add_setting( 'annocement_bar_link' , array(
  'capability' => 'edit_theme_options',
  'default' => '',
  'sanitize_callback' => 'sanitize_text_field',
) );


$wp_customize->add_control( 'annocement_bar_link', array(
  'type' => 'url',
  'section' => 'custom_section', // // Add a default or your own section
  'label' => __( 'Enter the URL' ),
  'description' => __( 'This is a custom URL.' ),
) );

$wp_customize->add_setting( 'call_to_action_link' , array(
  'capability' => 'edit_theme_options',
  'default' => '',
  'sanitize_callback' => 'sanitize_text_field',
) );


$wp_customize->add_control( 'call_to_action_link', array(
  'type' => 'url',
  'section' => 'custom_section_2', // // Add a default or your own section
  'label' => __( 'Enter the URL' ),
  'description' => __( 'This is a custom URL.' ),
) );

$wp_customize->add_setting( 'theme_text_change', array(
  'capability' => 'edit_theme_options',
  'default' => 'Lorem Ipsum Dolor Sit amet',
  'sanitize_callback' => 'sanitize_textarea_field',
) );



$wp_customize->add_control( 'theme_text_change', array(
  'type' => 'textarea',
  'section' => 'custom_section', // // Add a default or your own section
  'label' => __( 'Custom Text Area' ),
  'description' => __( 'This is a custom textarea.' ),
) );

$wp_customize->add_setting( 'theme_text_change2', array(
  'capability' => 'edit_theme_options',
  'default' => 'Get access now.',
  'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( 'theme_text_change2', array(
  'type' => 'text',
  'section' => 'custom_section_2', // // Add a default or your own section
  'label' => __( 'Custom Text box' ),
  'description' => __( 'Get access now.' ),
) );

/* Section to change the text ends here */

}
add_action( 'customize_register', 'child_theme_mytheme_customize_register' );