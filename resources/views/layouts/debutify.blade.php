<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        @if (env('APP_TRACKING'))
        <script>
          dataLayer = [];
        </script>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-M5BFQ4Q');</script>
        <!-- End Google Tag Manager -->
        @endif

        <title>{{ config('shopify-app.app_name') }}</title>

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@4.18.0/styles.min.css" />
        <link rel="stylesheet" href="{{ asset('css/debutify.css') }}?version={{time()}}" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

        @yield('styles')
    </head>

    <body class="template-@yield('title')">

        @if (env('APP_TRACKING'))
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M5BFQ4Q"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        @endif

        <div class="main">
          @include('components.sidebar')

          <div class="page-container">
            <div class="Polaris-Page">

                <!-- mobile menu button -->
                <div class="Polaris-Page__Content layout-item btn-open-nav-container" style="display:none;margin-bottom:0;">
                  <button type="button" class="btn-open-nav Polaris-Button Polaris-Button--sizeLarge Polaris-Button--fullWidth" tabindex="0" data-polaris-unstyled="true">
                    <span class="Polaris-Button__Content">
                      <span class="Polaris-Icon" style="margin-right:5px;">
                        <svg viewBox="0 0 20 20" class="v3ASA" focusable="false" aria-hidden="true"><path d="M19 11H1a1 1 0 1 1 0-2h18a1 1 0 1 1 0 2zm0-7H1a1 1 0 0 1 0-2h18a1 1 0 1 1 0 2zm0 14H1a1 1 0 0 1 0-2h18a1 1 0 1 1 0 2z"></path></svg>
                      </span>
                      Open menu
                    </span>
                  </button>
                </div>

                <!-- page title -->
                <div class="Polaris-Page-Header Polaris-Page-Header--separator">
                  @hasSection("breadcrumbs_link")
                  <div class="Polaris-Page-Header__Navigation">
                    <div class="Polaris-Page-Header__BreadcrumbWrapper">
                      <nav role="navigation">
                        <a class="Polaris-Breadcrumbs__Breadcrumb" href="{{env('APP_PATH')}}@yield('breadcrumbs_link')" data-polaris-unstyled="true">
                          <span class="Polaris-Breadcrumbs__ContentWrapper">
                            <span class="Polaris-Breadcrumbs__Icon">
                              <span class="Polaris-Icon">
                                <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16" fill-rule="evenodd"></path></svg>
                              </span>
                            </span>
                            <span class="Polaris-Breadcrumbs__Content page-title">@yield('breadcrumbs_title')</span>
                          </span>
                        </a>
                      </nav>
                    </div>
                  </div>
                  @endif

                  <div class="Polaris-Page-Header__MainContent">
                    <div class="Polaris-Page-Header__TitleActionMenuWrapper">
                      <div>
                        <div class="Polaris-Header-Title__TitleAndSubtitleWrapper">
                          <div class="Polaris-Header-Title">
                            <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge page-title">@yield('title')</h1>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                @include("components.general-banners")

                <div class="Polaris-Page__Content">
                  @yield('content')
                </div>

                <div class="Polaris-FooterHelp">
                   <div class="Polaris-FooterHelp__Content">
                      <div class="Polaris-FooterHelp__Icon"><span class="Polaris-Icon Polaris-Icon--colorTeal Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><circle cx="10" cy="10" r="9" fill="#ffffff"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8m0-4a1 1 0 1 0 0 2 1 1 0 1 0 0-2m0-10C8.346 4 7 5.346 7 7a1 1 0 1 0 2 0 1.001 1.001 0 1 1 1.591.808C9.58 8.548 9 9.616 9 10.737V11a1 1 0 1 0 2 0v-.263c0-.653.484-1.105.773-1.317A3.013 3.013 0 0 0 13 7c0-1.654-1.346-3-3-3"></path></svg></span></div>
                      <div class="Polaris-FooterHelp__Text">Need help?
                        <a href="https://debutify.crisp.help/" target="_blank" class="Polaris-Link" data-polaris-unstyled="true" data-no_loader="true">Visit Helpdesk</a>.
                      </div>
                  </div>
                </div>
            </div>
          </div>
        </div>

        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
        <script type="text/javascript" src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

        <!-- LinkMink -->
    		<script src="https://cdn.linkmink.com/lm-js/2.2.0/lm.js"></script>

        <!-- ActiveCampaign -->
        <script type="text/javascript">
        (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
        vgo('setAccount', '799504461');
        vgo('setTrackByDefault', true);
        vgo('process');
        </script>

        <!-- Shopify -->
        <script src="https://cdn.shopify.com/s/assets/external/app.js?{{ date('YmdH') }}"></script>

        <!-- Crisp -->
        <script type="text/javascript">
          $crisp = [];
          CRISP_TOKEN_ID = 'c77253db-15cd-4399-82ae-{{Auth::user()->id}}';
          CRISP_WEBSITE_ID = '06fc7d7b-6235-4972-8b15-033f45837454';
          (function(){d=document;s=d.createElement('script');s.src='//client.crisp.chat/l.js';s.async=1;d.getElementsByTagName('head')[0].appendChild(s);})();
        </script>

        <!-- Shopify/Polaris -->
        <script src="https://unpkg.com/@shopify/app-bridge{{ config('shopify-app.appbridge_version') ? '@'.config('shopify-app.appbridge_version') : '' }}"></script>
        <script type="text/javascript">
          // init shopify app
          var AppBridge = window['app-bridge'];
          var actions = AppBridge.actions;
          var TitleBar = actions.TitleBar;
          var Button = actions.Button;
          var Redirect = actions.Redirect;
          var Loading = actions.Loading;
          var createApp = AppBridge.default;
          var Toast = actions.Toast;
          var Modal = actions.Modal;

          var ShopifyApp = createApp({
              apiKey: '{{ config('shopify-app.api_key') }}',
              shopOrigin: '{{ Auth::user()->name }}',
              forceRedirect: true,
          });

          var breadcrumb = Button.create(ShopifyApp, { label: 'My breadcrumb' });
          const titleBarOptions = {
            title: 'My page title',
            breadcrumbs: breadcrumb
          };
          const ShopifyTitleBar = TitleBar.create(ShopifyApp, titleBarOptions);

          const loading = Loading.create(ShopifyApp);


          // show loading on href trigger
          $("a[href]:not([target='_blank'])").on("click",function(){
            loading.dispatch(Loading.Action.START);

          })

          // remove skeleton when page is loaded
          $(window).on("load", function() {
            $('.skeleton-wrapper,.Polaris-SkeletonPage__Page').css('display','none');
            $('#dashboard').css('display','block');
          });

          // dissmiss banner
          $('.dismiss-banner').click(function(){
            var banner = $(this).closest('.Polaris-Banner');
            banner.hide();
          });

          // open modal
          function openModal(modal){
            setTimeout(function(){
              $("body").addClass("modal-open");
              modal.addClass('open').show();
            }, 10);
          }

          // close modal
          function closeModal(modal){
            if(modal){
              modal.removeClass('open').hide();
            } else{
              $(".modal").removeClass('open').hide();
            }
            $("body").removeClass("modal-open");

            // plan page
            $('.link-uninstall').show();
            $('.btn-update').show();
            $('.cancel-link-uninstall').hide();
            $('.btn-uninstall').hide();
            $('.radio-button').off("click");
            $('.tutorial').attr("src","");

            // course view
            modal.find(".course-video").trigger("pause");

            // initiate checkout tracking - remove on modal close
            sessionStorage.removeItem('initiateCheckoutMonthly');
            sessionStorage.removeItem('initiateCheckoutYearly');
          }

          $(document).on('click','.close-modal',function(e){
            var modal = $(this).closest(".modal");
            closeModal(modal);
          });

          // select
          $(".Polaris-Select__Input").each(function(){
            var selectedText = $(this).find("option:selected").text();
            $(this).closest(".Polaris-Select").find(".Polaris-Select__SelectedOption").text(selectedText);
          });
          $(".Polaris-Select__Input").on('change', function(){
            var selectedText = $(this).find("option:selected").text();
            $(this).closest(".Polaris-Select").find(".Polaris-Select__SelectedOption").text(selectedText);
          });

          // button loading
          $('.btn-loading').click(function(){
            var $this = $(this);
            var btnText = $this.find('.Polaris-Button__Text').text();

            if( $this.hasClass('Polaris-Button--primary') || $this.hasClass('Polaris-Button--destructive')){
              var spinnerInk = 'Polaris-Spinner--colorWhite';
            } else{
              var spinnerInk = 'Polaris-Spinner--colorInkLightest';
            }

            loading.dispatch(Loading.Action.START);


            $('.disable-while-loading').addClass("Polaris-Button--disabled").prop("disabled", true);

            $this.addClass('Polaris-Button--disabled Polaris-Button--loading').prop("disabled", true).html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner '+ spinnerInk +' Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">'+btnText+'</span></span>');
          });

          // flash notification
        </script>
        @include('shopify-app::partials.flash_messages')

        <!-- Common script -->
        <script type="text/javascript">
          //crisp
          $crisp.push(["set", "user:email", "{{ $owner_details['email'] }}"]);

          //segments
          $crisp.push(["set", "session:segments", [["app", "{{$alladdons_plan}}"], true]]);

          @if($all_addons == 1)
          $crisp.push(["set", "session:segments", [["Technical support"]]]);
          @endif

          // visitor data
          $crisp.push(["set", "session:data", ["website", "{{$shop_domain}}"]]);

          $(document).ready(function(){
            //nav
            var activeNav = "active-nav";
            if (window.location.href.indexOf("themes") > -1) {
              $('.Polaris-Navigation a[href*="themes"]').addClass(activeNav);
            }
            if (window.location.href.indexOf("add_ons") > -1) {
              $('.Polaris-Navigation a[href*="add_ons"]').addClass(activeNav);
            }
            if (window.location.href.indexOf("courses") > -1) {
              $('.Polaris-Navigation a[href*="courses"]').addClass(activeNav);
            }
            if (window.location.href.indexOf("help") > -1) {
              $('.Polaris-Navigation a[href*="help"]').addClass(activeNav);
            }
            if (window.location.href.indexOf("support") > -1) {
              $('.Polaris-Navigation a[href*="support"]').addClass(activeNav);
            }
            if (window.location.href.indexOf("changelog") > -1) {
              $('.Polaris-Navigation a[href*="changelog"]').addClass(activeNav);
            }
            if (window.location.href.indexOf("mentoring") > -1) {
              $('.Polaris-Navigation a[href*="mentoring"]').addClass(activeNav);
            }
            if (window.location.href.indexOf("integrations") > -1) {
              $('.Polaris-Navigation a[href*="integrations"]').addClass(activeNav)
            }
            if (window.location.href.indexOf("feedback") > -1) {
              $('.Polaris-Navigation a[href*="feedback"]').addClass(activeNav)
            }
            if (window.location.href.indexOf("affiliate") > -1) {
              $('.Polaris-Navigation a[href*="affiliate"]').addClass(activeNav)
            }
            if (window.location.href.indexOf("winning-products") > -1) {
              $('.Polaris-Navigation a[href*="winning-products"]').addClass(activeNav)
            }
            if (window.location.href.indexOf("plans") > -1) {
              $('.Polaris-Navigation__Item[href*="plans"]').addClass(activeNav);
              $('.Polaris-Button[href*="plans"]').addClass("Polaris-Button--disabled").prop('disabled', true);
            }

            // toggle nav
            $(".btn-open-nav").click(function(){
              $(".Polaris-Navigation").addClass("open-menu");
            });
            $(".btn-close-nav").click(function(){
              $(".Polaris-Navigation").removeClass("open-menu");
            });

            // leave review banner
            $(".btn-leaveReview").click(function(){
                localStorage.setItem('leaveReview','yes');
            });
            if(localStorage.getItem("leaveReview")){} else{
              $(".reviewBanner").show();
            }
          });
          </script>

          <script>
            $(document).ready(function(){
              @if($theme_check == 0)
                $.get("{{ url('update_themecheck') }}", function(response){
                  if(response.status == 'success'){}
                });

                // app download tracking
                @if (env('APP_TRACKING'))
                window.dataLayer.push({
                  'event': 'app_download'
                });
                @endif
              @endif

              @if($trial_days && $trial_check == 0)
                $.get("{{ url('update_trail') }}", function(response){
                  if(response.status == 'success'){}
                });

                // start trial tracking
                @if (env('APP_TRACKING'))
                window.dataLayer.push({
                  'subscriptionId': '{{$hustleridMonthly}}',
                  'event': 'start_trial'
                });
                @endif
              @endif
            });
          </script>

          @yield('scripts')
          </body>
        </html>
