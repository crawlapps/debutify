<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="canonical" href="https://debutify.com/">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="theme-color" content="#5600e3">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="author" content="Debutify">

        <!-- Fav icon ================================================== -->
        <link sizes="192x192" rel="shortcut icon" href="/images/debutify-favicon.png" type="image/png">

        <!-- Title and description ================================================== -->
        <title>@yield("title") | Debutify</title>
        <meta name="description" content="Turn your store into a sales machine with Debutify - best free Shopify theme. High-converting, premium design and 24/7 live support. Download free today">

        <!-- Social meta ================================================== -->
        <meta property="og:site_name" content="Debutify">
        <meta property="og:url" content="https://debutify.com/">
        <meta property="og:title" content="The World's #1 Free Shopify Theme">
        <meta property="og:type" content="website">
        <meta property="og:description" content="Turn your store into a sales machine with Debutify - best free Shopify theme. High-converting, premium design and 24/7 live support. Download free today">
        <meta property="og:image" content="https://debutify.com/images/debutify-share.png">
        <meta property="og:image:secure_url" content="https://debutify.com/images/debutify-share.png">
        <meta name="twitter:site" content="@debutify">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="The World's #1 Free Shopify Theme">
        <meta name="twitter:description" content="Turn your store into a sales machine with Debutify - best free Shopify theme. High-converting, premium design and 24/7 live support. Download free today">

        <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "WebApplication",
          "name": "Debutify",
          "url": "https://debutify.com",
          "description": "The World's #1 Free Shopify Theme",
          "operatingSystem": "All",
          "applicationCategory": "BusinessApplication",
          "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.9",
            "ratingCount": "580"
          },
          "offers": {
            "@type": "Offer",
            "price": "0.00",
            "priceCurrency": "USD"
          }
        }
        </script>

        @if (env('APP_TRACKING'))
        <script>
          dataLayer = [];
        </script>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-M5BFQ4Q');</script>
        <!-- End Google Tag Manager -->
        @endif

        <!-- App style -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- page style -->
        @yield('styles')

        <!-- Plugins style -->
        <link rel="stylesheet" href="{{ asset('css/cookiealert.css') }}">
        <script src="https://kit.fontawesome.com/fdcc8b7628.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    </head>

    <body>
        @if (env('APP_TRACKING'))
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M5BFQ4Q"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        @endif

        <!-- Navigation -->
        <header class="nav-down">
            <nav id="menu" class="navbar navbar-expand-lg navbar-light bg-light" itemscope="" itemtype="http://schema.org/Organization">
              <div class="container-fluid">
                <a href="/" class="navbar-brand">
                  <img class="logo default-logo" src="/images/debutify-logo.png" width="200" alt="Debutify" itemprop="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavbar"
                        aria-controls="mainNavbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fas fa-bars"></span>
                </button>
                <div id="mainNavbar" class="collapse navbar-collapse justify-content-between">
                    <ul class="navbar-nav flex-fill">
                        <li class="nav-item mr-lg-auto">
                          <a href="https://www.shopify.com/?ref=debutify&utm_campaign=website-header" target="_blank" class="nav-link">
                              <span class="fab fa-shopify"></span>
                              Shopify Free Trial
                          </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/reviews">
                              <span class="fas fa-star"></span>
                              Reviews
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://debutifydemo.myshopify.com/" target="_blank">
                              <span class="fas fa-eye"></span>
                              Demo store
                            </a>
                        </li>
                    </ul>
                    <div class="nav-item mt-3 mt-lg-0 ml-lg-2">
                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#downloadModal">
                          <span class="fas fa-download"></span>
                          Free Download Now
                        </button>
                    </div>
                </div>
              </div>
            </nav>
        </header>


        <main role="main">
          @yield('content')
        </main>

        <hr/>

        <div class="shopify-section py-5">
          <div class="container">
            <div class="row align-items-center text-center text-md-left">
              <div class="col-md-1 mb-3 mb-md-0">
                <a href="https://www.shopify.com/?ref=debutify&utm_campaign=website-footer-banner" target="_blank">
                  <img class="img-fluid lazyload" data-src="/images/shopify-icon.svg" width="100" alt="shopify">
                </a>
              </div>
              <div class="col-md-7 mb-3 mb-md-0">
                <h4>Open a Shopify Online Store For Free!</h4>
                <p class="mb-0 text-muted">Sign up for a free Shopify 14-day trial and start selling today.</p>
              </div>
              <div class="col-md-4 text-md-right">
                <div class="text-center d-inline-block">
                  <a href="https://www.shopify.com/?ref=debutify&utm_campaign=website-footer-banner" target="_blank" class="btn btn-primary">Start your 14-Day free trial</a>
                  <br>
                  <small class="text-muted">Try Shopify for free. No credit card required.</small>
                </div>
              </div>
            </div>
          </div>
        </div>

        <footer class="text-white footer bg-gradient text-center">
          <section class="dropshipping-section py-5">
              <div class="container">
                  <div class="row align-items-center">
                      <div class="col-md-6">
                          <img data-src="images/new/dropshipping-img.png" alt="" class="img-fluid mb-3 lazyload" />
                      </div>
                      <div class="col-md-6">
                          <h2>Bootstrap Your Dropshipping Empire Today, FREE</h2>
                          <h6>Download Debutify and get <strong>all premium features FREE</strong> for 14 days.<br class="d-none d-sm-block"> Set up and install in 1 click!</h6>
                          <div class="download-wrapper text-center">
                              <div class="user-ratings mb-3">
                                  @include ("components.star-rating-badges")
                              </div>
                              <div class="download-btn">
                                  <button class="btn btn-warning btn-lg mb-3 animated pulse infinite" data-toggle="modal" data-target="#downloadModal">
                                    <span class="fas fa-download"></span>
                                    Free Download Now
                                  </button>
                                  <img src="images/new/arrow-yellow.png" alt="" class="img-fluid arrow-img animated pulse infinite" />
                              </div>
                              <p class="download-text">
                                  <span class="text-light">Easy install. No coding needed.<br />
                                  No credit card needed.</span>
                              </p>
                          </div>
                      </div>
                  </div>
              </div>
          </section>

          <hr/>

          <section class="footer-nav-section py-5">
            <div class="container">
                <a href="https://www.shopify.com/?ref=debutify&utm_campaign=website-shopify-partners" target="_blank">
                    <img src="/images/shopify-partner.png" alt="Shopify Partner" width="180">
                </a>
                <div class="row">
					<div class="col">
						<nav class="navbar navbar-expand navbar-dark justify-content-center">
							<ul class="navbar-nav mb-0">
            	                <li class="nav-item"><a class="nav-link" target="_blank" href="https://www.facebook.com/debutify/"><i class="fab fa-facebook fa-2x"></i></a></li>
                                <li class="nav-item"><a class="nav-link" target="_blank" href="https://www.instagram.com/debutify/"><i class="fab fa-instagram fa-2x"></i></a></li>
                                <li class="nav-item"><a class="nav-link" target="_blank" href="https://www.youtube.com/channel/UCm4-k3TAP2OPGZo47o-qZ5w"><i class="fab fa-youtube fa-2x"></i></a></li>
                                <li class="nav-item"><a class="nav-link" target="_blank" href="https://www.tiktok.com/@debutify"><i class="fab fa-tiktok fa-2x"></i></a></li>
  	            	            <li class="nav-item"><a class="nav-link" target="_blank" href="https://twitter.com/debutify"><i class="fab fa-twitter fa-2x"></i></a></li>
                                <li class="nav-item"><a class="nav-link" target="_blank" href="https://www.pinterest.com/debutify"><i class="fab fa-pinterest fa-2x"></i></a></li>
                            </ul>
  						</nav>
  					</div>
  				</div>
  				<div class="row mb-3">
  					<div class="col-12">
  						<nav class="navbar navbar-expand navbar-dark justify-content-center">
  							<ul class="navbar-nav flex-wrap justify-content-center">
                                <li class="nav-item"><a class="nav-link" href="/theme">Theme</a></li>
                                <li class="nav-item"><a class="nav-link" href="/add-ons">Add-ons</a></li>
                                <li class="nav-item"><a class="nav-link" href="/faq">FAQ</a></li>
                                <li class="nav-item"><a class="nav-link" href="/about">Meet the team</a></li>
                                <li class="nav-item"><a class="nav-link" href="/career">Career</a></li>
                                <li class="nav-item"><a class="nav-link" href="/pricing">Pricing</a></li>
                                <li class="nav-item"><a class="nav-link" href="/download">Download</a></li>
                                <li class="nav-item"><a class="nav-link" href="/affiliate">Affiliate</a></li>
                                <li class="nav-item"><a class="nav-link" href="/contact">Contact</a></li>
                                <li class="nav-item"><a class="nav-link" href="https://debutify.com/blog" target="_blank">Blog</a></li>
  							</ul>
  						</nav>
  					</div>
                    <div class="col-12">
                        <nav class="navbar navbar-expand navbar-dark justify-content-center">
  							<ul class="navbar-nav flex-wrap justify-content-center">
                                <li class="nav-item"><a class="nav-link" href="/terms-of-use"><small>Terms of use</small></a></li>
                                <li class="nav-item"><a class="nav-link" href="/privacy-policy"><small>Privacy policy</small></a></li>
                                <li class="nav-item"><a class="nav-link" href="/terms-of-sales"><small>Terms of sales</small></a></li>
  							</ul>
  						</nav>
                    </div>
  				</div>
              <div class="row" style="opacity:0.5;">
                <div class="col">
                  <small>Copyright {{ now()->year }} Debutify Inc. All rights reserved.</small>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <nav class="navbar navbar-expand navbar-dark justify-content-center">
                    <ul class="navbar-nav flex-wrap justify-content-center">
                      <li class="nav-item">
                        <a class="nav-link" href="https://debutify.crisp.watch/en/" target="_blank">
                          <small class="fas fa-check-circle"></small>
                          System status
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
      			</div>
          </section>
    		</footer>

        <!-- back to top -->
        <button type="button" class="btn btn-primary btn-sm back-to-top"><i class="fa fa-arrow-up"></i></button>

        <!-- Download box -->
        <div class="download-box bg-warning text-center">
            <a href="javascript:void(0);" data-toggle="modal" data-target="#downloadModal">
                <img src="images/new/downloads.png" alt="" class="img-fluid mb-2" />
                <div class="download-number">{{$nbShops}}</div>
                <div class="small">Downloads</div>
            </a>
        </div>

        <!-- Dowload Modal -->
        <div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="downloadModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-body p-md-4">

                <div class="text-center">
                  <a target="_blank" href="https://www.shopify.com/?ref=debutify&utm_campaign=website-modal-logo" class="d-inline-block mb-3 mx-auto"><img src="/images/shopify-logo.png" width="120" alt="shopify"></a>
                </div>

                <!-- downloadForm -->
                <form id="downloadForm" class="form-horizontal" method="POST" action="">
                  <p class="lead text-center" id="downloadModalLabel">Enter your name & email address.</p>
                  <div class="form-group text-left">
                    <input id="downloadName" required type="text" class="form-control form-control-lg" name="name" value="" placeholder="Name">
                  </div>
                  <div class="form-group text-left">
                    <input id="downloadEmail" required type="email" class="form-control form-control-lg" name="email" value="" placeholder="Email">
                  </div>
                  <button id="submitDownloadForm" type="submit" class="btn btn-primary btn-lg btn-block">
                    <span class="btn-text">
                      <span class="fas fa-download" aria-hidden="true"></span>
                      Free Download Now
                    </span>
                    <span class="btn-loading" style="display:none;">
                      <span class="fas fa-spin fa-spinner"></span>
                    </span>
                  </button>
                </form>

                <!-- domain form -->
                <form id="domainForm" class="form-horizontal" method="POST" action="{{ route('authenticate') }}" style="display:none;">
                  {{ csrf_field() }}
                  <p class="lead text-center">Enter your shopify domain. <span class="fas fa-question-circle toggle-download-info text-muted"></span></p>
                  <div class="form-group">
                    <input class="form-control form-control-lg" required type="text" name="shop" id="shop" placeholder="storename.myshopify.com" onkeyup="this.value = this.value.toLowerCase();">
                  </div>
                  <button type="submit" class="btn btn-primary btn-lg btn-block dbtfy-addtocart">
                    <span class="fas fa-download dbtfy-addtocart" aria-hidden="true"></span>
                    Free Download Now
                  </button>
                  <button style="display: none;" disabled="disabled" type="button" class="btn btn-primary btn-lg btn-block download-loading">
                    <span class="fas fa-spin fa-spinner"></span>
                  </button>
                  <div class="small text-center mt-2">
                    Don't have a Shopify Store yet?<br class="d-block d-sm-none">
                    <a target="_blank" href="https://www.shopify.com/?ref=debutify&utm_campaign=website-modal-get-started">
                      Get Started Today!
                    </a>
                  </div>
                </form>

                <div class="mt-2 text-center">
					<small class="font-weight-bold"><span class="fas fa-gift text-primary"></span> <span class="text-primary">BONUS:</span> Receive 5 Free winning products!</small>
                </div>

                <div class="p-3 rounded bg-grey download-info mt-3" style="display:none;">
					<p>
						<small>
							<span class="fa fa-question-circle"></span>
							<strong>Why do I need to enter my domain?</strong>
							<br>
							Your shopify domain is required to download Debutify app, where you will have access to our theme and add-ons.
						</small>
					</p>
					<p>
						<small>
							<span class="fa fa-question-circle"></span>
							<strong>What permission is your app asking for?</strong>
							<br>
							We only access the "manage store permission" to be able to edit theme files to install our theme and add-ons. We do not have access to any of your customer's data.
						</small>
					</p>
					<p>
						<small>
							<span class="fa fa-question-circle"></span>
							<strong>Why isn't Debutify in the Shopify App Store?</strong>
							<br>
							We couldn't get in the Shopify App Store because our functions are only compatible with Debutify theme.
						</small>
					</p>
					<hr>
					<small class="text-center">
    					<strong>
    						Have any questions?
    						<a href="#" class="close-modal" onclick="$crisp.push(['do', 'chat:open'])">chat with us.</a>
    					</strong>
	                </small>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Exit Modal -->
        <div class="modal fade" id="exitModal" tabindex="-1" role="dialog" aria-labelledby="exitModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="container-fluid p-0">
                  <div class="row">
                    <div class="col-lg-5 d-none d-lg-flex">
                      <img src="/images/exit-intent.jpeg" alt="" width="100%" class="img-fluid rounded">
                    </div>
                    <div class="col-lg-7 text-center">
                      <!-- exit form -->
                      <form id="exitForm" class="form-horizontal text-center" method="POST" action="">
                        <div class="form-group">
                          <p class="h2" id="exitModalLabel">FREE Winning Products & FREE Training You Don't Want to Miss</p>
                          <p class="lead">
                              Get the <strong>FREE</strong> ebook on <strong>FIVE $1,000,000 PRODUCTS</strong> in unsaturated niches, PLUS an exclusive invitation to
                              <strong>FREE MASTERCLASS WEBINAR</strong> by 7-figure entrepreneur Ricky Hayes.
                          </p>
                        </div>
                        <div class="form-group text-left">
                          <input id="exitName" required type="text" class="form-control form-control-lg" name="name" value="" placeholder="Name">
                        </div>
                        <div class="form-group text-left">
                          <input id="exitEmail" required type="email" class="form-control form-control-lg" name="email" value="" placeholder="Email">
                        </div>
                        <button id="submitExitForm" type="submit" class="btn btn-primary btn-lg btn-block">
                          <span class="btn-text">
                            <span class="fas fa-bolt" aria-hidden="true"></span>
                            Free Download Now
                          </span>
                          <span class="btn-loading" style="display:none;">
                            <span class="fas fa-spin fa-spinner"></span>
                          </span>
                        </button>
                      </form>
                      <div id="exitFormSuccess" style="display:none;">
                        <p class="h1">Thank you!</p>
                        <p class="lead">We just sent your $1,000,000 Winning Products to your email inbox.</p>
                        <a href="#" class="exitDownloadLink" data-dismiss="modal" data-toggle="modal" data-target="#downloadModal">Get Debutify theme 100% Free here</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Cookie Box -->
        <div class="alert alert-dismissible text-center cookiealert" role="alert">
          <div class="cookiealert-container">
              <b>Do you like cookies?</b> &#x1F36A; We use cookies to ensure you get the best experience on our website. <a href="http://cookiesandyou.com/" target="_blank">Learn more</a>
              <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
                  I agree
              </button>
          </div>
        </div>

        <!-- App js -->
        <script src="{{ asset('js/app.js') }}"></script>

        <!-- LinkMink -->
        <script src="https://cdn.linkmink.com/lm-js/2.2.0/lm.js"></script>

        <!-- ActiveCampaign -->
        <script type="text/javascript">
        (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
        vgo('setAccount', '799504461');
        vgo('setTrackByDefault', true);
        vgo('process');
        </script>

        <!-- lazysizes -->
        <script src="{{ asset('js/lazysizes.min.js') }}"></script>

        <!-- ReviewOnMyWebsite -->
        <script src="https://reviewsonmywebsite.com/js/embedLoader.js?id=16985fd9e429040ba7c6" type="text/javascript"></script>

        <!-- Start of Async ProveSource Code --><script>!function(o,i){window.provesrc&&window.console&&console.error&&console.error("ProveSource is included twice in this page."),provesrc=window.provesrc={dq:[],display:function(o,i){this.dq.push({n:o,g:i})}},o._provesrcAsyncInit=function(){provesrc.init({apiKey:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50SWQiOiI1ZGNjNTRlZmQxYzE0NTQzNDQzYjg3MmMiLCJpYXQiOjE1NzM2NzIxNzV9.fsXrgarvaQELgyPxn4HIvF0-pP9YR6YoXe-lXIlXBI0",v:"0.0.3"})};var r=i.createElement("script");r.type="text/javascript",r.async=!0,r["ch"+"ar"+"set"]="UTF-8",r.src="https://cdn.provesrc.com/provesrc.js";var e=i.getElementsByTagName("script")[0];e.parentNode.insertBefore(r,e)}(window,document);</script><!-- End of Async ProveSource Code -->

        <!-- exit intent -->
        <script src="/js/jquery.exitintent.min.js"></script>

        <!-- jquery validate -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>

        <!-- crisp -->
        <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="06fc7d7b-6235-4972-8b15-033f45837454";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

        <!-- OutplayHQ -->
        <script>
          (function(o, u, t, p, l, a, y, _, h, q) {
            if (!o[p] || !o[p]._q) {
              for (; _ < y.length; ) l(a, y[_++]);
              q = u.getElementsByTagName(t)[0];h = u.createElement(t);h.async = 1;
              h.src = "https://us1-cx.outplayhq.com/js/build.min.js";q.parentNode.insertBefore(h, q);o[p] = a;
            }
          })(window,document,"script","outplayhq",function(g, r) {
            g[r] = function() {
              g._q.push([r, arguments]);
            };
          },{ _q: [], _v: 1 },["init"],0);
          outplayhq.init("e515096f683a07731ee96b382d524557");
        </script>

        <script>

            $(document).ready(function(){

                $('#videoModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget); // Button that triggered the modal
                    var title = button.data('title');
                    var subtitle = button.data('subtitle');

                    @include("components.video-addons")

                    $('.tutorial').attr("src","https://www.youtube.com/embed/" + videoSource + "?autoplay=1");

                    $(".addon-title").text(title);
                    $(".addon-subtitle").text(subtitle);

                    if(videoSource){
                      $(".video-tutorial").show();
                    } else {
                      $(".video-tutorial").hide();
                    }
                });

                $('#videoModal').on('hide.bs.modal', function (e) {
                    $('.tutorial').attr("src","");
                });

                // hide crisp chatbox for outplayHQ leads
                if (window.location.href.indexOf("ophqt=") > -1) {
                  $crisp.push(['do', 'chat:hide']);
                }

                // crisp segments
                $crisp.push(["set", "session:segments", [["website"]]])

                @if (env('APP_TRACKING'))
                //landing page view tracking
                $(document).ready(function() {
                  if(sessionStorage.getItem("landingPageView")){} else{
                    window.dataLayer.push({'event': 'landing_page_view'});
                    sessionStorage.setItem('landingPageView','yes');
                  };
                });
                @endif

                // jQuery validate
                jQuery.validator.setDefaults({
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid');
                    }
                });
                jQuery.validator.addMethod("domain", function(value, element) {
                  return this.optional(element) || /.myshopify.com/.test(value);
                }, "Please enter your .myshopify.com domain");

                // open download modal
                $(document).on('click', '.toggleModal', function(){
                  $('#downloadModal').modal('show');
                });

                // close download modal
                $(document).on('click', '.close-modal', function(){
                  $('#downloadModal').modal('hide');
                });

                // on download modal open
                $('#downloadModal').on('shown.bs.modal', function () {
                  if( $("#downloadForm").is(":visible") ){
                    $('#downloadName').trigger('focus');
                  } else if( $("#domainForm").is(":visible") ){
                    $('#shop').trigger('focus');
                  }
                  @if (env('APP_TRACKING'))
                  //initiate download tracking
                  if(sessionStorage.getItem("initiateDownload")){} else{
                    window.dataLayer.push({'event': 'initiate_download'});
                    sessionStorage.setItem('initiateDownload','yes');
                  };
                  @endif
                });

                // more info content on download modal
                $(document).on('click', ".toggle-download-info", function(){
                  if( $(".download-info").is(":visible")){
                    $(".download-info").hide();
                  }
                  else{
                    $(".download-info").show();
                  }
                });
				$('#downloadModal').on('hidden.bs.modal', function () {
                  $(".download-info").hide();
                });
				$('input#shop').focusin(function(){
					$(".download-info").hide();
				});

                // download app email/name form
                var downloadForm = $("#downloadForm");
                downloadForm.validate({
                  submitHandler: function(form) {
                    var button = downloadForm.find("button[type='submit']");
                    var name = downloadForm.find('#downloadName').val();
                    var email = downloadForm.find('#downloadEmail').val();
                    var url = '{{ route('initiate_download')}}';
                    button.find(".btn-text").hide();
                    button.find(".btn-loading").show();
                    $.get(url+'?name='+name+'&email='+email, function(response){
                      if(response.status == 'success'){
                        $("#downloadForm").hide();
                        $("#domainForm").show();
                        $('#shop').trigger('focus');

                        @if (env('APP_TRACKING'))
                        //lead tracking
                        window.dataLayer.push({'event': 'lead'});
                        @endif
                      }
                    });
                  }
                });

                // exit intent email/name form
                var exitForm = $("#exitForm");
                exitForm.validate({
                  submitHandler: function(form) {
                    var button = exitForm.find("button[type='submit']");
                    var name = exitForm.find('#exitName').val();
                    var email = exitForm.find('#exitEmail').val();
                    var url = '{{ route('exit_intent')}}';
                    button.find(".btn-text").hide();
                    button.find(".btn-loading").show();
                    $.get(url+'?name='+name+'&email='+email, function(response){
                      if(response.status == 'success'){
                        exitForm.hide();
                        $("#exitFormSuccess").show();
                      }
                    });
                  }
                });

                // validate domain format .myshopify
                $('#domainForm').validate({
                  rules: {
                    shop: {
                      required: true,
                      nowhitespace: true,
                      domain: true
                    }
                  },
                  submitHandler: function(form) {
                    $(".dbtfy-addtocart").hide();
                    $(".download-loading").show();

                    @if (env('APP_TRACKING'))
                    //complete registration tracking
                    window.dataLayer.push({'event': 'complete_registration'});
                    @endif

                    form.submit();
                  }
                });

                // trigger exit intent modal
                $.exitIntent("enable");
                $(document).bind("exitintent", function() {
                  openExitIntent();
                });
                $(window).blur(function(e) {
                  if($("iframe").is(":focus")){} else{
                    openExitIntent();
                  }
                });

                function openExitIntent(){
                  if(sessionStorage.exitModalShown){ } else{
                    if($("#downloadModal").hasClass("show")) {} else{
                      $('#exitModal').modal('show');
                    }
                  }
                }

                $('#exitModal').on('shown.bs.modal', function () {
                  $("#exitName").trigger('focus');
                  sessionStorage.setItem("exitModalShown", "true");
                });

                $(".exitDownloadLink").on("click", function(){
                  $("#downloadForm").hide();
                  $("#domainForm").show();
                });

                // scrollin navigation
                var didScroll;
                var lastScrollTop = 0;
                var delta = 5;
                var navbarHeight = $('header').outerHeight();

                setInterval(function() {
                    if (didScroll) {
                        hasScrolled();
                        didScroll = false;
                    }
                }, 250);

                function hasScrolled() {
                    var st = $(this).scrollTop();

                    // Make sure they scroll more than delta
                    if(Math.abs(lastScrollTop - st) <= delta)
                        return;

                    // If they scrolled down and are past the navbar, add class .nav-up.
                    // This is necessary so you never see what is "behind" the navbar.
                    if (st > lastScrollTop && st > navbarHeight){
                        // Scroll Down
                        $('header').removeClass('nav-down').addClass('nav-up');
                    } else {
                        // Scroll Up
                        if(st + $(window).height() < $(document).height()) {
                            $('header').removeClass('nav-up').addClass('nav-down');
                        }
                    }

                    lastScrollTop = st;
                }

                // back to top
              	$(".back-to-top").on('click', function () {
      		        $("html, body").animate({scrollTop: 0}, 1000);
      	        });
                $(window).on('scroll', function(){
                    didScroll = true;
                    if ($(this).scrollTop() > 1000) {
                        $('.back-to-top').fadeIn();
                    } else {
                        $('.back-to-top').fadeOut();
                    }
                });
            });
        </script>

        <!-- page script -->
        @yield('scripts')
    </body>
</html>
