<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <link rel="canonical" href="https://debutify.com/admin/dashboard">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="theme-color" content="#5600e3">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="author" content="Debutify">

  <!-- Fav icon ================================================== -->
  <link sizes="192x192" rel="shortcut icon" href="/images/debutify-favicon.png" type="image/png">

  <title>Debutify Admin</title>

  <!-- App style -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!-- Plugin style -->
  <link href="{{ asset('css/tagsinput.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/5.0.7/sweetalert2.min.css">
  <!-- page style -->
  <style type="text/css">
    .swal2-container.fade.in {
        opacity: 1;
    }
  </style>
  @yield('styles')
</head>
<body>
  <div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('dashboard') }}">
                <img class="logo default-logo" src="/images/debutify-logo.png" width="200" alt="Debutify" itemprop="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard') }}"><span class="fas fa-users"></span> Users</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('products') }}"><span class="fas fa-funnel-dollar"></span> Products</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('courses') }}"><span class="fas fa-book"></span> Courses</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('themes') }}"><span class="fas fa-palette"></span> Themes</a>
                      </li>
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <span class="fas fa-user"></span> {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('Logout') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </div>
                      </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
      <div class="container-fluid">
        @yield('content')
      </div>
    </main>
  </div>

  <!-- App js -->
  <script src="{{ asset('js/app.js') }}"></script>

  <!-- LinkMink -->
  <script src="https://cdn.linkmink.com/lm-js/2.2.0/lm.js"></script>

  <!-- jQuery ajax/UI-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <!-- Plugins js -->
  <script type="text/javascript" src="{{ asset('js/tagsinput.min.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/5.0.7/sweetalert2.min.js"></script>
  <!-- page js -->
  @yield('scripts')
</body>
</html>
