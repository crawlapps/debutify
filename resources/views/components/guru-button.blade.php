@if($alladdons_plan == $guru)
<button type="button" class="Polaris-Button--fullWidth Polaris-Button Polaris-Button--primary" onclick="return openSubscriptionModal('{{$master_shop}}','{{$guru}}','{{$guruPriceMonthly}}','{{$guruPriceAnnually}}','{{$guruidMonthly}}','{{$guruidAnnually}}','{{$active_add_ons}}','{{$guruLimit}}','1','{{ $sub_plan }}','{{$all_addons}}');">
  <span class="Polaris-Button__Content">
    <span class="Polaris-Button__Text">My subscription</span>
  </span>
</button>
@else
<button type="button" class="Polaris-Button--fullWidth Polaris-Button @if($all_addons != 1) Polaris-Button--primary @endif" onclick="return openSubscriptionModal('{{$master_shop}}','{{$guru}}','{{$guruPriceMonthly}}','{{$guruPriceAnnually}}','{{$guruidMonthly}}','{{$guruidAnnually}}','{{$active_add_ons}}','{{$guruLimit}}','','','{{$all_addons}}');">
  <span class="Polaris-Button__Content">
    <span class="Polaris-Button__Text">Choose this plan</span>
  </span>
</button>
@endif
