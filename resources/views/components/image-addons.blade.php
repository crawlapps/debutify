@if ($addon->title == 'Add-to-cart animation')
<img data-src="https://img.youtube.com/vi/bjnHoBV8SCk/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Chat box')
<img data-src="https://img.youtube.com/vi/WZqVs5D0KzA/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Cookie box')
<img data-src="https://img.youtube.com/vi/gpmxPLbauw8/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Delivery time')
<img data-src="https://img.youtube.com/vi/rgtWh4RQWTw/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'F.A.Q page')
<img data-src="https://img.youtube.com/vi/lmhsHB5lHN8/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Instagram feed')
<img data-src="https://img.youtube.com/vi/a8_5N6wNnwI/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Live view')
<img data-src="https://img.youtube.com/vi/V1TYD-Z6OTg/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Product tabs')
<img data-src="https://img.youtube.com/vi/N0vh9pcnkag/mqdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Sales pop')
<img data-src="https://img.youtube.com/vi/Ud_YvHUAhYE/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Trust badge')
<img data-src="https://img.youtube.com/vi/Wjwv5G9Y8vk/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Sticky add-to-cart')
<img data-src="https://img.youtube.com/vi/Y6qmf5hEuSU/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Product video')
<img data-src="https://img.youtube.com/vi/h7t1QYObQXc/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Shop protect')
<img data-src="https://img.youtube.com/vi/sGZhpPAe2Yc/mqdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Mega menu')
<img data-src="https://img.youtube.com/vi/b4OtktdtatU/mqdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Newsletter pop-up')
<img data-src="https://img.youtube.com/vi/xRhq0EvBqVg/mqdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Collection add-to-cart')
<img data-src="https://img.youtube.com/vi/P8GwQ_rN8XI/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Upsell pop-up')
<img data-src="https://img.youtube.com/vi/G_UuhJeRzqs/mqdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Discount Saved')
<img data-src="https://img.youtube.com/vi/JproinpFJuU/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Cart countdown')
<img data-src="https://img.youtube.com/vi/1Eif6LLnnqg/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Linked options')
<img data-src="https://img.youtube.com/vi/zfHvGKgHecU/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Sales countdown')
<img data-src="https://img.youtube.com/vi/6YeuNEQWh8c/mqdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Inventory quantity')
<img data-src="https://img.youtube.com/vi/0y-TmTS2ryM/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Color swatches')
<img data-src="https://img.youtube.com/vi/LH_Xzwx-Vfg/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Cart discount')
<img data-src="https://img.youtube.com/vi/RvJe5-uUWEo/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Quick view')
<img data-src="https://img.youtube.com/vi/3AXJj7UOr60/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Smart search')
<img data-src="https://img.youtube.com/vi/fQmYha5aCFo/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Upsell bundles')
<img data-src="https://img.youtube.com/vi/PlcT73BM1wk/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Skip cart')
<img data-src="https://img.youtube.com/vi/TJhHJEwV9jg/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif

@if ($addon->title == 'Cart goal')
<img data-src="https://img.youtube.com/vi/5UetdWCAUGA/maxresdefault.jpg" alt="{{ $addon->title }}" class="img-fluid rounded lazyload lazypreload" width="100%">
@endif
