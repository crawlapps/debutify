@extends('layouts.debutify')
@section('title','plans')
@section('view-plans','view-plans')

@section('styles')
<style>
.Polaris-Page{
  max-width: 100%;
}
#inputExitIntent{
  outline:none;
  border:none;
  color:#fff;
  height: 0;
  margin: 0;
  padding: 0;
}
#inputExitIntent::-moz-selection { color: #fff}
#inputExitIntent::selection { color: #fff; }
}
</style>
@endsection


@section('content')
  @include("components.skeleton")

    <div id="dashboard" style="display:none;">
      <!-- if subscription status == unpaid -->
      @if($subscription_status == 'unpaid')
      <div class="subscriptionUnpaidBanner Polaris-Banner Polaris-Banner--statusCritical Polaris-Banner--withinPage" tabindex="0" role="alert" aria-live="polite" aria-labelledby="Banner4Heading" aria-describedby="Banner4Content">
        <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorRedDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
              <circle fill="currentColor" cx="10" cy="10" r="9"></circle>
              <path d="M2 10c0-1.846.635-3.543 1.688-4.897l11.209 11.209A7.954 7.954 0 0 1 10 18c-4.411 0-8-3.589-8-8m14.312 4.897L5.103 3.688A7.954 7.954 0 0 1 10 2c4.411 0 8 3.589 8 8a7.952 7.952 0 0 1-1.688 4.897M0 10c0 5.514 4.486 10 10 10s10-4.486 10-10S15.514 0 10 0 0 4.486 0 10"></path>
            </svg></span></div>
        <div>
          <div class="Polaris-Banner__Heading" id="Banner4Heading">
            <p class="Polaris-Heading">Failed payment: your account has been frozen</p>
          </div>
          <div class="Polaris-Banner__Content" id="Banner4Content">
            <p>Debutify access will be restored once your account has been updated with a valid payment method.</p>
            <div class="Polaris-Banner__Actions">
              <div class="Polaris-ButtonGroup">
                <div class="Polaris-ButtonGroup__Item">
                  <div class="Polaris-Banner__PrimaryAction">
                    <button type="button" class="Polaris-Button Polaris-Button--outline" onclick="return openUpdateCardModal();">
                      <span class="Polaris-Button__Content">
                        <span class="Polaris-Button__Text">Update payment method</span>
                      </span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      <!-- endif -->

      @if ($all_addons == 1 && $sub_plan == "month")
      <!-- yearly discount banner -->
      <div class="Polaris-Banner Polaris-Banner--statusSuccess Polaris-Banner--hasDismiss Polaris-Banner--withinPage" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner6Content">
        <div class="Polaris-Banner__Dismiss"><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Dismiss notification"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                    <path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                  </svg></span></span></span></button></div>
        <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorGreenDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
              <circle fill="currentColor" cx="10" cy="10" r="9"></circle>
              <path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8m2.293-10.707L9 10.586 7.707 9.293a1 1 0 1 0-1.414 1.414l2 2a.997.997 0 0 0 1.414 0l4-4a1 1 0 1 0-1.414-1.414"></path>
            </svg></span></div>
        <div class="Polaris-Banner__ContentWrapper">
          <div class="Polaris-Banner__Content" id="Banner6Content">
            <p>Switch to annual billing today and save 50% (${{ ($current_cost*12)/2 }}.00 USD). <button onclick="return claimYearlyOffer();" type="button" class="Polaris-Link Polaris-Link--monochrome">Upgrade now and save</button></p>
          </div>
        </div>
      </div>
      @endif

      @if($previous_plan)
      <!---Previus Subscription -->
      <div class="Polaris-Banner Polaris-Banner--hasDismiss Polaris-Banner--withinPage" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner2Heading" aria-describedby="Banner2Content">
        <div class="Polaris-Banner__Dismiss"><button type="button" class="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Dismiss notification"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                    <path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                  </svg></span></span></span></button></div>
        <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
              <path fill="currentColor" d="M2 3h11v4h6l-2 4 2 4H8v-4H3"></path>
              <path d="M16.105 11.447L17.381 14H9v-2h4a1 1 0 0 0 1-1V8h3.38l-1.274 2.552a.993.993 0 0 0 0 .895zM2.69 4H12v6H4.027L2.692 4zm15.43 7l1.774-3.553A1 1 0 0 0 19 6h-5V3c0-.554-.447-1-1-1H2.248L1.976.782a1 1 0 1 0-1.953.434l4 18a1.006 1.006 0 0 0 1.193.76 1 1 0 0 0 .76-1.194L4.47 12H7v3a1 1 0 0 0 1 1h11c.346 0 .67-.18.85-.476a.993.993 0 0 0 .044-.972l-1.775-3.553z"></path>
            </svg></span></div>
        <div>
          <div class="Polaris-Banner__Heading" id="Banner2Heading">
            <p class="Polaris-Heading">Your {{$alladdons_plan}} subscription price is still at ${{$previous_plan}}/{{$sub_plan}}</p>
          </div>
          <div class="Polaris-Banner__Content" id="Banner2Content">
            <p>Rest assured, pricing change below will not affect your active plan.</p>
          </div>
        </div>
      </div>
      @endif

      <div class="Polaris-Page__Content">
        <div class="Polaris-Card" style="overflow:initial;">
          <div class="">
            <div class="Polaris-DataTable">

              <!-- table start-->
              <table class="Polaris-DataTable__Table">

                <!-- thead start-->
                <thead>

                  <tr class="Polaris-DataTable__TableRow">
                    <!-- image header -->
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--header Polaris-DataTable__Cell--firstColumn" scope="col">
                      <img src="/svg/empty-state.svg" class="img-fluid" alt="">
                    </th>

                    <!-- freemium-->
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--header" scope="col">
                      @if($all_addons != 1 && !$trial_days)
                      <div class="badge-wrapper">
                        <span class="Polaris-Badge Polaris-Badge--statusInfo">Active plan</span>
                      </div>
                      @endif
                      <div class="Polaris-TextContainer">
                        <h3 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">Free</h3>
                        <p class="Polaris-TextStyle--variationSubdued">Only the basics</p>
                        <h3 class="Polaris-Heading">
                          $
                          <span class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">0</span>
                          /month
                        </h3>
                        <div class="sm-show">
                          @include("components.freemium-button")
                        </div>
                      </div>
                    </th>

                    <!-- starter-->
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--header" scope="col">
                      @if($alladdons_plan == $starter)
                      <div class="badge-wrapper">
                        <span class="Polaris-Badge Polaris-Badge--statusInfo">Active plan</span>
                      </div>
                      @endif
                      <div class="Polaris-TextContainer">
                        <h3 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">{{$starter}}</h3>
                        <p class="Polaris-TextStyle--variationSubdued">Great for starters</p>
                        <h3 class="Polaris-Heading">
                          $
                          <span class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">{{$starterPriceMonthly+0}}</span>
                          /month
                        </h3>
                        <div class="sm-show">
                          @include("components.starter-button")
                        </div>
                      </div>
                    </th>

                    <!-- hustler -->
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--header" scope="col">
                      <div class="badge-wrapper">
                        @if($alladdons_plan == $hustler)
                        <span class="Polaris-Badge Polaris-Badge--statusInfo">Active plan</span>
                        @else
                        <span class="Polaris-Badge Polaris-Badge--statusSuccess">Most popular</span>
                        @endif
                      </div>
                      <div class="Polaris-TextContainer">
                        <h3 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">{{$hustler}}</h3>
                        <p class="Polaris-TextStyle--variationSubdued">Perfect for businesses</p>
                        <h3 class="Polaris-Heading">
                          $
                          <span class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">{{$hustlerPriceMonthly+0}}</span>
                          /month
                        </h3>
                        <div class="sm-show">
                          @include("components.hustler-button")
                        </div>
                      </div>
                    </th>

                    <!-- guru -->
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--header" scope="col">
                      @if($alladdons_plan == $guru)
                      <div class="badge-wrapper">
                        <span class="Polaris-Badge Polaris-Badge--statusInfo">Active plan</span>
                      </div>
                      @endif
                      <div class="Polaris-TextContainer">
                        <h3 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">{{$guru}}</h3>
                        <p class="Polaris-TextStyle--variationSubdued">Made for experts</p>
                        <h3 class="Polaris-Heading">
                          $
                          <span class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">{{$guruPriceMonthly+0}}</span>
                          /month
                        </h3>
                        <div class="sm-show">
                          @include("components.guru-button")
                        </div>
                      </div>
                    </th>
                  </tr>
                </thead>

                <!-- tbody -->
                <tbody>
                  <!-- plans -->
                  <tr class="Polaris-DataTable__TableRow Sticky__tableRow plan-row">
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-DataTable__Cell--firstColumn">
                      <span class="table-section-title Polaris-Heading">
                        <span class="Polaris-Navigation__Icon">
                          <span class="Polaris-Icon">
                            <svg viewBox="0 0 20 20" class="v3ASA" focusable="false" aria-hidden="true"><path fill="currentColor" d="M4 7l-3 3 9 9 3-3z"></path><path d="M19 0h-9c-.265 0-.52.106-.707.293l-9 9a.999.999 0 0 0 0 1.414l9 9a.997.997 0 0 0 1.414 0l9-9A.997.997 0 0 0 20 10V1a1 1 0 0 0-1-1zm-9 17.586L2.414 10 4 8.414 11.586 16 10 17.586zm8-8l-5 5L5.414 7l5-5H18v7.586zM15 6a1 1 0 1 0 0-2 1 1 0 0 0 0 2"></path></svg>
                          </span>
                        </span>
                        Plans
                      </span>
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total">
                      @include("components.freemium-button")
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total">
                      @include("components.starter-button")
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total">
                      @include("components.hustler-button")
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total">
                      @include("components.guru-button")
                    </th>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Store licence</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">1</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">1</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">1</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">{{$store_limit + 1}}</td>
                  </tr>

                  <!-- themes -->
                  <tr class="Polaris-DataTable__TableRow Sticky__tableRow">
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-DataTable__Cell--firstColumn">
                      <span class="table-section-title Polaris-Heading">
                        <span class="Polaris-Navigation__Icon">
                          <span class="Polaris-Icon">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="20" height="20" viewBox="0 0 20 20"><path fill="currentColor" d="M11 1l8 8v8c0 1.104-0.896 2-2 2s-2-0.896-2-2v-6c0 1.104-0.896 2-2 2s-2-0.896-2-2v-10z"></path><path d="M17.979 17c0 0.551-0.448 1-1 1s-1-0.449-1-1v-6c0-0.552-0.447-1-1-1s-1 0.448-1 1c0 0.551-0.448 1-1 1s-1-0.449-1-1v-7.586l6 6v7.586zM8.394 14.929c-1.243 0-2.413 0.484-3.293 1.364l-1.414 1.414c-0.208 0.209-0.505 0.317-0.788 0.29-0.298-0.024-0.561-0.175-0.74-0.424-0.274-0.38-0.191-0.976 0.189-1.356l1.339-1.339c0.941-0.941 1.384-2.188 1.35-3.424l3.483 3.483c-0.042 0-0.083-0.008-0.126-0.008v0zM11.487 15.078l-6.585-6.586 5.077-5.078v7.586c0 1.525 1.148 2.774 2.624 2.962l-1.116 1.116zM19.686 8.293l-8-8c-0.191-0.191-0.447-0.283-0.707-0.283v-0.010c-0.014 0-0.027 0.007-0.041 0.008-0.105 0.004-0.208 0.023-0.31 0.060-0.009 0.004-0.019 0.003-0.028 0.007-0.002 0.001-0.003 0.001-0.004 0.001-0.112 0.047-0.208 0.117-0.294 0.196-0.009 0.009-0.021 0.012-0.030 0.021l-8 8c-0.391 0.391-0.391 1.023 0 1.414 1.035 1.036 1.035 2.722 0 3.757l-1.339 1.339c-1.071 1.072-1.243 2.765-0.398 3.939 0.52 0.722 1.323 1.177 2.202 1.248 0.081 0.007 0.162 0.010 0.243 0.010 0.793 0 1.555-0.313 2.12-0.879l1.414-1.414c1.004-1.005 2.755-1.004 3.758 0 0.187 0.188 0.441 0.293 0.707 0.293s0.52-0.105 0.707-0.293l2.293-2.293v1.586c0 1.654 1.346 3 3 3s3-1.346 3-3v-8c0-0.265-0.105-0.52-0.293-0.707v0z"></path></svg>
                          </span>
                        </span>
                        Themes
                      </span>
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Debutify theme</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>

                  <!-- support -->
                  <tr class="Polaris-DataTable__TableRow Sticky__tableRow">
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-DataTable__Cell--firstColumn">
                      <span class="table-section-title Polaris-Heading">
                        <span class="Polaris-Navigation__Icon">
                          <span class="Polaris-Icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path fill="currentColor" d="M10 1a9 9 0 0 0-9 9c0 1.769.518 3.413 1.398 4.804L1 19l4.196-1.398A8.954 8.954 0 0 0 10 19c4.971 0 9-4.029 9-9s-4.029-9-9-9z"></path><path d="M10 0C4.486 0 0 4.486 0 10c0 1.728.45 3.42 1.304 4.924l-1.253 3.76a1.001 1.001 0 0 0 1.265 1.264l3.76-1.253A9.947 9.947 0 0 0 10 20c5.514 0 10-4.486 10-10S15.514 0 10 0zm0 18a7.973 7.973 0 0 1-4.269-1.243.996.996 0 0 0-.852-.104l-2.298.766.766-2.299a.998.998 0 0 0-.104-.851A7.973 7.973 0 0 1 2 10c0-4.411 3.589-8 8-8s8 3.589 8 8-3.589 8-8 8zm0-9a1 1 0 1 0 0 2 1 1 0 0 0 0-2zM6 9a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm8 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"></path></svg>
                          </span>
                        </span>
                        Support
                      </span>
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Helpdesk</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Community</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Technical</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Priority support</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>

                  <!-- addons -->
                  <tr class="Polaris-DataTable__TableRow Sticky__tableRow">
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-DataTable__Cell--firstColumn">
                      <span class="table-section-title Polaris-Heading">
                        <span class="Polaris-Navigation__Icon">
                          <span class="Polaris-Icon">
                            <svg viewBox="0 0 20 20" class="v3ASA" focusable="false" aria-hidden="true"><path d="M1 1h7v7H1V1zm0 11h7v7H1v-7zm11 0h7v7h-7v-7z" fill="currentColor"></path><path d="M8 11H1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1v-7a1 1 0 0 0-1-1zm-1 7H2v-5h5v5zM8 0H1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1zM7 7H2V2h5v5zm12 4h-7a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1v-7a1 1 0 0 0-1-1zm-1 7h-5v-5h5v5zM12 6h2v2a1 1 0 1 0 2 0V6h2a1 1 0 1 0 0-2h-2V2a1 1 0 1 0-2 0v2h-2a1 1 0 1 0 0 2z"></path></svg>
                          </span>
                        </span>
                        Add-ons
                      </span>
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-TextStyle--variationStrong">0</th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-TextStyle--variationStrong">{{$starterLimit}}</th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-TextStyle--variationStrong">{{$hustlerLimit}}+</th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-TextStyle--variationStrong">{{$guruLimit}}+</th>
                  </tr>

                  @foreach($global_add_ons as $addon)
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3>
                        <span class="Polaris-TextStyle--variationStrong">{{$addon->title}}</span>
                        <span class="fas fa-question-circle ml-1 text-muted question-icon" onclick='return addonVideo("{{$addon->title}}","{{$addon->subtitle}}");'></span>
                      </h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell @if($loop->iteration > $starterLimit) plan-disabled @endif">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  @endforeach

                  <!-- product research -->
                  <tr class="Polaris-DataTable__TableRow Sticky__tableRow">
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-DataTable__Cell--firstColumn">
                      <span class="table-section-title Polaris-Heading">
                        <span class="Polaris-Navigation__Icon">
                          <span class="Polaris-Icon">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="20" height="20" viewBox="0 0 20 20"><path fill="currentColor" d="M19 10c0 4.971-4.029 9-9 9s-9-4.029-9-9c0-4.971 4.029-9 9-9s9 4.029 9 9z"></path><path d="M10 0c-5.514 0-10 4.486-10 10s4.486 10 10 10c5.514 0 10-4.486 10-10s-4.486-10-10-10zM10 18c-4.411 0-8-3.589-8-8s3.589-8 8-8c4.411 0 8 3.589 8 8s-3.589 8-8 8zM9.977 6.999c0.026 0.001 0.649 0.040 1.316 0.708 0.391 0.39 1.024 0.39 1.414 0 0.391-0.391 0.391-1.024 0-1.415-0.603-0.603-1.214-0.921-1.707-1.092v-0.201c0-0.552-0.447-1-1-1-0.552 0-1 0.448-1 1v0.185c-1.161 0.414-2 1.514-2 2.815 0 2.281 1.727 2.713 2.758 2.971 1.115 0.279 1.242 0.384 1.242 1.029 0 0.552-0.448 1-0.976 1.001-0.026-0.001-0.65-0.040-1.317-0.708-0.39-0.39-1.023-0.39-1.414 0-0.39 0.391-0.39 1.024 0 1.415 0.604 0.603 1.215 0.921 1.707 1.092v0.2c0 0.553 0.448 1 1 1 0.553 0 1-0.447 1-1v-0.184c1.162-0.414 2-1.514 2-2.816 0-2.28-1.726-2.712-2.757-2.97-1.115-0.279-1.243-0.384-1.243-1.030 0-0.551 0.449-1 0.977-1z"></path></svg>
                          </span>
                        </span>
                        Product research
                      </span>
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Oportunity level</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">Bronze</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">Silver</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">Gold</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Weekly products</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">15</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">25</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">30</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Videos</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Images</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Prices</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Spy tools</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Audiences</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Interest targeting</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Descriptions</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Expert opinion</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>

                  <!-- courses -->
                  <tr class="Polaris-DataTable__TableRow Sticky__tableRow">
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-DataTable__Cell--firstColumn">
                      <span class="table-section-title Polaris-Heading">
                        <span class="Polaris-Navigation__Icon">
                          <span class="Polaris-Icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path fill="currentColor" d="M10 19a2 2 0 0 1 2-2h7V1h-7a2 2 0 0 0-2 2 2 2 0 0 0-2-2H1v16h7a2 2 0 0 1 2 2z"></path><path d="M19 0h-7c-.768 0-1.469.29-2 .766A2.987 2.987 0 0 0 8 0H1a1 1 0 0 0-1 1v16a1 1 0 0 0 1 1h7a1 1 0 0 1 1 1 1 1 0 1 0 2 0 1 1 0 0 1 1-1h7a1 1 0 0 0 1-1V1a1 1 0 0 0-1-1zm-1 16h-6c-.352 0-.687.067-1 .179V3a1 1 0 0 1 1-1h6v14zM8 16H2V2h6a1 1 0 0 1 1 1v13.179A2.959 2.959 0 0 0 8 16zM6 4H5a1 1 0 1 0 0 2h1a1 1 0 1 0 0-2zm0 4H5a1 1 0 1 0 0 2h1a1 1 0 1 0 0-2zm8-2h1a1 1 0 1 0 0-2h-1a1 1 0 1 0 0 2zm0 4h1a1 1 0 1 0 0-2h-1a1 1 0 1 0 0 2zm-8 2H5a1 1 0 1 0 0 2h1a1 1 0 1 0 0-2z"></path></svg>
                          </span>
                        </span>
                        Training Courses
                      </span>
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                  </tr>
                  @foreach($courses as $course)
                  <?php
                  $plans = explode(',', $course->plans);
                  $isGuru = false; $isHustler = false; $isStarter = false; $isFreemium = false;
                  foreach($plans as $plan){
                    if($plan == 'guru'){
                      $isGuru = true;
                    }
                    if($plan == 'hustler'){
                      $isHustler = true;
                    }
                    if($plan == 'starter'){
                      $isStarter = true;
                    }
                    if($plan == 'freemium'){
                      $isFreemium = true;
                    }
                  }
                  ?>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3>
                        <span class="Polaris-TextStyle--variationStrong">{{$course->title}}</span>
                      </h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">
                      @if($isStarter)
                        @include("components.icon-yes")
                      @else
                        @include("components.icon-no")
                      @endif
                    </td>
                    <td class="Polaris-DataTable__Cell">
                      @if($isHustler)
                        @include("components.icon-yes")
                      @else
                        @include("components.icon-no")
                      @endif
                    </td>
                    <td class="Polaris-DataTable__Cell">
                      @if($isGuru)
                        @include("components.icon-yes")
                      @else
                        @include("components.icon-no")
                      @endif
                    </td>
                  </tr>
                  @endforeach

                  <!-- mentoring -->
                  <tr class="Polaris-DataTable__TableRow Sticky__tableRow">
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-DataTable__Cell--firstColumn">
                      <span class="table-section-title Polaris-Heading">
                        <span class="Polaris-Navigation__Icon">
                          <span class="Polaris-Icon">
                            <svg viewBox="0 0 20 20" class="v3ASA" focusable="false" aria-hidden="true"><path fill="currentColor" d="M7 5h6v10H7z"></path><path d="M19 18a1 1 0 0 1 0 2H1a1 1 0 0 1 0-2h18zm0-18a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h5V5a1 1 0 0 1 1-1h5V1a1 1 0 0 1 1-1h6zm-5 14h4V2h-4v12zm-6 0h4V6H8v8zm-6 0h4v-4H2v4z"></path></svg>
                          </span>
                        </span>
                        Mentoring
                      </span>
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Private 1-On-1 mentoring Facebook group</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Chance to win a 1-On-1 private live mentoring call with Ricky Hayes for 30 minutes</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-yes")</td>
                  </tr>

                  <!-- integrations -->
                  <tr class="Polaris-DataTable__TableRow Sticky__tableRow">
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total Polaris-DataTable__Cell--firstColumn">
                      <span class="table-section-title Polaris-Heading">
                        <span class="Polaris-Navigation__Icon">
                          <span class="Polaris-Icon">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="20" height="20" viewBox="0 0 20 20"><path fill="currentColor" d="M9 5c0 2.209-1.791 4-4 4s-4-1.791-4-4c0-2.209 1.791-4 4-4s4 1.791 4 4z"></path><path d="M17 4c-1.126 0-2.098 0.631-2.611 1.551l-4.408-0.734c-0.099-2.671-2.287-4.817-4.981-4.817-2.757 0-5 2.243-5 5s2.243 5 5 5c0.384 0 0.754-0.053 1.113-0.135l1.382 3.041c-0.904 0.734-1.495 1.841-1.495 3.094 0 2.206 1.794 4 4 4s4-1.794 4-4c0-0.918-0.323-1.753-0.844-2.429l2.906-3.736c0.297 0.099 0.608 0.165 0.938 0.165 1.654 0 3-1.346 3-3s-1.346-3-3-3zM2 5c0-1.654 1.346-3 3-3s3 1.346 3 3c0 1.654-1.346 3-3 3s-3-1.346-3-3zM7.931 9.031c0.772-0.563 1.374-1.336 1.724-2.241l4.398 0.733c0.070 0.396 0.216 0.764 0.426 1.090l-2.892 3.718c-0.488-0.211-1.023-0.331-1.587-0.331-0.236 0-0.464 0.030-0.688 0.070l-1.381-3.039zM10 18c-1.103 0-2-0.897-2-2s0.897-2 2-2c1.103 0 2 0.897 2 2s-0.897 2-2 2zM17 8c-0.552 0-1-0.448-1-1s0.448-1 1-1c0.552 0 1 0.448 1 1s-0.448 1-1 1z"></path></svg>
                          </span>
                        </span>
                        Integrations
                      </span>
                    </th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                    <th class="Polaris-DataTable__Cell Polaris-DataTable__Cell--total"></th>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Klaviyo</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">Coming soon</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">Coming soon</td>
                  </tr>
                  <tr class="Polaris-DataTable__TableRow">
                    <td class="Polaris-DataTable__Cell Polaris-DataTable__Cell--firstColumn">
                      <h3><span class="Polaris-TextStyle--variationStrong">Smsbump</span></h3>
                    </td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell">@include("components.icon-no")</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">Coming soon</td>
                    <td class="Polaris-DataTable__Cell Polaris-TextStyle--variationStrong">Coming soon</td>
                  </tr>

                </tbody>
              </table><!-- table end -->

            </div>
          </div>
        </div><!-- card end -->
      </div><!-- page layout end -->

    <!-- subscription modal -->
    <div id="subscriptionModal" class="modal fade-scales">
      <div>
        <div class="Polaris-Modal-Dialog__Container" data-polaris-layer="true" data-polaris-overlay="true">
          <div>
            <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header11" tabindex="-1">

              <div class="Polaris-Modal-Header">
                <div id="modal-header12" class="Polaris-Modal-Header__Title">
                  <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">
                    <span class="plan-name"></span>
                    plan
                  </h2>
                </div>
                <button type="button" class="Polaris-Modal-CloseButton close-modal disable-while-loading">
                  <span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored">
                    <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                      <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                    </svg>
                  </span>
                </button>
              </div>

              <div class="Polaris-Modal__BodyWrapper">
                <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true" polaris="[object Object]">

                  <!-- section billing cycle -->
                  <section class="Polaris-Modal-Section sectionBillingCycle">
                    <div class="Polaris-FormLayout">

                      <div class="Polaris-FormLayout__Item">
                        <div class="Polaris-TextContainer">
                          <h2 class="Polaris-Heading">Billing cycle</h2>
                          <p class="Polaris-TextStyle--variationSubdued hideOnActivePlan">Choose how often you'd like to be billed.</p>
                          @if ($all_addons == 1)
                          <p class="Polaris-TextStyle--variationSubdued showOnActivePlan">
                            Your subscription renews on {{$billingDate}} → US${{$next_invoice_total}}
                            @if($applyTaxes) (tax included) @endif
                          </p>
                          @endif
                        </div>
                      </div>

                      <div class="Polaris-FormLayout__Item">
                        <div class="Polaris-Card" style="margin:0;border: .1rem solid var(--p-divider-subdued-on-surface,#dfe3e8);box-shadow:none;">
                          <div class="Polaris-ResourceList__ResourceListWrapper">
                            <ul class="Polaris-ResourceList" aria-live="polite" style="border-top:0;">
                              <li class="Polaris-ResourceList__ItemWrapper">
                                <div class="Polaris-ResourceItem">
                                  <label class="Polaris-Choice" for="paidMonthlyRadio">
                                    <span class="Polaris-Choice__Control">
                                      <span class="Polaris-RadioButton">
                                        <input id="paidMonthlyRadio" name="paymentRadio" type="radio" class="Polaris-RadioButton__Input radio-button" aria-describedby="optionalHelpText" value="monthly" {{ $sub_plan == "month" ? 'checked=checked' : '' }}>
                                        <span class="Polaris-RadioButton__Backdrop"></span>
                                        <span class="Polaris-RadioButton__Icon"></span>
                                      </span>
                                    </span>
                                    <span class="Polaris-Choice__Label">
                                      $<span class="monthly-price"></span> USD every 30 days
                                    </span>
                                    <span style="margin-left:auto;">
                                      @if($all_addons == 1 && $sub_plan == 'month')
                                      <span class="Polaris-Badge Polaris-Badge--statusInfo active-badge">Active plan</span>
                                      @endif
                                    </span>
                                  </label>
                                </div>
                              </li>
                              <li class="Polaris-ResourceList__ItemWrapper">
                                <div class="Polaris-ResourceItem">
                                  <label class="Polaris-Choice" for="paidAnnuallyRadio">
                                    <span class="Polaris-Choice__Control">
                                      <span class="Polaris-RadioButton">
                                        <input id="paidAnnuallyRadio" name="paymentRadio" type="radio" class="Polaris-RadioButton__Input radio-button" aria-describedby="disabledHelpText" value="annually" {{ $sub_plan == 'Yearly' ? 'checked=checked': '' }}>
                                        <span class="Polaris-RadioButton__Backdrop"></span>
                                        <span class="Polaris-RadioButton__Icon"></span>
                                      </span>
                                    </span>

                                    <span class="Polaris-Choice__Label">
                                      $<span class="annual-price"></span> USD every year
                                    </span>

                                    <span style="margin-left:auto;">
                                      @if($all_addons == 1 && $sub_plan == 'Yearly')
                                      <span class="Polaris-Badge Polaris-Badge--statusInfo active-badge">Active plan</span>
                                      @endif
                                      <span class="Polaris-TextStyle--variationPositive annual-discount-badge">Save <span class="annual-discount-money"></span></span>
                                    </span>
                                  </label>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div class="Polaris-FormLayout__Item mt-2">
                        <p class="Polaris-TextStyle--variationPositive save-message-annually small">You are saving <span class="annual-discount-percentage"></span> with this plan</p>
                        <p class="Polaris-TextStyle--variationNegative save-message-monthly small">Switch to annual billing and save <span class="annual-discount-percentage"></span></p>
                      </div>
                    </div>
                  </section>

                  <!-- section payment method -->
                  <section class="Polaris-Modal-Section sectionPaymentMethod">
                    <form action="" method="post" id="StripeForm" class="">
      	              @csrf
                      <div class="Polaris-FormLayout">

                        <div class="Polaris-FormLayout__Item">
                          <div class="Polaris-TextContainer">
                            <h2 class="Polaris-Heading">Payment method</h2>
                            <!-- <p class="Polaris-TextStyle--variationSubdued">Choose how you'd like to pay for Debutify.</p> -->
                          </div>
                        </div>

                        <div class="Polaris-FormLayout__Item">
                          <div class="">
                            <!-- For card Details -->
                            @if($all_addons == 1)
                            <div class="Polaris-Stack Polaris-Stack--spacingTight">
                              <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                <p class="Polaris-TextStyle--variationSubdued">
                                  <span class="currentCoupon Polaris-Subheading">{{$card_brand}}</span>
                                  <span>**** **** **** {{$card_number}}</span>
                                  <span>→ {{$card_expire}}</span>
                                </p>
                              </div>
                              <div class="Polaris-Stack__Item">
                                <button type="button" class="Polaris-Button Polaris-Button--plain close-modal" onclick="return openUpdateCardModal();">
                                  <span class="Polaris-Button__Content">
                                    <span class="Polaris-Button__Text">Update</span>
                                  </span>
                                </button>
                              </div>
                            </div>
                            @else
                            <div class="Polaris-Labelled__LabelWrapper">
                              <div class="Polaris-Label">
                                <label for="credit_card" class="Polaris-Label__Text">Credit card</label>
                              </div>
                            </div>
                            <div id="card-elementsub" class="stripe-custom-input">
                            </div>
                            <div id="card-errorsub" class="Polaris-Labelled__HelpText" role="alert"></div>
                            @endif

                            <input type="hidden" name="payment_cycle" id="payment_cycle" value="" class="payment_cycle" />
                            <input type="hidden" name="subtotal_price" id="subtotal_price" value="" class="subtotal_price" />
                            <input type="hidden" name="plan_id" id="plan_id" value="" class="plan_id" />
                            <input type="hidden" name="sub_plan" id="sub_plan" value="" class="sub_plan" />
                            <input type="hidden" name="alladdons_plan" id="alladdons_plan" value="{{ $alladdons_plan }}" />
                            <input type="hidden" name="email" value="{{ $owner_details['email'] }}" />
                            <input type="hidden" name="new_coupon" value="" id="addnewcoupon" />
                          </div>
                        </div>
                      </div>
                    </form>
                  </section>

                  <!-- section coupon code -->
                  <section class="Polaris-Modal-Section sectionCouponCode">
                    <div class="Polaris-FormLayout">

                      <div class="Polaris-FormLayout__Item">
                        <div class="Polaris-TextContainer">
                          <h2 class="Polaris-Heading">Coupon code</h2>
                          @if($discount)
                          <p class="Polaris-TextStyle--variationSubdued showOnActivePlan">
                            <span class="currentCoupon Polaris-Subheading">{{$coupon_name}}</span>
                            →
                            <span class="couponDescription">
                              {{$percent_off}}
                              {{$coupon_duration}}
                              @if ($coupon_duration_months)
                              {{$coupon_duration_months}} months
                              @endif
                            </span>
                          </p>
                          @endif
                        </div>
                      </div>

                      <div class="Polaris-FormLayout__Item">

                        <!-- new coupon -->
                        <div class="Polaris-Stack Polaris-Stack--spacingTight hideOnActivePlan">
                          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                            <div class="Polaris-TextField">
                              <input id="newCoupon" class="Polaris-TextField__Input" type="text" name="newCoupon" aria-label="New coupon" aria-labelledby="" aria-invalid="false" value="">
                              <div class="Polaris-TextField__Backdrop"></div>
                            </div>
                          </div>
                          <div class="Polaris-Stack__Item">
                            <button type="button" class="Polaris-Button disable-while-loading btn-new-coupon" onclick="return applyNewCoupon();">
                              <span class="Polaris-Button__Content">
                                <span class="Polaris-Button__Text">Apply coupon</span>
                              </span>
                            </button>
                          </div>
                        </div>

                        <!-- subscription coupon -->
                        <form action="" method="post" id="SubscriptionCouponForm" class="showOnActivePlan">
                          @csrf
                          <div class="Polaris-Stack Polaris-Stack--spacingTight">
                            <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                              <div class="Polaris-TextField">
                                <input id="currentplan_id" type="hidden" name="currentplan_id" value="">
                                <input id="subscriptionCoupon" class="Polaris-TextField__Input" type="text" name="subscription_coupon" aria-label="Subscription coupon" aria-labelledby="" aria-invalid="false" value="">
                                <div class="Polaris-TextField__Backdrop"></div>
                              </div>
                            </div>
                            <div class="Polaris-Stack__Item">
                              <button type="button" class="Polaris-Button disable-while-loading btn-subscription-coupon" onclick="return applySubscriptionCoupon();">
                                <span class="Polaris-Button__Content">
                                  <span class="Polaris-Button__Text">Apply coupon</span>
                                </span>
                              </button>
                            </div>
                          </div>
                        </form>

                        <div class="Polaris-Labelled__HelpText" id="PolarisTextField2HelpText"><span>Coupon codes are case sensitive.</span></div>
                      </div>
                    </div>
                  </section>

                  @if($alladdons_plan == $guru)
                  <!-- section linked store -->
                  <section class="Polaris-Modal-Section showOnActivePlan sectionLinkedStore">
                    <div class="Polaris-FormLayout">
                      <div class="Polaris-FormLayout__Item">
                        <div class="Polaris-TextContainer">
                          <h2 class="Polaris-Heading">
                            Store licences
                            <span class="Polaris-Badge Polaris-Badge--statusSuccess">
                            {{$store_count}}/{{$store_limit}}
                            </span>
                          </h2>
                        </div>
                      </div>

                      @if($store_count < $store_limit)
                      <div class="Polaris-FormLayout__Item">
                        <form action="" method="post" id="addStoreForm" class="">
                          @csrf

                          <div class="Polaris-Labelled__LabelWrapper">
                            <div class="Polaris-Label">
                              <label id="LinkedDomainLabel" for="LinkedStore" class="Polaris-Label__Text">Shopify domain</label>
                            </div>
                          </div>
                          <div class="Polaris-Stack Polaris-Stack--spacingTight">
                            <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                              <div class="Polaris-TextField primfield">
                                <input placeholder="myotherstore.myshopify.com" id="LinkedStore" class="Polaris-TextField__Input" min="-Infinity" max="Infinity" step="1" type="url" aria-invalid="false" aria-multiline="false" name="childstore" value="">
                                <div class="Polaris-TextField__Backdrop"></div>
                              </div>
                            </div>
                            <div class="Polaris-Stack__Item">
                              <button type="button" class="Polaris-Button disable-while-loading save-store" onclick="return addchildstore();">
                                <span class="Polaris-Button__Content">
                                  <span class="Polaris-Button__Text">Share licence</span>
                                </span>
                              </button>
                            </div>
                          </div>
                          <div class="Polaris-Labelled__HelpText"><span>Install Debutify app before sharing your licence.</span></div>

                        </form>
                      </div>
                      @endif

                      <div class="Polaris-FormLayout__Item">
                        <form action="" method="post" id="removestores" class="">
                        @csrf
                          <div class="Polaris-Stack">
                          @foreach($child_stores as $child_store)
                          <div class="Polaris-Stack__Item">
                            <span class="Polaris-Tag">
                              <span title="{{$child_store->store}}" class="Polaris-Tag__TagText">{{$child_store->store}}</span>
                              <button type="button" aria-label="Remove Wholesale" class="Polaris-Tag__Button disable-while-loading removeLinkedStore{{$child_store->id}}" onclick="return removestore('{{$child_store->id}}','{{$child_store->store}}');">
                                <span class="Polaris-Icon">
                                  <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                    <path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                                  </svg>
                                </span>
                              </button>
                            </span>
                          </div>
                          @endforeach
                          <input type="hidden" name="child_store" value="" id="child_store">
                          <input type="hidden" name="store_id" value="" id="storeid">
                          </div>
                        </form>
                      </div>
                    </div>
                  </section>
                  @endif

                  <!-- section total -->
                  <section class="Polaris-Modal-Section hideOnActivePlan sectionTotal">
                    <div class="Polaris-FormLayout">

                      <div class="Polaris-FormLayout__Item">
                        <div class="Polaris-TextContainer">
                          <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">
                            <span class="plan-name"></span> plan
                          </h2>
                          <p class="Polaris-TextStyle--variationSubdued">$<span class="plan-price"></span> USD @if($applyTaxes) + tax @endif every <span class="billing-days"></span></p>
                          <div class="mt-2">
                            <p class="Polaris-TextStyle--variationPositive save-message-annually small">You are saving <span class="annual-discount-percentage"></span> with this plan</p>
                            <p class="Polaris-TextStyle--variationNegative save-message-monthly small">Switch to annual billing and save <span class="annual-discount-percentage"></span></p>
                          </div>
                        </div>
                      </div>

                      <div class="Polaris-FormLayout__Item">
                        <div class="Polaris-Stack">
                          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                            <span>Subtotal</span>
                          </div>
                          <div class="Polaris-Stack__Item">
                            $<span class="plan-price"></span>
                          </div>
                        </div>
                      </div>

                      <div class="Polaris-FormLayout__Item plan-discount">
                        <div class="Polaris-Stack">
                          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                            <span>Discount</span>
                            <span class="Polaris-TextStyle--variationSubdued showOnnewcoupon"></span>
                          </div>
                          <div class="Polaris-Stack__Item">
                            $<span class="discounts"></span>
                          </div>
                        </div>
                      </div>

                      <div class="Polaris-FormLayout__Item plan-prorated">
                        <div class="Polaris-Stack">
                          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                            <span>Prorated credit</span>
                            <span class="Polaris-TextStyle--variationSubdued"></span>
                          </div>
                          <div class="Polaris-Stack__Item">
                            $<span class="prorated-amount"></span>
                          </div>
                        </div>
                      </div>

                      <div class="Polaris-FormLayout__Item plan-balance">
                        <div class="Polaris-Stack">
                          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                            <span>Account balance</span>
                            <span class="Polaris-TextStyle--variationSubdued"></span>
                          </div>
                          <div class="Polaris-Stack__Item">
                            $<span class="prorated-balance"></span>
                          </div>
                        </div>
                      </div>

                      <div class="Polaris-FormLayout__Item">
                        <div class="Polaris-Stack Polaris-TextStyle--variationStrong">
                          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                            <span>Billed now</span>
                          </div>
                          <div class="Polaris-Stack__Item">
                            $<span class="total-price gtm-total-price"></span> @if($applyTaxes) + tax @endif
                          </div>
                        </div>
                      </div>

                    </div>
                  </section>

                </div>
              </div>

              <!-- plan modal footer -->
              <div class="Polaris-Modal-Footer hideOnActivePlan" id="modalFooter">
                <div class="Polaris-Modal-Footer__FooterContent">
                  <div class="Polaris-Stack Polaris-Stack--alignmentCenter flex-nowrap">
                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                      <p class="Polaris-TextStyle--variationSubdued small">By clicking on "Start plan" you agree to our <a href="/terms" target="_blank">terms</a>.</p>
                    </div>
                     <div class="Polaris-Stack__Item">
                      <div class="Polaris-ButtonGroup">
                        <div class="Polaris-ButtonGroup__Item">
                          <button type="button" class="Polaris-Button close-modal disable-while-loading">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">Cancel</span>
                            </span>
                          </button>
                        </div>
                        <div class="Polaris-ButtonGroup__Item">
                        @if($all_addons == 1)
                          <button type="button" class="Polaris-Button Polaris-Button--primary all_addon_subscribe disable-while-loading" onclick="return AllAdonsSubscriptioncard();">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">Start plan</span>
                            </span>
                          </button>
                        @else
                          <button type="button" class="Polaris-Button Polaris-Button--primary all_addon_subscribe disable-while-loading" onclick="return AllAdonsSubscription();">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">Start plan</span>
                            </span>
                          </button>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
							</div>

              @if($all_addons == 1)
              <!-- cancel modal footer -->
              <div class="Polaris-Modal-Footer showOnActivePlan" id="modalFooterCancel" style="display:none;">
                <div class="Polaris-Modal-Footer__FooterContent">
                  <div class="Polaris-Stack Polaris-Stack--alignmentCenter">

                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                      <button type="button" class="Polaris-Button Polaris-Button--plain link-uninstall disable-while-loading">
                        <span class="Polaris-Button__Content">
                          <span class="Polaris-Button__Text">Cancel subscription</span>
                        </span>
                      </button>
                      <button type="button" class="Polaris-Button Polaris-Button--plain cancel-link-uninstall disable-while-loading" style="display:none;">
                        <span class="Polaris-Button__Content">
                          <span class="Polaris-Button__Text">Never mind</span>
                        </span>
                      </button>
                    </div>
                    <div class="Polaris-Stack__Item">
                      <div class="Polaris-ButtonGroup">
                        <div class="Polaris-ButtonGroup__Item">
                          <button type="button" class="Polaris-Button close-modal disable-while-loading">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">Cancel</span>
                            </span>
                          </button>
                        </div>
                        <div class="Polaris-ButtonGroup__Item btn-update">
                          <button type="button" class="Polaris-Button Polaris-Button--primary disable-while-loading close-modal" onclick="return openUpdateCardModal();">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">Update card</span>
                            </span>
                          </button>
                        </div>
                        <div class="Polaris-ButtonGroup__Item btn-uninstall" style="display:none;">
                          <form action="" method="post" id="cancel_form">
                            @csrf
                            <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                              <div style="" class="Polaris-Stack__Item">
                                <div class="Polaris-ButtonGroup">
                                  <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                                    <input type="hidden" name="sub_plan" value='{{ $sub_plan == "month" ? "month" : "Yearly" }}'>
                                    <input type="hidden" name="cancel_subscription" value="cancel">
                                    <button type="button" class="Polaris-Button Polaris-Button--destructive btn-cancel-subscription disable-while-loading" onclick="return openCancelModal('{{$store_count}}','{{ $sub_plan }}','{{ $trial_end_date }}');">
                                      <span class="Polaris-Button__Content">
                                        <span class="Polaris-Button__Text">Cancel subscription</span>
                                      </span>
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              @endif

            </div>
          </div>
        </div>
      </div>
      <div class="Polaris-Backdrop"></div>
    </div>

    <!-- exit intent modal -->
    <div id="exitIntentModal" class="modal fade-scales">
      <div>
        <div class="Polaris-Modal-Dialog__Container undefined" data-polaris-layer="true" data-polaris-overlay="true">
          <div>
            <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header2" tabindex="-1">
              <div class="Polaris-Modal-Header">
                <div id="modal-header2" class="Polaris-Modal-Header__Title">
                  <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">WAIT! Don't go..</h2>
                </div><button class="Polaris-Modal-CloseButton close-modal disable-while-loading" aria-label="Close"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                      <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                    </svg></span></button>
              </div>
              <div class="Polaris-Modal__BodyWrapper">
                <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true">
                  <section class="Polaris-Modal-Section text-center">
                    <div class="Polaris-TextContainer">
                      <img src="/svg/offer.svg" role="presentation" alt="" class="" width="300">
                      <p class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Unlock 20% off your first month</p>
                      <p class="Polaris-Subheading">One-time-offer ONLY! Your Success Is Just Around The Corner. We Are Here For YOU!</p>
                      <button type="button" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeLarge acceptExitIntent disable-while-loading">
                        <span class="Polaris-Button__Content">
                          <span class="Polaris-Button__Text">Copy coupon code</span>
                        </span>
                      </button>
                      <p class="Polaris-Subheading">Use code: DEBUTIFY20</p>
                      <input id="inputExitIntent" readonly type="text" value="{{$new_code}}">
                    </div>
                  </section>
                </div>
              </div>
              <div class="Polaris-Modal-Footer">
                <div class="Polaris-Modal-Footer__FooterContent">
                  <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
                    <div class="Polaris-Stack__Item">
                      <div class="Polaris-ButtonGroup">
                        <div class="Polaris-ButtonGroup__Item">
                          <button type="button" class="close-modal Polaris-Button disable-while-loading">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">No thanks</span>
                            </span>
                          </button>
                        </div>
                        <div class="Polaris-ButtonGroup__Item">
                          <button type="button" class="Polaris-Button Polaris-Button--primary acceptExitIntent disable-while-loading">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">Copy coupon code</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="Polaris-Backdrop"></div>
    </div>


    <!-- offer discount modal -->
    <div id="offerDiscountModal" class="modal fade-scales">
      <div>
        <div class="Polaris-Modal-Dialog__Container undefined" data-polaris-layer="true" data-polaris-overlay="true">
          <div>
            <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header2" tabindex="-1">
              <div class="Polaris-Modal-Header">
                <div id="modal-header2" class="Polaris-Modal-Header__Title">
                  <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">WAIT! Don't go..</h2>
                </div><button class="denyOffer Polaris-Modal-CloseButton close-modal disable-while-loading" aria-label="Close"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                      <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                    </svg></span></button>
              </div>
              <div class="Polaris-Modal__BodyWrapper">
                <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true">
                  <section class="Polaris-Modal-Section text-center">
                    <div class="Polaris-TextContainer">
                      <img src="/svg/offer.svg" role="presentation" alt="" class="" width="300">
                      <p class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Unlock 50% off your monthly subscription for 3 months</p>
                      <p class="Polaris-Subheading">One-time-offer ONLY! Your Success Is Just Around The Corner. We Are Here For YOU!</p>
                      <button type="button" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeLarge acceptOffer disable-while-loading">
                        <span class="Polaris-Button__Content">
                          <span class="Polaris-Button__Text">Claim offer now</span>
                        </span>
                      </button>
                    </div>
                  </section>
                </div>
              </div>
              <div class="Polaris-Modal-Footer">
                <div class="Polaris-Modal-Footer__FooterContent">
                  <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
                    <div class="Polaris-Stack__Item">
                      <div class="Polaris-ButtonGroup">
                        <div class="Polaris-ButtonGroup__Item">
                          <button type="button" class="close-modal Polaris-Button denyOffer disable-while-loading">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">No thanks</span>
                            </span>
                          </button>
                        </div>
                        <div class="Polaris-ButtonGroup__Item">
                          <button type="button" class="Polaris-Button Polaris-Button--primary acceptOffer disable-while-loading">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">Claim offer now</span>
                            </span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="Polaris-Backdrop"></div>
    </div>

    <!-- addon video modal -->
    <div id="videoModal" class="modal fade-scales">
      <div>
        <div class="Polaris-Modal-Dialog__Container undefined" data-polaris-layer="true" data-polaris-overlay="true">
          <div>
            <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header2" tabindex="-1">
              <div class="Polaris-Modal-Header">
                <div id="modal-header2" class="Polaris-Modal-Header__Title">
                  <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall addon-title"></h2>
                </div><button class="Polaris-Modal-CloseButton close-modal" aria-label="Close"><span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                      <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                    </svg></span></button>
              </div>
              <div class="Polaris-Modal__BodyWrapper">
                <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true">
                  <section class="Polaris-Modal-Section">
                    <div class="Polaris-TextContainer">
                      <p class="addon-subtitle"></p>
                    </div>
                    <div class="Polaris-TextContainer video-tutorial" style="margin-top:2rem;">
                      <iframe class="tutorial" width="100%" height="295" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                  </section>
                </div>
              </div>
              <div class="Polaris-Modal-Footer">
                <div class="Polaris-Modal-Footer__FooterContent">
                  <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
                    <div class="Polaris-Stack__Item">
                      <div class="Polaris-ButtonGroup">
                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="close-modal Polaris-Button"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Close</span></span></button></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="Polaris-Backdrop"></div>
    </div>

    <!-- update card modal -->
    <div id="updateCardModal" class="modal fade-scales" role="dialog">
       <div>
         <div class="Polaris-Modal-Dialog__Container" data-polaris-layer="true" data-polaris-overlay="true">
           <div>
             <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header11" tabindex="-1">

               <div class="Polaris-Modal-Header">
                 <div class="Polaris-Modal-Header__Title">
                   <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Update payment method</h2>
                 </div>
                 <button type="button" class="Polaris-Modal-CloseButton close-modal disable-while-loading">
                   <span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored">
                     <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                       <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                     </svg>
                   </span>
                 </button>
               </div>

               <form action="{{ route('updatecreditCard') }}" method="post" id="updateCardForm" class="Polaris-Modal__BodyWrapper">
                 @csrf
                 <div class="Polaris-Modal__BodyWrapper">
                   <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true" polaris="[object Object]">
                     <section class="Polaris-Modal-Section">
                       <div class="Polaris-FormLayout">
                         <div class="Polaris-FormLayout__Item">
                           <div class="">
                             <div class="Polaris-Labelled__LabelWrapper">
                               <div class="Polaris-Label">
                                 <label for="credit_card" class="Polaris-Label__Text">Credit card</label>
                               </div>
                             </div>
                             <div id="card-elementsub-update" class="stripe-custom-input">
                             </div>
                             <div id="card-errorsub-update" class="Polaris-Labelled__HelpText" role="alert"></div>
                           </div>
                         </div>
                       </div>
                     </section>
                   </div>
                 </div>

                 <div class="Polaris-Modal-Footer">
                   <div class="Polaris-Modal-Footer__FooterContent">
                     <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                        <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
                        <div class="Polaris-Stack__Item">
                         <div class="Polaris-ButtonGroup">
                           <div class="Polaris-ButtonGroup__Item">
                             <button type="button" class="Polaris-Button close-modal disable-while-loading">
                               <span class="Polaris-Button__Content">
                                 <span class="Polaris-Button__Text">Cancel</span>
                               </span>
                             </button>
                           </div>
                           <div class="Polaris-ButtonGroup__Item">
                             <button type="button" class="Polaris-Button Polaris-Button--primary all_addon_subscribe disable-while-loading" onclick="return updateCardForm();">
                               <span class="Polaris-Button__Content">
                                 <span class="Polaris-Button__Text">Update card</span>
                               </span>
                             </button>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </form>

             </div>
           </div>
         </div>
       </div>
       <div class="Polaris-Backdrop"></div>
     </div>

     <!-- Uninstall Addon modal -->
     <div id="uninstalladdonModal" class="modal fade-scales" role="dialog">
       <div>
         <div class="Polaris-Modal-Dialog__Container" data-polaris-layer="true" data-polaris-overlay="true">
           <div>
             <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header11" tabindex="-1">
               <div class="Polaris-Modal-Header">
                 <div class="Polaris-Modal-Header__Title">
                   <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">{{$starter}} plan add-ons limit ({{$starterLimit}}) exceeded</h2>
                 </div>
                 <button type="button" class="Polaris-Modal-CloseButton close-modal disable-while-loading">
                   <span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored">
                     <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                       <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                     </svg>
                   </span>
                 </button>
               </div>
               <form action="{{ route('delete_multipl_addons') }}" method="post" id="uninstalladdonForm" class="Polaris-Modal__BodyWrapper">
                 @csrf
                 <div class="Polaris-Modal__BodyWrapper">
                   <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true" polaris="[object Object]">
                     <section class="Polaris-Modal-Section">
                       <p class="mb">Please uninstall {{$active_add_ons - $starterLimit}} add-ons to be able to select the {{$starter}} plan.</p>
                       <div class="Polaris-Card">
                         <ul class="Polaris-OptionList__Options" id="PolarisOptionList9-0" aria-multiselectable="true">
                          @foreach($global_add_ons as $key=>$addon)
                            @if($addon->status == 1)
                            <li class="Polaris-OptionList-Option" tabindex="-1">
                              <label for="PolarisOptionList9-0-{{$key}}" class="Polaris-OptionList-Option__Label">
                                <div class="Polaris-OptionList-Option__Checkbox">
                                  <div class="Polaris-OptionList-Checkbox">
                                    <input id="PolarisOptionList9-0-{{$key}}" name=addons[] type="checkbox" class="Polaris-OptionList-Checkbox__Input" aria-checked="false" value="{{$addon->id}}">
                                    <div class="Polaris-OptionList-Checkbox__Backdrop"></div>
                                    <div class="Polaris-OptionList-Checkbox__Icon">
                                      <span class="Polaris-Icon">
                                      <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></svg>
                                      </span>
                                    </div>
                                  </div>
                                </div>{{$addon->title}}
                              </label>
                            </li>
                            @endif
                          @endforeach
                        </ul>
                      </div>
                    </section>
                  </div>
                </div>

                <div class="Polaris-Modal-Footer">
                 <div class="Polaris-Modal-Footer__FooterContent">
                   <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                      <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
                      <div class="Polaris-Stack__Item">
                       <div class="Polaris-ButtonGroup">
                         <div class="Polaris-ButtonGroup__Item">
                           <button type="button" class="Polaris-Button close-modal disable-while-loading">
                             <span class="Polaris-Button__Content">
                               <span class="Polaris-Button__Text">Cancel</span>
                             </span>
                           </button>
                         </div>
                         <div class="Polaris-ButtonGroup__Item">
                           <button type="button" class="Polaris-Button Polaris-Button--primary uninstall_Addon disable-while-loading" onclick="return uninstalladdonForm();">
                             <span class="Polaris-Button__Content">
                               <span class="Polaris-Button__Text">Uninstall add-ons</span>
                             </span>
                           </button>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
              </form>

             </div>
           </div>
         </div>
       </div>
       <div class="Polaris-Backdrop"></div>
     </div>
@endsection

@section('scripts')
    @parent
    <script src="/js/jquery.exitintent.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>

    <script type="text/javascript">
      // init shopify title bar
      ShopifyTitleBar.set({
          title: 'Plans',
      });

      // clear session storage item
      sessionStorage.removeItem('addPaymentInfo');
      sessionStorage.removeItem('initiateCheckoutMonthly');
      sessionStorage.removeItem('initiateCheckoutYearly');
      sessionStorage.removeItem("exitIntenShown");

      // open update card modal if coming from /update-card view
      $(window).ready(function(){
        if(sessionStorage.getItem("updateCard")){
          openUpdateCardModal();
          sessionStorage.removeItem('updateCard');
        };
      });

      $('.link-uninstall').click(function(){
        $('.link-uninstall').hide();
        $('.btn-update').hide();
        $('.cancel-link-uninstall').show();
        $('.btn-uninstall').show();
      });

      $('.cancel-link-uninstall').click(function(){
        $('.link-uninstall').show();
        $('.btn-update').show();
        $('.cancel-link-uninstall').hide();
        $('.btn-uninstall').hide();
      });

      // exit intent modal
      @if ($all_addons != 1 && $theme_count != 0)
        function openExitIntent(){
          if (sessionStorage.exitIntenShown) {} else {
            //show modal
            if( $(".modal").hasClass("open") ){} else{
              var modal = $("#exitIntentModal");
              openModal(modal);
              sessionStorage.setItem("exitIntenShown", "true");
            }
          }
        }

        $(".acceptExitIntent").click(function(){
          $("#inputExitIntent").focus();
          $("#inputExitIntent").select();
          document.execCommand('copy');
          toastNotice = Toast.create(ShopifyApp, {
              message: "Coupon code copied",
              duration: 3000,
           });
          toastNotice.dispatch(Toast.Action.SHOW);

          // close modal
          var modal = $("#exitIntentModal").closest(".modal");
          closeModal(modal);
        });

        $.exitIntent("enable");
        $(document).bind("exitintent", function() {
          openExitIntent();
        });

        $(window).blur(function(e) {
          if($("iframe").is(":focus")){}else{
            openExitIntent();
          }
        });
      @endif

      // claim offer trigger
      function claimYearlyOffer(){
        $(".Polaris-DataTable__Table .Polaris-Button--primary").trigger("click");
      }

      // addon video modal
      function addonVideo(title,subtitle){
        @include("components.video-addons")

        $('.tutorial').attr("src","https://www.youtube.com/embed/" + videoSource);

        $(".addon-title").text(title);
        $(".addon-subtitle").text(subtitle);

        if(videoSource){
          $(".video-tutorial").show();
        } else{
          $(".video-tutorial").hide();
        }

        // show modal
        var modal = $("#videoModal");
        openModal(modal);
      }

      // coupons for active subscriptions
      function applySubscriptionCoupon(coupon){
        var subscriptionCouponInput = $('#subscriptionCoupon');
        var couponValue = subscriptionCouponInput.val();
        var exitCode = "{{$exit_code}}";
        var newCode = "{{$new_code}}";

        // empty
        if( !couponValue ){
			    toastNotice = Toast.create(ShopifyApp, {
            message: "No coupon code entered",
            duration: 3000,
            isError: true,
          });
          toastNotice.dispatch(Toast.Action.SHOW);
          return false;
        }

        // prevent coupons offered to new customers only (coupons only for active subscriptions)
        if(couponValue == "20FOR20K" || couponValue == "DEBUTIFYFIFTY"){
          subscriptionCouponInput.val("");
          toastNotice = Toast.create(ShopifyApp, {
            message: "Coupon code not applicable to active subscriptions",
            duration: 3000,
            isError: true,
          });
          toastNotice.dispatch(Toast.Action.SHOW);
          return false;
        }

        // prevent monthly coupons to be used on yearly plans (coupons only for monthly subscriptions)
        if($("#paidAnnuallyRadio").is(':checked') ){
          if( couponValue == exitCode || couponValue == newCode || couponValue == "STAYSAFE30"){
            subscriptionCouponInput.val("");
            toastNotice = Toast.create(ShopifyApp, {
              message: "Coupon code not applicable to yearly plans",
              duration: 3000,
              isError: true,
            });
            toastNotice.dispatch(Toast.Action.SHOW);
              return false;
          }
        }

        loading.dispatch(Loading.Action.START);

        $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
        $('.btn-subscription-coupon').addClass('Polaris-Button--loading');
        $('.btn-subscription-coupon').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">Apply coupon</span></span>');

        if(coupon){
          $("#subscriptionCoupon").val(coupon);
        }

        var plan_id = $('#plan_id').val();
        $('#currentplan_id').val(plan_id);
        var form = document.getElementById('SubscriptionCouponForm');
        form.setAttribute("action","{{ route('applycoupon') }}");
        form.submit();
      }

      // coupons for new subscriptions
      function applyNewCoupon(){
        var coupon = $('#newCoupon').val();
        var newCouponInput = $('#newCoupon');
        var couponValue = newCouponInput.val();
        var exitCode = "{{$exit_code}}";
        var newCode = "{{$new_code}}";
        var annual_price = $('#subtotal_price').val();

        // empty
        if(!couponValue){
          toastNotice = Toast.create(ShopifyApp, {
            message: "No coupon code entered",
            duration: 3000,
            isError: true,
          });
          toastNotice.dispatch(Toast.Action.SHOW);
          return false;
        }

        @if($all_addons != 1)
        // prevent coupons offered to existing customers only (coupons only for active subscriptions)
        if(couponValue == exitCode || couponValue == "STAYSAFE30"){
          $('#newCoupon').val('');
          toastNotice = Toast.create(ShopifyApp, {
            message: "Coupon code not applicable to new subscriptions",
            duration: 3000,
            isError: true,
          });
          toastNotice.dispatch(Toast.Action.SHOW);
          return false;
        }
        @endif

        // prevent monthly coupons to be used on yearly plans (coupons only for monthly subscriptions)
        if($("#paidAnnuallyRadio").is(':checked') ){
          if(couponValue == newCode || couponValue == exitCode || couponValue == "STAYSAFE30"){
            $('#newCoupon').val('');
              toastNotice = Toast.create(ShopifyApp, {
              message: "Coupon code not applicable to yearly plans",
              duration: 3000,
              isError: true,
            });
            toastNotice.dispatch(Toast.Action.SHOW);
            return false;
          }
        }

        loading.dispatch(Loading.Action.START);

        $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
        $('.btn-new-coupon').addClass('Polaris-Button--loading');
        $('.btn-new-coupon').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">Apply coupon</span></span>');

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $.ajax({
            url: "/app/getcoupon",
            data: {'new_coupon' : couponValue},
            type: 'POST',
            cache: false,
            success: function(response){
                loading.dispatch(Loading.Action.STOP);

                var total ='';
                var discount ='';
                var percent_off ='';
                var coupon_name = '';
                var coupon_duration ='';
                var coupon_duration_months ='';
                if(response.status){
                    coupon_name = response.coupon_name;
                    coupon_duration = response.coupon_duration;
                    if(coupon_duration =='repeating'){
                      coupon_duration = 'for';
                    }
                    coupon_duration_months = response.coupon_duration_months;
                    if (coupon_duration_months){
                      coupon_duration_months = coupon_duration_months+ ' months';
                    }else{
                      coupon_duration_months = '';
                    }
                    if(response.percent_off){
                      discount = annual_price*response.percent_off/100;
                      percent_off = response.percent_off+'% off';
                    } else{
                      discount = response.amount_off/100;
                      var percent_off_1 = response.amount_off/100;
                      var  percent_off_2 = percent_off_1.toFixed(2);
                      percent_off = 'US$'+percent_off_2+' off';
                    }
                    $('#addnewcoupon').val(coupon_name);
                    $('.showOnnewcoupon').html('<span class="newcouponDescription"> → '+percent_off+' '+coupon_duration+' '+coupon_duration_months+'</span>');
                    $('.plan-discount').show();
                    toastNotice = Toast.create(ShopifyApp, {
                        message: response.status,
                        duration: 3000,
                    });
                    toastNotice.dispatch(Toast.Action.SHOW);
                } else {
                    $('.showOnnewcoupon').html('');
                    discount = 0;
                    $('.plan-discount').hide();
                    toastNotice = Toast.create(ShopifyApp, {
                      message: "Invalid coupon code",
                      duration: 3000,
                      isError: true,
                    });
                    toastNotice.dispatch(Toast.Action.SHOW);
                }

                // recalculate total price if there's a prorated amount
                if($(".prorated-amount").text()){
                  var toatal = $('.total-price').text();
                  total = toatal - discount;
                  if(total < 0){
                    $('.total-price').text("0.00");
                  } else{
                    $('.total-price').text(total.toFixed(2));
                  }
                } else{
                  total = annual_price - discount;
                  $('.total-price').text(total.toFixed(2));
                }

                $('.subtotal').text(annual_price);
                $('.discounts').text(discount.toFixed(2));
                $('.disable-while-loading').removeClass('Polaris-Button--disabled').prop("disabled", false);
                $('.btn-new-coupon').removeClass('Polaris-Button--loading');
                $('.btn-new-coupon').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Apply coupon</span></span>');
            }
        });
      }

      // all add-ons subscription
      function openSubscriptionModal(master_shop,plan_name,monthly_price,annual_price,monthly_id,annual_id,active_add_ons,plan_limit,active_plan,sub_plan,all_addons){
        // Setup modal elements
        $('.plan-name').text(plan_name);
        $('.payment_cycle').val(plan_name);
        $('.annual-price').text((annual_price*1).toFixed(2));
        $('.monthly-price').text((monthly_price*1).toFixed(2));
        $(".annual-discount-money").text("$"+ ((monthly_price*12)-annual_price).toFixed(2) +" USD");
        $(".annual-discount-percentage").text( ((annual_price/(monthly_price*12))*100).toFixed(0) +"%");
        $('.discounts').text('0.00');
        $('#newCoupon').val('');
        $('.plan-discount').hide();
        $('.showOnnewcoupon').html('');
        $('#addnewcoupon').val('');
        var trialdays = '{{$trial_days}}';

        // show notice if it's a managed store
        if(master_shop){
          toastNotice = Toast.create(ShopifyApp, {
            message: "This store's subscription is managed by "+master_shop,
            duration: 3000,
            isError: true,
          });
          toastNotice.dispatch(Toast.Action.SHOW);
          return false;
        }

        @if($store_count > 0)
        // remove linked stores before downgrading plan
        if(plan_name != "{{$guru}}"){
          toastNotice = Toast.create(ShopifyApp, {
            message: "Remove linked stores to downgrade plan",
            duration: 3000,
            isError: true,
          });
          toastNotice.dispatch(Toast.Action.SHOW);
          return false;
        }
        @endif

        // show notice if too many addons to downgrade
      	if(parseInt(active_add_ons) > parseInt(plan_limit)){
            toastNotice = Toast.create(ShopifyApp, {
              message: "Too many add-ons activated to downgrade plan",
              duration: 3000,
              isError: true,
            });
            toastNotice.dispatch(Toast.Action.SHOW);
            var modals = $("#uninstalladdonModal");
            openModal(modals);
            return false;
        }

        // hide linked domains if not guru plan
        if(plan_name != "{{$guru}}"){
          $('.sectionLinkedStore').hide();
        } else{
          $('.sectionLinkedStore').show();
        }

        // setPlanDetails
        var subMonthly = "monthly";
        var subYearly = "yearly";

        function setPlanDetails(showPlan){
          // reset values
          $('.discounts').text('0.00');
          $('#newCoupon').val('');
          $('.plan-discount').hide();
          $('.showOnnewcoupon').html('');
          $('#addnewcoupon').val('');
          $('.showOnActivePlan').hide();
          $('.hideOnActivePlan').show();
          $(".plan-prorated").hide();
          $(".plan-balance").hide();
          $(".save-message-monthly").hide();
          $(".save-message-annually").hide();

          // calculate proration of subscription for upgrade/downgrade
          function getProration(id,price){
            if(all_addons == 1){
              var plan_id = id;
              var plan_price = price;
              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });

              $.ajax({
                url: "/app/prorateamount",
                data: {'plan_id' : plan_id},
                type: 'POST',
                cache: false,
                success: function(response){
                  prorated_amount = response.prorated_amount;
                  prorated_amount = prorated_amount  * -1;

                  if(prorated_amount){
                    var total =plan_price -prorated_amount;
                    $(".plan-prorated").show();
                    if(total > 0){
                      $('.total-price').text((total).toFixed(2));
                    } else{
                      $('.total-price').text("0.00");
                      $(".plan-balance").show();
                      $(".prorated-balance").text((total * -1).toFixed(2));
                    }
                    $(".prorated-amount").text((prorated_amount).toFixed(2));
                  }
                }
              });
            }
          }

          // show yearly
          if (showPlan == "yearly"){
            $("#paidAnnuallyRadio").prop('checked', true);
            $('.plan-price').text((annual_price*1).toFixed(2));
            $('.total-price').text((annual_price*1).toFixed(2));
            $(".billing-days").text("year");
            $(".save-message-annually").show();

            // stripe form
            $('#plan_id').val(annual_id);
            $("#subtotal_price").val(annual_price);
            $('#sub_plan').val("Yearly");
            // stripe form end

            // get proration
            getProration(annual_id,annual_price);

            if(active_plan == 1){
              if(sub_plan == 'Yearly'){
                $('.showOnActivePlan').show();
                $('.hideOnActivePlan').hide();
              }
              else if (sub_plan == 'month'){
                $('.showOnActivePlan').hide();
                $('.hideOnActivePlan').show();
              }
            }

            @if (env('APP_TRACKING'))
            // initiate checkout tracking - annually
            if(sub_plan != 'Yearly'){
              if(sessionStorage.getItem("initiateCheckoutYearly")){} else{
                var subscriptionValue = annual_price;
                var subscriptionId = annual_id;
                window.dataLayer.push({
                  'subscriptionValue': subscriptionValue,
                  'subscriptionId': subscriptionId,
                  'event': 'initiate_checkout'
                });
                sessionStorage.setItem('initiateCheckoutYearly','yes');
              };
            }
            @endif
          }

          // show monthly
          else if(showPlan == "monthly"){
            $("#paidMonthlyRadio").prop('checked', true);
            $('.plan-price').text((monthly_price*1).toFixed(2));
            $('.total-price').text((monthly_price*1).toFixed(2));
            $(".billing-days").text("30 days");
            $(".save-message-monthly").show();

            // stripe form
            $('#plan_id').val(monthly_id);
            $("#subtotal_price").val(monthly_price);
            $('#sub_plan').val("month");
            // stripe form end

            // get proration
            getProration(monthly_id,monthly_price);

            if(active_plan == 1){
              if(sub_plan == 'Yearly'){
                $('.showOnActivePlan').hide();
                $('.hideOnActivePlan').show();
              }
              else if (sub_plan == 'month'){
                $('.showOnActivePlan').show();
                $('.hideOnActivePlan').hide();
              }
            }

            @if (env('APP_TRACKING'))
            // initiate checkout tracking - monthly
            if(sub_plan != 'month'){
              if(sessionStorage.getItem("initiateCheckoutMonthly")){} else{
                var subscriptionValue = monthly_price;
                var subscriptionId = monthly_id;
                window.dataLayer.push({
                  'subscriptionValue': subscriptionValue,
                  'subscriptionId': subscriptionId,
                  'event': 'initiate_checkout'
                });
                sessionStorage.setItem('initiateCheckoutMonthly','yes');
              };
            }
            @endif
          }
        }

        // active plan
        if(active_plan == 1){
          if(sub_plan == 'Yearly'){
            setPlanDetails(subYearly);
            $('.annual-discount-badge').hide();
          }
          else if (sub_plan == 'month'){
            setPlanDetails(subMonthly);
          }
          $('.active-badge').show();
        }

        // no active plan
        else {
          setPlanDetails(subYearly);
          $('.annual-discount-badge').show();
          $('.active-badge').hide();
        }

        // on click yearly
        $("#paidAnnuallyRadio").on("click",function(){
          setPlanDetails(subYearly);
        });

        // on click monthly
        $("#paidMonthlyRadio").on("click",function(){
          setPlanDetails(subMonthly);
        });

        // set form action
        var form = document.getElementById('StripeForm');
        form.setAttribute("action","{{ route('all_subscription') }}");

        // open modal
        var modal = $("#subscriptionModal");
        openModal(modal);
      }

      // cancel subscription
      function openCancelModal(store_count,subplan,trialdays){
        var currentCoupon = "{{$coupon_name}}";
        var exitCode = "{{$exit_code}}";

        // offer discount
        function offerDiscount(){
          $(".acceptOffer").click(function(){
            applySubscriptionCoupon(exitCode);
            $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
            $('.acceptOffer').addClass('Polaris-Button--loading');
            $('.acceptOffer').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorWhite Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">Claim offer now</span></span>');
          });

          $(".denyOffer").click(function(){
            cancel();
          });

          // show modal
          var modal = $("#offerDiscountModal");
          openModal(modal);
        }

        // cancel
        function cancel(){
          var message = "Are you sure you want to cancel your subscription? This action cannot be reversed.";
          if(store_count > 0){
            message = "Are you sure you want to cancel your subscription? All linked store's subscriptions will also be canceled. This action cannot be reversed.";
          }
          var form = document.getElementById('cancel_form');
          form.setAttribute("action","{{ route('cancel_all_subscription') }}");
          ShopifyApp.Modal.confirm({
            title: "Cancel subscription",
            message: message,
            okButton: "Cancel subscription",
            cancelButton: "Never mind",
            style: "danger"
          }, function(result){
            if(result){
              loading.dispatch(Loading.Action.START);

              $(".link-uninstall").trigger('click');
              $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
              $('.btn-cancel-subscription').addClass('Polaris-Button--loading').removeClass('Polaris-Button--primary');
              $('.btn-cancel-subscription').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorWhite Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">Cancel subscription</span></span>');

              @if (env('APP_TRACKING'))
              //unsubscribe tracking
              window.dataLayer.push({'event': 'unsubscribe'});
              @endif

              setTimeout(function(){
                var forms = document.getElementById('cancel_form');
                forms.submit();
              }, 100);
            }
            else{
              closeModal();
            }
          });
        }

        setTimeout(function(){
          if (subplan == "month" && currentCoupon != exitCode){
            offerDiscount();
          } else{
            cancel();
          }
        }, 100);
      }

      // Update card modal
      function openUpdateCardModal(){
        //show modal
        var modal = $("#updateCardModal");
        openModal(modal);
      }

      // add linked store
      function addchildstore(){
        var store = $('#LinkedStore').val();
        if(store.indexOf(".myshopify.com") >-1){
          $('.primfield').removeClass('Polaris-TextField--error');
        } else{
          $('.primfield').addClass('Polaris-TextField--error');
          return false;
        }
        var child_store = store.substring(0, store.indexOf(".myshopify.com") + '.myshopify.com'.length);
        if (child_store.indexOf("http://") == 0 || child_store.indexOf("https://") == 0) {
          child_store = child_store.replace(/^https?\:\/\//i, "");
        }
        $('#LinkedStore').val(child_store);

        loading.dispatch(Loading.Action.START);

        var form = document.getElementById("addStoreForm");
        form.setAttribute("action","{{ route('addchildstore') }}");
        $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
        $('.save-store').addClass('Polaris-Button--loading');
        $('.save-store').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">Share licence</span></span>');
        form.submit();
      }

      // remove linked store
      function removestore(id, store){
        var form = document.getElementById("removestores");
        form.setAttribute("action","{{ route('removechildstore') }}");
        $('#storeid').val(id);
        $('#child_store').val(store);
          ShopifyApp.Modal.confirm({
            title: "Remove "+store,
            message: "Are you sure you want to stop sharing your licence with "+store+"?",
            okButton: "Remove",
            cancelButton: "Cancel",
            style: "danger"
          }, function(result){
            if(result){
              loading.dispatch(Loading.Action.START);

              $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
              $('.removeLinkedStore'+id).addClass('Polaris-Button--loading');
              $('.removeLinkedStore'+id).html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">X</span></span>');
              setTimeout(function(){ var forms = document.getElementById('removestores'); forms.submit(); }, 100);
            }
          });
      }
    </script>


    <!-- Stripe -->
    <script>
      // Create a Stripe client.
      var stripe = Stripe('{{ env("STRIPE_KEY") }}');
      //var stripe = Stripe('pk_test_xx3BMEbt4brARgYbm45coGn7');
      // console.log('key'+'{{ env("SHOPIFY_API_REDIRECT") }}');
      // Create an instance of Elements.
      var elements = stripe.elements();
      var elementsub = stripe.elements();

      // Custom styling can be passed to options when creating an Element.
      // (Note that this demo uses a wider set of styles than the guide below.)
      var style = {
        base: {
          color: '#32325d',
          lineHeight: '18px',
          fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
          fontSmoothing: 'antialiased',
          fontSize: '16px',
          '::placeholder': {
            color: '#aab7c4'
          }
        },
        invalid: {
          color: '#fa755a',
          iconColor: '#fa755a'
        }
      };

      // Create an instance of the card Element.
      var card = elements.create('card', {style: style});

      // Add an instance of the card Element into the `card-element` <div>.
      @if($all_addons != 1)
      var cardsub = elementsub.create('card', {style: style});
      cardsub.mount('#card-elementsub');
      cardsub.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errorsub');
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {

          @if (env('APP_TRACKING'))
          //add payment info tracking
          if(sessionStorage.getItem("addPaymentInfo")){} else{
            window.dataLayer.push({'event': 'add_payment_info'});
            sessionStorage.setItem('addPaymentInfo','yes');
          };
          @endif

          displayError.textContent = '';
        }
      });
      @endif
      card.mount('#card-elementsub-update');

      // Handle real-time validation errors from the card Element.
      card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors-update');
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {
          displayError.textContent = '';
        }
      });

      window.addEventListener('message', function(event) {
        // IMPORTANT: Check the origin of the data!
        if (~event.origin.indexOf('https://debutify.com')) {
            // The data has been sent from your site

            // The data sent with postMessage is stored in event.data
          //  console.log(event.data);
        } else {
            // The data hasn't been sent from your site!
            // Be careful! Do not use it.
            return;
        }
      });

      // Submit the form with the token ID.
      function stripeTokenHandler(token,form_id) {
        console.log("stripeTokenHandler");
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById(form_id);
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('id', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        try {
          var lmref = lmFinished()
          var linkminkInput = document.createElement('input');
          linkminkInput.setAttribute('type', 'hidden');
          linkminkInput.setAttribute('name', 'linkminkRef');
          linkminkInput.setAttribute('value', lmref);
          form.appendChild(linkminkInput);
        } catch(error) {
          console.error(error);
        };
        // Submit the form
        form.submit();
      }

      // change subscription plan
      function AllAdonsSubscriptioncard(){
        var form = document.getElementById('StripeForm');
        loading.dispatch(Loading.Action.START);

        $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
        $('.all_addon_subscribe').addClass('Polaris-Button--loading');
        $('.all_addon_subscribe').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorWhite Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">Start plan</span></span>');

        // get total value after coupon code
        var subscriptionValue = $(".gtm-total-price").text();

        @if (env('APP_TRACKING'))
        //purchase tracking
        window.dataLayer.push({
          'subscriptionValue': subscriptionValue,
          'event': 'purchase'
        });
        @endif

        @if (env('APP_TRACKING'))
        //subscribe tracking
        window.dataLayer.push({
          'subscriptionValue': subscriptionValue,
          'event': 'subscribe'
        });
        @endif

        form.submit();
      }

      // create new subscription plan
      function AllAdonsSubscription(){
        stripe.createToken(cardsub).then(function(result) {
          if (result.error) {
            // Inform the user if there was an error.
            var errorElement = document.getElementById('card-errorsub');
            errorElement.textContent = result.error.message;
          } else {
            loading.dispatch(Loading.Action.START);

            $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
            $('.all_addon_subscribe').addClass('Polaris-Button--loading');
            $('.all_addon_subscribe').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorWhite Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">Start plan</span></span>');

            // get total value after coupon code
            var subscriptionValue = $(".gtm-total-price").text();

            @if (env('APP_TRACKING'))
            //purchase tracking
            window.dataLayer.push({
              'subscriptionValue': subscriptionValue,
              'event': 'purchase'
            });
            @endif

            @if (env('APP_TRACKING'))
            //subscribe tracking
            window.dataLayer.push({
              'subscriptionValue': subscriptionValue,
              'event': 'subscribe'
            });
            @endif

            setTimeout(function(){
              // Send the token to your server.
              stripeTokenHandler(result.token,'StripeForm');
            }, 100);
          }
        });
      }

      // update card - deprecated, replaced by customer source
      function updateCardForm() {
          stripe.createSource(card).then(function(result) {
          if (result.error) {
            // Inform the user if there was an error
            var errorElement = document.getElementById('card-errorsub-update');
            errorElement.textContent = result.error.message;
          } else {
            // Send the source to your server
            loading.dispatch(Loading.Action.START);

            $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
            $('.all_addon_subscribe').addClass('Polaris-Button--loading');
            $('.all_addon_subscribe').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorWhite Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">Update card</span></span>');
            stripeSourceHandler(result.source);
          }
        });

        // stripe.createToken(card).then(function(result) {
        //   if (result.error) {
        //     // Inform the user if there was an error.
        //     var errorElement = document.getElementById('card-errorsub-update');
        //     errorElement.textContent = result.error.message;
        //   } else {
        //     $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
        //     $('.all_addon_subscribe').addClass('Polaris-Button--loading');
        //     $('.all_addon_subscribe').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><img src="data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjAgMjAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTcuMjI5IDEuMTczYTkuMjUgOS4yNSAwIDEgMCAxMS42NTUgMTEuNDEyIDEuMjUgMS4yNSAwIDEgMC0yLjQtLjY5OCA2Ljc1IDYuNzUgMCAxIDEtOC41MDYtOC4zMjkgMS4yNSAxLjI1IDAgMSAwLS43NS0yLjM4NXoiIGZpbGw9IiM5MTlFQUIiLz48L3N2Zz4K" alt="" class="Polaris-Spinner Polaris-Spinner--colorWhite Polaris-Spinner--sizeSmall" draggable="false" role="status" aria-label="Loading"></span><span class="Polaris-Button__Text">Update card</span></span>');

        //     // Send the token to your server.
        //     stripeTokenHandler(result.token,'updateCardForm');
        //   }
        // });
      }

      // source update customer
      function stripeSourceHandler(source) {
        console.log("stripeSourceHandler");
        // Insert the source ID into the form so it gets submitted to the server
        var form = document.getElementById('updateCardForm');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeSource');
        hiddenInput.setAttribute('value', source.id);
        form.appendChild(hiddenInput);
        try {
          var lmref = lmFinished();
          var linkminkInput = document.createElement('input');
          linkminkInput.setAttribute('type', 'hidden');
          linkminkInput.setAttribute('name', 'linkminkRef');
          linkminkInput.setAttribute('value', lmref);
          form.appendChild(linkminkInput);
        } catch(error) {
          console.error(error);
        }
        // Submit the form
        form.submit();
      }

      // uninstall addon
      function uninstalladdonForm(){
        var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });
        // console.log(val.length);
        if(val.length > 0){
          var form = document.getElementById('uninstalladdonForm');
          loading.dispatch(Loading.Action.START);

          $('.disable-while-loading').addClass('Polaris-Button--disabled').prop("disabled", true);
          $('.uninstall_Addon').addClass('Polaris-Button--loading');
          $('.uninstall_Addon').html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><span class="Polaris-Spinner Polaris-Spinner--colorWhite Polaris-Spinner--sizeSmall"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M7.229 1.173a9.25 9.25 0 1011.655 11.412 1.25 1.25 0 10-2.4-.698 6.75 6.75 0 11-8.506-8.329 1.25 1.25 0 10-.75-2.385z"></path></svg></span><span role="status"><span class="Polaris-VisuallyHidden">Loading</span></span></span><span class="Polaris-Button__Text">Uninstall add-ons</span></span>');
         form.submit();
        }
      }
  </script>
@endsection
