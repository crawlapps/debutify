@extends('layouts.debutify')
@section('title','theme library')
@section('view-themes','view-themes')

@section('styles')
<style>
.pricingBanner{
  display: none;
}
</style>
@endsection

@section('content')
  @if (session('new_version'))
  <!-- New theme update banner -->
  <div class="newThemeVersionBanner Polaris-Banner Polaris-Banner--statusInfo Polaris-Banner--hasDismiss Polaris-Banner--withinPage" tabindex="0" role="alert" aria-live="polite" aria-labelledby="Banner3Heading" aria-describedby="Banner3Content">
    <div class="Polaris-Banner__Dismiss"><button type="button" class="dismiss-banner Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Dismiss notification"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                <path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
              </svg></span></span></span></button></div>
    <div class="Polaris-Banner__Ribbon">
      <span class="Polaris-Icon Polaris-Icon--colorTealDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop">
        <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><circle cx="10" cy="10" r="9" fill="currentColor"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8m1-5v-3a1 1 0 0 0-1-1H9a1 1 0 1 0 0 2v3a1 1 0 0 0 1 1h1a1 1 0 1 0 0-2m-1-5.9a1.1 1.1 0 1 0 0-2.2 1.1 1.1 0 0 0 0 2.2"></path></svg>
      </span>
    </div>
    <div>
      <div class="Polaris-Banner__Heading">
        <p class="Polaris-Heading">New Debutify {{ $version }} update available</p>
      </div>
    </div>
  </div>
  @endif

  @if(@$latestupload)
  <!-- Theme uploaded banner -->
  <div style="display:none;" class="themeUploadedBanner Polaris-Banner Polaris-Banner--statusSuccess Polaris-Banner--hasDismiss Polaris-Banner--withinPage" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner3Heading" aria-describedby="Banner3Content">
    <div class="Polaris-Banner__Dismiss"><button type="button" class="dismiss-banner Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly" aria-label="Dismiss notification"><span class="Polaris-Button__Content"><span class="Polaris-Button__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                <path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
              </svg></span></span></span></button></div>
    <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorGreenDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
          <circle fill="currentColor" cx="10" cy="10" r="9"></circle>
          <path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8m2.293-10.707L9 10.586 7.707 9.293a1 1 0 1 0-1.414 1.414l2 2a.997.997 0 0 0 1.414 0l4-4a1 1 0 1 0-1.414-1.414"></path>
        </svg></span></div>
    <div>

      <div class="Polaris-Banner__Heading" id="Banner3Heading">
        <p class="Polaris-Heading">You successfully added {{$latestupload->shopify_theme_name}}</p>
      </div>
      <div class="Polaris-Banner__Content" id="Banner3Content">
        <div class="Polaris-Banner__Actions">
          <div class="Polaris-ButtonGroup">
            <div class="Polaris-ButtonGroup__Item">
              <div class="Polaris-Banner__PrimaryAction">
                <a href="https://{{ $shop_domain}}/admin/themes/{{$latestupload->shopify_theme_id}}/editor" target="_blank" class="Polaris-Button Polaris-Button--outline">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Customize theme</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif

  @if(@$theme_count > 0)
  <!-- skeleton -->
  <div class="Polaris-SkeletonPage__Page skeleton-wrapper" role="status" aria-label="Page loading">
    <div class="Polaris-SkeletonPage__Content">

      <div class="Polaris-Card">

        <div class="Polaris-Card__Section">
          <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
            <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
              <h2 class="Polaris-Heading">Themes</h2>
            </div>
            <div class="Polaris-Stack__Item">
              <div class="Polaris-ButtonGroup">
                <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                  <button style="visibility:hidden;" type="submit" class="Polaris-Button">
                    <span class="Polaris-Button__Content">
                      <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Add Debutify {{ $version }}</span></span>
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="Polaris-ResourceList__ResourceListWrapper">
          <ul class="Polaris-ResourceList" aria-live="polite">
            @foreach($store_themes as $theme)
            <li class="Polaris-ResourceList__ItemWrapper">
              <div class="">
                <div class="Polaris-ResourceItem__Container Polaris-ResourceItem--alignmentCenter">
                  <div class="Polaris-ResourceItem__Owned">
                    <div class="Polaris-ResourceItem__Media">
                      <div style="margin:0;border-radius:50%;overflow:hidden;" class="Polaris-SkeletonThumbnail Polaris-SkeletonThumbnail--sizeSmall Polaris-Avatar__Image"></div>
                    </div>
                  </div>
                  <div class="Polaris-ResourceItem__Content">
                    <div class="Polaris-SkeletonBodyText__SkeletonBodyTextContainer">
                      <div class="Polaris-SkeletonBodyText"></div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            @endforeach
          </ul>
        </div>

      </div>

    </div>
  </div>
  @endif

  <div id="dashboard" @if($theme_count >= 1) style="display:none;" @endif>
    @if($theme_count > 0)
    <div class="Polaris-Card">

      <div class="Polaris-Card__Section">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Heading">Themes</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item">
                <button type="submit" class="Polaris-Button refresh_button btn-loading" onclick="return refreshThemes();">
                  <span class="Polaris-Button__Content">
                    <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Refresh</span></span>
                  </span>
                </button>
              </div>
              <div class="Polaris-ButtonGroup__Item">
                <button type="submit" class="Polaris-Button Polaris-Button--primary" onclick="return openDownloadThemeModal();">
                  <span class="Polaris-Button__Content">
                    <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Add Debutify {{ $version }}</span></span>
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="Polaris-ResourceList__ResourceListWrapper">
        <ul class="Polaris-ResourceList" aria-live="polite">
          @foreach($store_themes as $theme)
          <li class="Polaris-ResourceList__ItemWrapper">
            <div class="Polaris-ResourceItem Polaris-ResourceItem--persistActions open-modal" onclick="return openThemeModal('{{ $theme->shopify_theme_id }}','{{ $theme->shopify_theme_name }}','{{ $shop_domain }}');">
              <a class="Polaris-ResourceItem__Link" aria-describedby="" aria-label="View details for" tabindex="0" data-polaris-unstyled="true"></a>
              <div class="Polaris-ResourceItem__Container Polaris-ResourceItem--alignmentCenter" id="">
                <div class="Polaris-ResourceItem__Owned">
                  <div class="Polaris-ResourceItem__Media">
                    <span aria-label="" role="img" class="Polaris-Avatar Polaris-Avatar--styleSix Polaris-Avatar--sizeMedium Polaris-Avatar--hasImage">
                      @if($theme->role == 1)
                        <img src="/svg/unlock.svg" class="Polaris-Avatar__Image" alt="" role="presentation">
                      @else
                        <img src="/svg/lock.svg" class="Polaris-Avatar__Image" alt="" role="presentation">
                      @endif
                    </span>
                  </div>
                </div>
                <div class="Polaris-ResourceItem__Content">
                  <h3>
                    <span class="Polaris-Heading">{{ $theme->shopify_theme_name }}</span>
                    @if($theme->role == 1)
                      <span class="Polaris-Badge Polaris-Badge--statusSuccess">Live</span>
                    @else
                      <span class="Polaris-Badge">Unpublished</span>
                    @endif
                  </h3>
                  <div></div>
                </div>
              </div>
            </div>
          </li>
          @endforeach
        </ul>
      </div>
    </div>

    <!-- cutomize theme Modal -->
    <div id="themeModal" class="modal fade-scale" style="display:none;">
      <div>
        <div class="Polaris-Modal-Dialog__Container" data-polaris-layer="true" data-polaris-overlay="true">
          <div>
            <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header11" tabindex="-1">
              <div class="Polaris-Modal-Header">
                <div id="modal-header11" class="Polaris-Modal-Header__Title">
                  <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall theme-title"></h2>
                </div>
                <button type="button" class="Polaris-Modal-CloseButton close-modal">
                  <span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored">
                    <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                      <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                    </svg>
                  </span>
                </button>
              </div>

              <div class="Polaris-Modal__BodyWrapper">
                <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true" polaris="[object Object]">
                  <section class="Polaris-Modal-Section">
                    <div class="Polaris-TextContainer">
                      <p>Click the button below to customize your Debutify theme.</p>
                    </div>
                  </section>
                </div>
              </div>

              <div class="Polaris-Modal-Footer">
                <div class="Polaris-Modal-Footer__FooterContent">
                  <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
                    <div class="Polaris-Stack__Item">
                      <div class="Polaris-ButtonGroup">
                        <div class="Polaris-ButtonGroup__Item">
                          <button type="button" class="Polaris-Button close-modal">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">Cancel</span>
                            </span>
                          </button>
                        </div>
                        <div class="Polaris-ButtonGroup__Item">
                          <a href="" target="_blank" class="customize_theme close-modal"><button type="button" class="Polaris-Button Polaris-Button--primary">
                            <span class="Polaris-Button__Content">
                              <span class="Polaris-Button__Text">Customize</span>
                            </span>
                          </button></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="Polaris-Backdrop"></div>
    </div>

    <!-- download theme modal -->
    <div id="DownloadThemeModal" class="modal fade-scale" style="display:none;">
      <div>
        <div class="Polaris-Modal-Dialog__Container" data-polaris-layer="true" data-polaris-overlay="true">
          <div>
            <div class="Polaris-Modal-Dialog__Modal" role="dialog" aria-labelledby="modal-header11" tabindex="-1">

              <div class="Polaris-Modal-Header">
                <div id="modal-header11" class="Polaris-Modal-Header__Title">
                  <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeSmall">Add Debutify {{ $version }}</h2>
                </div>
                <button type="button" class="Polaris-Modal-CloseButton close-modal disable-while-loading">
                  <span class="Polaris-Icon Polaris-Icon--colorInkLighter Polaris-Icon--isColored">
                    <svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                      <path d="M11.414 10l6.293-6.293a.999.999 0 1 0-1.414-1.414L10 8.586 3.707 2.293a.999.999 0 1 0-1.414 1.414L8.586 10l-6.293 6.293a.999.999 0 1 0 1.414 1.414L10 11.414l6.293 6.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill-rule="evenodd"></path>
                    </svg>
                  </span>
                </button>
              </div>

              <div class="Polaris-Modal__BodyWrapper">
                <div class="Polaris-Modal__Body Polaris-Scrollable Polaris-Scrollable--vertical" data-polaris-scrollable="true" polaris="[object Object]">
                  <section class="Polaris-Modal-Section">
                    <div class="Polaris-TextContainer">
                      <p>Select which theme to copy settings from or start fresh. This new version will appear in your theme library as unpublished and will not affect your live store. This process can take several minutes.</p>
                    </div>
                  </section>
                </div>
              </div>

              <div class="Polaris-Modal-Footer">
                <div class="Polaris-Modal-Footer__FooterContent">
                  <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                    <div class="Polaris-Stack__Item Polaris-Stack__Item--fill"></div>
                    <div class="Polaris-Stack__Item">

                      <form id="download_theme_form" method="POST" action="" enctype="multipart/form-data">
                        @csrf
                        <div class="Polaris-ButtonGroup">
                          <div class="Polaris-ButtonGroup__Item">
                            <!-- <button type="button" class="Polaris-Button close-modal">
                              <span class="Polaris-Button__Content">
                                <span class="Polaris-Button__Text">Cancel</span>
                              </span>
                            </button> -->
                            <div class="Polaris-Select">
                              <select id="themeSelect" name="theme_id" class="Polaris-Select__Input disable-while-loading themeselect" aria-invalid="false">
                                <option value="">Fresh copy</option>
                                @foreach($store_themes as $theme)
                                <option value="{{ $theme->shopify_theme_id }}">{{ $theme->shopify_theme_name }}</option>
                                @endforeach
                              </select>
                              <div class="Polaris-Select__Content" aria-hidden="true">
                                <span class="Polaris-Select__SelectedOption"></span>
                                <span class="Polaris-Select__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true"><path d="M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z" fill-rule="evenodd"></path></svg></span></span>
                              </div>
                              <div class="Polaris-Select__Backdrop"></div>
                            </div>
                          </div>
                          <div class="Polaris-ButtonGroup__Item">
                            <button type="button" class="Polaris-Button Polaris-Button--primary download_theme btn-loading" onclick="return themedownload();">
                              <span class="Polaris-Button__Content">
                                <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Add to theme library</span></span>
                              </span>
                            </button>
                          </div>
                        </div>
                      </form>

                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="Polaris-Backdrop"></div>
    </div>
    @else
    <!-- empty state -->
    <div class="Polaris-EmptyState Polaris-EmptyState--withinPage">
      <div class="Polaris-EmptyState__Section">
        <div class="Polaris-EmptyState__DetailsContainer">
          <div class="Polaris-EmptyState__Details">
            <div class="Polaris-TextContainer">
              <p class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
                Download Debutify theme
                <span class="Polaris-Badge Polaris-Badge--statusSuccess">{{ $version }}</span>
              </p>
              <div class="Polaris-EmptyState__Content">
                <p>Start your eCommerce journey with<br class="d-none d-sm-block"> The World's #1 Free Shopify theme.</p>
              </div>
            </div>
            <div class="Polaris-EmptyState__Actions">
              <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                <div class="Polaris-Stack__Item">
                  <form id="download_theme_form1" method="POST" action="" enctype="multipart/form-data">
                    @csrf
                    <button type="submit" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeLarge download_theme btn-loading" onclick="return themedownload_post();">
                      <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Add to theme library</span></span>
                    </button>
                  </form>
                </div>
              </div>
            </div>
            <div class="Polaris-EmptyState__FooterContent">
              <div class="Polaris-TextContainer">
                <p>It will not affect your live store until you publish it.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="Polaris-EmptyState__ImageContainer"><img src="/svg/empty-state-7.svg" role="presentation" alt="" class="Polaris-EmptyState__Image"></div>
      </div>
    </div>
    {{--<div class="Polaris-EmptyState">
      <div class="row text-center">
        <div class="col-sm">
          <div class="Polaris-TextContainer  Polaris-TextContainer--spacingTight">
            <img src="/svg/illustration-5.svg" alt="" class="img-fluid" style="height:100px">
            <h2 class="Polaris-Heading">1. Forever free</h2>
            <p>Debutify theme will always be 100% free and 100% awesome.</p>
          </div>
        </div>
        <div class="col-sm">
          <div class="Polaris-TextContainer  Polaris-TextContainer--spacingTight">
            <img src="/svg/illustration-7.svg" alt="" class="img-fluid" style="height:100px">
            <h2 class="Polaris-Heading">2. High converting</h2>
            <p>Built for speed and optimized for maximum conversions.</p>
          </div>
        </div>
        <div class="col-sm">
          <div class="Polaris-TextContainer  Polaris-TextContainer--spacingTight">
            <img src="/svg/illustration-9.svg" alt="" class="img-fluid" style="height:100px">
            <h2 class="Polaris-Heading">3. Made by the community</h2>
            <p>We understand what it takes to make a successful Shopify store and we're here to help you do it.</p>
          </div>
        </div>
      </div>
    </div>--}}
    @endif

  </div>
@endsection

@section('scripts')
    @parent
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        // init shopify title bar
        ShopifyTitleBar.set({
            title: 'Themes',
        });

        $(document).ready(function() {
          // show theme uploaded banner
          var themeBannerView = localStorage.getItem('themeBannerView') || '';
          if (themeBannerView == 'yes') {
            $('.themeUploadedBanner').show();
            localStorage.setItem('themeBannerView','');
          }
        });

        // open theme modal
        function openThemeModal(theme_id,title,shop_domain){
          $('.theme-title').text("Customize "+title);
          $('.customize_theme').attr('href','https://'+shop_domain+'/admin/themes/'+theme_id+'/editor');

          // show modal
          var modal = $("#themeModal");
          openModal(modal);
        }

        // open download theme modal
        function openDownloadThemeModal(){
          // show modal
          var modal = $("#DownloadThemeModal");
          openModal(modal);
        }

        // refresh theme function
        function refreshThemes(){
          var form = document.getElementById('download_theme_form');
          form.setAttribute("action","get_theme_refresh");
          form.submit();
        }

        // theme download > 1
        function themedownload(){
          var form = document.getElementById('download_theme_form');
          form.setAttribute("action","{{ route('download_theme_post') }}");
           $.ajax({
            url: '/app/theme',
            type: 'POST',
            data: $('#download_theme_form').serialize(),
            success: function(result) {
              loading.dispatch(Loading.Action.STOP);

              if(result.status == 'ok'){
                  toastNotice = Toast.create(ShopifyApp, {
                      message: result.message,
                      duration: 3000,
                  });
                  toastNotice.dispatch(Toast.Action.SHOW);
                  localStorage.setItem('themeBannerView','yes');
                  window.location.href= "/app/add_ons";
              }
              else if(result.status == 'error'){
                  toastNotice = Toast.create(ShopifyApp, {
                    message: result.message,
                    duration: 3000,
                    isError: true,
                  });
                  toastNotice.dispatch(Toast.Action.SHOW);
                  window.location.href= "themes";
              }
            }
          });
        }

        // first theme download
        function themedownload_post(){
          var form = document.getElementById('download_theme_form1');
          form.setAttribute("action","{{ route('theme_download_post') }}");
           $.ajax({
            url: '/app/theme/download',
            type: 'POST',
            data: $('#download_theme_form1').serialize(),
            success: function(result) {
              loading.dispatch(Loading.Action.STOP);

              if(result.status == 'ok'){
                  toastNotice = Toast.create(ShopifyApp, {
                      message: result.message,
                      duration: 3000,
                  });
                  toastNotice.dispatch(Toast.Action.SHOW);
                  localStorage.setItem('themeBannerView','yes');
                  window.location.href= "/app/add_ons";
              }
              else if(result.status == 'error'){
                  toastNotice = Toast.create(ShopifyApp, {
                    message: result.message,
                    duration: 3000,
                    isError: true,
                  });
                  toastNotice.dispatch(Toast.Action.SHOW);
                  window.location.href= "themes";
              }
            }
          });
        }
    </script>
@endsection
