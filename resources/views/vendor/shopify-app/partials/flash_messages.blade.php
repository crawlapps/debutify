<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {

        @if (session()->has('status'))
            var toastNotice = Toast.create(ShopifyApp, {
                message: "{{ session('status') }}",
                duration: 3000,
            });
            toastNotice.dispatch(Toast.Action.SHOW);
        @endif

        @if (session()->has('error'))
            var toastNotice = Toast.create(ShopifyApp, {
                message: "{{ session('error') }}",
                duration: 3000,
                isError: true,
            });
            toastNotice.dispatch(Toast.Action.SHOW);
        @endif
    });
</script>
