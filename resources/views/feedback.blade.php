@extends('layouts.debutify')
@section('title','feedback')
@section('view-feedback','view-feedback')

@section('styles')
<style>
.pollly-embed{
  margin-top:2rem;
}
.pricingBanner{
  display: none;
}
</style>
@endsection

@section('content')
<div id="dashboard">
  <div class="pollly-embed text-center" data-id="Pv8OBjmO"></div>
</div>
@endsection

@section('scripts')
  @parent
  <script src="https://poll.ly/scripts/embed.js"></script>
  <script type="text/javascript">
      // ESDK page and bar title
      ShopifyTitleBar.set({
          title: 'Feedback',
      });
  </script>
@endsection
