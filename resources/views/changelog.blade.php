@extends('layouts.debutify')
@section('title','changelog')
@section('view-changelog','view-changelog')

@section('styles')
<style>
.pricingBanner{
  display: none;
}
.Polaris-Badge{
  vertical-align: inherit;
}
.old .Polaris-Badge{
  display: none;
}
</style>
@endsection

@section('content')
<div id="dashboard">

  <div class="Polaris-Card">
    <div class="Polaris-Card__Section">
      <h2 class="Polaris-Heading">April 02, 2020 <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></h2>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Add-ons</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}add_ons" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Cart goals <span class="Polaris-TextStyle--variationSubdued">→ Offer free shipping when a specific cart goal amount is reached</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">FAQ page <span class="Polaris-TextStyle--variationSubdued">→ New interactive search bar + Subcategory questions + Updated styling</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Upsell bundles <span class="Polaris-TextStyle--variationSubdued">→ New placement under the add-to-cart button + Fixed a bug when using more than 20+ upsell bundles</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Product tabs <span class="Polaris-TextStyle--variationSubdued">→ Updated styling + Fixed a bug when using more than 20+ product tabs</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Cart countdown <span class="Polaris-TextStyle--variationSubdued">→ Cart page updated styling + Code improvement for better performance</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--statusInfo">Updated</span></li>
        <li class="Polaris-List__Item">Smart search <span class="Polaris-TextStyle--variationSubdued">→ Fixed a bug where pressing "enter" would redirect to a 404 error page</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Cart discount <span class="Polaris-TextStyle--variationSubdued">→ Fixed a bug where pressing "enter" would redirect to a 404 error page</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Quick view <span class="Polaris-TextStyle--variationSubdued">→ Fixed a bug where the image slider would collapse when resizing the page</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Upsell bundles <span class="Polaris-TextStyle--variationSubdued">→ Fixed a bug when using more than 20+ upsell blocks</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Upsell pop-up <span class="Polaris-TextStyle--variationSubdued">→ Fixed a bug when using more than 20+ upsell pop-ups</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Product research</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}winning-products" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Now available on all paid plans <span class="Polaris-TextStyle--variationSubdued">→ New products categorization Bronze/Silver/Gold for each plans</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Product filters <span class="Polaris-TextStyle--variationSubdued">→ Filter products by niche, profit margins, opportunity level or use our search bar</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Courses</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}courses" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">embedded courses <span class="Polaris-TextStyle--variationSubdued">→ Courses are now embedded directly in Debtutify app</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Beginner Shopify course <span class="Polaris-TextStyle--variationSubdued">→ Updated course structure</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Beginner Facebook ads course <span class="Polaris-TextStyle--variationSubdued">→ Updated course structure</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Beginner Google course <span class="Polaris-TextStyle--variationSubdued">→ Updated course structure</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Beginner Product research course <span class="Polaris-TextStyle--variationSubdued">→ Updated course structure</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Mentoring</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}mentoring" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">embedded mentoring calls <span class="Polaris-TextStyle--variationSubdued">→ View all new and past mentoring call winners directly in Debutify app</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Plans</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}plans" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Starter plan <span class="Polaris-TextStyle--variationSubdued">→ adjusted pricing</span> <span class="Polaris-Badge Polaris-Badge--statusInfo Polaris-Badge--sizeSmall">Updated</span></li>
      </ul>
    </div>
  </div>

  <div class="Polaris-Card old">
    <div class="Polaris-Card__Section">
      <h2 class="Polaris-Heading">February 29, 2020 <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></h2>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Add-ons</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}add_ons" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Quick view <span class="Polaris-TextStyle--variationSubdued">→ Quickly view product details & add-to-cart in a pop-up display</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Smart search <span class="Polaris-TextStyle--variationSubdued">→ Show search results immediately as you type into the search field</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Mega menu <span class="Polaris-TextStyle--variationSubdued">→ Fixed a bug on stores with more than 50+ products</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Product video <span class="Polaris-TextStyle--variationSubdued">→ Compatibility with Quick view + disabled thumbnail video trigger</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Upsell bundles <span class="Polaris-TextStyle--variationSubdued">→ Fixed a bug on stores with more than 50+ products</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">Updated</span></li>
      </ul>
    </div>
  </div>

  <div class="Polaris-Card old">
    <div class="Polaris-Card__Section">
      <h2 class="Polaris-Heading">February 26, 2020 <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></h2>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Plans</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}plans" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">14 free trial <span class="Polaris-TextStyle--variationSubdued">→ added new 14 day free trial without entering credit card details</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Add-ons</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}add_ons" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Cart discount <span class="Polaris-TextStyle--variationSubdued">→ Allow customers to enter discount codes before checkout</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Color swatches <span class="Polaris-TextStyle--variationSubdued">→ Add color/image swatches to your product options</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Upsell bundles <span class="Polaris-TextStyle--variationSubdued">→ Display frequently bought together product bundles</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Skip cart <span class="Polaris-TextStyle--variationSubdued">→ Skip the cart page and go straight to checkout</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Cart countdown <span class="Polaris-TextStyle--variationSubdued">→ fixed bug that caused the timer to sometime not start with cart page enabled</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Newsletter pop-up <span class="Polaris-TextStyle--variationSubdued">→ fixed bug that caused the page to jump down</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">Updated</span></li>
      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Support</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="https://debutify.crisp.help" target="_blank" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Helpdesk <span class="Polaris-TextStyle--variationSubdued">→ New Helpdesk support to be able to help you even faster and provide better support</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
      </ul>
    </div>
  </div>

  <div class="Polaris-Card old">
    <div class="Polaris-Card__Section">
      <h2 class="Polaris-Heading">February 04, 2020 <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></h2>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Add-ons</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}add_ons" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Product tabs <span class="Polaris-TextStyle--variationSubdued">→ add icons to each tabs</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Trust badge <span class="Polaris-TextStyle--variationSubdued">→ remove badges from product page</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">Updated</span></li>
      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">Winning products</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}winning-products" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Products <span class="Polaris-TextStyle--variationSubdued">→ now integrated directly on the app</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Details <span class="Polaris-TextStyle--variationSubdued">→ now includes prices margins, images, videos, audiences, spy tools, facebook interest and more!</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
      </ul>
    </div>
  </div>

  <div class="Polaris-Card old">
    <div class="Polaris-Card__Section">
      <h2 class="Polaris-Heading">January 13, 2020 <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></h2>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">plans</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}plans" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">{{$starter}} plan <span class="Polaris-TextStyle--variationSubdued">→ adjusted pricing</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">{{$hustler}} plan <span class="Polaris-TextStyle--variationSubdued">→ now includes all add-ons + adjusted pricing</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
      </ul>
    </div>
  </div>

  <div class="Polaris-Card old">
    <div class="Polaris-Card__Section">
      <h2 class="Polaris-Heading">December 11, 2019 <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></h2>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">plans</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}plans" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">{{$guru}} plan <span class="Polaris-TextStyle--variationSubdued">→ now has 3 store licenses (shared across different Shopify stores)</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">themes</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}themes" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Theme updater <span class="Polaris-TextStyle--variationSubdued">→ copy all your previous theme settings when adding a new Debutify theme <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></span></li>
      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
          <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
            <h2 class="Polaris-Subheading">add-ons</h2>
          </div>
          <div class="Polaris-Stack__Item">
            <div class="Polaris-ButtonGroup">
              <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                <a href="{{env('APP_PATH')}}add_ons" class="Polaris-Button Polaris-Button--plain">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Explore</span></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">Sales countdown <span class="Polaris-TextStyle--variationSubdued">→ display a sales countdown in your product page</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Inventory quantity <span class="Polaris-TextStyle--variationSubdued">→ display the stock level of your product variant</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Linked options <span class="Polaris-TextStyle--variationSubdued">→ hide unavailable and sold out variant options</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Cart countdown <span class="Polaris-TextStyle--variationSubdued">→ display a countdown timer in the cart drawer/page</span> <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span></li>
        <li class="Polaris-List__Item">Product tabs <span class="Polaris-TextStyle--variationSubdued">→ multiple product type/tags visibility settings</span> <span class="Polaris-Badge Polaris-Badge--statusWarning Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Newsletter pop-up <span class="Polaris-TextStyle--variationSubdued">→ disable time-based trigger setting</span> <span class="Polaris-Badge Polaris-Badge--statusWarning Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Collection add-to-cart <span class="Polaris-TextStyle--variationSubdued">→ button style setting</span> <span class="Polaris-Badge Polaris-Badge--statusWarning Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Upsell pop-up <span class="Polaris-TextStyle--variationSubdued">→ minor color fix</span> <span class="Polaris-Badge Polaris-Badge--statusWarning Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Sticky add-to-cart <span class="Polaris-TextStyle--variationSubdued">→ simplified mobile option, sales price, star ratings, Discount saved, Sales countdown, Inventory quantity</span> <span class="Polaris-Badge Polaris-Badge--statusWarning Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Discount saved <span class="Polaris-TextStyle--variationSubdued">→ minor update for Sticky add-to-cart compatibility</span> <span class="Polaris-Badge Polaris-Badge--statusWarning Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Sales pop <span class="Polaris-TextStyle--variationSubdued">→ major design improvement and new settings</span> <span class="Polaris-Badge Polaris-Badge--statusWarning Polaris-Badge--sizeSmall">Updated</span></li>
        <li class="Polaris-List__Item">Delivery time <span class="Polaris-TextStyle--variationSubdued">→ show as simple day option (ex:ships in 2-5 days)</span> <span class="Polaris-Badge Polaris-Badge--statusWarning Polaris-Badge--sizeSmall">Updated</span></li>

      </ul>
    </div>
    <div class="Polaris-Card__Section">
      <div class="Polaris-Card__SectionHeader">
        <h2 aria-label="Items" class="Polaris-Subheading">General</h2>
      </div>
      <ul class="Polaris-List">
        <li class="Polaris-List__Item">
          Vote for future add-ons to develop
          <a href="{{env('APP_PATH')}}feedback" class="Polaris-Button Polaris-Button--plain">
            <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">add a suggestion</span></span>
          </a>
          <span class="Polaris-Badge Polaris-Badge--statusSuccess Polaris-Badge--sizeSmall">New</span>
        </li>
      </ul>
    </div>
  </div>

</div>
@endsection

@section('scripts')
  @parent
  <script type="text/javascript">
      // ESDK page and bar title
      ShopifyTitleBar.set({
          title: 'Changelog',
      });
  </script>
@endsection
