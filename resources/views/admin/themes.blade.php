@extends('layouts.app')
@section('styles')
<style>
.btn-add-product{
  position: fixed;
  bottom: 30px;
  right: 30px;
}
</style>
@endsection
@section('content')
<div class="row">
  <div class="col">

    @if (session('status'))
      <div class="alert alert-success" role="alert">
        {{ session('status') }}
      </div>
    @endif

    <div class="table-responsive rounded">
      <table class="table table-bordered table-hover mb-0">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Version</th>
            <th scope="col">Link</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($themes as $theme)
          <tr>
            <th scope="row">{{ $theme->id }}</th>
            <td>Debutify {{ $theme->version }}</td>
            <td><a href="{{ $theme->url }}" target="_blank">{{ $theme->url }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </div>
</div>

<a href="{{ route('addtheme') }}" class="btn btn-danger btn-lg rounded-circle shadow-lg btn-add-product">
  <span class="fas fa-plus"></span>
</a>
@endsection
