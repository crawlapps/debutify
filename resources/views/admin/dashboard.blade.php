@extends('layouts.admin')

@section('styles')
<style>
.results tr[visible='false'],
.no-result{
  display:none;
}
.results tr[visible='true']{
  display:table-row;
}
.card{
  height: 100%;
}
.pagination-shop .pagination{
	justify-content: center;
}
</style>
@endsection

@section('content')
<div class="row">
  <div class="col">

    <div class="form-group">
        <form class="form-inline" action="{{url()->current()}}" >
    			<!--@csrf-->
          <div class="form-group flex-fill">
            <input type="text" class="user_search form-control form-control-lg w-100" name="q" id="search" placeholder="Search users.." value="{{ $keyword }}">
          </div>
    			<!-- <input type="Submit" value="Search" class="btn btn-primary btn-lg ml-2"> -->
    		</form>
    </div>

    <div class="all-users">
      <div class="table-responsive rounded">
        <table class="table table-bordered table-hover mb-2 results">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Status</th>
              <th scope="col">Date added</th>
              <th scope="col">Email</th>
              <th scope="col">Domain</th>
              <th scope="col">Plan</th>
              <th scope="col">Trial</th>
              <th scope="col">Last Activity</th>
              <!-- <th scope="col">Shopify referral</th> -->
           </tr>
            <tr class="warning no-result">
              <td colspan="7">No result</td>
            </tr>
          </thead>
          <tbody>
            @foreach ($shops as $shop)
            <tr>
              <th scope="col">{{ $shop->id }}</th>
              <td>
                @if ($shop->status == 'Active')
                <span class="badge badge-primary">{{$shop->status}}</span>
                @else
                <span class="badge badge-light">{{$shop->status}}</span>
                @endif
              </td>
              <td>{{ $shop->created_at }}</td>
              <td>{{ $shop->email }}</td>
              <td>{{ $shop->name }}<br>{{ $shop->custom_domain }}</td>
              <td>
              @if($shop->alladdons_plan == null || $shop->alladdons_plan == 'freemium')
                @if($shop->trial_days)
                <span class="badge badge-primary">Trial ({{$shop->count}})</span>
                @else
                <span class="badge badge-light">Freemium ({{$shop->count}})</span>
                @endif
              @endif
              @if($shop->alladdons_plan == 'basic')
                <span class="badge badge-info">Basic ({{$shop->count}})</span>
              @endif
              @if($shop->alladdons_plan == 'Starter')
                <span class="badge badge-success">Starter ({{$shop->count}})</span>
              @endif
               @if($shop->alladdons_plan == 'Hustler')
                <span class="badge badge-danger">Hustler ({{$shop->count}})</span>
              @endif
               @if($shop->alladdons_plan == 'Guru')
                <span class="badge badge-warning">Guru ({{$shop->count}})</span>
              @endif
              </td>
              <td>
                <form id="free_trialdays_form" method="POST" action="{{ route('addtrialdays') }}" enctype="multipart/form-data">
                  @csrf
                  <input type='hidden' name='email' value="{{ $shop->email }}">
                  <input type='hidden' name='shopify_domain' value="{{ $shop->name }}">
                  @if($shop->alladdons_plan == null || $shop->alladdons_plan == 'freemium')
                  <input type='number' name='trial_days' value="{{ $shop->trial_days }}" class="form-control mb-2" min="0" max="60">
                  <button class="btn btn-secondary btn-sm btn-block btn-ladda" data-style="zoom-in" onclick="return addfreetrial();">Update trial</button>
                  @endif
                </form>
              </td>
              <td>{{ $shop->lastactivity }}</td>
              {{--<td>
                <form id="free_Addons_form" method="POST" action="{{ route('freeaddon') }}" enctype="multipart/form-data">
                @csrf
                <input type='hidden' name='email' value="{{ $shop->email }}">
                <input type='hidden' name='shopify_domain' value="{{ $shop->name }}">
                <input type='hidden' name='status' value="{{ $shop->referral }}">
                @if($shop->referral)
                <span class="d-none">Referral</span>
                <button class="btn btn-secondary btn-sm btn-block btn-ladda" data-style="zoom-in" onclick="return changefreeaddons();"><span class="fas fa-times iconshow"></span></button>
                @else
                <button class="btn btn-light btn-sm btn-block freeaddon_change" onclick="return changefreeaddons();"><span class="fas fa-plus iconshow"></span><span class="spinner-border text-dark spinner" style="display: none;"></span></button>
                @endif
                </form>
              </td>--}}
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="pagination-shop text-center">{{ $shop_pagination->links() }}</div>
    </div>
  </div>
</div>
@endsection

@section('scripts')

<script>
  $(document).ready(function() {

   //  $(".search").keyup(function () {
   //    var searchTerm = $(".search").val();
   //    var listItem = $('.results tbody').children('tr');
   //    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

   //    $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
   //          return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
   //      }
   //    });

   //    $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
   //      $(this).attr('visible','false');
   //    });

   //    $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
   //      $(this).attr('visible','true');
   //    });

   //    var jobCount = $('.results tbody tr[visible="true"]').length;
   //    $('.counter').text(jobCount + ' item');

   //    if(jobCount == '0') {$('.no-result').show();}
   //    else {$('.no-result').hide();}
   // });

    src = "{{ route('users_search') }}";
    $("#search").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    query : request.term
                },
                success: function(result) {
                  if(result.status == 'success'){
                    var html = result.html;
                    $('.all-users').html(html);
                  }
                }
            });
        },
        minLength: 0,
    });
  });

  function changefreeaddons(){
    var form = document.getElementById('free_Addons_form');
    form.submit();
  }
  function addfreetrial(){
    var form = document.getElementById('free_trialdays_form');
    form.submit();
  }
</script>
@endsection
