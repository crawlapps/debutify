@extends('layouts.app')

<style type="text/css">
  a.theme_dashboard button.btn.btn-primary {
      margin-bottom: 10px;
  }
</style>

@section('content')
<div class="row">
  <div class="col">

    <div class="card">
      <h4 class="card-header">Upload new theme version</h4>
      <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('new_theme') }}" enctype="multipart/form-data">
             {{ csrf_field() }}
          <!-- <div class="form-group">
            <label for="formGroupExampleInput">Theme Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="">
          </div> -->
          <div class="form-group">
            <label for="version">Theme Version</label>
            <input type="text" class="form-control" id="version" name="version" placeholder="">
          </div>
          <div class="form-group">
            <label for="theme_file">Upload File</label>
            <input type="file" class="form-control-file" id="theme_file" name="theme_file" accept="application/zip">
          </div>
          <button type="submit" class="btn btn-primary btn-lg">Submit theme</button>
        </form>
      </div>
    </div>

  </div>
</div>
@endsection
