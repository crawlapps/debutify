@extends('layouts.debutify')
@section('title','onboarding')
@section('view-onboarding','view-onboarding')

@section('styles')
<style>
.reviewBanner,.page-header{
  display: none!important;
}
</style>
@endsection

@section('content')
    <div id="dashboard">
      <form id="download_theme_form" method="POST" action="{{ route('download_theme_post') }}" enctype="multipart/form-data">
        @csrf
        <div class="Polaris-EmptyState">
          <div class="Polaris-EmptyState__Section">
            <div class="Polaris-EmptyState__DetailsContainer">
              <div class="Polaris-EmptyState__Details">
                <div class="Polaris-TextContainer">
                  <p class="Polaris-DisplayText Polaris-DisplayText--sizeExtraLarge">
                    Debutify theme
                    <span class="Polaris-Badge Polaris-Badge--statusSuccess">{{ $version }}</span>
                  </p>
                  <div class="Polaris-EmptyState__Content">
                    <p>The World's #1 Free Shopify theme.</p>
                  </div>
                </div>
                <div class="Polaris-EmptyState__Actions">
                  <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
                    <div class="Polaris-Stack__Item">
                      <button type="submit" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeLarge download_theme btn-loading">
                        <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Add to theme library</span></span>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="Polaris-EmptyState__FooterContent">
                  <div class="Polaris-TextContainer">
                    <p>It will not affect your live store until you publish it.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="Polaris-EmptyState__ImageContainer"><img src="/svg/empty-state-theme.svg" role="presentation" alt="" class="Polaris-EmptyState__Image"></div>
          </div>
        </div>
      </form>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
    // ESDK page and bar title
    var istest = '{{ ShopifyApp::shop()->isGrandfathered() }}';

    ShopifyTitleBar.set({
        title: 'Onboarding',
    });

    </script>

    <script type="text/javascript">
        $(document).on('click','.download_theme', function(e){
                e.preventDefault();
                $.ajax({
                        url: '/app/theme',
                        type: 'POST',
                        data: $('#download_theme_form').serialize(),
                        beforeSend: function(){
                        },
                        success: function(result) {
                            console.log(result);
                            loading.dispatch(Loading.Action.STOP);

                            if(result.status == 'ok'){
                                toastNotice = Toast.create(ShopifyApp, {
                                    message: result.message,
                                    duration: 3000,
                                });
                                toastNotice.dispatch(Toast.Action.SHOW);
                                localStorage.setItem('themeBannerView','yes');
                                window.location.href= "/app/add_ons";
                            }
                            else if(result.status == 'error'){
                                toastNotice = Toast.create(ShopifyApp, {
                                    message: result.message,
                                    duration: 3000,
                                    isError: true,
                                });
                                toastNotice.dispatch(Toast.Action.SHOW);
                                window.location.href= "/app";
                            }
                        }
                });
        });
    </script>


@endsection
