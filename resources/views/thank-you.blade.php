@extends('layouts.debutify')
@section('title','Thank you')
@section('thank-you','thank-you')

@section('styles')
<style>
#dashboard{
  min-height: 80vh;
}
.Polaris-Page-Header,.Polaris-FooterHelp{
  display:none;
}
</style>
@endsection

@section('content')
<div id="dashboard" class="d-flex justify-content-center align-items-center">

  <div class="Polaris-TextContainer text-center mx-auto">
    <img src="/svg/empty-state-6.svg" role="presentation" alt="" class="mx-auto img-fluid" width="450">
    <p class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
      Thank you for subscribing!
    </p>
    <p>Don't hesitate to <a href="{{env('APP_PATH')}}support">contact us</a> if you have any questions.</p>
    <a href="{{env('APP_PATH')}}add_ons" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeLarge">
      <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Install Add-ons</span></span>
    </a>
  </div>

</div>
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
      // init shopify title bar
      ShopifyTitleBar.set({
          title: 'Thank you',
      });
    </script>
@endsection
