@extends('layouts.debutify')
@section('title','Good Bye')
@section('good-bye','good-bye')

@section('styles')
<style>
#dashboard{
  min-height: 80vh;
}
.Polaris-Page-Header,.Polaris-FooterHelp{
  display:none;
}
</style>
@endsection

@section('content')
<div id="dashboard" class="d-flex justify-content-center align-items-center">

  <div class="Polaris-TextContainer text-center mx-auto">
    <img src="/svg/empty-state.svg" role="presentation" alt="" class="mx-auto img-fluid" width="300">
    <p class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
      Sorry to see you go!
    </p>
    <p>Don't hesitate to <a href="{{env('APP_PATH')}}support">contact us</a> if you have any questions.</p>
    <a href="{{env('APP_PATH')}}plans" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeLarge">
      <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">View plans</span></span>
    </a>
  </div>

</div>
@endsection

@section('scripts')
    @parent

    <script type="text/javascript">
      // init shopify title bar
      ShopifyTitleBar.set({
          title: 'Good Bye',
      });
    </script>
@endsection
