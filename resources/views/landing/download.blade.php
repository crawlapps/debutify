@extends('layouts.landing')

@section('title','Download')

@section('styles')
@endsection

@section('content')
	<section class="section bg-gradient">
		<div class="container">
			<div class="row justify-content-md-center text-center">
				<div class="col-md-10">
					<h1 class="display-4 mb-3">Download</h1>
					<p class="lead mb-0">Install or login into Debutify app.</p>
				</div>
			</div>
		</div>
	</section>

  <section class="section">
    <div class="container">
      <div class="row text-center justify-content-center">
				<div class="col-md-6">
					<div class="card">
						<div class="card-body">
							<form id="formShop2" class="form-horizontal" method="POST" action="{{ route('authenticate') }}">
								{{ csrf_field() }}
								<div class="form-group text-center">
									<h6 class="mb-0">Enter your shopify domain. <span class="fas fa-question-circle toggle-download-info text-muted"></span></label>
								</div>
								<div class="form-group">
									<input class="form-control form-control-lg" required type="text" name="shop" id="shop" placeholder="storename.myshopify.com" onkeyup="this.value = this.value.toLowerCase();">
								</div>
								<button type="submit" class="btn btn-primary btn-lg btn-block dbtfy-addtocart">
									<span class="fas fa-bolt dbtfy-addtocart" aria-hidden="true"></span>
									Free Download Now
								</button>
								<button style="display: none;" disabled="disabled" type="button" class="btn btn-primary btn-lg btn-block download-loading">
									<span class="fas fa-spin fa-spinner"></span>
								</button>
								<div class="small text-center mt-2">
									Don't have a Shopify Store yet?<br class="d-block d-sm-none">
									<a target="_blank" href="https://www.shopify.com/?ref=debutify&utm_campaign=website">
										Get Started Today!
									</a>
								</div>
							</form>

							<div class="p-3 mt-3 rounded download-info" style="display:none;">
								<p>
									<small>
										<span class="fa fa-question-circle"></span>
										<strong>Why do I need to enter my domain?</strong>
										<br>
										Your shopify domain is required to download Debutify app, where you will have access to our theme and add-ons.
									</small>
								</p>
								<p>
									<small>
										<span class="fa fa-question-circle"></span>
										<strong>What permission is your app asking for?</strong>
										<br>
										We only access the "manage store permission" to be able to edit theme files to install our theme and add-ons. We do not have access to any of your customer's data.
									</small>
								</p>
								<p>
									<small>
										<span class="fa fa-question-circle"></span>
										<strong>Why isn't Debutify in the Shopify App Store?</strong>
										<br>
										We couldn't get in the Shopify App Store because our functions are only compatible with Debutify theme.
									</small>
								</p>
								<hr>
								<small class="text-center">
									<strong>
										Have any questions?
										<a href="/faq">See our F.A.Q</a>
										or
										<a href="#" class="close-modal" onclick="$crisp.push(['do', 'chat:open'])">chat with us.</a>
									</strong>
								</small>
							</div>

						</div>
					</div>
				</div>
      </div>
    </div>
  </section>
@endsection

@section('scripts')
<script>
$(document).ready(function(){

	@if (env('APP_ENV') == 'production')
	//initiate download tracking
	if(sessionStorage.getItem("initiateDownload")){} else{
		window.dataLayer.push({'event': 'initiate_download'});
		sessionStorage.setItem('initiateDownload','yes');
	};
	@endif

	$('#formShop2').validate({
		rules: {
			shop: {
				required: true,
				nowhitespace: true,
				domain: true
			}
		},
		submitHandler: function(form) {
			$(".dbtfy-addtocart").hide();
			$(".download-loading").show();

			@if (env('APP_ENV') == 'production')
			//complete registration tracking
			window.dataLayer.push({'event': 'complete_registration'});
			@endif

			form.submit();
		}
	});
});
</script>
@endsection
