@extends('layouts.landing')

@section('title','About us')

@section('styles')
@endsection

@section('content')
	<section class="section bg-gradient">
		<div class="container">
			<div class="row justify-content-md-center text-center">
				<div class="col-md-10">
					<h1 class="display-4 mb-3">About us</h1>
					<p class="lead mb-0">Meet the expert team behind the making of Debutify.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="section">
		<div class="container">
	  		<div class="row justify-content-center">
				<div class="col-md-4 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-raphael-bergeron.jpeg" alt="debutify-raphael-bergeron">
					<h4>Raphael Bergeron</h4>
					<p class="text-muted">— CEO & Founder</p>
				</div>
				<div class="col-md-4 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-ricky-hayes.jpg" alt="debutify-ricky-hayes">
					<h4>Ricky Hayes</h4>
					<p class="text-muted">— Co-founder & Head of marketing</p>
				</div>
			</div>
		</div>
	</section>

	<hr>

	<section class="section">
    	<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-abdul.jpg" alt="debutify-abdul">
					<h6>Abdul</h6>
					<p class="text-muted">— Senior Laravel Developer</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-abhay-purohit.jpg" alt="debutify-abhay-purohit">
					<h6>Abhay Purohit</h6>
					<p class="text-muted">— Email Marketing Automation</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-abhi-dabra.png" alt="debutify-abhi-dabra">
					<h6>Abhi Dabra</h6>
					<p class="text-muted">— Influencer Manager</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-abhilesh-kumar.jpg" alt="debutify-abhilesh-kumar">
					<h6>Abhilesh Kumar</h6>
					<p class="text-muted">— Google and Bings Ads Manager</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-abhishek-thakur.jpg" alt="debutify-abhishek-thakur">
					<h6>Abhishek Thakur</h6>
					<p class="text-muted">— Customer Support specialist</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-adil-abbas.jpg" alt="debutify-adil-abbas">
					<h6>Adil Abbas</h6>
					<p class="text-muted">— Technical Support specialist</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-andreja-nesic.png" alt="debutify-andreja-nesic">
					<h6>Andreja Nesic</h6>
					<p class="text-muted">— Marketing Lead</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-arabo.jpg" alt="debutify-arabo">
					<h6>Arabo</h6>
					<p class="text-muted">— DevOps Engineer</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-brendan-parker.jpg" alt="debutify-brendan-parker">
					<h6>Brendan Parker</h6>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-charles-hugo-coulombe.jpg" alt="debutify-charles-hugo-coulombe">
					<h6>Charles-Hugo Coulombe</h6>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-chavdar-angelov.jpg" alt="debutify-chavdar-angelov">
					<h6>Chavdar Angelov</h6>
					<p class="text-muted">— Bookkeeper/Budgeting</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-connor-curlewis.jpg" alt="debutify-connor-curlewis">
					<h6>Connor Curlewis</h6>
					<p class="text-muted">— Filmmaker</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-divesh-kumar.jpg" alt="debutify-divesh-kumar">
					<h6>Divesh Kumar</h6>
					<p class="text-muted">— SEO and Internet Marketing</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-divine-ignacio.jpg" alt="debutify-divine-ignacio">
					<h6>Divine Ignacio</h6>
					<p class="text-muted">— Video Editor</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-fred.jpg" alt="debutify-fred">
					<h6>Fred</h6>
					<p class="text-muted">— Web Developer</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-gaurav-tandon.png" alt="debutify-gaurav-tandon">
					<h6>Gaurav Tandon</h6>
					<p class="text-muted">— Facebook Ads</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-gerald-ainomugisha.jpg" alt="debutify-gerald-ainomugisha">
					<h6>Gerald Ainomugisha</h6>
					<p class="text-muted">— Content Writer</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-joseph-ianni.jpg" alt="debutify-joseph-ianni">
					<h6>Joseph Ianni</h6>
					<p class="text-muted">— Host of Economics, a Debutify Podcast</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-mangesh-dutta.png" alt="debutify-mangesh-dutta">
					<h6>Mangesh Dutta</h6>
					<p class="text-muted">— Podcast Producer</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-ashraful-alam.jpg" alt="debutify-ashraful-alam">
					<h6>Md. Ashraful Alam</h6>
					<p class="text-muted">— LinkedIn Manager</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-mirza.jpg" alt="debutify-mirza">
					<h6>Mirza</h6>
					<p class="text-muted">— Customer Technical Support</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-md-rakib-hasan-rony.jpg" alt="debutify-md-rakib-hasan-rony">
					<h6>Md. Rakib Hasan Rony</h6>
					<p class="text-muted">— TikTok Marketing Expert</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-md-rezaul-islam.jpg" alt="debutify-md-rezaul-islam">
					<h6>Md. Rezaul Islam</h6>
					<p class="text-muted">— Product Researcher</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-muhammad-afzaal.jpg" alt="debutify-muhammad-afzaal">
					<h6>Muhammad Afzaal</h6>
					<p class="text-muted">— Project Manager</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-muhammad-owais.jpg" alt="debutify-muhammad-owais">
					<h6>Muhammad Owais</h6>
					<p class="text-muted">— Customer Technical Support</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-muhammad-talal.png" alt="debutify-muhammad-talal">
					<h6>Muhammad Talal</h6>
					<p class="text-muted">— Senior Lead Developer</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-onur.jpg" alt="debutify-onur">
					<h6>Onur</h6>
					<p class="text-muted">— Laravel Lead</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-peddy-jun-capegsan.jpg" alt="debutify-peddy-jun-capegsan">
					<h6>Peddy Jun Capegsan</h6>
					<p class="text-muted">— Technical Support specialist</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-prashanth-sreedharan.png" alt="debutify-prashanth-sreedharan">
					<h6>Prashanth Sreedharan</h6>
					<p class="text-muted">— Head of Design</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-ridhhish-desai.jpg" alt="debutify-ridhhish-desai">
					<h6>Ridhhish Desai</h6>
					<p class="text-muted">— YouTube Video Ads Expert</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-shadman-khan-bishal.jpg" alt="debutify-shadman-khan-bishal">
					<h6>Shadman Khan Bishal</h6>
					<p class="text-muted">— Pinterest Ads and Marketing</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-shahid-pavel.jpg" alt="debutify-shahid-pavel">
					<h6>Shahid Pavel</h6>
					<p class="text-muted">— Instagram, Pinterest and Twitter Manager</p>
				</div>
				<div class="col-md-3 mb-3">
					<img class="rounded img-fluid lazyload mb-3" data-src="/images/debutify-vaibhav.jpg" alt="debutify-vaibhav">
					<h6>Vaibhav Shah</h6>
					<p class="text-muted">— Senior Full Stack Developer</p>
				</div>
			</div>
			<div class="row">
				<div class="col-12 text-center">
					<div class="alert alert-primary text-center" role="alert">
						<strong>Want to join our growing team?</strong>
						<a href="/career">Explore carreer oportunities.</a>
					</div>
				</div>
      		</div>
    	</div>
	</section>


@endsection

@section('scripts')

@endsection
