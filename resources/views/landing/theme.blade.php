@extends('layouts.landing')

@section('title','Theme')

@section('styles')

@endsection

@section('content')
  <div class="section bg-gradient">
    <div class="container">
      <div class="row justify-content-md-center text-center">
        <div class="col-md-10">
          <h1 class="display-4 mb-3">Theme</h1>
          <p class="lead mb-0">Clean code, fast page loading speed and features that matters.</p>
        </div>
      </div>
		</div>
	</div>

	<!-- 1 -->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0">
          <img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-currency-converter.png" width="100%" alt="Currency converter">
        </div>
        <div class="col-12 col-md">
            <h2>Currency converter.</h2>
    		<ul class="list-unstyled lead">
    			<li><span class="fas fa-check-circle text-primary"></span> Automatic country detection</li>
    			<li><span class="fas fa-check-circle text-primary"></span> Multiple placement option</li>
    			<li><span class="fas fa-check-circle text-primary"></span> Multi-currency checkout with Shopify Payments</li>
    			<li><span class="fas fa-check-circle text-primary"></span> Loading icon while changing currency</li>
    		</ul>
    		<button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
				Free Download Now
			</button>
		</div>
      </div>
    </div>
  </section>

  <hr>

	<!-- 2 -->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0 order-md-2">
			<img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-product-sliders.png" width="100%" alt="Product sliders">
        </div>
        <div class="col-12 col-md">
            <h2>Product sliders.</h2>
    		<ul class="list-unstyled lead">
    			<li><span class="fas fa-check-circle text-primary"></span> Number of products to show</li>
    			<li><span class="fas fa-check-circle text-primary"></span> Mobile & desktop swipe</li>
    			<li><span class="fas fa-check-circle text-primary"></span> Autoplay slider and change speed</li>
    			<li><span class="fas fa-check-circle text-primary"></span> Arrow/dots visibility</li>
    		</ul>
            <button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
				Free Download Now
            </button>
		</div>
      </div>
    </div>
  </section>

  <hr>

	<!-- 3 -->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0">
			<img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-slideshow.png" width="100%" alt="Customizable slideshow">
        </div>
        <div class="col-12 col-md">
            <h2>Customizable slideshow.</h2>
			<ul class="list-unstyled lead">
				<li><span class="fas fa-check-circle text-primary"></span> Slideshow mobile/desktop height</li>
				<li><span class="fas fa-check-circle text-primary"></span> Fade-in animation</li>
				<li><span class="fas fa-check-circle text-primary"></span> Customizable slider options</li>
			</ul>
            <button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
				Free Download Now
			</button>
		</div>
      </div>
    </div>
  </section>

  <hr>

	<!-- 4 -->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0 order-md-2">
			<img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-header.png" width="100%" alt="Customizable header">
        </div>
        <div class="col-12 col-md">
			<h2>Customizable header.</h2>
			<ul class="list-unstyled lead">
				<li><span class="fas fa-check-circle text-primary"></span> Navigation left/right/center/hidden</li>
				<li><span class="fas fa-check-circle text-primary"></span> Transparent header</li>
				<li><span class="fas fa-check-circle text-primary"></span> Sticky header</li>
				<li><span class="fas fa-check-circle text-primary"></span> Transparent logo over slideshow</li>
			</ul>
            <button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
				Free Download Now
			</button>
		</div>
      </div>
    </div>
  </section>

  <hr>

	<!-- 5 -->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0">
			<img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-footer.png" width="100%" alt="Customizable footer">
        </div>
        <div class="col-12 col-md">
			<h2>Customizable footer.</h2>
			<ul class="list-unstyled lead">
				<li><span class="fas fa-check-circle text-primary"></span> Menus/text/image/social medias/newsletter</li>
				<li><span class="fas fa-check-circle text-primary"></span> Custom column order</li>
				<li><span class="fas fa-check-circle text-primary"></span> Payment icons</li>
				<li><span class="fas fa-check-circle text-primary"></span> Email & phone links</li>
			</ul>
            <button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
					Free Download Now
			</button>
		</div>
      </div>
    </div>
  </section>

  <hr>

	<!-- 6-->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0 order-md-2">
			<img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-guarantee-bar.png" width="100%" alt="Guarantee bar">
        </div>
        <div class="col-12 col-md">
			<h2>Guarantee bar.</h2>
			<ul class="list-unstyled lead">
				<li><span class="fas fa-check-circle text-primary"></span> Choose between 1000+ icons</li>
				<li><span class="fas fa-check-circle text-primary"></span> Add icons, title, text and links</li>
				<li><span class="fas fa-check-circle text-primary"></span> Change columns order</li>
			</ul>
            <button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
				Free Download Now
			</button>
		</div>
      </div>
    </div>
  </section>

  <hr>

	<!-- 7 -->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0">
          <img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-product-images.png" width="100%" alt="Product images">
        </div>
        <div class="col-12 col-md">
            <h2>Product images.</h2>
			<ul class="list-unstyled lead">
				<li><span class="fas fa-check-circle text-primary"></span> Featured image slider</li>
				<li><span class="fas fa-check-circle text-primary"></span> Synced thumbnail slider</li>
				<li><span class="fas fa-check-circle text-primary"></span> Thumbnail/stacked layout</li>
				<li><span class="fas fa-check-circle text-primary"></span> Left/right alignement</li>
				<li><span class="fas fa-check-circle text-primary"></span> Zoom on hover</li>
			</ul>
            <button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
				Free Download Now
			</button>
		</div>
      </div>
    </div>
  </section>

  <hr>

	<!-- 8-->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0 order-md-2">
			<img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-product-details.png" width="100%" alt="Product details">
        </div>
        <div class="col-12 col-md">
			<h2>Product details.</h2>
			<ul class="list-unstyled lead">
				<li><span class="fas fa-check-circle text-primary"></span> Sticky when scrolling</li>
				<li><span class="fas fa-check-circle text-primary"></span> Dropdown/button variants</li>
				<li><span class="fas fa-check-circle text-primary"></span> Full-width add-to-cart button</li>
				<li><span class="fas fa-check-circle text-primary"></span> Add-to-cart button icon</li>
				<li><span class="fas fa-check-circle text-primary"></span> Dynamic checkout button</li>
				<li><span class="fas fa-check-circle text-primary"></span> Product tags list</li>
				<li><span class="fas fa-check-circle text-primary"></span> Left/center alignement</li>
			</ul>
            <button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
				Free Download Now
			</button>
		</div>
      </div>
    </div>
  </section>

  <hr>

	<!-- 9 -->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0">
			<img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-related-products.png" width="100%" alt="Related products">
        </div>
        <div class="col-12 col-md">
			<h2>Related products.</h2>
			<ul class="list-unstyled lead">
				<li><span class="fas fa-check-circle text-primary"></span> Grid/slider sliders</li>
				<li><span class="fas fa-check-circle text-primary"></span> Advanced product filters</li>
				<li><span class="fas fa-check-circle text-primary"></span> Dynamic recommendations</li>
				<li><span class="fas fa-check-circle text-primary"></span> Customizable slider options</li>
			</ul>
            <button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
				Free Download Now
			</button>
		</div>
      </div>
    </div>
  </section>

  <hr>

	<!-- 10-->
	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md mb-3 mb-md-0 order-md-2">
			<img class="img-fluid rounded shadow lazyload" data-src="/images/debutify-testimonials.png" width="100%" alt="Product testimonials">
        </div>
        <div class="col-12 col-md">
            <h2>Product testimonials.</h2>
			<ul class="list-unstyled lead">
				<li><span class="fas fa-check-circle text-primary"></span> Testimonial slider under product page</li>
				<li><span class="fas fa-check-circle text-primary"></span> Quote icon or images</li>
				<li><span class="fas fa-check-circle text-primary"></span> Customizable slider options</li>
			</ul>
            <button href="#" class="btn btn-outline-primary toggleModal">
                <span class="fas fa-download toggleModal"></span>
				Free Download Now
			</button>
		</div>
      </div>
    </div>
  </section>
@endsection

@section('scripts')

@endsection
