<div class="row accordion-section">
  <div class="col">

    <div class="accordion" id="faq">
      <div class="faq mb-3">
        <a class="btn btn-light btn-block btn-lg collapsed" type="button" data-toggle="collapse" data-target="#a1" aria-expanded="true" aria-controls="a1">
          <span class="fas fa-question-circle text-primary"></span>
          Why Is Debutify Free?
        </a>
        <div id="a1" class="collapse" aria-labelledby="q1" data-parent="#faq">
          <div class="card-body">
            Because it was about time someone created a more than "decent" free Shopify theme! There are more than 1 000 000+ Shopify stores, and only 10 basic free themes offered by Shopify, that's nonsense. Debutify theme will forever be free and awesome!
          </div>
        </div>
      </div>

      <div class="faq mb-3">
        <a class="btn btn-light btn-block btn-lg collapsed" type="button" data-toggle="collapse" data-target="#a2" aria-expanded="false" aria-controls="a2">
          <span class="fas fa-question-circle text-primary"></span>
          What Is Debutify App?
        </a>
        <div id="a2" class="collapse" aria-labelledby="q2" data-parent="#faq">
          <div class="card-body">
            Debutify is much more than a traditionnal shopify theme. On our app, you will be able to automatically download our free theme in one-click and have access to powerful integrated features.
          </div>
        </div>
      </div>

      <div class="faq mb-3">
        <a class="btn btn-light btn-block btn-lg collapsed" type="button" data-toggle="collapse" data-target="#a3" aria-expanded="false" aria-controls="a3">
          <span class="fas fa-question-circle text-primary"></span>
          What Permission Is Debutify App Accessing?
        </a>
        <div id="a3" class="collapse" aria-labelledby="q3" data-parent="#faq">
          <div class="card-body">
            We only access the "manage store permission" to be able to download Debutify theme and add-ons to your store. We do not have access to any of your customer's data.
          </div>
        </div>
      </div>

      <div class="faq mb-3">
        <a class="btn btn-light btn-block btn-lg collapsed" type="button" data-toggle="collapse" data-target="#a4" aria-expanded="false" aria-controls="a4">
          <span class="fas fa-question-circle text-primary"></span>
          What Are Debutify Add-Ons?
        </a>
        <div id="a4" class="collapse" aria-labelledby="q4" data-parent="#faq">
          <div class="card-body">
            Add-on are simple yet powerful features that can be added to your store in Debutify App. They are completely integrated into your Debutify theme: Doesn't affect page load speed (like a typical Shopify App) and automatically matches your theme style and settings.
          </div>
        </div>
      </div>

      <div class="faq mb-3">
        <a class="btn btn-light btn-block btn-lg collapsed" type="button" data-toggle="collapse" data-target="#a5" aria-expanded="false" aria-controls="a5">
          <span class="fas fa-question-circle text-primary"></span>
          Can I swap add-ons?
        </a>
        <div id="a5" class="collapse" aria-labelledby="q5" data-parent="#faq">
          <div class="card-body">
            Yes! If you don't like an add-on, you can uninstall it and pick another one.
          </div>
        </div>
      </div>

      <div class="faq mb-3">
        <a class="btn btn-light btn-block btn-lg collapsed" type="button" data-toggle="collapse" data-target="#a6" aria-expanded="false" aria-controls="a6">
          <span class="fas fa-question-circle text-primary"></span>
          Will add-ons slow down my website?
        </a>
        <div id="a6" class="collapse" aria-labelledby="q6" data-parent="#faq">
          <div class="card-body">
            No! Unlike Shopify apps, our add-ons have almost no impact on page load speed.
          </div>
        </div>
      </div>

      <div class="faq mb-3">
        <a class="btn btn-light btn-block btn-lg collapsed" type="button" data-toggle="collapse" data-target="#a7" aria-expanded="false" aria-controls="a6">
          <span class="fas fa-question-circle text-primary"></span>
          Can I change plan later?
        </a>
        <div id="a7" class="collapse" aria-labelledby="q7" data-parent="#faq">
          <div class="card-body">
            Yes! You can upgrade or downgrade plan at any time in Debutify App.
          </div>
        </div>
      </div>

      <div class="faq mb-3">
        <a class="btn btn-light btn-block btn-lg collapsed" type="button" data-toggle="collapse" data-target="#a8" aria-expanded="false" aria-controls="a6">
          <span class="fas fa-question-circle text-primary"></span>
          Is there regular updates?
        </a>
        <div id="a8" class="collapse" aria-labelledby="q8" data-parent="#faq">
          <div class="card-body">
            Yes! We areconstently improving and updating our theme and add-ons based on your feedback!
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
