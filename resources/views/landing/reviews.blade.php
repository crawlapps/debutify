@extends('layouts.landing')

@section('title','Reviews')

@section('styles')
@endsection

@section('content')
	<section class="section bg-gradient">
		<div class="container">
			<div class="row justify-content-md-center text-center">
				<div class="col-md-10">
					<h1 class="display-4 mb-3">Reviews</h1>
					<p class="lead mb-3">Here's what our customers are saying about Debutify.</p>
					<div class="user-ratings mb-0">
						@include ("components.star-rating-badges")
					</div>
				</div>
			</div>
		</div>
	</section>

  <section class="section">
    <div class="container">

      <ul class="nav nav-pills mb-5 justify-content-center" id="pills-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="pills-testimonials-tab" data-toggle="pill" href="#pills-testimonials" role="tab" aria-controls="pills-testimonials" aria-selected="false">Testimonials</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="pills-youtubers-tab" data-toggle="pill" href="#pills-youtubers" role="tab" aria-controls="pills-youtubers" aria-selected="false">Videos</a>
        </li>
      </ul>

      <div class="tab-content" id="pills-tabContent">
        <!-- testimonials -->
        <div class="tab-pane fade show active" id="pills-testimonials" role="tabpanel" aria-labelledby="pills-testimonials-tab">
          <div data-token="lgVWQJqytBlPmrJ4AsVQ0RUokYVbtZG9srIvU0ttVMDhyPRHQp" class="romw-reviews"></div>
        </div>

        <!-- youtubers -->
        <div class="tab-pane fade" id="pills-youtubers" role="tabpanel" aria-labelledby="pills-youtubers-tab">
			<div class="row">
              @foreach($testimonials as $testimonial)
              <div class="col-md-6 mb-3">
                <div class="video-wrapper">
                  <iframe class="rounded lazyload" width="100%" height="305" data-src="https://www.youtube.com/embed/{{$testimonial}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
              @endforeach
            </div>
          <div class="row">
            @foreach($youtubers as $youtuber)
            <div class="col-md-6 mb-3">
              <div class="video-wrapper">
                <iframe class="rounded lazyload" width="100%" height="305" data-src="https://www.youtube.com/embed/{{$youtuber}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            @endforeach
          </div>
        </div>

      </div>

    </div>
  </section>
@endsection

@section('scripts')

@endsection
