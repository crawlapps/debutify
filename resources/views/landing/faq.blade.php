@extends('layouts.landing')

@section('title','FAQ')

@section('styles')
@endsection

@section('content')
<section class="section bg-gradient">
  <div class="container">
    <div class="row justify-content-md-center text-center">
      <div class="col-md-10">
        <h1 class="display-4 mb-3">Frequently asked questions</h1>
        <p class="lead mb-0">We know you have some questions in mind, we've tried to list the most important ones!</p>
      </div>
    </div>
  </div>
</section>

<section class="section">
  <div class="container">
    @include('landing.faq-module')
  </div>
</section>
@endsection

@section('scripts')
@endsection
