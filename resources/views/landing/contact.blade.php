@extends('layouts.landing')

@section('title','Contact')

@section('styles')
@endsection

@section('content')
	<section class="section bg-gradient">
		<div class="container">
			<div class="row justify-content-md-center text-center">
				<div class="col-md-10">
					<h1 class="display-4 mb-3">Contact</h1>
					<p class="lead mb-0">Get in touch with our team, we're here to help.</p>
				</div>
			</div>
		</div>
	</section>

  <section class="section">
    <div class="container">
      <div class="row justify-content-md-center">
				<div class="col-md-6">
					@if(session('success'))
			    	<div class="alert alert-success text-center" role="alert">
					  	<strong>Thank you for contacting us!</strong> We will answer under 48h.
						</div>
						@if (env('APP_TRACKING'))
						<script type="text/javascript">
							$(document).ready(function() {
								window.dataLayer.push({'event': 'contact'});
							});
						</script>
						@endif
					@endif

					<form action="/contacted" method="post" accept-charset="utf-8" id="contact_form">
						@csrf
						<div class="form-group">
					    <label for="nameContact">Name</label>
							<input id="nameContact" class="form-control" type="text" name="nameContact" aria-invalid="false"  value="" required autofocus="autofocus">
					  </div>
					  <div class="form-group">
					    <label for="emailContact">Email address</label>
							<input id="emailContact" class="form-control" type="email" name="emailContact" aria-invalid="false"  value="" required>
					  </div>
						<div class="form-group">
					    <label for="phoneContact">Phone number</label>
							<input id="phoneContact" class="form-control" type="tel" name="phoneContact" aria-invalid="false"  value="">
					  </div>
						<div class="form-group">
					    <label for="messageContact">Message</label>
							<textarea id="messageContact" class="form-control" rows="3" name="messageContact" aria-invalid="false"  aria-multiline="true" required style="height:90px;"></textarea>
					  </div>
						<div>
							<div class="g-recaptcha" data-sitekey="6LeaksQUAAAAAAbf1usuBgTiODfg1Ea6SBCl_uL2"></div>
							<input id="btnSubmit" type="submit" value="Submit" class="btn btn-primary btn-lg mt-3">
						</div>
					</form>

			</div>
      </div>
    </div>
  </section>
@endsection

@section('scripts')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>
$("#btnSubmit").click(function(event) {
	if (grecaptcha.getResponse() == ""){
		event.preventDefault();
    alert("Please check the reCAPTCHA box");
	}
});

function submitcontactform(e){
	var form = document.getElementById('contact_form');
  form.submit();
}
</script>
@endsection
