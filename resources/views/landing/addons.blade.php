@extends('layouts.landing')

@section('title','Add-ons')

@section('styles')
<style>
.video-wrapper{
  margin-top: auto;
}
</style>
@endsection

@section('content')
  <section class="section bg-gradient">
    <div class="container">
      <div class="row justify-content-md-center text-center">
        <div class="col-md-10">
          <h1 class="display-4 mb-3">@yield('title')</h1>
          <p class="lead mb-0">Choose from {{$nbAddons}}+ Premium features. One-Click plug & play setup, no coding required.</p>
        </div>
      </div>
		</div>
	</section>

	<section class="section section-addon">
		<div class="container">
			<div class="row text-center">
				@foreach($global_add_ons as $addon)
					<div class="col-12 col-md-6 mb-5 d-flex flex-column">
						<h6><span class="fas fa-video text-primary"></span> {{ $addon->title }}</h6>
						<p class="text-muted">{{ $addon->subtitle }}.</p>
                        <div class="img-wrapper shadow" data-toggle="modal" data-target="#videoModal" data-title="{{ $addon->title }}" data-subtitle="{{ $addon->subtitle }}">
                          <span class="fab fa-youtube fa-4x text-primary icon-hover"></span>
                          @include("components.image-addons")
                        </div>
					</div>
				@endforeach
				<div class="col-12 col-md mb-0 mb-md-3">
					<h4>And more!</h4>
					<p class="text-muted">New add-ons are popping every week in Debutify. Stay tunned!</p>
                    <button class="btn btn-primary btn-lg toggleModal align-self-center">
						<span class="fas fa-bolt toggleModal" aria-hidden="true"></span>
                        Free Download Now
                    </button>
				</div>
			</div>
		</div>
	</section>

  <div class="modal fade" tabindex="-1" role="dialog" id="videoModal">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="addon-title mb-0"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p class="addon-subtitle"></p>
          <div class="video-wrapper video-tutorial">
            <iframe class="rounded tutorial" width="100%" height="295" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary toggleModal" data-dismiss="modal">
            <span class="fas fa-bolt toggleModal" aria-hidden="true"></span>
            Free Download Now
          </button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
@endsection
