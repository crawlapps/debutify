@extends('layouts.landing')

@section('title','Career')

@section('styles')
@endsection

@section('content')
	<section class="section bg-gradient">
		<div class="container">
			<div class="row justify-content-md-center text-center">
				<div class="col-md-10">
					<h1 class="display-4 mb-3">Career</h1>
					<p class="lead mb-0">Join the team behind the World's #1 Free Shopify Theme.</p>
				</div>
			</div>
		</div>
	</section>

  <section class="section">
    <div class="container">
      <div class="row text-center">
				<div class="col">
					<h3>Current position available</h3>
					<p>Shopify front-end developer</p>
					<p>Technical support specialist</p>
					<p>Email marketing specialist</p>
					<p>Social medias manager</p>
					<a class="btn btn-primary btn-lg" href="/contact">Submit application</a>
				</div>
      </div>
    </div>
  </section>
@endsection

@section('scripts')
<script>
function submitcontactform(e){
	console.log(e);
	var form = document.getElementById('contact_form');
      form.submit();
}
</script>
@endsection
