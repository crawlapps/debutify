@extends('layouts.landing')

@section('title','Pricing')

@section('styles')

@endsection

@section('content')
<section class="section bg-gradient">
  <div class="container">
    <div class="row justify-content-md-center text-center">
      <div class="col-md-10">
        <h1 class="display-4 mb-3">Pricing</h1>
        <p class="lead mb-0">14-Day Free Trial. No credit card required. No lock-in-contracts.</p>
      </div>
    </div>
	</div>
</section>

<section class="section">
	<div class="container">
    @include('landing.pricing-module')
  </div>
</section>

<hr />

<section class="section">
  <div class="container">
      <div class="row mb-3">
          <div class="col-lg-10 offset-lg-1 text-center">
              <h2>Frequently Asked Questions</h2>
              <p class="lead">We know you have some questions in mind, we've tried to list the most important ones!</p>
          </div>
      </div>
      @include('landing.faq-module')
  </div>
</section>

@endsection

@section('scripts')
@endsection
