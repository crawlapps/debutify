<div class="row text-center price-plan-section">
    <div class="col">
        <div class="card-deck text-center">
            <div class="card mb-3">
                <div class="card-img-top">
                    <img data-src="images/new/free-plan.png" alt="" class="img-fluid pt-4 pb-4 lazyload" />
                    <div class="plan-price">
                        <h4><strong>$0</strong></h4>
                        <h6>Free</h6>
                        <p>1 Store License</p>
                    </div>
                </div>
                <div class="card-body">
                    <p>Debutify Theme</p>
                    <p>Facebook group (Support)</p>
                </div>
                <div class="card-footer">
                    <button class="btn btn-warning" data-toggle="modal" data-target="#downloadModal">Download Now</button>
                </div>
            </div>
            <div class="card mb-3">
                <div class="card-img-top">
                    <img data-src="images/new/starter-plan.png" alt="" class="img-fluid pt-4 pb-4 lazyload" />
                    <div class="plan-price">
                        <h4><strong>$19</strong>/Month</h4>
                        <h6>Starter</h6>
                        <p>1 Store License</p>
                    </div>
                </div>
                <div class="card-body">
                    <p>Debutify Theme</p>
                    <p>Facebook, email, live chat <strong>(Full Support)</strong></p>
                    <p>Any 3 Add-ons</p>
                    <p>Integrations</p>
                </div>
                <div class="card-footer">
                    <button class="btn btn-warning" data-toggle="modal" data-target="#downloadModal">Start Free Trial</button>
                </div>
            </div>
            <div class="w-100 d-sm-block d-lg-none"></div>
            <div class="card mb-3 hustler-plan">
                <div class="card-img-top">
                    <img data-src="images/new/hustler-plan.png" alt="" class="img-fluid pt-4 pb-4 lazyload" />
                    <div class="plan-price">
                        <h4><strong>$47</strong>/Month</h4>
                        <h6>Hustler</h6>
                        <p>1 Store License</p>
                    </div>
                </div>
                <div class="card-body">
                    <p>Debutify Theme</p>
                    <p>Facebook, email, live chat <strong>(Full Support)</strong></p>
                    <p>All 28 add-ons and future add-ons</p>
                    <p>Integrations</p>
                </div>
                <div class="card-footer">
                    <button class="btn btn-warning animated pulse infinite" data-toggle="modal" data-target="#downloadModal">Start Free Trial</button>
                </div>
            </div>
            <div class="card mb-3">
                <div class="card-img-top">
                    <img data-src="images/new/guru-plan.png" alt="" class="img-fluid pt-4 pb-4 lazyload" />
                    <div class="plan-price">
                        <h4><strong>$97</strong>/Month</h4>
                        <h6>Guru</h6>
                        <p>3 Store Licenses</p>
                    </div>
                </div>
                <div class="card-body">
                    <p>Debutify Theme</p>
                    <p>Facebook, email, live chat <strong>(Priority Full Support)</strong></p>
                    <p>All 28 add-ons and future add-ons</p>
                    <p>Integrations</p>
                    <p>Mentoring</p>
                    <p>Product research tool</p>
                    <p>Advanced Courses</p>
                </div>
                <div class="card-footer">
                    <button class="btn btn-warning" data-toggle="modal" data-target="#downloadModal">Start Free Trial</button>
                </div>
            </div>
        </div>
    </div>
</div>
