@extends('layouts.landing')

@section('title','Affiliate')

@section('styles')
@endsection

@section('content')
	<section class="section bg-gradient">
		<div class="container">
			<div class="row justify-content-md-center text-center">
				<div class="col-md-10">
					<h1 class="display-4 mb-3">Become a Debutify partner</h1>
					<p class="lead mb-0">Earn lifetime monthly recurring revenue promoting the best Shopify theme on the market.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="section">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-sm mb-3 mb-sm-0">
          <img class="px-3" src="/images/debutify-pitch-2.png" width="100%" alt="mobile optimized">
        </div>
        <div class="col-12 col-sm">
          <h2>30% recurring comission.</h2>
					<p>Create your affiliate link in seconds and start sharing it!</p>
					<ul class="list-unstyled lead">
						<li><span class="fas fa-check-circle text-primary"></span> 30% Lifetime Monthly Comission + Bonuses</li>
						<li><span class="fas fa-check-circle text-primary"></span> Promote A Tool With a 20% Landing Page Conversion Rate</li>
						<li><span class="fas fa-check-circle text-primary"></span> Get Paid To Give Value To People</li>
					</ul>
					<a href="https://app.linkmink.com/a/debutify/181" target="_blank" class="btn btn-primary btn-lg">Join now</a>
				</div>
      </div>
    </div>
  </section>
@endsection

@section('scripts')
<script>
$(".fb-post-container").hide();

function iframeLoaded(){

  function iframeLoop(){
    if( $('.fb-post-last').is('[fb-xfbml-state="rendered"]') ){

      //resize iframe
      $(".fb_iframe_widget_fluid span, iframe").css({
        'max-width' : '100%',
      });

      //show fb-reviews
      $(".fb-loading-icon").hide();
      $(".fb-post-container").show().addClass('animated fadeIn');

      //clear interval
      clearInterval(interval);
    }
  }

  var interval = setInterval(function(){
    iframeLoop();
  }, 500);
}

$(document).ready(function() {
  iframeLoaded();
});

</script>
@endsection
