@extends('layouts.debutify')
@section('title','Technical support')
@section('view-integrations','view-technical-support')

@section('styles')
<style>
.banner-trial{
  display: none;
}
</style>
@endsection

@if($all_addons != 1)
  @section('bannerTitle','available only on Paid plans')
  @section('bannerLink','upgrade to '. $starter .', '. $hustler . ' or '. $guru .' plans')
@endif

@section('content')
<div id="dashboard">
  <div class="Polaris-EmptyState Polaris-EmptyState--withinPage">
    <div class="Polaris-EmptyState__Section">
      <div class="Polaris-EmptyState__DetailsContainer">
        <div class="Polaris-EmptyState__Details">
          <div class="Polaris-TextContainer">
            <p class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
              Get technical help
            </p>
            <div class="Polaris-EmptyState__Content">
              <p>Live chat with our technical support team available to help you 7 days a week.</p>
            </div>
          </div>
          <div class="Polaris-EmptyState__Actions">
            <div class="Polaris-Stack Polaris-Stack--alignmentCenter">
              <div class="Polaris-Stack__Item">
                @if($all_addons == 1)
                <button type="button" class="btn-technical Polaris-Button Polaris-Button--primary Polaris-Button--sizeLarge">
                  <span class="Polaris-Button__Content">
                    <span class="Polaris-Button__Text">Start live chat</span>
                  </span>
                </button>
                @else
                <a href="{{env('APP_PATH')}}plans" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeLarge">
                  <span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Unlock technical support</span></span>
                </a>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="Polaris-EmptyState__ImageContainer"><img src="/svg/empty-state-1.svg" role="presentation" alt="" class="Polaris-EmptyState__Image"></div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
  @parent
  <script type="text/javascript">
      // ESDK page and bar title
      ShopifyTitleBar.set({
          title: 'Integrations',
      });

      $(document).ready(function(){
        $(".btn-technical").on("click", function(){
          $crisp.push(['do', 'chat:open']);
        });
      });
  </script>
@endsection
