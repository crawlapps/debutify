<?php

// remove dbtfy scripts when add-ons become == 0
if (! function_exists('no_addon_activate')) {
    function no_addon_activate($StoreThemes, $shop) {
        foreach ($StoreThemes as $theme) {
          $url = env('APP_URL');
          $JS_url = $url.'/js/dbtfy.js';

            // remove dbtfy-adons.js.liquid in theme.liquid
            try{
                $theme_file1 = $shop->api()->request(
                    'GET',
                    '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                    ['asset' => ['key' => 'layout/theme.liquid'] ]
                )['body']['asset']['value'];

                if( ( $pos1 = strrpos( $theme_file1 , "{{ 'dbtfy-addons.js' | asset_url }}" ) ) !== false ) {
                    $updated_theme1 = str_replace( '<script src="{{ \'dbtfy-addons.js\' | asset_url }}" defer="defer"></script> <!-- Header hook for plugins ================================================== -->' , '<!-- Header hook for plugins ================================================== -->' , $theme_file1 );

                    $updated_theme_file = $shop->api()->request(
                      'PUT',
                      '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                      ['asset' => ['key' => 'layout/theme.liquid', 'value' => $updated_theme1] ]
                    );
                }
            }
            catch(\GuzzleHttp\Exception\ClientException $e){
                logger('Remove debutify-addons js theme lquid file throws client exception');
            }
            catch(\Exception $e){
                logger('Remove debutify-addons js theme lquid file throws exception');
            }

            // remove dbtfy-addons.js.liquid asset
            try{
              $delete_debutify_addon = $shop->api()->request(
                  'DELETE',
                  '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                  ['asset' => ['key' => 'assets/dbtfy-addons.js.liquid'] ]
              );
            }
                catch(\GuzzleHttp\Exception\ClientException $e){
                logger('Delete debutify-addons js throws client exception');
            }
            catch(\Exception $e){
                logger('Delete debutify-addons js throws exception');
            }

            // remove dbtfy-js in theme.liquid
            try{
                $theme_file = $shop->api()->request(
                    'GET',
                    '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                    ['asset' => ['key' => 'layout/theme.liquid'] ]
                )['body']['asset']['value'];

                if( ( $pos = strrpos( $theme_file , '/js/dbtfy.js') ) !== false ) {
                    $updated_theme = str_replace( '<script src="'.$JS_url.'" async defer></script></head>' , '</head>' , $theme_file );

                    $updated_theme_file = $shop->api()->request(
                      'PUT',
                      '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                      ['asset' => ['key' => 'layout/theme.liquid', 'value' => $updated_theme] ]
                    );
                }
            }
            catch(\GuzzleHttp\Exception\ClientException $e){
                logger('add JS file throws client exception');
            }
            catch(\Exception $e){
                logger('add JS file throws exception');
            }

            // remove dbtfy.js in theme.js.liquid
            try{
                $themeJS_file = $shop->api()->request(
                      'GET',
                      '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                      ['asset' => ['key' => 'assets/theme.js.liquid'] ]
                )['body']['asset']['value'];

                $themejs = (string) 'function appendScript(filepath){ if($(\' head script[src="'.$JS_url.'"] \').length > 0){ return; } var ele = document.createElement("script");ele.setAttribute("defer", "defer");ele.setAttribute("src", filepath);$("head").append(ele);} appendScript("'.$JS_url.'");';
                if( ( $jsPos = strpos( $themeJS_file , $JS_url  ) ) !== false ) {
                    $updated_themeJS = str_replace( $themejs , ' ' , $themeJS_file );
                    $updated_themeJS_file = $shop->api()->request(
                        'PUT',
                        '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                        ['asset' => ['key' => 'assets/theme.js.liquid', 'value' => $updated_themeJS] ]
                    );
                }
            }
            catch(\GuzzleHttp\Exception\ClientException $e){
                  logger('add JS file throws client exception');
            }
            catch(\Exception $e){
                  logger('add JS file throws exception');
            }
        }
    }
}

// add dbtfy-addons.js.liquid
if (! function_exists('add_debutify_JS')) {
    function add_debutify_JS($StoreThemes, $shop) {
        $url = env('APP_URL');
        $JS_url = $url.'/js/dbtfy.js';

        foreach ($StoreThemes as $theme) {
            // add dbtfy-addons.js.liquid asset
            $check_addon = false;

            $dbtfy_addons = (string) "/* start-dbtfy-addons *//* start-register */$(document).ready(function(){ var sections = new theme.Sections(); });/* end-register */";
            try {
                /* Add all addon JS */
                $theme_js_content = $shop->api()->request(
                    'GET',
                    '/admin/themes/'.$theme->shopify_theme_id.'/assets.json',
                    ['asset' => ['key' => 'assets/dbtfy-addons.js.liquid'] ]
                )['body']['asset']['value'];

                if( ( $pos = strpos( $theme_js_content , "/* start-register */" ) ) === false ) {
                    try {
                        $create_trustbadge_snippet = $shop->api()->request(
                            'PUT',
                            '/admin/themes/'.$theme->shopify_theme_id.'/assets.json',
                            ['asset' => ['key' => 'assets/dbtfy-addons.js.liquid', 'value' => $dbtfy_addons] ]
                        );

                    }catch(\GuzzleHttp\Exception\ClientException $e){
                        logger('add all addon js throws client exception');
                    }
                    catch(\Exception $e){
                        logger('add all addon js throws exception');
                    }
                    $check_addon = true;
                }
            }
            catch(\GuzzleHttp\Exception\ClientException $e){
                logger('add JS file code throws client exception');
                try {
                    $create_trustbadge_snippet = $shop->api()->request(
                        'PUT',
                        '/admin/themes/'.$theme->shopify_theme_id.'/assets.json',
                        ['asset' => ['key' => 'assets/dbtfy-addons.js.liquid', 'value' => $dbtfy_addons] ]
                    );
                    $check_addon = true;
                }
                catch(\GuzzleHttp\Exception\ClientException $e){
                    logger('add all addon js throws client exception in catch');
                }
                catch(\Exception $e){
                    logger('add all addon js throws exception in catch');
                }
            }
            catch(\Exception $e){
                logger('add JS file code throws exception');
                try {
                    $create_trustbadge_snippet = $shop->api()->request(
                        'PUT',
                        '/admin/themes/'.$theme->shopify_theme_id.'/assets.json',
                        ['asset' => ['key' => 'assets/dbtfy-addons.js.liquid', 'value' => $dbtfy_addons] ]
                    );
                    $check_addon = true;
                }
                catch(\GuzzleHttp\Exception\ClientException $e){
                    logger('add all addon js throws client exception in catch');
                }
                catch(\Exception $e){
                    logger('add all addon js throws exception in catch');
                }
            }

            // add dbtfy-addons.js.liquid in theme.liquid
            $addon_js = "{{ 'dbtfy-addons.js' | asset_url }}";
            if($check_addon){
                try {
                    logger("dbtify-addon ".$addon_js);
                    $theme_file = $shop->api()->request(
                        'GET',
                        '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                        ['asset' => ['key' => 'layout/theme.liquid'] ]
                    )['body']['asset']['value'];
                    if( ( $jsPos = strpos( $theme_file , $addon_js ) ) === false ) {
                        if( ( $pos = strrpos( $theme_file , '<!-- Header hook for plugins ================================================== -->' ) ) !== false ) {
                            $updated_theme = str_replace( '<!-- Header hook for plugins ================================================== -->' , '<script src="'.$addon_js.'" defer="defer"></script> <!-- Header hook for plugins ================================================== -->' , $theme_file );
                            $updated_theme_file = $shop->api()->request(
                                  'PUT',
                                  '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                                  ['asset' => ['key' => 'layout/theme.liquid', 'value' => $updated_theme] ]
                              );
                        }
                    }
                }
                catch(\GuzzleHttp\Exception\ClientException $e){
                    logger('add debutify JS file theme.liquid throws client exception');
                    if( ( $jsPos = strpos( $theme_file , $addon_js ) ) === false ) {
                        if( ( $pos = strrpos( $theme_file , '</head>' ) ) !== false ) {
                            $updated_theme = str_replace( '</head>' , '<script src="'.$addon_js.'" defer="defer"></script></head>' , $theme_file );
                            $updated_theme_file = $shop->api()->request(
                                  'PUT',
                                  '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                                  ['asset' => ['key' => 'layout/theme.liquid', 'value' => $updated_theme] ]
                              );
                        }
                    }
                }
                    catch(\Exception $e){
                    logger('add JS file theme.liquid throws exception');
                }
            }
        }
    }
}

// add dbtfy.js script on theme.liquid & theme.js.liquid
if (! function_exists('addServerJS')) {
    function addServerJS($StoreThemes, $shop) {
        $url = env('APP_URL');
        $JS_url = $url.'/js/dbtfy.js';
        foreach ($StoreThemes as $theme) {

            // add dbtfy.js on theme.liquid
            try {
                $theme_file = $shop->api()->request(
                    'GET',
                    '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                    ['asset' => ['key' => 'layout/theme.liquid'] ]
                )['body']['asset']['value'];
                if( ( $jsPos = strpos( $theme_file , '/js/dbtfy.js') ) === false ) {
                    if( ( $pos = strrpos( $theme_file , '</head>' ) ) !== false ) {
                        $updated_theme = str_replace( '</head>' , '<script src="'.$JS_url.'" async defer></script></head>' , $theme_file );

                        $updated_theme_file = $shop->api()->request(
                            'PUT',
                            '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                            ['asset' => ['key' => 'layout/theme.liquid', 'value' => $updated_theme] ]
                        );
                    }
                }
            }
            catch(\GuzzleHttp\Exception\ClientException $e){
                logger('add JS file throws client exception');
                sleep(5);
                $theme_file = $shop->api()->request(
                    'GET',
                    '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                    ['asset' => ['key' => 'layout/theme.liquid'] ]
                )['body']['asset']['value'];

                if( ( $jsPos = strpos( $theme_file , '/js/dbtfy.js'  ) ) === false ) {
                    if( ( $pos = strrpos( $theme_file , '</head>' ) ) !== false ) {
                        $updated_theme = str_replace( '</head>' , '<script src="'.$JS_url .'" async defer></script></head>' , $theme_file );
                        $updated_theme_file = $shop->api()->request(
                            'PUT',
                            '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                            ['asset' => ['key' => 'layout/theme.liquid', 'value' => $updated_theme] ]
                        );
                    }
                }
            }
            catch(\Exception $e){
                logger('add JS file throws exception');
                sleep(5);
                $theme_file = $shop->api()->request(
                    'GET',
                    '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                    ['asset' => ['key' => 'layout/theme.liquid'] ]
                )['body']['asset']['value'];

                if( ( $jsPos = strpos( $theme_file , '/js/dbtfy.js' ) ) === false ) {
                    if( ( $pos = strrpos( $theme_file , '</head>' ) ) !== false ) {
                        $updated_theme = str_replace( '</head>' , '<script src="'.$JS_url .'" async defer></script></head>' , $theme_file );

                        $updated_theme_file = $shop->api()->request(
                            'PUT',
                            '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                            ['asset' => ['key' => 'layout/theme.liquid', 'value' => $updated_theme] ]
                        );
                    }
                }
            }

            // add dbtfy.js on theme.js
            try{
                $themeJS_file = $shop->api()->request(
                    'GET',
                    '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                    ['asset' => ['key' => 'assets/theme.js.liquid'] ]
                )['body']['asset']['value'];

                $themejs = (string) 'function appendScript(filepath){ if($(\' head script[src="'.$JS_url.'"] \').length > 0){ return; } var ele = document.createElement("script");ele.setAttribute("defer", "defer");ele.setAttribute("src", filepath);$("head").append(ele);} appendScript("'.$JS_url.'");';

                if( ( $jsPos = strpos( $themeJS_file , '/js/dbtfy.js'  ) ) === false ) {
                    $updated_themeJS = str_replace( $themeJS_file , $themeJS_file.$themejs , $themeJS_file );

                    $updated_themeJS_file = $shop->api()->request(
                        'PUT',
                        '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                        ['asset' => ['key' => 'assets/theme.js.liquid', 'value' => $updated_themeJS] ]
                    );
                }
            }
            catch(\GuzzleHttp\Exception\ClientException $e){
                logger('Update theme.js file throws client exception');
                sleep(10);
                $themeJS_file = $shop->api()->request(
                    'GET',
                    '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                    ['asset' => ['key' => 'assets/theme.js.liquid'] ]
                )['body']['asset']['value'];

                $themejs = (string) 'function appendScript(filepath){ if($(\' head script[src="'.$JS_url.'"] \').length > 0){ return; } var ele = document.createElement("script");ele.setAttribute("defer", "defer");ele.setAttribute("src", filepath);$("head").append(ele);} appendScript("'.$JS_url.'");';

                if( ( $jsPos = strpos( $themeJS_file , '/js/dbtfy.js'  ) ) === false ) {
                    $updated_themeJS = str_replace( $themeJS_file , $themeJS_file.$themejs , $themeJS_file );

                    $updated_themeJS_file = $shop->api()->request(
                        'PUT',
                        '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                        ['asset' => ['key' => 'assets/theme.js.liquid', 'value' => $updated_themeJS] ]
                    );
                }
            }
            catch(\Exception $e){
                logger('Update theme.js file throws exception');
                sleep(10);
                $themeJS_file = $shop->api()->request(
                    'GET',
                    '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                    ['asset' => ['key' => 'assets/theme.js.liquid'] ]
                )['body']['asset']['value'];

                $themejs = (string) 'function appendScript(filepath){ if($(\' head script[src="'.$JS_url.'"] \').length > 0){ return; } var ele = document.createElement("script");ele.setAttribute("defer", "defer");ele.setAttribute("src", filepath);$("head").append(ele);} appendScript("'.$JS_url.'");';

                if( ( $jsPos = strpos( $themeJS_file , '/js/dbtfy.js' ) ) === false ) {
                    $updated_themeJS = str_replace( $themeJS_file , $themeJS_file.$themejs , $themeJS_file );

                    $updated_themeJS_file = $shop->api()->request(
                        'PUT',
                        '/admin/api/themes/'.$theme->shopify_theme_id.'/assets.json',
                        ['asset' => ['key' => 'assets/theme.js.liquid', 'value' => $updated_themeJS] ]
                    );
                }
            }

        }
    }
}

?>
