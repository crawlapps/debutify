<?php

namespace App\Jobs;

use App\User;
use App\StoreThemes;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ThemesDeleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('Theme deleted '.$this->shopDomain.' Data='. json_encode($this->data->id));

        $theme_updated = StoreThemes::where('shopify_theme_id', $this->data->id)->delete();

        $shop = User::where('name',$this->shopDomain)->first();
        if($shop->shopify_theme_id == $this->data->id)
        {
            $shop->shopify_theme_id = null;
            $shop->theme_id = null;
           // $shop->theme_url = null;
            $shop->save();
        }


    }
}
