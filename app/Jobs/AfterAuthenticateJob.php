<?php

namespace App\Jobs;

use DateTime;
use App\User;
use App\Jobs\ActiveCampaignJob;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $shop;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $shop)
    {
        $this->shop = $shop;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      //  $shopData = $this->shop->api()->request(
      //       'GET',
      //       '/admin/api/shop.json',
      //       []
      //   )->body->shop;

      // $shop_owner = $shopData['shop_owner;
      // $shop_owner_name = explode(" ", $shop_owner);
      if($this->shop->trial_days > 0){
        $subscription = 'Trial';
      }else if($this->shop->alladdons_plan == null || $this->shop->alladdons_plan == '' || $this->shop->alladdons_plan == 'freemium'){
        $subscription = 'Freemium';
      }else{
        $subscription = $this->shop->alladdons_plan;
      }
      $this->addactive_campaign($this->shop,$subscription);
       // $MailChimp = new MailChimp(env('MAILCHIMP_APIKEY'));
       // $email = $MailChimp->subscriberHash($shopData['email);
        if ($this->shop->subscribed == 0) {
          //$subscription = 'Freemium';
          //$date = new DateTime();
          //$new_formats_trial = $date->format('Y-m-d');
         // $creation_date = $this->shop->created_at->format('Y-m-d');

        //   if($new_formats_trial <= $creation_date){
        //       if($this->shop->trial_ends_at == null){
        //         $subscription = 'Trial';
        //       }else{
        //         $subscription = 'Freemium';
        //       }
        //     }
            // $groupsApi = (new \MailerLiteApi\MailerLite(env('MailerLite_APIKEY')))->groups();

            //     $groupId = env('MailerLite_GROUP_ID');

            //     $subscriber = [
            //        'email' => $shopData['email,
            //        'fields' => [
            //            'name' => $shop_owner_name[0],
            //            'last_name' => $shop_owner_name[1],
            //            'company' => $shopData['domain,
            //            'city'=>$shopData['city,
            //            'country'=>$shopData['country_name,
            //            'phone'=>$shopData['phone,
            //            'state'=>$shopData['province,
            //            'zip'=>$shopData['zip,
            //            'status' => 'active',
            //            'subscription' => $subscription
            //        ]
            //     ];

            //     $addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);
            //         logger('after authenticate subscriber added with info='.json_encode($addedSubscriber));
            //     if($addedSubscriber){
               $this->shop->subscribed=1;
           // }
        }
        //
        if($this->shop->trial_check == null || $this->shop->trial_check == ''){
          $this->shop->trial_check = 0;
        }
        if($this->shop->theme_check == null || $this->shop->theme_check == ''){
          $this->shop->theme_check = 0;
        }
        $date = new DateTime();
        $new_formats_trial = $date->format('Y-m-d');
        $creation_date = $this->shop->created_at->format('Y-m-d');
        if($new_formats_trial <= $creation_date){
            if($this->shop->trial_ends_at == null){
              $license_key = Hash::make(Str::random(12));
              $this->shop->license_key = $license_key;
              $trial_end_days = date('Y-m-d', strtotime($new_formats_trial. ' + 14 days'));
              $this->shop->trial_ends_at = $trial_end_days;
              $this->shop->sub_trial_ends_at = 1;
              $this->shop->trial_days = 14;
              if($this->shop->trial_check != 1){
                  $this->shop->trial_check = 0;
              }
              $this->shop->save();
            // Active Campaign post data
              $this->addactive_campaign($this->shop,'Trial');

               // // logger($this->shop->email);
               //  $groupsApi = (new \MailerLiteApi\MailerLite(env('MailerLite_APIKEY')))->groups();
               //  // print($shopData['email);
               //  $groupId = env('MailerLite_GROUP_ID');
               //    $subscriber = [
               //      'email' => $this->shop->email,
               //      'fields' => [
               //          'subscription' => 'trial',
               //          'status' => 'active'
               //       ]
               //    ];
               //  $addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);
            }
        }
        $this->shop->save();

        //$addTag = ['tags' => [['name' => 'uninstalled', 'status' => 'inactive']]];
       // $result = $MailChimp->post('lists/'.env('MAILCHIMP_LIST_ID').'/members/'.$email.'/tags', $addTag);
       // logger('after authenticate tag removed='.json_encode($result));
    }
    public function addactive_campaign($shop, $Subscription){
      logger($Subscription);
      $shopData = $shop->api()->request(
                  'GET',
                  '/admin/api/shop.json',
                  []
              )['body']['shop'];
              $shop_owner = $shopData['shop_owner'];
              $shop_owner_name = explode(" ", $shop_owner);

        $post = array(
          'email'                    => $shopData['email'],
          'first_name'               => $shop_owner_name[0],
          'last_name'                => $shop_owner_name[1],
          'phone'                    => $shopData['phone'],
          'field[1,0]'               => $Subscription, //Subscription
          'field[6,0]'               => $shopData['domain'], // Company
          'field[7,0]'               => 'Installed', //App Status
          'field[9,0]'               => $shopData['country_name'], //country
          'field[10,0]'              => $shopData['city'], //city
          'field[11,0]'              => $shopData['zip'], //zip
          'field[12,0]'              => $shopData['address1'], //Address line 1
          'field[13,0]'              => $shopData['address2'], //Address line 2
          'field[14,0]'              => $shopData['province'], //Province
          'field[15,0]'              => $shopData['primary_locale'], //Language
          'field[16,0]'              => $shopData['name'], //Store name
          'p[3]'                     => env('app_user'),
      );
        logger(json_encode($post));
      $api_action = 'contact_edit';
      ActiveCampaignJob::dispatch($shop->email,'app', $post, $api_action);
    }
}
