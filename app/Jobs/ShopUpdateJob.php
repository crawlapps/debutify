<?php

namespace App\Jobs;

use Exception;
use App\User;
use App\Jobs\ActiveCampaignJob;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ShopUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $webhook    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger("Shop update job");
        $shopData = $this->data;
       // logger(json_encode($shopData));
        $shop = User::where('name',$this->shopDomain)->first();

        try{
            $address = array('line1' => $shopData['address1'], 'city' => $shopData['city'], 'country' => $shopData['country_name'], 'line2' => $shopData['address2'], 'postal_code' => $shopData['zip'], 'state' => $shopData['province']);
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $stripe_cust = \Stripe\Customer::retrieve($shop->stripe_id);
            if($stripe_cust){
                if($shop->stripe_id)
                {
                    \Stripe\Customer::update(
                      $shop->stripe_id,
                      [
                        'email' => $shopData['email'],
                        'description' => $shop->name,
                        'name' => $shopData['shop_owner'],
                        'address' => $address,
                        'phone' => $shopData['phone']
                      ]
                    );
                }
            }
        }catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            logger(json_encode($body['error']));
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            logger(json_encode($body['error']));

        } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            logger(json_encode($body['error']));

        } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed
          // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            logger(json_encode($body['error']));

        } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed
            $body = $e->getJsonBody();
            logger(json_encode($body['error']));
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
          // yourself an email
            $body = $e->getJsonBody();
            logger(json_encode($body['error']));
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            logger(json_encode($body['error']));
        }

        $shop_owner = $shopData['shop_owner'];
        $shop_owner_name = explode(" ", $shop_owner);
        if($shop->trial_days > 0){
            $subscription ='Trial';
        }else if($shop->alladdons_plan == 'freemium' || $shop->alladdons_plan == ''){
            $subscription = 'Freemium';
        }else{
            $subscription = $shop->alladdons_plan;
        }
        $post = array(
          'email'                    => $shopData['email'],
          'first_name'               => $shop_owner_name[0],
          'last_name'                => $shop_owner_name[1],
          'phone'                    => $shopData['phone'],
          'field[1,0]'               => $subscription, //Subscription
          'field[6,0]'               => $shopData['domain'], // Company
          'field[7,0]'               => 'Installed', //App Status
          'field[9,0]'               => $shopData['country_name'], //country
          'field[10,0]'              => $shopData['city'], //city
          'field[11,0]'              => $shopData['zip'], //zip
          'field[12,0]'              => $shopData['address1'], //Address line 1
          'field[13,0]'              => $shopData['address2'], //Address line 2
          'field[14,0]'              => $shopData['province'], //Province
          'field[15,0]'              => $shopData['primary_locale'], //Language
          'field[16,0]'              => $shopData['name'], //Store name
          'p[3]'                     => env('app_user'),
      );
       //logger(json_encode($post));
        $api_action = 'contact_edit';
        ActiveCampaignJob::dispatch($shop->email, 'app', $post, $api_action);

        $shop->email = $this->data->email;
        $shop->name = $this->data->myshopify_domain;
        $shop->custom_domain = $this->data->domain;
        $shop->shop_update = 1;
        $shop->save();

        // $shop_owner = $shopData['shop_owner'];
        // $shop_owner_name = explode(" ", $shop_owner);
        // $groupsApi = (new \MailerLiteApi\MailerLite(env('MailerLite_APIKEY')))->groups();

        // $groupId = env('MailerLite_GROUP_ID');
        // if($shop->alladdons_plan == null || $shop->alladdons_plan == ''){
        //   if($shop->trial_days > 0){
        //     $alladdons_plan ='trial';
        //   }else{
        //     $alladdons_plan ='freemium';
        //   }
        // }else{
        //   $alladdons_plan = $shop->alladdons_plan;
        // }
        // $subscriber = [
        //    'email' => $shopData['email'],
        //    'fields' => [
        //        'name' => $shop_owner_name[0],
        //        'last_name' => $shop_owner_name[1],
        //        'company' => $shopData['domain'],
        //        'city'=>$shopData['city'],
        //        'country'=>$shopData['country_name'],
        //        'phone'=>$shopData['phone'],
        //        'state'=>$shopData['province'],
        //        'zip'=>$shopData['zip'],
        //        'subscription' => $alladdons_plan,
        //        'status' => 'active'
        //    ]
        // ];

        // $addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);
    }
}
