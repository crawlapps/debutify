<?php

namespace App\Jobs;

use App\User;
use DateTime;
use App\StoreThemes;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ThemesCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $webhook    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('Theme created '.$this->shopDomain.' Data='. json_encode($this->data));
        if(!empty($this->data)){
            if( ( $pos = strrpos( $this->data->name , 'Debutify' ) ) !== false ) {
                $shop = User::where('name',$this->shopDomain)->whereNull('deleted_at')->first();
                if($shop){
                    $shop->theme_check = 1;
                    $shop->save();
                     // Add in Database
                    $theme_count_trial = StoreThemes::where('user_id', $shop->id)->where('status', 1)->count();
                    if($theme_count_trial == 0 && $shop->trial_ends_at != null && $shop->trial_days == null){
                        $date = new DateTime();
                        $new_formats_trial = $date->format('Y-m-d');
                        $trial_end_days = date('Y-m-d', strtotime($new_formats_trial. ' + 14 days'));
                        $shop->trial_days = 14;
                        $shop->trial_ends_at = $trial_end_days;
                        $shop->save();
                    }
                    $theme_count = StoreThemes::where('shopify_theme_id', $this->data->id)->count();
                    if($theme_count == 0){
                        $StoreTheme = new StoreThemes;
                        $StoreTheme->shopify_theme_id = $this->data->id;
                        $StoreTheme->shopify_theme_name = $this->data->name;
                        $StoreTheme->role = 0;
                        $StoreTheme->status = 1;
                        $StoreTheme->user_id = $shop->id;
                        $StoreTheme->save();
                    }
                }
            }else{
                $shop = User::where('name',$this->shopDomain)->first();
                $theme_count = StoreThemes::where('shopify_theme_id', $this->data->id)->count();
                if($theme_count == 0){
                    $StoreTheme = new StoreThemes;
                    $StoreTheme->shopify_theme_id = $this->data->id;
                    $StoreTheme->shopify_theme_name = $this->data->name;
                    $StoreTheme->role = 0;
                    $StoreTheme->status = 0;
                    $StoreTheme->user_id = $shop->id;
                    $StoreTheme->save();
                }
            }
        }
    }
}
