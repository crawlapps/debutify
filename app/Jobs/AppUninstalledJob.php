<?php

namespace App\Jobs;

use App\User;
use App\Jobs\ActiveCampaignJob;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AppUninstalledJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $webhook The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('uninstalled='.$this->shopDomain->toNative());
        $shop = User::where('name',$this->shopDomain->toNative())->first();
        $shop->license_key = null;
        $shop->deleted_at = date("Y-m-d H:i:s");
        $shop->save();
        $post = array(
          'email' => $shop->email,
          'field[7,0]'=> 'Uninstalled',
          'p[3]'=> env('app_user'),
          );
          $api_action = 'contact_edit';
          ActiveCampaignJob::dispatch($shop->email,'app', $post, $api_action);


        // $post = array(
        //    'app_status'            => 'Uninstalled',
        //    'p[3]'                  => env('app_user'),
        // );

        // $api_action = 'contact_edit';
        // ActiveCampaignJob::dispatch($shop->email, 'app', $post, $api_action);

        // $groupsApi = (new \MailerLiteApi\MailerLite(env('MailerLite_APIKEY')))->groups();
        // $groupId = env('MailerLite_GROUP_ID');

        // $subscriber = [
        //    'email' => $shop->email,
        //    'fields' => [
        //        'status' => 'unactive'
        //    ]
        // ];

        // $addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);

        // logger('tag added='.json_encode($addedSubscriber));

        // Cancel subscription
        // $activated_addons = AddOns::where('user_id', $shop->id)->where('status', 1)->count();
        // if($activated_addons >= 1){
        //     $shop = User::where('name',$this->shopDomain)->first();

        //     try{
        //         $shop->subscription('main')->cancelNow();
        //     }catch(\Stripe\Error\Card $e) {
        //         // Since it's a decline, \Stripe\Error\Card will be caught
        //         $body = $e->getJsonBody();
        //         $err  = $body['error'];
        //         logger('declined='.$err['message']);
        //     } catch (\Stripe\Error\RateLimit $e) {
        //         // Too many requests made to the API too quickly
        //         $body = $e->getJsonBody();
        //         $err  = $body['error'];
        //         logger('RateLimit='.$err['message']);
        //     } catch (\Stripe\Error\InvalidRequest $e) {
        //       // Invalid parameters were supplied to Stripe's API
        //         $body = $e->getJsonBody();
        //         $err  = $body['error'];
        //         logger('InvalidRequest='.$err['message']);
        //     } catch (\Stripe\Error\Authentication $e) {
        //       // Authentication with Stripe's API failed
        //       // (maybe you changed API keys recently)
        //         $body = $e->getJsonBody();
        //         $err  = $body['error'];
        //         logger('Authentication='.$err['message']);
        //     } catch (\Stripe\Error\ApiConnection $e) {
        //       // Network communication with Stripe failed
        //         $body = $e->getJsonBody();
        //         $err  = $body['error'];
        //         logger('ApiConnection='.$err['message']);
        //     } catch (\Stripe\Error\Base $e) {
        //       // Display a very generic error to the user, and maybe send
        //       // yourself an email
        //         $body = $e->getJsonBody();
        //         $err  = $body['error'];
        //         logger('Base='.$err['message']);
        //     } catch (Exception $e) {
        //       // Something else happened, completely unrelated to Stripe
        //         $body = $e->getJsonBody();
        //         $err  = $body['error'];
        //         logger('Exception='.$err['message']);
        //     }

            // if ($shop->subscription('main')->cancelled()) {
            //     $addons = AddOns::where('user_id', $shop->id)->where('status', 1)->get();
            //     foreach ($addons as $addon) {
            //         $addon->status = 0;
            //         $addon->save();
            //     }

            //     logger('cancelled subscription');
            // }
        // }

    }
}
