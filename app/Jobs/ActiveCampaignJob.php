<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
class ActiveCampaignJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $email;
    protected $api_action;
    protected $post;
    protected $app;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email,$app,$post, $api_action)
    {
        $this->email = $email;
        $this->post = $post;
        $this->api_action = $api_action;
        $this->app = $app;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('job hitting');
        $url = 'http://debutify.api-us1.com';
        /*-------------Get Contact List with ids -------------*/

        if($this->api_action == 'contact_edit'){
            // logger(json_encode($this->post));
            //logger("Action");
           //logger(json_encode($this->api_action));
            $params = array(
              'api_key' => env('ACTIVECAMPAIGN_API_KEY'),
              'api_action' => 'contact_view_email',
              'api_output' => 'serialize',
              'email' => $this->email,
            );

            // This section takes the input fields and converts them to the proper format
            $query = "";
            foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
            $query = rtrim($query, '& ');

            // clean up the url
            $url = rtrim($url, '/ ');

            // This sample code uses the CURL library for php to establish a connection,
            // submit your request, and show (print out) the response.
            if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

            // If JSON is used, check if json_decode is present (PHP 5.2.0+)
            if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
                die('JSON not supported. (introduced in PHP 5.2.0)');
            }

            // define a final API request - GET
            $api = $url . '/admin/api.php?' . $query;

            $request = curl_init($api); // initiate curl object
            curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
            //curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
            curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

            $response = (string)curl_exec($request); // execute curl fetch and store results in $response

            // additional options may be required depending upon your server configuration
            // you can find documentation on curl options at http://www.php.net/curl_setopt
            curl_close($request); // close curl object

            if ( !$response ) {
                die('Nothing was returned. Do you have a connection to Email Marketing server?');
            }

            $results = unserialize($response);
            //logger(json_encode($results));
            if($results['result_code'] == 0){
                $this->api_action = 'contact_add';
            }else{
              if($this->app == 'new subc'){
                if($results['tags']){
                  $this->remove_tags($this->email);
                }
              }

              $this->post['id'] =$results['id'];
            }
        }


        $params = array(

          // the API Key can be found on the "Your Settings" page under the "API" tab.
          // replace this with your API Key
          'api_key' => env('ACTIVECAMPAIGN_API_KEY'),

          'api_action' => $this->api_action,

          // define the type of output you wish to get back
          // possible values:
          // - 'xml'  :      you have to write your own XML parser
          // - 'json' :      data is returned in JSON format and can be decoded with
          //                 json_decode() function (included in PHP since 5.2.0)
          // - 'serialize' : data is returned in a serialized format and can be decoded with
          //                 a native unserialize() function
          'api_output' => 'serialize',
        );

        // This section takes the input fields and converts them to the proper format
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        // This section takes the input data and converts it to the proper format
        $data = "";
        foreach($this->post as $key => $value ) $data .= urlencode($key) . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

        // clean up the url
        $url = rtrim($url, '/ ');

        // This sample code uses the CURL library for php to establish a connection,
        // submit your request, and show (print out) the response.
        if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

        // If JSON is used, check if json_decode is present (PHP 5.2.0+)
        if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
            die('JSON not supported. (introduced in PHP 5.2.0)');
        }

        // define a final API request - GET
        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
        //curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request); // execute curl post and store results in $response

        // additional options may be required depending upon your server configuration
        // you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close($request); // close curl object


        $result = unserialize($response);
       // logger("Result");
       //logger(json_encode($result));
    }
    public function remove_tags($email){
      $url = 'http://debutify.api-us1.com';
      $params = array(
        'api_key'      => env('ACTIVECAMPAIGN_API_KEY'),
        'api_action'   => 'contact_tag_remove',
        'api_output'   => 'serialize',
      );

    $post = array(
        'email' => $email,
        'tags' => 'Canceled',
    );

    // This section takes the input fields and converts them to the proper format
    $query = "";
    foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
    $query = rtrim($query, '& ');

    // This section takes the input data and converts it to the proper format
    $data = "";
    foreach( $post as $key => $value ) $data .= urlencode($key) . '=' . urlencode($value) . '&';
    $data = rtrim($data, '& ');

    // clean up the url
    $url = rtrim($url, '/ ');

    // This sample code uses the CURL library for php to establish a connection,
    // submit your request, and show (print out) the response.
    if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

    // If JSON is used, check if json_decode is present (PHP 5.2.0+)
    if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
        die('JSON not supported. (introduced in PHP 5.2.0)');
    }

    // define a final API request - GET
    $api = $url . '/admin/api.php?' . $query;

    $request = curl_init($api); // initiate curl object
    curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
    curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
    curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
    //curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS
    curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

    $response = (string)curl_exec($request); // execute curl post and store results in $response

    // additional options may be required depending upon your server configuration
    // you can find documentation on curl options at http://www.php.net/curl_setopt
    curl_close($request); // close curl object

    if ( !$response ) {
        die('Nothing was returned. Do you have a connection to Email Marketing server?');
    }
    $result = unserialize($response);
  }
}
