<?php

namespace App\Jobs;

use App\User;
use App\StoreThemes;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ThemesUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       logger('Theme updated '.$this->shopDomain.' data='. json_encode($this->data));
       $shop = User::where('name', $this->shopDomain)->first();
       if ($this->data->role == 'main') {
       		$main_theme_count = StoreThemes::where('user_id', $shop->id)->where('role', 1)->count();
       		if($main_theme_count > 0){
                	$main_theme = StoreThemes::where('user_id', $shop->id)->where('role', 1)->first();
                	$main_theme->role = 0;
                	$main_theme->shopify_theme_name = $this->data->name;
                	$main_theme->save();
            }
       }

       $theme_count = StoreThemes::where('shopify_theme_id', $this->data->id)->count();
       if ($theme_count > 0) {
            $theme_updated = StoreThemes::where('shopify_theme_id', $this->data->id)->first();
            $theme_updated->shopify_theme_name = $this->data->name;
            if ($this->data->role == 'main') {
                $theme_updated->role = 1;
            }else{
                $theme_updated->role = 0;
            }
            $theme_updated->shopify_theme_name = $this->data->name;
            $theme_updated->save();
       }
       logger('update webhook ends');


    }
}
