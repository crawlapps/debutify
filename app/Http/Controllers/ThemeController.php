<?php

namespace App\Http\Controllers;

use DateTime;
use Exception;
use App\User;
use App\Taxes;
use App\AddOns;
use App\Themes;
use App\Course;
use App\FreeAddon;
use App\StripePlan;
use App\ChildStore;
use App\StoreThemes;
use App\Subscription;
use App\GlobalAddons;
use App\MentoringCall;
use App\WinningProduct;
use App\Jobs\ActiveCampaignJob;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;

class ThemeController extends Controller{
    public $subscription_status;
    public $secong_addon_name;
    public $third_theme_name;
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */

    public function __construct(){
        $shop = Auth::user();
        //$shopdomain = $shop->getDomain()->toNative();

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        if($shop){
          // Mentoring Data
          $mentoringWinnercount = MentoringCall::where('user_id', $shop->id)->where('days', '>', 0)->count();
          // if($mentoringCallWinner){
          //   logger("mentoring Call Winner store");
          //   logger(json_encode($mentoringCallWinner));
          // }
          // user details
            $owner_details = array();
            $shopData = $shop->api()->request(
                'GET',
                '/admin/api/shop.json',
                []
            )['body']['shop'];
            $shop_owner = $shopData['shop_owner'];
            $shop_owner_name = explode(" ", $shop_owner);

            $latest_theme = Themes::orderBy('id', 'desc')->first();
            //$StoreThemes = StoreThemes::where('user_id', $shop->id)->where('status', 1)->get();

            $addons_count = GlobalAddons::count();
            $user_id = $shop->id;
            $already_addon_activated = AddOns::where('user_id',$shop->id)->where('status',1)->count();
            $global_add_ons = GlobalAddons::leftJoin('add_ons',function ($join) use ($user_id) {
                $join->on('global_addons.id', '=' , 'add_ons.global_id') ;
                $join->where('add_ons.user_id','=', $user_id) ;
            })->select('global_addons.*','add_ons.status', 'add_ons.global_id', 'add_ons.user_id')->orderBy('title')->get();
            $latestupload = StoreThemes::where('user_id', $shop->id)->where('status', 1)->orderBy('id', 'desc')->first();
            $StoreThemes = StoreThemes::where('user_id', $shop->id)->where('status', 1)->orderBy('role', 'desc')->orderBy('id', 'desc')->get();
            $theme_count = count($StoreThemes);

            $name = $shopData['shop_owner'];
            $owner = explode(' ', $name);
            $owner_details['fname']= $owner[0];
            if(empty($owner[1]))
            {
                $owner_details['lname'] = '';
            } else {
                $owner_details['lname'] = $owner[1];
            }
            $owner_details['email']= $shopData['email'];

            // $shopdomain = Session::get('shopify_domain');

            // trial
            $trial_end_date = null;
            $show_end_at = null;
            $subscription = Subscription::where('user_id',$shop->id)->orderBy('id', 'desc')->first();

            $getstripesub = null;
            $billingDate = null;
            if ($subscription && isset($subscription->stripe_id)) {

              $getstripesub = \Stripe\Subscription::retrieve($subscription->stripe_id);
              $date = new DateTime();

              $date->setTimestamp($getstripesub->current_period_end);
              $new_format = $date->format('Y-m-d');
              $billingDate = $new_format;
              if($subscription->trial_ends_at != null){
                  $dates = new DateTime();
                  $new_formats = $dates->format('Y-m-d');
                  $dates = new DateTime($subscription->trial_ends_at);
                  $end_at = $dates->format('Y-m-d');
                  if($end_at >= $new_formats){
                      $show_end_at = $dates->format('M. d, Y');
                      $diff = strtotime($new_formats) - strtotime($end_at);
                      $days = abs(round($diff / 86400));
                      $trial_end_date = $days;
                  }
               }
            } else{
              $show_end_at = Carbon::now()->addWeek()->format('M. d, Y');
              $trial_end_date = "7";
            }

            // plans
            $freemium = 'Freemium';
            $starter = 'Starter';
            $hustler = 'Hustler';
            $guru = 'Guru';
            $starterLimit = '3';
            $hustlerLimit = $addons_count;
            $guruLimit = $addons_count;

            if($shop->alladdons_plan == $starter){
              $addonLimit = $starterLimit;
            }
            elseif($shop->alladdons_plan == $hustler){
              $addonLimit = $hustlerLimit;
            }
            elseif ($shop->alladdons_plan == $guru) {
              $addonLimit = $guruLimit;
            }
            else {
              $addonLimit = '0';
            }

            // master store check
            $master_shop = '';
            if($shop->master_account != 1){
                $ch_stores =[];
                $child_store = ChildStore::where('store', $shop->name)->first();
                if($child_store){
                  // Cancle Subscription
                  $master_shops = User::where('id', $child_store->user_id)->first();
                  $master_shop = $master_shops->name;
                  // guru plan of child store
                  $shop->all_addons = 1;
                  $shop->alladdons_plan = $master_shops->alladdons_plan;
                  $shop->sub_plan = $master_shops->sub_plan;
                  // license key created
                  $license_key = Hash::make(Str::random(12));
                  $shop->license_key = $license_key;
                 // $shop->sub_trial_ends_at = $master_shops->sub_trial_ends_at;
                  $shop->save();
                } else{
                    if ($subscription && isset($subscription->stripe_id)) {
                      if($getstripesub->status == "canceled"){
                        if($shop->all_addons == 1){
                          $this->delete_all_addon($shop, 'child');
                          $subscription->ends_at = $date;
                          $subscription->save();
                        }
                      }
                    }
                }
            } else{
              $ch_stores = ChildStore::where('user_id', $shop->id)->get();
              if ($subscription && isset($subscription->stripe_id)) {
                if($getstripesub->status == "canceled"){
                  if($shop->all_addons == 1){
                    $child_store_d = ChildStore::where('user_id', $shop->id)->delete();
                    $this->delete_all_addon($shop, 'master');
                    $subscription->ends_at = $date;
                    $subscription->save();
                  }
                }
              }
            }

            // subscription status
            $subscription_status = '';
            $subscriptions = Subscription::where('user_id',$shop->id)->whereNull('ends_at')->orderBy('id', 'desc')->first();
            if($subscriptions){
                $stripe_subs = \Stripe\Subscription::retrieve($subscriptions->stripe_id);
                if($stripe_subs->status == 'past_due'){
                  $subscription_status = 'past_due';
                }else if($stripe_subs->status == 'unpaid'){
                  $subscription_status = 'unpaid';
                  $shop->license_key = null;
                }else if($stripe_subs->status == 'active'){
                  $license_key = Hash::make(Str::random(12));
                  $shop->license_key = $license_key;
                }
                $shop->save();
            }

            // NEW trial setup -> Add trials Days in database
            $trial_check = $shop->trial_check;
           // logger('trial_check '.$trial_check);
            if(($shop->trial_ends_at != null || $shop->trial_ends_at != '') && $shop->sub_trial_ends_at == 1){
              $dates = new DateTime();
              $new_formats_trial = $dates->format('Y-m-d');
              $trial_dates = new DateTime($shop->trial_ends_at);
              if($new_formats_trial < $trial_dates->format('Y-m-d')){
                $shop->sub_trial_ends_at = 1;
                $license_key = Hash::make(Str::random(12));
                $shop->license_key = $license_key;
                $diff_trial = strtotime($new_formats_trial) - strtotime($shop->trial_ends_at);
                $days_trial = abs(round($diff_trial / 86400));
              } else{
                if($shop->sub_trial_ends_at == 1){
                  //$shop->license_key = null;
                 // $this->mailerLite($shop, 'freemium');
                  $this->addactive_campaign($shop,'Freemium','app', '');
                }
                $shop->sub_trial_ends_at = 0;
                $days_trial = 0;
              }
              $shop->trial_days = $days_trial;
              $shop->save();
            }

            $trial_days = $shop->trial_days;

            // set default trial plan
            $trial_plan = $hustler;

            // if trial is over
            if($trial_days == 0 || $trial_days == null){
              $trial_days = null;
              $all_addons = $shop->all_addons;
              $sub_plan = $shop->sub_plan;
              $alladdons_plan = $shop->alladdons_plan;
            }
            // else fake hustler subscription for trial
            else{
              $all_addons = 1;
              $sub_plan = 'month';
              $alladdons_plan = $trial_plan;
              $addonLimit = $hustlerLimit;
            }

            // trial check
            if($trial_check == null || $trial_check == ''){
              $trial_check = 0;
            }

            // theme check
            if($shop->theme_check == null || $shop->theme_check == ''){
              $shop->theme_check = 0;
            }

            // stripe plan
            $StripePlan = StripePlan:: all();
            foreach ($StripePlan as $plan) {
              if($plan->name =='hustlerPriceMonthly'){
                  $hustlerPriceMonthly = $plan->cost;
                  $hustleridMonthly = $plan->stripe_plan;
              }
              if($plan->name =='guruPriceMonthly'){
                  $guruPriceMonthly = $plan->cost;
                  $guruidMonthly = $plan->stripe_plan;
              }
            }

            $this->subscription_status = $subscription_status;
            $today = Carbon::now()->format('M. d, Y');
            $courses = Course::get();

            View::share([
              'owner_details' => $owner_details,
              'fname' => $owner_details['fname'],
              'name' => $owner_details['lname'],
              'email' => $owner_details['email'],
              'version' => $latest_theme->version,
              'freemium' => $freemium,
              'starter' => $starter,
              'hustler' => $hustler,
              'guru' => $guru,
              'starterLimit' => $starterLimit,
              'hustlerLimit' => $hustlerLimit,
              'guruLimit' => $guruLimit,
              'all_addons'=> $all_addons,
              'trial_end_date'=> $trial_days,
              'show_end_at' => $show_end_at,
              'alladdons_plan'=> $alladdons_plan,
              'trial_plan'=> $trial_plan,
              'shop_domain' => $shop->name,
              'sub_plan' => $sub_plan,
              'billingDate' => $billingDate,
              'theme_count'=>$theme_count,
              'addons_count'=>$addons_count,
              'addonLimit' => $addonLimit,
              'global_add_ons' => $global_add_ons,
              'active_add_ons' => $already_addon_activated,
              'latestupload'=>$latestupload,
              'store_themes' => $StoreThemes,
              'master_shop' => $master_shop,
              'child_stores' => $ch_stores,
              'subscription_status' => $subscription_status,
              'today' => $today,
              'user_id' => $user_id,
              'trial_end_at' => $shop->trial_ends_at,
              'trial_days' => $trial_days,
              'theme_check' => $shop->theme_check,
              'trial_check' => $trial_check,
              'guruPriceMonthly' => $guruPriceMonthly,
              'guruidMonthly' => $guruidMonthly,
              'hustlerPriceMonthly' => $hustlerPriceMonthly,
              'hustleridMonthly' => $hustleridMonthly,
              'mentoringWinnercount' => $mentoringWinnercount,
              'courses' => $courses
            ]);
        }
    }

    // app index view
    public function index(Request $request){
        $shop = Auth::user();
        $latest_theme = Themes::orderBy('id', 'desc')->first();
        $StoreThemes = StoreThemes::where('user_id', $shop->id)->where('status', 1)->get();
        $shopify_themes = array();
        $theme_count = StoreThemes::where('user_id', $shop->id)->where('status', 1)->count();

        $shopData = $shop->api()->request(
            'GET',
            '/admin/api/shop.json',
            []
        )['body']['shop'];
        $shop->email = $shopData['email'];
        $shop->save();
        $freemimum = 'freemium';
        foreach ($StoreThemes as $theme) {
            $shopify_themes[] = array('label'=>$theme->shopify_theme_name, 'href'=>'/admin/themes/'.$theme->shopify_theme_id.'/editor', 'target'=>'new');
        }

        if($shop->alladdons_plan != $freemimum && $shop->alladdons_plan != null){
            if ($theme_count == 0) {
                return redirect()->route('theme_view');
            } else {
                $theme_current_version = Themes::where('id', $shop->theme_id)->get();
                if($shop->theme_id != null && $shop->shopify_theme_id != null){
                    if ($latest_theme->id > $shop->theme_id) {
                        $theme_url = $latest_theme->url;
                        $request->session()->flash('new_version', $theme_url);
                    }
                }
                return redirect()->route('theme_addons');
            }
        } else {
            if ($theme_count == 0) {
              return redirect()->route('theme_view');
            } else {
              return redirect()->route('theme_addons');
            }
        }
    }

    // onboarding view
    public function onboarding(){
        return redirect()->route('theme_view');
    }

    // support view
    public function support(StripePlan $plan, Request $request){
        if($this->subscription_status =='unpaid'){
          return redirect()->route('plans');
        }
        return view('support', [
          'plan' => $plan,
        ]);
    }

    // technical support view
    public function technicalSupport(StripePlan $plan, Request $request){
        if($this->subscription_status =='unpaid'){
          return redirect()->route('plans');
        }
        return view('technical-support', [
          'plan' => $plan,
        ]);
    }

    // changelog view
    public function changelog(StripePlan $plan, Request $request){
        if($this->subscription_status =='unpaid'){
          return redirect()->route('plans');
        }
        return view('changelog', [
          'plan' => $plan,
        ]);
    }

    // integrations view
    public function integrations(StripePlan $plan, Request $request){
        if($this->subscription_status =='unpaid'){
        return redirect()->route('plans');
      }
        return view('integrations', [
          'plan' => $plan,
        ]);
    }

    // affiliate view
    public function affiliate(StripePlan $plan, Request $request){
        if($this->subscription_status =='unpaid'){
          return redirect()->route('plans');
        }
        return view('affiliate', [
          'plan' => $plan,
        ]);
    }

    // courses view
    public function courses(StripePlan $plan, Request $request){
      if($this->subscription_status =='unpaid'){
      }
      $courses = Course::orderBy('id', 'desc')->paginate(24);
      // print_r($courses);
      return view('courses', [
        'plan' => $plan,
        'courses' => $courses
      ]);
    }

    public function viewCourse(StripePlan $plan, Request $request,$id){
      $course = Course::where('courses.id',$id)->with(['modules.steps' => function ($query) { $query->orderBy('position', 'asc');}])->first();
      $course->sub_plans = explode(",", $course->plans);
      $course = json_decode($course);

      // echo "<pre>";
      // print_r($course);
      // echo "</pre>";

      return view('view-course', [
        'plan' => $plan,
        'course' => $course
      ]);
    }

    // mentoring view
    public function mentoring(StripePlan $plan, Request $request){
      $mentoringWinners = MentoringCall::orderBy('id', 'desc')->get();
      if($this->subscription_status =='unpaid'){
        return redirect()->route('plans');
      }
      return view('mentoring', [
        'plan' => $plan,
        'mentoringWinners' => $mentoringWinners
      ]);
    }

    function getAddonsPlan($shop){
      // duplicated data for trial
      $trial_days = $shop->trial_days;
      $trial_plan = 'Hustler';
      if($trial_days == 0 || $trial_days == null){
        $alladdons_plan = $shop->alladdons_plan;
      }else{
        $alladdons_plan = $trial_plan;
      }
      return $alladdons_plan;
    }

    // winning products view bob
    public function winning_products(StripePlan $plan, Request $request){

      $shop = Auth::user();
      if($this->subscription_status =='unpaid'){
        return redirect()->route('plans');
      }

      $products = [];
      // DB::enableQueryLog();
      $hiddenProducts = '';

      $alladdons_plan = $this->getAddonsPlan($shop);

      // set product limitation per plan
      if($alladdons_plan == 'Guru'){
        $products = WinningProduct::where('saturationlevel', 'gold')->orderBy('id', 'desc')->paginate(24);
      }elseif ($alladdons_plan == 'Hustler') {
        $products = WinningProduct::where('saturationlevel', 'silver')->orderBy('id', 'desc')->paginate(24);
        $hiddenProducts = WinningProduct::where('saturationlevel', 'silver')->orderBy('id', 'desc')->count();
      }elseif ($alladdons_plan == 'Starter') {
        $products = WinningProduct::where('saturationlevel', 'bronze')->orderBy('id', 'desc')->paginate(24);
        $hiddenProducts = WinningProduct::where('saturationlevel', 'bronze')->orderBy('id', 'desc')->count();
      }

      // print_r(DB::getQueryLog());

      $productCount = WinningProduct::count();
      $productss = [];

      foreach ($products as $key => $product) {
        $date = new DateTime();
        $new_formats = $date->format('Y-m-d');
        $new_format = $product->created_at->format('Y-m-d');
        $diff = strtotime($new_formats) - strtotime($new_format);
        $days = abs(round($diff / 86400));
        $product->days = $days;

        if($days <= 7){
          $product->new_product = true;
        }else{
          $product->new_product = false;
        }

        if($product->opinion){
          $formattedOpinion = str_replace(["\r\n", "\r", "\n"], "<br/>", $product->opinion);
          $formattedOpinion2 = str_replace('"', "'", $formattedOpinion);
          $product->opinion = $formattedOpinion2;
        }

        if($product->description){
          $formattedDescription = str_replace(["\r\n", "\r", "\n"], "<br/>", $product->description);
          $formattedDescription2 = str_replace('"', "'", $formattedDescription);
          $product->description = $formattedDescription2;
        }

        $productss[] = $product;
      }

      return view('winning-products', [
        'plan' => $plan,
        'products' => $productss,
        'productCount' => $productCount,
        'productspagination'=> $products,
        'hiddenProducts' => $hiddenProducts
      ]);
    }

    public function paginateWinningProducts(StripePlan $plan, Request $request){
      $shop = Auth::user();
      if($this->subscription_status =='unpaid'){
        return redirect()->route('plans');
      }
      $products = [];
      // DB::enableQueryLog();
      $hiddenProducts = '';

      $alladdons_plan = $this->getAddonsPlan($shop);

      // set product limitation per plan
      if($alladdons_plan == 'Guru'){
        $products = WinningProduct::where('saturationlevel', 'gold')->orderBy('id', 'desc')->paginate(24);
      }elseif ($alladdons_plan == 'Hustler') {
        $products = WinningProduct::where('saturationlevel', 'silver')->orderBy('id', 'desc')->paginate(24);
        $hiddenProducts = WinningProduct::where('saturationlevel', 'silver')->orderBy('id', 'desc')->count();
      }elseif ($alladdons_plan == 'Starter') {
        $products = WinningProduct::where('saturationlevel', 'bronze')->orderBy('id', 'desc')->paginate(24);
        $hiddenProducts = WinningProduct::where('saturationlevel', 'bronze')->orderBy('id', 'desc')->count();
      }

      // print_r(DB::getQueryLog());

      $productCount = WinningProduct::count();
      $productss = [];

      foreach ($products as $key => $product) {
        $date = new DateTime();
        $new_formats = $date->format('Y-m-d');
        $new_format = $product->created_at->format('Y-m-d');
        $diff = strtotime($new_formats) - strtotime($new_format);
        $days = abs(round($diff / 86400));
        $product->days = $days;

        if($days <= 7){
          $product->new_product = true;
        }else{
          $product->new_product = false;
        }

        if($product->opinion){
          $formattedOpinion = str_replace(["\r\n", "\r", "\n"], "<br/>", $product->opinion);
          $formattedOpinion2 = str_replace('"', "'", $formattedOpinion);
          $product->opinion = $formattedOpinion2;
        }

        if($product->description){
          $formattedDescription = str_replace(["\r\n", "\r", "\n"], "<br/>", $product->description);
          $formattedDescription2 = str_replace('"', "'", $formattedDescription);
          $product->description = $formattedDescription2;
        }

        $productss[] = $product;
      }

      $html = View::make('searched-winning-products',['plan' => $plan, 'products' => $productss, 'productspagination'=> $products]);

      $response = $html->render();
      return response()->json([
          'status' => 'success',
          'html' => $response
      ]);
    }

    public function filterWinningProducts(StripePlan $plan, Request $request){
      $shop = Auth::user();
      if($this->subscription_status =='unpaid'){
        return redirect()->route('plans');
      }

      //DB::enableQueryLog();
      $q = WinningProduct::query();
      if($request->query('saturation') != ''){
        $q->where('saturationlevel', 'like', '%' . $request->query('saturation') . '%');
      }

      if($request->query('q') != ''){
        $q->where(function ($query) use ($request) {
          $query->where('name', 'like', '%' . $request->query('q') . '%')
            ->orWhere('aliexpresslink', 'like', '%' . $request->query('q') . '%')
            ->orWhere('price', 'like', '%' . $request->query('q') . '%');
        });
      }

      if($request->query('category') != ''){
        $q->where('category', 'like', '%' . $request->query('category') . '%');
      }

      if($request->query('profit') != ''){
        $profit = explode('-', $request->query('profit'));
        if(count($profit) >= 2){
          $min = $profit[0]; $max = $profit[1];
          $q->whereBetween('profit', [(int)$min, (int)$max]);
        }else{
          $q->where('profit', '>=', (int)$request->query('profit'));
        }
      }

      $products = [];

      $alladdons_plan = $this->getAddonsPlan($shop);

      // set product limitation per plan
      if($alladdons_plan == 'Guru'){
        $products = $q->orderBy('id', 'desc')->paginate(24);
      }elseif ($alladdons_plan == 'Hustler') {
        $q->where(function ($query) {
          $query->where('saturationlevel', 'bronze')->orWhere('saturationlevel', 'silver');
        });
        $products = $q->orderBy('id', 'desc')->paginate(24);
        $hiddenProducts = $q->orderBy('id', 'desc')->count();
      }elseif ($alladdons_plan == 'Starter') {
        $products = $q->where('saturationlevel', 'bronze')->orderBy('id', 'desc')->paginate(24);
        $hiddenProducts = $q->where('saturationlevel', 'bronze')->orderBy('id', 'desc')->count();
      }

      // print_r(DB::getQueryLog());

      $productss = [];
      foreach ($products as $key => $product) {
        $date = new DateTime();
        $new_formats = $date->format('Y-m-d');
        $new_format = $product->created_at->format('Y-m-d');
        $diff = strtotime($new_formats) - strtotime($new_format);
        $days = abs(round($diff / 86400));
        $product->days = $days;

        if($days <=7){
          $product->new_product = true;
        }else{
          $product->new_product = false;
        }

        if($product->opinion){
          $formattedOpinion = str_replace(["\r\n", "\r", "\n"], "<br/>", $product->opinion);
          $formattedOpinion2 = str_replace('"', "'", $formattedOpinion);
          $product->opinion = $formattedOpinion2;
        }

        if($product->description){
          $formattedDescription = str_replace(["\r\n", "\r", "\n"], "<br/>", $product->description);
          $formattedDescription2 = str_replace('"', "'", $formattedDescription);
          $product->description = $formattedDescription2;
        }

        $productss[] = $product;
      }

      $html = View::make('searched-winning-products',['plan' => $plan, 'products' => $productss, 'productspagination'=> $products]);

      $response = $html->render();
      return response()->json([
          'status' => 'success',
          'html' => $response
      ]);
    }

    // feedback view
    public function feedback(StripePlan $plan, Request $request){
      //logger('feedback');
      if($this->subscription_status =='unpaid'){
        return redirect()->route('plans');
      }
      return view('feedback', [
        'plan' => $plan,
      ]);
    }

    // theme view
    public function theme_view(StripePlan $plan, Request $request){
        $shop = Auth::user();
        $latest_theme = Themes::orderBy('id', 'desc')->first();
        $user_id = $shop->id;
        $theme_count = StoreThemes::where('user_id', $shop->id)->where('status', 1)->count();

        if($theme_count > 0){
            $theme_current_version = Themes::where('id', $shop->theme_id)->get();
            if($shop->theme_id != null && $shop->shopify_theme_id != null){
                if ($latest_theme->id > $shop->theme_id) {
                    $theme_url = $latest_theme->url;
                    $request->session()->flash('new_version', $theme_url);
                }
            }
        }

        // from old onboarding view
        $StoreThemes = StoreThemes::where('user_id', $shop->id)->where('status', 1)->get();
        $shopify_themes = array();
        foreach ($StoreThemes as $theme) {
            $shopify_themes[] = array('label'=>$theme->shopify_theme_name, 'href'=>'/admin/themes/'.$theme->shopify_theme_id.'/editor', 'target'=>'new');
        }

        if($this->subscription_status =='unpaid'){
          return redirect()->route('plans');
        }

        return view('theme_view', [
          'plan' => $plan,
          'shopify_themes' => json_encode($shopify_themes, true)
        ]);
    }

    // plans view
    public function plans(StripePlan $plan, Request $request){
        $shop = Auth::user();
        // echo "<pre>";
        // print_r($shop);
        // echo "</pre>";
        // die();
        // $shop = User::where('id', '179')->first();
        $latest_theme = Themes::orderBy('id', 'desc')->first();
        $free_addons = FreeAddon::where('shopify_domain', $shop->name)->where('status',1)->first();
        $theme_count = StoreThemes::where('user_id', $shop->id)->count();
        $StoreThemes = StoreThemes::where('user_id', $shop->id)->where('status', 1)->orderBy('role', 'desc')->orderBy('id', 'desc')->get();
        $exit_code = '50OFF3MONTHS';
        $new_code = 'DEBUTIFY20';
        $coupon_name = '';
        $percent_off = '';
        $coupon_duration = '';
        $coupon_duration_months = '';
        $previous_plan = '';
        $discount = '';
        $next_invoice_total = '';
        $card_expire = '';
        $card_number = '';
        $card_brand ='';
        $card_data = '';
        $current_plan = '';
        $current_cost = '';

        // subcription active
        if($shop->all_addons == 1){
          $subscriptions = Subscription::where('user_id',$shop->id)->whereNull('ends_at')->orderBy('id', 'desc')->first();
          if($subscriptions){
            $stripe_subs =  \Stripe\Subscription::retrieve($subscriptions->stripe_id);
            $stripe_customer = \Stripe\Customer::retrieve($shop->stripe_id);
            $stripe_upcoming_invoice = \Stripe\Invoice::upcoming(["subscription" => $stripe_subs]);

            // get customer card details
            $card_data = $stripe_customer->sources->data[0]->card;

            // if json return of $card_data is empty
            if($card_data){} else{
              $card_data = $stripe_customer->sources->data[0];
            }

            $card_expire_m = $card_data->exp_month;
            $card_expire_y = $card_data->exp_year;
            if($card_expire_m < 10){
              $card_expire_m = '0'.$card_expire_m;
            }
            $card_expire = $card_expire_m.'/'.$card_expire_y;
            $card_number = $card_data->last4;
            $card_brand = $card_data->brand;
            // End Of card

            $current_plan = $stripe_subs->plan;
            $discount = $stripe_subs->discount;
            // $next_invoice_total_1 = $stripe_upcoming_invoice->total/100;
            $next_invoice_total_1 = $stripe_upcoming_invoice->amount_due/100;
            $next_invoice_total = number_format($next_invoice_total_1, 2);

            // get subscription discount
            if($discount){
              $coupon_name = $discount->coupon->name;
              $coupon_duration = $discount->coupon->duration;
              $coupon_duration_months = $discount->coupon->duration_in_months;

              if($discount->coupon->percent_off){
                $percent_off = $discount->coupon->percent_off.'% off';
              } else{
                $percent_off_1 = $discount->coupon->amount_off/100;
                $percent_off_2 = number_format($percent_off_1, 2);
                $percent_off = 'US$'.$percent_off_2.' off';
              }
            }

            // get old pricing for current users
            if($current_plan){
              $current_cost = $current_plan->amount/100;
              if($current_cost == '10' || $current_cost == '84' || $current_cost == '27' || $current_cost == '197' || $current_cost == '15' || $current_cost == '90'){
                $previous_plan = $current_cost;
              }
            }
          }
        }

        $StripePlan = StripePlan:: all();
        if($free_addons){
          $free_addons = $free_addons->status;
        }
        if($theme_count > 0){
          $theme_current_version = Themes::where('id', $shop->theme_id)->get();
          if($shop->theme_id != null && $shop->shopify_theme_id != null){
            if ($latest_theme->id > $shop->theme_id) {
              $theme_url = $latest_theme->url;
              $request->session()->flash('new_version', $theme_url);
            }
          }
        }

        // get active plan price and ID
        foreach ($StripePlan as $plan) {
          if($plan->name =='starterPriceAnnually'){
               $starterPriceAnnually = $plan->cost;
               $starteridAnnually = $plan->stripe_plan;
          }
          if($plan->name =='starterPriceMonthly'){
              $starterPriceMonthly = $plan->cost;
              $starteridMonthly = $plan->stripe_plan;
          }
          if($plan->name =='hustlerPriceAnnually'){
              $hustlerPriceAnnually = $plan->cost;
              $hustleridAnnually = $plan->stripe_plan;
          }
          if($plan->name =='hustlerPriceMonthly'){
              $hustlerPriceMonthly = $plan->cost;
              $hustleridMonthly = $plan->stripe_plan;
          }
          if($plan->name =='guruPriceAnnually'){
              $guruPriceAnnually = $plan->cost;
              $guruidAnnually = $plan->stripe_plan;
          }
          if($plan->name =='guruPriceMonthly'){
              $guruPriceMonthly = $plan->cost;
              $guruidMonthly = $plan->stripe_plan;
          }
        }

        // child store count
        $store_count = ChildStore::where('user_id', $shop->id)->count();
        $store_limit = '2';

        // taxes
        $applyTaxes = false;
        $shopData = $shop->api()->request(
            'GET',
            '/admin/api/shop.json',
            []
        )['body']['shop'];

        if($shopData['country_name'] == 'Canada'){
          $applyTaxes = true;
        }

        // $tax_id = getTaxId($shopData->province);

        return view('plans', [
          'starterPriceAnnually' => $starterPriceAnnually,
          'starterPriceMonthly' => $starterPriceMonthly,
          'hustlerPriceAnnually' => $hustlerPriceAnnually,
          'hustlerPriceMonthly' => $hustlerPriceMonthly,
          'guruPriceAnnually' => $guruPriceAnnually,
          'guruPriceMonthly' => $guruPriceMonthly,
          'starteridAnnually' => $starteridAnnually,
          'starteridMonthly' => $starteridMonthly,
          'hustleridAnnually' => $hustleridAnnually,
          'hustleridMonthly' => $hustleridMonthly,
          'guruidAnnually' => $guruidAnnually,
          'guruidMonthly' => $guruidMonthly,
          'plan' => $plan,
          'free_addons'=> $free_addons,
          'store_count' => $store_count,
          'store_limit' => $store_limit,
          'exit_code' => $exit_code,
          'new_code' => $new_code,
          'coupon_name' => $coupon_name,
          'percent_off' =>$percent_off,
          'coupon_duration' =>$coupon_duration,
          'coupon_duration_months' =>$coupon_duration_months,
          'discount' => $discount,
          'previous_plan' => $previous_plan,
          'next_invoice_total' => $next_invoice_total,
          'card_expire' => $card_expire,
          'card_number' => $card_number,
          'card_brand' => $card_brand,
          'card_data' => $card_data,
          'all_addons' => $shop->all_addons,
          'alladdons_plan'=> $shop->alladdons_plan,
          'sub_plan' => $shop->sub_plan,
          'current_plan' => $current_plan,
          'current_cost' => $current_cost,
          'applyTaxes' => $applyTaxes
        ]);
    }

    // add-ons view
    public function theme_addons(StripePlan $plan, Request $request){
        $shop = Auth::user();

        $latest_theme = Themes::orderBy('id', 'desc')->first();
        $free_addons = FreeAddon::where('shopify_domain', $shop->name)->where('status',1)->first();

        if($free_addons){
          $free_addons = $free_addons->status;
        }
        $theme_count = StoreThemes::where('user_id', $shop->id)->where('status',1)->count();
        if($theme_count > 0){
          $theme_current_version = Themes::where('id', $shop->theme_id)->get();
          if($shop->theme_id != null && $shop->shopify_theme_id != null){
            if ($latest_theme->id > $shop->theme_id) {
              $theme_url = $latest_theme->url;
              $request->session()->flash('new_version', $theme_url);
            }
          }
        }
        if($this->subscription_status =='unpaid'){
          return redirect()->route('plans');
        }
        $request->session()->keep(['status', 'message', 'theme_id_cstm']);
        // if($theme_count > 0){
          return view('add_ons', [
            'free_addons'=>$free_addons
          ]);
        // } else{
          // return redirect()->route('theme_view');
        // }
    }

    // free addon view
    public function free_addons(){
        if($this->subscription_status =='unpaid'){
          return redirect()->route('plans');
        }
        return view('free_addons');
    }

    // update card view
    public function update_card(){
        return view('updatecard');
    }

    // report bug view
    public function report_bug_pop(){
        if($this->subscription_status =='unpaid'){
          return redirect()->route('plans');
        }
        return view('report_bug_popup');
    }

    // thank you view
    public function thank_you(){
        return view('thank-you');
    }

    // good bye view
    public function good_bye(){
        return view('good-bye');
    }



    // activate addon function
    public function active_addons($addon_id, $themes_id, $update_addon, $key){
        $shop = Auth::user();
        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_id', $themes_id)->where('status', 1)->get();
        $delivery_addon_activated = AddOns::where('user_id',$shop->id)->where('global_id', 4)->where('status',1)->count();
        $addon_count = AddOns::where('user_id', $shop->id)->where('status', 1)->count();
        $StoreThemes=[];

        foreach ($StoreTheme as $theme) {
            try{
                $get_theme = $shop->api()->request(
                      'GET',
                      '/admin/api/themes/'.$theme->shopify_theme_id.'.json'
                )['body']['theme'];
                $StoreThemes[] = $theme;
            }
            catch(\GuzzleHttp\Exception\ClientException $e){
                logger('update schema on live view addon throws client exception');
            }
            catch(\Exception $e){
                logger('update schema on live view addon throws exception');
            }
        }

        if($StoreThemes){
            // add dbtfy scripts (function in helper.php)
            if($key == 0){
              add_debutify_JS($StoreThemes, $shop);
              sleep(3);
              addServerJS($StoreThemes, $shop);
            }

            // activate add-ons
            switch ($addon_id) {
                case 1:
                    activate_trustbadge_addon($StoreThemes, $shop);
                    break;
                case 2:
                    activate_liveview_addon($StoreThemes, $shop, $delivery_addon_activated);
                    break;
                case 3:
                    activate_cookibox_addon($StoreThemes, $shop);
                    break;
                case 4:
                    activate_deliverytime_addon($StoreThemes, $shop);
                    break;
                case 5:
                    activate_addtocart_animation_addon($StoreThemes, $shop);
                    break;
                case 6:
                    activate_salespop_addon($StoreThemes, $shop);
                    break;
                // case 7:
                //     activate_instagramfeed_addon($StoreThemes, $shop);
                //     break;
                case 8:
                    if($update_addon == 1){}else{
                      activate_producttab_addon($StoreThemes, $shop);
                    }
                    break;
                case 9:
                    activate_chatbox_addon($StoreThemes, $shop);
                    break;
                case 10:
                    activate_faqepage_addon($StoreThemes, $shop);
                    break;
                case 11:
                    activate_sticky_addtocart_addon($StoreThemes, $shop);
                    break;
                case 12:
                    activate_product_video_addon($StoreThemes, $shop);
                    break;
                case 13:
                    activate_shop_protect_addon($StoreThemes, $shop);
                    break;
                case 14:
                    if($update_addon == 1){}else{
                      activate_mega_menu_addon($StoreThemes, $shop);
                    }
                    break;
                case 15:
                    activate_newsletter_popup_addon($StoreThemes, $shop);
                    break;
                case 16:
                    activate_collection_addtocart_addon($StoreThemes, $shop);
                    break;
                case 17:
                    activate_upsell_popup_addon($StoreThemes, $shop);
                    break;
                case 18:
                    activate_discount_saved_addon($StoreThemes, $shop);
                    break;
                case 19:
                    activate_sales_countdown_addon($StoreThemes, $shop);
                    break;
                case 20:
                    activate_inventory_quantity_addon($StoreThemes, $shop);
                    break;
                case 21:
                    activate_linked_options_addon($StoreThemes, $shop);
                    break;
                case 22:
                    activate_cart_countdown_addon($StoreThemes, $shop);
                    break;
                case 23:
                    activate_colorswatches_addon($StoreThemes, $shop);
                    break;
                case 24:
                    activate_cart_discount_addon($StoreThemes, $shop);
                    break;
                case 25:
                    activate_upsell_bundles_addon($StoreThemes, $shop);
                    break;
                case 26:
                    activate_skip_cart_addon($StoreThemes, $shop);
                    break;
                case 27:
                    activate_smart_search_addon($StoreThemes, $shop);
                    break;
                case 28:
                    activate_quick_view_addon($StoreThemes, $shop);
                    break;
                case 29:
                    activate_cart_goal_addon($StoreThemes, $shop);
                    break;
            }
        }
    }

    // deactivate addon function
    public function deactive_addons($shop,$StoreThemes,$addon_id,$checkaddon,$update_addon){
        if($StoreThemes){
            // remove dbtfy scripts (function in helper.php)
            if($checkaddon == 1){
                no_addon_activate($StoreThemes, $shop);
            }

            // uninstall add-ons
            switch ($addon_id) {
                case 1:
                    deactivate_trustbadge_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 2:
                    deactivate_liveview_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 3:
                    deactivate_cookibox_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 4:
                    deactivate_deliverytime_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 5:
                    deactivate_addtocart_animation_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 6:
                    deactivate_salespop_addon($StoreThemes, $shop, $checkaddon);
                    break;
                // case 7:
                //     deactivate_instagramfeed_addon($StoreThemes, $shop, $checkaddon);
                //     break;
                case 8:
                    deactivate_producttabs_addon($StoreThemes, $shop, $checkaddon, $update_addon);
                    break;
                case 9:
                    deactivate_chatbox_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 10:
                    deactivate_faqepage_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 11:
                    deactivate_sticky_addtocart_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 12:
                    deactivate_product_video_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 13:
                    deactivate_shop_protect_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 14:
                    deactivate_mega_menu_addon($StoreThemes, $shop, $checkaddon, $update_addon);
                    break;
                case 15:
                    deactivate_newsletter_popup_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 16:
                    deactivate_collection_addtocart_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 17:
                    deactivate_upsell_popup_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 18:
                    deactivate_discount_saved_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 19:
                    deactivate_sales_countdown_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 20:
                    deactivate_inventory_quantity_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 21:
                    deactivate_linked_options_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 22:
                    deactivate_cart_countdown_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 23:
                    deactivate_colorswatches_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 24:
                    deactivate_cart_discount_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 25:
                    deactivate_upsell_bundles_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 26:
                    deactivate_skip_cart_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 27:
                    deactivate_smart_search_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 28:
                    deactivate_quick_view_addon($StoreThemes, $shop, $checkaddon);
                    break;
                case 29:
                    deactivate_cart_goal_addon($StoreThemes, $shop, $checkaddon);
                    break;
            }
        }
    }

    // downlod theme function
    public function download_theme(Request $request){
      logger("Download theme");
      $shop = Auth::user();
      $latest_theme = Themes::orderBy('id', 'desc')->first();
        $count_themes = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_name', 'Debutify '.$latest_theme->version)->where('status', 1)->count();
        $last_themes = StoreThemes::where('user_id', $shop->id)->where('status', 1)->orderBy('id', 'desc')->first();
        $theme_name = '';

        // name theme
        if ($count_themes > 0) {
            $theme_name = 'Copy of '.$last_themes->shopify_theme_name;
        }
        else{
            $theme_name = 'Debutify '.$latest_theme->version;
        }
       // $shop->save();

        $theme = array('theme' =>array(
                          'name' => $theme_name,
                          'src' => $latest_theme->url,
                          )
                      );
        try{
            $upload_theme_response  = $shop->api()->request('POST', '/admin/api/themes.json', $theme)['body']['theme'];
        }
        catch (\Exception $e) {
            $upload_theme_response = $e;
        }

        // theme downloaded successfully
        if (isset($upload_theme_response['id'])) {
          // $theme_count_trial = StoreThemes::where('user_id', $shop->id)->count();
         //    if($theme_count_trial == 0 && $shop->trial_ends_at != null && $shop->trial_days == null){
         //        $date = new DateTime();
         //        $shop->trial_days = 14;
         //    }
          //  save in shop
            $shop->theme_id = $latest_theme->id;
            $shop->theme_url = $latest_theme->url;
            $shop->lastactivity = new DateTime();
            $shop->shopify_theme_id = $upload_theme_response['id'];
            $shop->theme_check = 1;
            $shop->save();
            $theme_count = StoreThemes::where('shopify_theme_id', $upload_theme_response['id'])->where('status',1)->count();
            if($theme_count == 0){
                $StoreTheme = new StoreThemes;
                $StoreTheme->shopify_theme_id = $upload_theme_response['id'];
                $StoreTheme->shopify_theme_name = $upload_theme_response['name'];
                $StoreTheme->role = 0;
                $StoreTheme->status = 1;
                $StoreTheme->user_id = $shop->id;
                $StoreTheme->save();
            }

            sleep(5);

            // install active add-ons on new theme
            $activated_addons = AddOns::where('user_id', $shop->id)->where('status', 1)->get();
            $delivery_addon_activated = 0;
            $update_addon = 0;
            foreach ($activated_addons as $key=>$addon) {
                $this->active_addons($addon->global_id, $upload_theme_response['id'], $update_addon, $key);
            }
            if($request->get('theme_id')){
              $schemas = $shop->api()->request(
                  'GET',
                  '/admin/api/themes/'.$request->get('theme_id').'/assets.json',
                  ['asset' => ['key' => 'config/settings_data.json'] ]
              )['body']['asset']['value'];

              $original_schemas = $schemas;
              $update_schema_settings = $shop->api()->request(
                'PUT',
                '/admin/api/themes/'.$upload_theme_response['id'].'/assets.json',
                ['asset' => ['key' => 'config/settings_data.json', 'value' => $original_schemas] ]
              );
            }

            $is_theme_processing = $upload_theme_response['processing'];
            if( !$is_theme_processing ){
                return response()->json([
                    'status' => 'ok',
                    'message' => 'Theme added'
                ]);
            }else{
                do {
                    $is_theme_processing = $this->getThemeProcessing($upload_theme_response['id']);
                    sleep(2);   //wait for 5 sec for next function call
                    if( !$is_theme_processing ){
                        return response()->json([
                            'status' => 'ok',
                            'message' => 'Theme added'
                        ]);
                    }

                } while($is_theme_processing === TRUE);
            }
//            return response()->json([
//                'status' => 'ok',
//                'message' => 'Theme added'
//            ]);
        }

        // theme download error
        else{
            try{
                $shopify_themes = $shop->api()->request(
                      'GET',
                      '/admin/api/themes.json'
                )['body']['themes'];
                $theme_count = count($shopify_themes);
               // logger($theme_count);
                if($theme_count >= 20){
                    $message = 'Your online store has a maximum of 20 themes. Remove unused themes to add more.';
                } else{
                    $message = 'Error Occurred. Please try again';
                }
            }
            catch(\GuzzleHttp\Exception\ClientException $e){
                logger('getting themes throws client exception');
            }
            catch(\Exception $e){
                logger('getting themes throws exception');
            }
            return response()->json([
                'status' => 'error',
                'message' => $message
            ]);
        }
    }

    // ajax download theme function
    public function theme_download_post(Request $request){
        try {
            logger("theme_download_post");
            $shop = Auth::user();
            $latest_theme = Themes::orderBy('id', 'desc')->first();
            $count_themes = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_name',
                'Debutify '.$latest_theme->version)->where('status', 1)->count();

            $theme_name = '';
            if ($count_themes > 0) {
                $theme_name = 'Copy of Debutify '.$latest_theme->version;
            } else {
                $theme_name = 'Debutify '.$latest_theme->version;
            }

            $shop->theme_id = $latest_theme->id;
            $shop->theme_url = $latest_theme->url;
            $shop->save();

            $theme = array(
                'theme' => array(
                    'name' => $theme_name,
                    'src' => $latest_theme->url,
                )
            );

            try {
                $upload_theme_response = $shop->api()->request(
                    'POST',
                    '/admin/api/themes.json',
                    $theme
                )['body']['theme'];
            } catch (\Exception $e) {
                $upload_theme_response = $e->getMessage();
            }

            if (isset($upload_theme_response['id'])) {
                $shop->lastactivity = new DateTime();
                $shop->shopify_theme_id = $upload_theme_response['id'];
                $shop->theme_check = 1;
                $shop->save();
                $theme_count = StoreThemes::where('shopify_theme_id', $upload_theme_response['id'])->where('status',
                    1)->count();
                if ($theme_count == 0) {
                    $StoreTheme = new StoreThemes;
                    $StoreTheme->shopify_theme_id = $upload_theme_response['id'];
                    $StoreTheme->shopify_theme_name = $upload_theme_response['name'];
                    $StoreTheme->role = 0;
                    $StoreTheme->status = 1;
                    $StoreTheme->user_id = $shop->id;
                    $StoreTheme->save();
                }

                // Add trial days on first theme download
                //$theme_count_trial = StoreThemes::where('user_id', $shop->id)->where('status', 1)->count();
                // if($theme_count_trial == 1 && $shop->trial_ends_at != null){
                // $this->add_free_trail($request);
                // $this->mailerLite($shop, 'trial');
                //   $date = new DateTime();
                //   $new_formats_trial = $date->format('Y-m-d');
                //   $trial_end_days = date('Y-m-d', strtotime($new_formats_trial. ' + 14 days'));
                //   $shop->trial_days = 14;
                //   $shop->sub_trial_ends_at = 1;
                //   $shop->trial_ends_at = $trial_end_days;
                //   $shop->save();
                //  }

                $is_theme_processing = $upload_theme_response['processing'];
                if( !$is_theme_processing ){
                    return response()->json([
                        'status' => 'ok',
                        'message' => 'Theme added'
                    ]);
                }else{
                    do {
                        $is_theme_processing = $this->getThemeProcessing($upload_theme_response['id']);
                        sleep(2);   //wait for 5 sec for next function call
                        if( !$is_theme_processing ){
                            return response()->json([
                                'status' => 'ok',
                                'message' => 'Theme added'
                            ]);
                        }

                    } while($is_theme_processing === TRUE);
                }
            } else {
                try {
                    $shopify_themes = $shop->api()->request(
                        'GET',
                        '/admin/api/themes.json'
                    )['body']['themes'];
                    $theme_count = count($shopify_themes);
                    if ($theme_count >= 20) {
                        $message = 'Your online store has a maximum of 20 themes. Remove unused themes to add more.';
                    } else {
                        $message = 'Error Occurred. Please try again';
                    }
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    logger('getting themes throws client exception');
                } catch (\Exception $e) {
                    logger('getting themes throws exception');
                }
                return response()->json([
                    'status' => 'error',
                    'message' => $message
                ]);
                $request->session()->flash('error', $message);
            }

            // $StoreThemes = StoreThemes::where('user_id', $shop->id)->where('status', 1)->get();
            // $shopify_themes = array();
            // foreach ($StoreThemes as $theme) {
            //     $shopify_themes[] = array('label'=>$theme->shopify_theme_name, 'href'=>'/admin/themes/'.$theme->shopify_theme_id.'/editor', 'target'=>'new');
            // }
            // return Redirect::to('add_ons');
        }catch (\Exception $e) {
            dd($e);
        }

    }

    // is theme completely download or procesing
    public function getThemeProcessing($theme_id){
        $shop = Auth::user();
        $endPoint = '/admin/api/themes/'. $theme_id .'.json';
        try{
            $is_theme_processing  = $shop->api()->request('GET', $endPoint )['body']['theme'];
        }
        catch (\Exception $e) {
            $is_theme_processing = $e->getMessage();
        }
        return $is_theme_processing['processing'];
    }

    // get license key function
    public function getLicenseKey(Request $request){
        $shopDomain = $request->shopDomain;
        if(empty($shopDomain)){
            return response()->json([
                    'status' => 'shop domain empty',
                    'license_key' => 'empty'
            ]);
        } else {
            $shop = User::whereNotNull('shopify_token')->where('name', $shopDomain)->first();
            if(empty($shop)){
                 return response()->json([
                    'status' => 'uninstalled',
                    'license_key' => 'empty'
                ]);
            }
            else{
                //logger('its here='.$shop->license_key);
                return response()->json([
                    'status' => 'ok',
                    'license_key' => $shop->license_key
                ]);
            }
        }
    }

    // theme refresh function
    public function get_theme_refresh(StripePlan $plan,Request $request){
        $shop = Auth::user();
        // $latest_theme = Themes::orderBy('id', 'desc')->first();
        $user_id = $shop->id;
        //get themes from store
        $get_all_themes = $shop->api()->request(
              'GET',
              '/admin/api/themes.json'
        )['body']['themes'];
        //logger(json_encode($get_all_themes));

        foreach ($get_all_themes as $get_all_theme) {
            $themes_count = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_id', $get_all_theme->id)->count();
            if($themes_count == 0){
                try {
                    $schema = $shop->api()->request(
                        'GET',
                        '/admin/api/themes/'.$get_all_theme->id.'/assets.json',
                        ['asset' => ['key' => 'config/settings_schema.json'] ]
                    )['body']['asset']['value'];
                    //logger(json_encode($schema));
                    if( ( $pos = strrpos( $schema , 'debutify' ) ) !== false || ( $pos = strrpos( $schema , 'Debutify' ) ) !== false ) {
                        $StoreTheme = new StoreThemes;
                        $StoreTheme->shopify_theme_id = $get_all_theme->id;
                        $StoreTheme->shopify_theme_name = $get_all_theme->name;
                        $StoreTheme->role = 0;
                        $StoreTheme->status = 1;
                        $StoreTheme->user_id = $shop->id;
                        $StoreTheme->save();
                       // logger('theme create '.$get_all_theme->name);
                     }
                } catch(\GuzzleHttp\Exception\ClientException $e){
                  logger('theme created chron throws exception');
                }
            }
        }

        sleep(10);

        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('status', 1)->orderBy('role', 'desc')->orderBy('id', 'desc')->get();
        foreach ($StoreTheme as $theme) {
            try{
                $get_theme = $shop->api()->request(
                      'GET',
                      '/admin/api/themes/'.$theme->shopify_theme_id.'.json'
                )['body']['theme'];
               $StoreThemes[] = $theme;
            }
            catch(\GuzzleHttp\Exception\ClientException $e){
                $theme_updated = StoreThemes::where('shopify_theme_id', $theme->shopify_theme_id)->delete();
                if($shop->shopify_theme_id == $theme->id){
                    $shop->shopify_theme_id = null;
                    $shop->theme_id = null;
                    //$shop->theme_url = null;
                    $shop->save();
                }
                logger('theme not found on shopify store');
            }
            catch(\Exception $e){
                $theme_updated = StoreThemes::where('shopify_theme_id', $theme->shopify_theme_id)->delete();
                if($shop->shopify_theme_id == $theme->id){
                    $shop->shopify_theme_id = null;
                    $shop->theme_id = null;
                   // $shop->theme_url = null;
                    $shop->save();
                }
                logger('theme not found on shopify store');
            }
        }

        $theme_count = StoreThemes::where('user_id', $shop->id)->where('status',1)->count();
        $request->session()->flash('status', 'Theme library refreshed');
        return redirect()->route('theme_view');
    }

    // subscription function
    public function all_subscription(Request $request, StripePlan $plan){
        $shop = Auth::user();
        logger('all subscription shop='.$shop->name);
        Session::put('tax_user_id', $shop->id);
        // die();
        $shopData = $shop->api()->request(
            'GET',
            '/admin/api/shop.json',
            []
        )['body']['shop'];
        $tax_rates = array();
        if($shopData['country_name'] == 'Canada'){
          logger('domain='.$shop->name.', province='.$shopData['province']);
          // $tax_id = getTaxId($shopData->province);
          $tax = Taxes::where('region',$shopData['province'])->first();
          if($tax){
            $tax_id = $tax->stripe_taxid;
          }else{
            $tax = Taxes::where('region','New-Brunswick')->first();
            $tax_id = $tax->stripe_taxid;
          }
          logger('returning tax='.$tax_id);
          $tax_rates[] = $tax_id;
        }

        // address for stripe
        $address = array('line1' => $shopData['address1'], 'city' => $shopData['city'], 'country' => $shopData['country_name'], 'line2' => $shopData['address2'], 'postal_code' => $shopData['zip'], 'state' => $shopData['province']);
        $theme_count = StoreThemes::where('user_id', $shop->id)->where('status',1)->count();

        $isBrandNewSubscription = false;
        try{
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            // cancel a subscription if already Created
            if($shop->all_addons == 1){
                // Remove Child Store onchange master store plans
                if($shop->alladdons_plan == 'Guru' && $request->get('payment_cycle') != 'Guru'){
                  $shop->master_account = null;
                  $store_count = ChildStore::where('user_id', $shop->id)->count();
                  if($store_count > 0){
                    $child_stores = ChildStore::where('user_id', $shop->id)->get();
                    foreach ($child_stores as $key => $child_store) {
                      $shops = User::where('name', $child_store->store)->first();
                      if($shops){
                        $this->delete_all_addon($shops, 'child');
                      }
                    }
                    $child_store_d = ChildStore::where('user_id', $shop->id)->delete();
                  }
                }
                // subscription update
                $subscription = Subscription::where('user_id',$shop->id)->orderBy('id', 'desc')->first();

                // Delete Discount code on upgrade/downgrade
                $sub = \Stripe\Subscription::retrieve($subscription->stripe_id);
                if($sub->discount){
                  $deleteDiscount = $sub->deleteDiscount();
                }
                $subscription_stripe = \Stripe\Subscription::retrieve($subscription->stripe_id);

                // See what the next invoice would look like with a plan switch
                // upgrade/downgrade subscription
                if( $coupon = $request->get('new_coupon')) {
                    $update_subscription = \Stripe\Subscription::update($subscription->stripe_id, [
                    'coupon' => $request->get('new_coupon'),
                    'cancel_at_period_end' => null,
                    'proration_behavior' => "always_invoice",
                    'items' => [
                      [
                        'id' => $subscription_stripe->items->data[0]->id,
                        'plan' => $request->get('plan_id'),
                      ],
                    ],
                    'default_tax_rates' => $tax_rates,
                  ]);
                }else{
                    $update_subscription = \Stripe\Subscription::update($subscription->stripe_id, [
                    'cancel_at_period_end' => false,
                    'proration_behavior' => "always_invoice",
                    'items' => [
                      [
                        'id' => $subscription_stripe->items->data[0]->id,
                        'plan' => $request->get('plan_id'),
                      ],
                    ],
                    'default_tax_rates' => $tax_rates,
                  ]);
                }

                // Session::put('tax_user_id', $shop->id);
                // $shop->subscription('main')->syncTaxPercentage();

                // $shop->subscription('main')->cancelNow();
              // $shop->alladdons_plan = 'freemium';
              // $shop->sub_plan = null;
            }
            else{
              // if($shop->sub_trial_ends_at){
              //   $subscription = Subscription::where('user_id',$shop->id)->orderBy('id', 'desc')->first();
              //   $date = new DateTime();
              //   $new_format = $date->format('Y-m-d');
              //   $dates = new DateTime($subscription->trial_ends_at);
              //   $end_at = $dates->format('Y-m-d');

              //   if($end_at > $new_format && $subscription->trial_ends_at != null){
              //       $diff = strtotime($new_format) - strtotime($end_at);
              //       $trialDays = abs(round($diff / 86400));
              //       if( $coupon = $request->get('new_coupon') ) {
              //           $shop->newSubscription('main', $request->get('plan_id'))
              //           ->withMetadata(['lm_data' => $request->linkminkRef])
              //           ->trialDays($trialDays)
              //           ->withCoupon($coupon)
              //           ->create($request->stripeToken, [
              //               'email' => $request->get('email'),
              //               'description' => $shop->name,
              //               'name' => $shopData->shop_owner,
              //               'address' => $address,
              //               'phone' => $shopData->phone
              //           ]);
              //       }
              //       else {
              //           $shop->newSubscription('main', $request->get('plan_id'))
              //           ->withMetadata(['lm_data' => $request->linkminkRef])
              //           ->trialDays($trialDays)
              //           ->create($request->stripeToken, [
              //               'email' => $request->get('email'),
              //               'description' => $shop->name,
              //               'name' => $shopData->shop_owner,
              //               'address' => $address,
              //               'phone' => $shopData->phone
              //           ]);
              //       }
              //   } else {
              //       if( $coupon = $request->get('new_coupon') ) {
              //           $shop->newSubscription('main', $request->get('plan_id'))
              //           ->withMetadata(['lm_data' => $request->linkminkRef])
              //           ->withCoupon($coupon)
              //           ->create($request->stripeToken, [
              //               'email' => $request->get('email'),
              //               'description' => $shop->name,
              //               'name' => $shopData->shop_owner,
              //               'address' => $address,
              //               'phone' => $shopData->phone
              //           ]);
              //       }
              //       else{
              //           $shop->newSubscription('main', $request->get('plan_id'))
              //           ->withMetadata(['lm_data' => $request->linkminkRef])
              //           ->create($request->stripeToken, [
              //               'email' => $request->get('email'),
              //               'description' => $shop->name,
              //               'name' => $shopData->shop_owner,
              //               'phone' => $shopData->phone,
              //               'address' => $address

              //           ]);
              //       }
              //   }
              // }
              // else{
                $isBrandNewSubscription = true;
                if($shop->stripe_id == '' || $shop->stripe_id == null){
                  // $static_customer = \Stripe\Customer::create([
                  //   'description' => 'My First Test Customer (created for API docs)',
                  // ]);
                  logger('create new customer on stripe, email='.$request->get('email'));
                  $customer = \Stripe\Customer::create([
                    'source' => $request->stripeToken,
                    'email' => $request->get('email'),
                    'description' => $shop->name,
                    'name' => $shopData['shop_owner'],
                    'address' => $address,
                    'phone' => $shopData['phone'],
                  ]);
                }else{
                  logger('update existing customer on stripe');
                  $customer = \Stripe\Customer::update(
                      $shop->stripe_id,
                      [
                        'email' => $request->get('email'),
                        'description' => $shop->name,
                        'name' => $shopData['shop_owner'],
                        'address' => $address,
                        'phone' => $shopData['phone'],
                      ]
                    );
                }
                logger('customer created='.json_encode($customer));
                $shop->stripe_id = $customer->id;
                if( $coupon = $request->get('new_coupon') ) {
                	// $shop->newSubscription('main', $request->get('plan_id'))
                 //      ->withMetadata(['lm_data' => $request->linkminkRef])
                 //      ->withCoupon($coupon)
                 //      ->create($request->stripeToken, [
                 //          'email' => $request->get('email'),
                 //          'description' => $shop->name,
                 //          'name' => $shopData->shop_owner,
                 //          'address' => $address,
                 //          'phone' => $shopData->phone
                 //    ]);
                    $subscription_created = \Stripe\Subscription::create([
          					  'customer' => $shop->stripe_id,
          					  'coupon' => $coupon,
          					  'items' => [['plan' => $request->get('plan_id')]],
          					  'metadata' => ['lm_data' => $request->linkminkRef],
          					  'default_tax_rates' => $tax_rates,
          					]);
                }
                else{
                	// $shop->newSubscription('main', $request->get('plan_id'))
                 //    ->withMetadata(['lm_data' => $request->linkminkRef])
                 //    ->create($request->stripeToken, [
                 //        'email' => $request->get('email'),
                 //        'description' => $shop->name,
                 //        'name' => $shopData->shop_owner,
                 //        'address' => $address,
                 //        'phone' => $shopData->phone
                 //    ]);
                    $subscription_created = \Stripe\Subscription::create([
          					  'customer' => $shop->stripe_id,
          					  'items' => [['plan' => $request->get('plan_id')]],
          					  'metadata' => ['lm_data' => $request->linkminkRef],
          					  'default_tax_rates' => $tax_rates,
          					]);
                }
                $subscription = new Subscription;
                $subscription->user_id = $shop->id;
                $subscription->name = 'main';
                $subscription->stripe_id = $subscription_created->id;
                $subscription->stripe_plan = $request->get('plan_id');
                $subscription->quantity = 1;
                $subscription->save();
            }

            // Create new subscription
            $shop->sub_trial_ends_at = 0;
            $shop->trial_days = 0;
            $shop_owner = $shopData['shop_owner'];
            $shop_owner_name = explode(" ", $shop_owner);
            // Active campain
            $this->addactive_campaign($shop,$request->get('payment_cycle'),'new subc','');
            // $groupsApi = (new \MailerLiteApi\MailerLite(env('MAILERLITE_APIKEY')))->groups();

            // $groupId = env('MAILERLITE_GROUP_ID');

            // $subscriber = [
            //    'email' => $shopData->email,
            //    'fields' => [
            //        'name' => $shop_owner_name[0],
            //        'last_name' => $shop_owner_name[1],
            //        'company' => $shopData->domain,
            //        'city'=>$shopData->city,
            //        'country'=>$shopData->country_name,
            //        'phone'=>$shopData->phone,
            //        'state'=>$shopData->province,
            //        'zip'=>$shopData->zip,
            //        'subscription' => $request->get('payment_cycle'),
            //        'status' => 'active',
            //    ]
            // ];

            // $addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);
            /*--Save Shop Data---*/
        }
        catch(\Stripe\Error\Card $e) {
            logger("Since it's a decline, \Stripe\Error\Card will be caught");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\RateLimit $e) {
            logger("Too many requests made to the API too quickly");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\InvalidRequest $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            logger("Invalid parameters were supplied to Stripe's API, error=".$err['message']);
            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\Authentication $e) {
            logger("Authentication with Stripe's API failed(maybe you changed API keys recently)");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\ApiConnection $e) {
            logger("Network communication with Stripe failed");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\Base $e) {
            logger("Display a very generic error to the user, and maybe send yourself an email");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (Exception $e) {
            logger("Something else happened, completely unrelated to Stripe");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        }
        // echo "sub_plan=".$request->get('sub_plan');
        $shop->all_addons = 1;
        $shop->alladdons_plan = $request->get('payment_cycle');
        $shop->sub_plan = $request->get('sub_plan');
        // license key created
        $license_key = Hash::make(Str::random(12));
        $shop->license_key = $license_key;
        $shop->custom_domain = $shopData['domain'];
        $shop->save();

        if ($isBrandNewSubscription) {
            $request->session()->flash('isBrandNewSubscription', $isBrandNewSubscription);
        }

        if($request->get('payment_cycle') == 'guru'){
            $request->session()->flash('subscription', 'Guru');
        }
        elseif($request->get('payment_cycle') == 'hustler'){
            $request->session()->flash('subscription', 'Hustler');
        }
        else{
            $request->session()->flash('subscription', 'Starter');
        }

        $request->session()->flash('status', 'Subscription activated');

        // if($theme_count <= 0){
        //   $urls = 'theme_view';
        // } else{
          $urls = 'thankyou';
        // }

        return redirect()->route($urls);
    }

    // update credit card function
    public function updateCreditCard(Request $request, StripePlan $plan){
        $shop = Auth::user();
        try{
          \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
          $customer = \Stripe\Customer::update(
            $shop->stripe_id,
            ["source" => $request->stripeSource]
          );
          $request->session()->flash('status', 'Payment method updated');
          return redirect()->route('plans');
        }
        catch(\Stripe\Error\Card $e) {
            logger("Since it's a decline, \Stripe\Error\Card will be caught");
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $request->session()->flash('error', $err['message']);
            return redirect()->route('plans');
        } catch (\Stripe\Error\RateLimit $e) {
            logger("Too many requests made to the API too quickly");
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $request->session()->flash('error', $err['message']);
            return redirect()->route('plans');
        } catch (\Stripe\Error\InvalidRequest $e) {
            logger("Invalid parameters were supplied to Stripe's API");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('plans');
        } catch (\Stripe\Error\Authentication $e) {
            logger("Authentication with Stripe's API failed(maybe you changed API keys recently)");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('plans');
        } catch (\Stripe\Error\ApiConnection $e) {
            logger("Network communication with Stripe failed");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('plans');
        } catch (\Stripe\Error\Base $e) {
            logger("Display a very generic error to the user, and maybe send yourself an email");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('plans');
        } catch (Exception $e) {
            logger("Something else happened, completely unrelated to Stripe");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('plans');
        }
    }

    // delete add-on function
    public function delete_addons(Request $request){
        $shop = Auth::user();
        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('status', 1)->get();
        $addon = AddOns::where('user_id', $shop->id)->where('global_id', $request->get('addon_id'))->first();
        foreach ($StoreTheme as $theme) {
            try{
            $get_theme = $shop->api()->request(
                  'GET',
                  '/admin/api/themes/'.$theme->shopify_theme_id.'.json'
            )['body']['theme'];
               $StoreThemes[] = $theme;
            }catch(\GuzzleHttp\Exception\ClientException $e){
                logger('update schema on live view addon throws client exception');
            }
            catch(\Exception $e){
                logger('update schema on live view addon throws exception');
            }
        }
        $addon_count = AddOns::where('user_id', $shop->id)->where('status', 1)->count();
        $checkaddon = 0;
        if($addon_count <= 1)
        {
            $checkaddon =1;
        }else{
            $checkaddon = 0;
        }
        $addon->status = 0;
        $addon->shedule_time = 0;
        $addon->save();
        $update_addon= 0;
        $this->deactive_addons($shop,$StoreThemes,$request->get('addon_id'), $checkaddon, $update_addon);
        $request->session()->flash('status', 'Add-on uninstalled');
        return redirect()->route('theme_addons');
    }

    // install add-ons function
    public function install_addons(Request $request){
        $shop = Auth::user();
        // $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('status', 1)->get();
         $addon_count = AddOns::where('user_id', $shop->id)->where('global_id', $request->get('addon_id'))->count();

        if($addon_count==0){
            $addon = new AddOns;
            $addon->global_id = $request->get('addon_id');
            $addon->user_id = $shop->id;
            $addon->status = 1;
            $addon->shedule_time = 1;
            $addon->save();
        }else{
            $addon = AddOns::where('user_id', $shop->id)->where('global_id', $request->get('addon_id'))->first();
            $addon->status = 1;
            $addon->shedule_time = 1;
            $addon->save();
        }
        // save last activity in shop
        $shop->lastactivity = new DateTime();
        $shop->save();

        $update_addon = 0;
        $key = 0;
        $this->active_addons($request->get('addon_id'), $request->get('theme_id'),  $update_addon, $key);
        $request->session()->flash('status', 'Add-on installed');
        //****
        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_id', $request->get('theme_id'))->first();
        $global_add_ons = GlobalAddons::where('id', $request->get('addon_id'))->first();
        $request->session()->flash('message', 'You successfully installed '.$global_add_ons->title.' on '.$StoreTheme->shopify_theme_name);
        $request->session()->flash('theme_id_cstm', $request->get('theme_id'));
        //
        return redirect()->route('theme_addons');
    }

    // cancel subscription function
    public function cancel_all_subscription(Request $request, StripePlan $plan){
        $shop = Auth::user();
        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('status', 1)->get();
        logger("cancel all subscription");
        $shop->subscription('main')->cancelNow();
        // $shopData = $shop->api()->request(
        //         'GET',
        //         '/admin/api/shop.json',
        //         []
        //     )['body']->shop;
        //     $shop_owner = $shopData->shop_owner;
        //     $shop_owner_name = explode(" ", $shop_owner);
        // Active campain
          $this->addactive_campaign($shop,'Freemium','app', 'Canceled');
        // $groupsApi = (new \MailerLiteApi\MailerLite(env('MAILERLITE_APIKEY')))->groups();
        // $groupId = env('MAILERLITE_GROUP_ID');
        // $subscriber = [
        //    'email' => $shop->email,
        //    'fields' => [
        //        'subscription' => 'freemium',
        //        'status' => 'active'
        //    ]
        // ];

        // $addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);
        // Remove All addons from Child Store
        if($shop->alladdons_plan == 'Guru'){
            $shop->master_account = null;
            $store_count = ChildStore::where('user_id', $shop->id)->count();
            if($store_count > 0){
              $child_stores = ChildStore::where('user_id', $shop->id)->get();
              foreach ($child_stores as $key => $child_store) {
                $shops = User::where('name', $child_store->store)->first();
                if($shops){
                  $this->delete_all_addon($shops, 'child');
                }
              }
              $child_store_d = ChildStore::where('user_id', $shop->id)->delete();
            }
          }
        //delete shop addons from themes
        $this->delete_all_addon($shop, 'master');

        $request->session()->flash('status', 'Subscription canceled');
        //return redirect()->route('theme_addons');
        return redirect()->route('goodbye');
   }

    // install all add-ons function
    public function install_All_addons(Request $request){
        $shop = Auth::user();
        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_id', $request->get('theme_id'))->first();
      //  $activated_addons = AddOns::where('user_id', $shop->id)->where('status', 0)->get();
       // $global_add_ons_count = GlobalAddons::count();
       // $addons_count = AddOns::where('user_id', $shop->id)->count();
       // $delivery_addon_activated = AddOns::where('user_id',$shop->id)->where('global_id', 4)->where('status',1)->count();
       // $user_id = $shop->id;
        $checkaddon = 0;
       // $global_add_ons = GlobalAddons::pluck('id')->toArray();
       // $diff_addons=array_diff($global_add_ons,$request->get('addons'));
       // logger(json_encode($diff_addons));
         $update_addon =0;
         //$addons_name = array();
            /* save in store */
            foreach ($request->get('addons') as $key => $mltaddons) {
              sleep(1);
              $this->active_addons($mltaddons, $request->get('theme_id'),  $update_addon, $key);
            }
            // foreach ($diff_addons as $key => $mltaddons) {
            //   sleep(1);
            //   $this->deactive_addons($shop,$StoreTheme,$mltaddons, $checkaddon, $update_addon, $key);
            // }
            // if($addons_count== $global_add_ons_count){
            //     foreach ($activated_addons as $key=> $addon) {
            //         $this->active_addons($addon->global_id, $request->get('theme_id'),  $update_addon, $key);
            //         $addons_name = array($addon->global_id);
            //     }
            // }else{
            //     foreach ($global_add_ons as $key=> $global) {
            //         $this->active_addons($global->id, $request->get('theme_id'), $update_addon, $key);
            //         $addons_name = array($global->id);
            //     }
            // }
            /* save in database */

            $shop->lastactivity = new DateTime();
            $shop->save();
            foreach ($request->get('addons') as $global) {
              $addon_count = AddOns::where('user_id', $shop->id)->where('global_id', $global)->count();
              if($addon_count == 0){
                  $addon = new AddOns;
                  $addon->global_id = $global;
                  $addon->user_id = $shop->id;
                  $addon->status = 1;
                  $addon->shedule_time = 1;
                  $addon->invoiceitem = null;
                  $addon->save();
              }else{
                $addon = AddOns::where('user_id', $shop->id)->where('global_id', $global)->first();
                $addon->status = 1;
                $addon->shedule_time = 1;
                $addon->invoiceitem = null;
                $addon->save();
              }
            }
            $request->session()->flash('status', 'All add-ons installed');
            //$global_add_ons = GlobalAddons::where('id', $request->get('addon_id'))->first();
            $request->session()->flash('message', 'You successfully installed all Add-Ons on '.$StoreTheme->shopify_theme_name);
            $request->session()->flash('theme_id_cstm', $request->get('theme_id'));
            return redirect()->route('theme_addons');
    }

    // update add-on function
    public function update_addons(Request $request){
        $shop = Auth::user();
        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_id', $request->get('theme_id'))->where('status', 1)->get();
        $delivery_addon_activated = AddOns::where('user_id',$shop->id)->where('global_id', 4)->where('status',1)->count();
        foreach ($StoreTheme as $theme) {
                try{
                $get_theme = $shop->api()->request(
                      'GET',
                      '/admin/api/themes/'.$theme->shopify_theme_id.'.json'
                )['body']['theme'];
                   $StoreThemes[] = $theme;
                }catch(\GuzzleHttp\Exception\ClientException $e){
                    logger('update schema on live view addon throws client exception');
                }
                catch(\Exception $e){
                    logger('update schema on live view addon throws exception');
                }
            }
        // save last activity in shop
        $shop->lastactivity = new DateTime();
        $shop->save();

        $checkaddon = 0;
        $update_addon = 1;
        $key = 0;
        $this->deactive_addons($shop,$StoreThemes,$request->get('addon_id'), $checkaddon, $update_addon);
        sleep(3);
        $this->active_addons($request->get('addon_id'),$request->get('theme_id'),  $update_addon, $key);
        $request->session()->flash('status', 'Add-on updated');
        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_id', $request->get('theme_id'))->first();
        $global_add_ons = GlobalAddons::where('id', $request->get('addon_id'))->first();
      $request->session()->flash('message', 'You successfully updated '.$global_add_ons->title.' on '.$StoreTheme->shopify_theme_name);
      $request->session()->flash('theme_id_cstm', $request->get('theme_id'));
         return redirect()->route('theme_addons');
    }

    // cancel subscription function
    public function cancel_subscription(Request $request){
        $shop = Auth::user();
        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('status', 1)->get();
        foreach ($StoreTheme as $theme) {
                try{
                $get_theme = $shop->api()->request(
                      'GET',
                      '/admin/api/themes/'.$theme->shopify_theme_id.'.json'
                )['body']['theme'];
                   $StoreThemes[] = $theme;
                }catch(\GuzzleHttp\Exception\ClientException $e){
                    logger('update schema on live view addon throws client exception');
                }
                catch(\Exception $e){
                    logger('update schema on live view addon throws exception');
                }
            }
        $addon = AddOns::where('user_id', $shop->id)->where('global_id', $request->get('addon_id'))->first();
        $globaladdon = GlobalAddons::where('id', $request->get('addon_id'))->first();

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
      //  \Stripe\Stripe::setApiVersion("2019-05-16");
        $subscription = \Stripe\Subscription::all(['limit' => 1]);
       $date = new DateTime();
        $date->setTimestamp($subscription->data[0]['billing_cycle_anchor']);
        $new_format = $date->format('d');
        $invoice_id =$addon->invoiceitem;
        logger(json_encode($invoice_id));
        $price =  $globaladdon->price/100;
        $dt = new DateTime();
        $day = $dt->format('d');
        $totaldays = '';
        if($day < $new_format)
        {
          $totaldays= $day + 7;
        }
        else{
            $totaldays = $new_format - $day;
            $totaldays = $totaldays * -1;
        }
        $checkaddon = 0;
        $total = ($price/30)*($totaldays);
        $amount1 =number_format($total, 2);
        $amount = $amount1* 100;

        $addon->status = 0;
        $addon->shedule_time = 0;
        $addon->save();

        $addon_count = AddOns::where('user_id', $shop->id)->where('status', 1)->count();
        try{
            if($addon_count < 1)
            {
                // Check if Last addons activated
                $checkaddon = 1;
                $shop->license_key = null;
                $shop->alladdons_plan = 'freemium';
                $shop->save();
                // $shopData = $shop->api()->request(
                //   'GET',
                //   '/admin/api/shop.json',
                //   []
                // )['body']->shop;
                // $shop_owner = $shopData->shop_owner;
                // $shop_owner_name = explode(" ", $shop_owner);
                // Active campain
                $this->addactive_campaign($shop,'Freemium','app', 'Canceled');

                // $groupsApi = (new \MailerLiteApi\MailerLite(env('MAILERLITE_APIKEY')))->groups();

                // $groupId = env('MAILERLITE_GROUP_ID');

                // $subscriber = [
                //    'email' => $shop->email,
                //    'fields' => [
                //        'subscription' => 'freemium',
                //        'status' => 'active'
                //    ]
                // ];

                // $addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);

            }
            else{
                // Check if more than one addons activated
                $checkaddon = 0;
           }

        }catch(\Stripe\Error\Card $e) {
            logger("Since it's a decline, \Stripe\Error\Card will be caught");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\RateLimit $e) {
            logger("Too many requests made to the API too quickly");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\InvalidRequest $e) {
            logger("Invalid parameters were supplied to Stripe's API");
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\Authentication $e) {
            logger("Authentication with Stripe's API failed(maybe you changed API keys recently)");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\ApiConnection $e) {
            logger("Network communication with Stripe failed");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (\Stripe\Error\Base $e) {
            logger("Display a very generic error to the user, and maybe send yourself an email");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        } catch (Exception $e) {
            logger("Something else happened, completely unrelated to Stripe");
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $request->session()->flash('error', $err['message']);
            return redirect()->route('theme_addons');
        }
        $update_addon= 0;
        $this->deactive_addons($shop,$StoreThemes,$request->get('addon_id'), $checkaddon, $update_addon);
        $request->session()->flash('status', 'Add-on uninstalled');

        return redirect()->route('theme_addons');
    }

    // mailerLite function
    public function mailerLite($shop, $subs){
      //$groupsApi = (new \MailerLiteApi\MailerLite(env('MAILERLITE_APIKEY')))->groups();
      // print($shopData->email);
      //$groupId = env('MAILERLITE_GROUP_ID');
      // if($subs == 'trial'){
      //     $subscriber = [
      //       'email' => $shop->email,
      //       'fields' => [
      //           'subscription' => $subs,
      //           'status' => 'active'
      //        ]
      //     ];
      // } else{
      //   $subscriber = [
      //      'email' => $shop->email,
      //      'fields' => [
      //          'subscription' => 'freemium',
      //          'status' => 'active'
      //      ]
      //   ];
      // }

      // Active campain
      // if($subs == 'trial'){
      //   $subscriber ='Trial';
      // }else{
      //   $subscriber ='Freemium';
      // }
      //$addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);
     // logger(json_encode($addedSubscriber));
    }

    // update all add-ons function
    public function update_Active_addons(Request $request){
        $shop = Auth::user();
        $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_id', $request->get('theme_id'))->where('status', 1)->get();
        // $StoreThemes =[];
        // foreach ($StoreTheme as $theme) {
        //     try{
        //     $get_theme = $shop->api()->request(
        //           'GET',
        //           '/admin/api/themes/'.$theme->shopify_theme_id.'.json'
        //     )['body']['theme'];
        //        $StoreThemes[] = $theme;
        //     }catch(\GuzzleHttp\Exception\ClientException $e){
        //         logger('update schema on live view addon throws client exception');
        //     }
        //     catch(\Exception $e){
        //         logger('update schema on live view addon throws exception');
        //     }
        // }
       // $activated_addons = AddOns::where('user_id', $shop->id)->where('status', 1)->get();

        // save last activity in shop
        $shop->lastactivity = new DateTime();
        $shop->save();

        if($StoreTheme){
          $checkaddon = 0;
          $update_addon= 1;
          foreach ($request->get('addons') as $key => $mltaddons) {
            $this->deactive_addons($shop,$StoreTheme,$mltaddons, $checkaddon, $update_addon, $key);
          }
          sleep(1);
          foreach ($request->get('addons') as $key => $mltaddons) {
            $this->active_addons($mltaddons, $request->get('theme_id'),  $update_addon, $key);
          }
        }
        // foreach ($activated_addons as $key=>$addon) {
          //    $this->deactive_addons($shop,$StoreThemes,$addon->global_id, $checkaddon, $update_addon, $key);
          // }
        //sleep(3);
        // foreach ($activated_addons as $key=>$addon) {
        //     $this->active_addons($addon->global_id, $request->get('theme_id'),  $update_addon, $key);
        //     $global_add_ons = GlobalAddons::where('id', $addon->global_id)->first();
        //     $addon_name[] = $global_add_ons->title;
        // }
        $request->session()->flash('status', 'All add-ons updated');

      // $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('shopify_theme_id', $request->get('theme_id'))->first();
        //$name_string = implode(",",$addon_name);
        $request->session()->flash('theme_id_cstm', $request->get('theme_id'));
        $request->session()->flash('message', 'You successfully updated all Add-Ons on '.$StoreTheme[0]['shopify_theme_name']);

        return redirect()->route('theme_addons');
    }

    // add linked store function
    public function addchildstore(Request $request){
        $shop = Auth::user();
        logger('Add master shop='.$shop->name);
        if($shop->name == $request->get('childstore')){
            $request->session()->flash('error', $request->get('childstore').' is a master account');
            return redirect()->route('plans');
        }
        $shops = User::where('name', $request->get('childstore'))->first();
        if($shops){
          if($shops->alladdons_plan != 'freemium' && $shops->alladdons_plan != null){
            $request->session()->flash('error', 'This store already has an active subscription');
            return redirect()->route('plans');
          }
        }
        $storecount = ChildStore::where('store', $request->get('childstore'))->count();
        if($storecount > 0){
          $request->session()->flash('error', 'This store is already linked');
          return redirect()->route('plans');
        }
        $shop->master_account = 1;
        $shop->save();
        $store_count = ChildStore::where('user_id', $shop->id)->count();
        if($store_count < 3){
          $stores = new ChildStore;
          $stores->store = $request->get('childstore');
          $stores->user_id = $shop->id;
          $stores->save();
        }

        $request->session()->flash('status', 'Store licence shared');
        return redirect()->route('plans');
    }

    // remove linked store function
    public function removechildstore(Request $request){
        $id = $request->get('store_id');
        $shops = User::where('name', $request->get('child_store'))->first();
        $stores = ChildStore::find($id);
        $stores->delete();
        if($shops){
          $this->delete_all_addon($shops, 'child');
        }
        $request->session()->flash('status', 'Store licence removed');
        return redirect()->route('plans');
    }

    // Delete all addons function
    public function delete_all_addon($shops, $action){
        $StoreTheme = StoreThemes::where('user_id', $shops->id)->where('status', 1)->get();
        $shops->all_addons = null;
        $shops->alladdons_plan = 'freemium';
        $shops->sub_plan = null;
        $shops->license_key = null;
        // if($action == 'child'){
        //  $shops->sub_trial_ends_at = null;
        // }
        $shops->save();
        $addons = AddOns::where('user_id', $shops->id)->where('status', 1)->get();
        $checkaddon = 1;
        // foreach ($StoreTheme as $theme) {
        //     try{
        //         // $get_theme = $shops->api()->request(
        //         //       'GET',
        //         //       '/admin/api/themes/'.$theme->shopify_theme_id.'.json'
        //         // )['body']['theme'];
        //         $StoreThemes[] = $theme;
        //     }catch(\GuzzleHttp\Exception\ClientException $e){
        //         logger('update schema on live view addon throws client exception');
        //     }
        //     catch(\Exception $e){
        //         logger('update schema on live view addon throws exception');
        //     }
        // }
        $update_addon= 0;
        foreach ($addons as $addon) {
          sleep(2);
            $this->deactive_addons($shops,$StoreTheme,$addon->global_id, $checkaddon, $update_addon);
            $addon->status = 0;
            $addon->shedule_time = 0;
            $addon->save();
        }
    }

    // subscription coupon function
    public function applycoupon(Request $request){
        try{
            $shop = Auth::user();
          $shopData = $shop->api()->request(
            'GET',
            '/admin/api/shop.json',
            []
          )['body']['shop'];
          $tax_rates = array();
          if($shopData['country_name'] == 'Canada'){
            logger('domain='.$shop->name.', province='.$shopData['province']);
            // $tax_id = getTaxId($shopData->province);
            $tax = Taxes::where('region',$shopData['province'][0]);
            if($tax){
              $tax_id = $tax->stripe_taxid;
            }else{
              $tax = Taxes::where('region','New-Brunswick')->first();
              $tax_id = $tax->stripe_taxid;
            }
            logger('returning tax='.$tax_id);
            $tax_rates[] = $tax_id;
          }
          \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
          $subscription = Subscription::where('user_id',$shop->id)->orderBy('id', 'desc')->first();
          $done = \Stripe\Subscription::update($subscription->stripe_id, [
              'coupon' => $request->get('subscription_coupon'),
              'default_tax_rates' => $tax_rates,
          ]);
          // $request->session()->flash('status', $request->get('subscription_coupon').' coupon code applied');
          $request->session()->flash('status', 'Coupon code applied');
          return redirect()->route('plans');
        }
        catch(\Stripe\Error\Card $e) {
           // logger("Since it's a decline, \Stripe\Error\Card will be caught");
            $request->session()->flash('error', "Invalid coupon code");
            return redirect()->route('plans');
        } catch (\Stripe\Error\RateLimit $e) {
           // logger("Too many requests made to the API too quickly");
            $request->session()->flash('error', "Invalid coupon code");
            return redirect()->route('plans');
        } catch (\Stripe\Error\InvalidRequest $e) {
            //logger("Invalid parameters were supplied to Stripe's API");
            $request->session()->flash('error', "Invalid coupon code");
            return redirect()->route('plans');
        } catch (\Stripe\Error\Authentication $e) {
            //logger("Authentication with Stripe's API failed(maybe you changed API keys recently)");
            $request->session()->flash('error', "Invalid coupon code");
            return redirect()->route('plans');
        } catch (\Stripe\Error\ApiConnection $e) {
            //logger("Network communication with Stripe failed");
            $request->session()->flash('error', "Invalid coupon code");
            return redirect()->route('plans');
        } catch (\Stripe\Error\Base $e) {
           logger("Display a very generic error to the user, and maybe send yourself an email");
            $request->session()->flash('error', "Invalid coupon code");
            return redirect()->route('plans');
        } catch (Exception $e) {
            logger("Something else happened, completely unrelated to Stripe");
            $request->session()->flash('error', "Invalid coupon code");
            return redirect()->route('plans');
        }
    }

    // new coupon functionn
    public function getcoupon(Request $request){
        try{
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $valid = \Stripe\Coupon::retrieve($request->get('new_coupon'));

            if($valid->valid){
              $coupon_name = $valid->name;
              $coupon_duration = $valid->duration;
              $coupon_duration_months = $valid->duration_in_months;
                if($valid->percent_off != null){
                    return response()->json([
                    // 'status' => $request->get('new_coupon').' coupon code applied',
                    'status' => 'Coupon code applied',
                    'percent_off' => $valid->percent_off,
                    'coupon_name' => $coupon_name,
                    'coupon_duration' => $coupon_duration,
                    'coupon_duration_months' => $coupon_duration_months
                  ]);
                } else{
                  return response()->json([
                    // 'status' => $request->get('new_coupon').' coupon code applied',
                    'status' => 'Coupon code applied',
                    'amount_off' => $valid->amount_off,
                    'coupon_name' => $coupon_name,
                    'coupon_duration' => $coupon_duration,
                    'coupon_duration_months' => $coupon_duration_months
                ]);
                }
            } else{
                return "invalid coupon code";
            }
        }
        catch(\Stripe\Error\Card $e) {
            logger("Since it's a decline, \Stripe\Error\Card will be caught");
            return "invalid coupon code";
        } catch (\Stripe\Error\RateLimit $e) {
            logger("Too many requests made to the API too quickly");
            return "invalid coupon code";
        } catch (\Stripe\Error\InvalidRequest $e) {
            logger("Invalid parameters were supplied to Stripe's API");
            return "invalid coupon code";
        } catch (\Stripe\Error\Authentication $e) {
            logger("Authentication with Stripe's API failed(maybe you changed API keys recently)");
             return "invalid coupon code";
        } catch (\Stripe\Error\ApiConnection $e) {
            logger("Network communication with Stripe failed");
            return "invalid coupon code";
        } catch (\Stripe\Error\Base $e) {
            logger("Display a very generic error to the user, and maybe send yourself an email");
            return "invalid coupon code";
        } catch (Exception $e) {
            logger("Something else happened, completely unrelated to Stripe");
             return "invalid coupon code";
        }
    }

    // delete multiple addons function
    function delete_multipl_addons(Request $request){
        $shop = Auth::user();
      $StoreTheme = StoreThemes::where('user_id', $shop->id)->where('status', 1)->get();
      foreach ($StoreTheme as $theme) {
          try{
          $get_theme = $shop->api()->request(
                'GET',
                '/admin/api/themes/'.$theme->shopify_theme_id.'.json'
          )['body']['theme'];
             $StoreThemes[] = $theme;
          }catch(\GuzzleHttp\Exception\ClientException $e){
              logger('update schema on live view addon throws client exception');
          }
          catch(\Exception $e){
              logger('update schema on live view addon throws exception');
          }
      }

      foreach ($request->get('addons') as $key => $mltaddons) {
        $addon = AddOns::where('user_id', $shop->id)->where('global_id', $mltaddons)->first();
        $addon_count = AddOns::where('user_id', $shop->id)->where('status', 1)->count();
        $checkaddon = 0;
        if($addon_count <= 1)
        {
            $checkaddon =1;
        }else{
            $checkaddon = 0;
        }
        $addon->status = 0;
        $addon->shedule_time = 0;
        $addon->save();
        $update_addon= 0;
        $this->deactive_addons($shop,$StoreThemes,$mltaddons, $checkaddon, $update_addon);
      }
      $request->session()->flash('status', 'Add-on uninstalled');
      return redirect()->route('plans');
    }

    // add free trial days function
    function add_free_trail(Request $request){
        $shop = Auth::user();
        //$this->mailerLite($shop, 'trial');
        $this->addactive_campaign($shop,'Trial','app', '');
        $date = new DateTime();
        $new_formats_trial = $date->format('Y-m-d');
        $trial_end_days = date('Y-m-d', strtotime($new_formats_trial. ' + 14 days'));
        $shop->trial_days = 14;
        $shop->trial_ends_at = $trial_end_days;
        $shop->sub_trial_ends_at = 1;
        $shop->trial_check = 0;
        $shop->save();
        $request->session()->flash('trial');
        $request->session()->flash('status', 'Free trial activated');
        return redirect()->route('theme_addons');
    }

    //update trial function
    function update_trail(){
      //logger("trial_check function");
      $shop = Auth::user();
      $shop->trial_check = 1;
      $shop->save();
    }

    //app installed check
    function update_themecheck(){
      //logger("trial_check function");
      $shop = Auth::user();
      $shop->theme_check = 1;
      $shop->save();
    }

    //active campaing function
    public function addactive_campaign($shop,$Subscription, $app, $tags){
      $shopData = $shop->api()->request(
          'GET',
          '/admin/api/shop.json',
          []
      )['body']['shop'];
      $shop_owner = $shopData['shop_owner'];
      $shop_owner_name = explode(" ", $shop_owner);
        $shop_owner = $shopData['shop_owner'];
        $shop_owner_name = explode(" ", $shop_owner);
        $post = array(
          'email'                    => $shopData['email'],
          'first_name'               => $shop_owner_name[0],
          'last_name'                => $shop_owner_name[1],
          'phone'                    => $shopData['phone'],
          'field[1,0]'               => $Subscription, //Subscription
          'field[6,0]'               => $shopData['domain'], // Company
          'field[7,0]'               => 'Installed', //App Status
          'field[9,0]'               => $shopData['country_name'], //country
          'field[10,0]'              => $shopData['city'], //city
          'field[11,0]'              => $shopData['zip'], //zip
          'field[12,0]'              => $shopData['address1'], //Address line 1
          'field[13,0]'              => $shopData['address2'], //Address line 2
          'field[14,0]'              => $shopData['province'], //Province
          'field[15,0]'              => $shopData['primary_locale'], //Language
          'field[16,0]'              => $shopData['name'], //Store name
          'tags'                     => $tags, //tags
          'p[3]'                     => env('APP_USER'),
      );
        logger(json_encode($post));
      $api_action = 'contact_edit';
      ActiveCampaignJob::dispatch($shop->email, $app, $post, $api_action);
    }

    // product research saaturation filer function
    public function changeSaturation(){
      $products = WinningProduct::orderBy('id', 'desc')->get();

      foreach ($products as $key => $product) {
        if($product->saturationlevel == 'low'){
          $product->saturationlevel = 'gold';
        }elseif ($product->saturationlevel == 'medium') {
          $product->saturationlevel = 'silver';
        }elseif ($product->saturationlevel == 'high') {
          $product->saturationlevel = 'bronze';
        }
        $product->save();
      }
      echo "all products updated successfully";
    }

    // prorated stripe plan upgrade function
    public function proratedAmount(Request $request){
      $shop = Auth::user();
      $subscription = Subscription::where('user_id',$shop->id)->orderBy('id', 'desc')->first();
      \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
      $proration_date = time();
      logger($subscription->stripe_id);
      $subscription_stripe = \Stripe\Subscription::retrieve($subscription->stripe_id);
        // and proration set:
      $items = [
          [
              'id' => $subscription_stripe->items->data[0]->id,
              'plan' => $request->get('plan_id'), # Switch to new plan
          ],
      ];

      $invoice = \Stripe\Invoice::upcoming([
          'customer' => $subscription_stripe->customer,
          'subscription' => $subscription->stripe_id,
          'subscription_items' => $items,
          'subscription_proration_date' => $proration_date,
      ]);

      logger('invoice='.json_encode($invoice));
      // Calculate the proration cost:
      $cost = 0;
      $current_prorations = [];
      $cost = $invoice->lines->data[0]->amount;
      // foreach ($invoice->lines->data as $line) {
      //   //logger($line->period->start);
      //   //logger($proration_date);
      //     if ($line->period->start - $proration_date <= 1) {
      //         array_push($current_prorations, $line);
      //         $cost += $line->amount;
      //     }
      // }
      logger('prorated amount='.json_encode($cost));
      $amount = $cost/100;
      return response()->json([
          'status' => 'success',
          'prorated_amount' => $amount
      ]);
      //die;
    }

    public function updateAllShopSubscriptions() {
      /*
      $shops = User::whereNotNull('shopify_token')->get();
        // logger("Updation canada shops subscriptions started from cron");
        foreach ($shops as $key => $shop) {
            try{
                $shopData = $shop->api()->request(
                    'GET',
                    '/admin/api/shop.json',
                    []
                )['body']->shop;
                $tax_rates = array();
                if($shopData->country_name == 'Canada'){
                  // $tax_id = getTaxId($shopData->province);
                  $tax = Taxes::where('region',$shopData->province)->first();
                  if($tax){
                    $tax_id = $tax->stripe_taxid;
                  }else{
                    $tax = Taxes::where('region','New-Brunswick')->first();
                    $tax_id = $tax->stripe_taxid;
                  }
                  echo 'id='.$shop->id.' ,domain='.$shop->name.', province='.$shopData->province.', returning tax='.$tax_id."<br>";
                  logger('id='.$shop->id.' ,domain='.$shop->name.', province='.$shopData->province.', returning tax='.$tax_id);
                  $tax_rates[] = $tax_id;
                  $subscription = Subscription::where('user_id',$shop->id)->orderBy('id', 'desc')->first();
                  if($subscription){
                    try{
                      \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                      // Delete Discount code on upgrade/downgrade
                      $sub = \Stripe\Subscription::retrieve($subscription->stripe_id);
                      if($sub->discount){
                        $deleteDiscount = $sub->deleteDiscount();
                      }
                      $subscription_stripe = \Stripe\Subscription::retrieve($subscription->stripe_id);
                      // echo "<pre>";
                      // print_r($subscription_stripe);
                      // echo "</pre>";
                      if (empty($subscription_stripe->default_tax_rates)) {
                        $update_subscription = \Stripe\Subscription::update($subscription->stripe_id, [
                          'cancel_at_period_end' => false,
                          'items' => [
                            [
                              'id' => $subscription_stripe->items->data[0]->id,
                              'plan' => $subscription_stripe->plan->id,
                            ],
                          ],
                          'default_tax_rates' => $tax_rates,
                        ]);
                        echo ' updated subscription with tax, strip id='.$subscription->stripe_id." <br>";
                        logger(' updated subscription with tax, strip id='.$subscription->stripe_id);
                      }else{
                        print_r($subscription_stripe->default_tax_rates);
                        logger('tax already exist='.json_encode($subscription_stripe->default_tax_rates));
                      }
                    }catch(\Stripe\Error\Card $e) {
                       logger("Since it's a decline, \Stripe\Error\Card will be caught, strip id=".$subscription->stripe_id);
                    } catch (\Stripe\Error\RateLimit $e) {
                       logger("Too many requests made to the API too quickly, strip id=".$subscription->stripe_id);
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        logger("Invalid parameters were supplied to Stripe's API, strip id=".$subscription->stripe_id);
                    } catch (\Stripe\Error\Authentication $e) {
                        logger("Authentication with Stripe's API failed(maybe you changed API keys recently), strip id=".$subscription->stripe_id);
                    } catch (\Stripe\Error\ApiConnection $e) {
                        logger("Network communication with Stripe failed, strip id=".$subscription->stripe_id);
                    } catch (\Stripe\Error\Base $e) {
                      logger("Display a very generic error to the user, and maybe send yourself an email, strip id=".$subscription->stripe_id);
                    } catch (Exception $e) {
                      logger("Something else happened, completely unrelated to Stripe, strip id=".$subscription->stripe_id);
                    }
                  }
                }
                // die();
              }catch(\GuzzleHttp\Exception\ClientException $e){
                logger("shop ClientException, shop id=".$shop->id);
              }catch(\GuzzleHttp\Exception\RequestException $e){
                logger("shop RequestException, shop id=".$shop->id);
              }catch (Exception $e) {
                logger("Something else happened, completely unrelated to Stripe, shop id=".$shop->id);
              }
      }
*/
      $shop = User::where('name','test-raph1.myshopify.com')->first();
      $Subscription = Subscription::where('user_id',$shop->id)->get();
      echo "<pre>";
      print_r($Subscription);
      echo "</pre>";
  }
}
?>
