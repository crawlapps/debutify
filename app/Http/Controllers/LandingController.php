<?php

namespace App\Http\Controllers;

use View;
use Redirect;
use Storage;
use App\User;
use App\Themes;
use App\Contact;
use App\StripePlan;
use App\GlobalAddons;
use App\Jobs\ActiveCampaignJob;
use Illuminate\Http\Request;

class LandingController extends Controller{
    /**
     * Show the application landing page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __construct(){
      $nbShops = User::count();
      $nbAddons = GlobalAddons::count();
      $global_add_ons = GlobalAddons::orderBy('title')->get();
      $StripePlan = StripePlan:: all();

      foreach ($StripePlan as $plan) {
          if($plan->name =='starterPriceAnnually'){
               $starterPriceAnnually = $plan->cost;
               $starteridAnnually = $plan->stripe_plan;
          }
          if($plan->name =='starterPriceMonthly'){
              $starterPriceMonthly = $plan->cost;
              $starteridMonthly = $plan->stripe_plan;
          }
          if($plan->name =='hustlerPriceAnnually'){
              $hustlerPriceAnnually = $plan->cost;
              $hustleridAnnually = $plan->stripe_plan;
          }
          if($plan->name =='hustlerPriceMonthly'){
              $hustlerPriceMonthly = $plan->cost;
              $hustleridMonthly = $plan->stripe_plan;
          }
          if($plan->name =='guruPriceAnnually'){
              $guruPriceAnnually = $plan->cost;
              $guruidAnnually = $plan->stripe_plan;
          }
          if($plan->name =='guruPriceMonthly'){
              $guruPriceMonthly = $plan->cost;
              $guruidMonthly = $plan->stripe_plan;
          }
        }

        $starter = 'Starter';
        $hustler = 'Hustler';
        $guru = 'Guru';
        $starterLimit = '3';
        $hustlerLimit = $nbAddons;
        $guruLimit = $nbAddons;

        //$screenshots = File::allFiles('/images/testimonials/');
        $screenshots = Storage::allFiles('/images/testimonials/');

        $testimonials = [
          "10" => "7abhQECTmWQ",
          "9" => "GDj9vQ05Q0U",
          "8" => "nk6yMbcK2G0",
          "7" => "ozJIYWIlCxA",
          "6" => "DUaPaf1-ylY",
          "5" => "38wCMRKWcYA",
          "4" => "yFD6Mx32PUA",
          "3" => "9NTcHMWhSsI",
          "2" => "1xRSQid9sHI",
          "1" => "MWfxWA0KdNw"
        ];

        $youtubers = [
          "12" => "ATwNAUd9L20",
          "11" => "XCgV9Vg9wD0",
          "10" => "KhZFUa7dmHQ",
          "9" => "wro6hcas3KQ",
          "8" => "_ktBYMWbHcQ",
          "7" => "GbWKoWto1qg",
          "6" => "8EjWCReZBB0",
          "5" => "O_8z7qflxDU",
          "4" => "Q9DGlJ1IcL8",
          "3" => "Li5Ogqr8Wzc",
          "2" => "VcZNtIqh3tU",
          "1" => "bD9dHm0fp0M"
        ];

        View::share([
          'nbShops' => $nbShops,
          'starter' => $starter,
          'hustler' => $hustler,
          'guru' => $guru,
          'starterPriceAnnually' => $starterPriceAnnually,
          'starterPriceMonthly' => $starterPriceMonthly,
          'hustlerPriceAnnually' => $hustlerPriceAnnually,
          'hustlerPriceMonthly' => $hustlerPriceMonthly,
          'guruPriceAnnually' => $guruPriceAnnually,
          'guruPriceMonthly' => $guruPriceMonthly,
          'starteridAnnually' => $starteridAnnually,
          'starteridMonthly' => $starteridMonthly,
          'hustleridAnnually' => $hustleridAnnually,
          'hustleridMonthly' => $hustleridMonthly,
          'guruidAnnually' => $guruidAnnually,
          'guruidMonthly' => $guruidMonthly,
          'testimonials' => $testimonials,
          'screenshots' => $screenshots,
          'youtubers' => $youtubers,
          'nbAddons' => $nbAddons,
          'starterLimit' => $starterLimit,
          'hustlerLimit' => $hustlerLimit,
          'guruLimit' => $guruLimit,
          'global_add_ons' => $global_add_ons
        ]);
    }

    // home view
    public function landing(){
        $latest_theme = Themes::orderBy('id', 'desc')->first();

        return view('landing.landing',[
          'version' => $latest_theme->version
        ]);
    }

    // privacy view
    public function old(){
        return view('landing-old');
    }

    // privacy view
    public function privacy(){
        return view('landing.privacy');
    }

    // download view
    public function download(){
      return view('landing.download');
    }

    // chat view
    public function chat(){
        return view('landing.chat');
    }

    // theme view
		public function theme(){
        return view('landing.theme');
    }

    // terms view
    public function terms(){
				return view('landing.terms');
    }

    // sales terms view
    public function sales_terms(){
				return view('landing.sales-terms');
    }

    // about view
    public function about(){
				return view('landing.about');
    }

    // career view
    public function career(){
				return view('landing.career');
    }

    // training view
    public function training(){
      return view('landing.training');
    }

    // reviews view
    public function reviews(){
			return view('landing.reviews');
    }

    // faq view
		public function faq(){
			return view('landing.faq');
    }

    // affiliate view
    public function affiliate(){
			return view('landing.affiliate');
    }

    // pricing view
    public function pricing(){
        return view('landing.pricing');
    }

    // addons view
    public function addons(){
        //$global_add_ons = GlobalAddons::orderBy('title')->get();
        return view('landing.addons');
    }

    // testimonials view
    public function testimonials(){
        return view('landing.testimonials');
    }

    // youtubers view
    public function youtubers(){
        return view('landing.youtubers');
    }

    // contact view
    public function contact(){
				return view('landing.contact',[
          'status' => 'not done'
				]);
    }

    public function initiateDownload(Request $request){
      $name = $request->get('name');
      $email = $request->get('email');
      $user_name = explode(" ", $name);
      $last_name = '';
      if(count($user_name) >= 2 || isset($user_name[1])){
        $last_name = $user_name[1];
      }

      $post = array(
           'email' => $email,
           'first_name' => $user_name[0],
           'last_name' => $last_name,
           'status' => 'active',
           'p[6]' => env('INITIATE_DOWNLOAD'),
      );

      $api_action = 'contact_add';
      ActiveCampaignJob::dispatch('', 'dashboard', $post, $api_action);

      return response()->json([
          'status' => 'success',
          'name' => $name,
          'email' => $email
      ]);
    }

    public function exitIntent(Request $request){
      $name = $request->get('name');
      $email = $request->get('email');
      $user_name = explode(" ", $name);
      $last_name = '';
      if(count($user_name) >= 2 || isset($user_name[1])){
        $last_name = $user_name[1];
      }

      $post = array(
           'email' => $email,
           'first_name' => $user_name[0],
           'last_name' => $last_name,
           'status' => 'active',
           'p[4]' => env('EXIT_INTENT'),
      );

      $api_action = 'contact_add';
      ActiveCampaignJob::dispatch('', 'dashboard', $post, $api_action);

      return response()->json([
          'status' => 'success',
          'name' => $name,
          'email' => $email
      ]);
    }

    // contacted message
    public function contacted(Request $request){
        $msg = '
          Hello!<br><br>
          We received a contact us request from : <br>'
          . 'Name: '. $request->get('nameContact') . '<br>'
          . 'Email: ' . $request->get('emailContact') . '<br>'
          . 'Phone: ' . $request->get('phoneContact') . '<br><br>'
          . 'Message: '. $request->get('messageContact') . '<br><br><br>
          Please contact him asap and keep rocking!<br>
          The Debutify Team
        ';

        // use wordwrap() if lines are longer than 70 characters
        $msg = wordwrap($msg, 70);

        // https://app.sendgrid.com/guide/integrate/langs/php
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom(getenv('DEBUTIFY_SUPPORT_EMAIL'), "Debutify");
        $email->setSubject("A client from debutify sent a Contact request");
        $email->addTo(getenv('DEBUTIFY_SUPPORT_EMAIL'), "Debutify");
        $email->addContent("text/html", $msg);
        $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
        try {
          $response = $sendgrid->send($email);
          \Log::info('Contact us email sent! From : ' . $request->get('emailContact'));
        } catch (Exception $e) {
          \Log::error('Failed to send contact us email');
        }

        // save in database
        $contact = new Contact;
        $contact->name = $request->get('nameContact');
        $contact->email = $request->get('emailContact');
        $contact->phone_no = $request->get('phoneContact');
        $contact->message = $request->get('messageContact');
        $contact->save();
        return Redirect::to('contact')->withSuccess('done');
    }
}
