<?php

namespace App\Http\Controllers;

use File;
use Hash;
use DateTime;
use App\Step;
use App\User;
use App\AddOns;
use App\Themes;
use App\Course;
use App\Module;
use App\AdminUser;
use App\FreeAddon;
use App\StripePlan;
use App\WinningProduct;
use App\Jobs\ActiveCampaignJob;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        $StripePlan = StripePlan:: all();

        foreach ($StripePlan as $plan) {
            if($plan->name =='starterPriceAnnually'){
                $starterPriceAnnually = $plan->cost;
            }
            if($plan->name =='starterPriceMonthly'){
                $starterPriceMonthly = $plan->cost;
            }
            if($plan->name =='hustlerPriceAnnually'){
                $hustlerPriceAnnually = $plan->cost;
            }
            if($plan->name =='hustlerPriceMonthly'){
                $hustlerPriceMonthly = $plan->cost;
            }
            if($plan->name =='guruPriceAnnually'){
                $guruPriceAnnually = $plan->cost;
            }
            if($plan->name =='guruPriceMonthly'){
                $guruPriceMonthly = $plan->cost;
            }
        }
	  $keyword = '';
	  if ($request->isMethod('get') && $request->input('q') != '') {
		  $keyword = $request->input('q');
		  $shop = User::where(function ($query) use($keyword) {
				$query->where('name', 'like', '%' . $keyword . '%')
				->orWhere('custom_domain', 'like', '%' . $keyword . '%')
				->orWhere('email', 'like', '%' . $keyword . '%')
				->orWhere('created_at', 'like', '%' . $keyword . '%');
			})->orderBy('id', 'desc')
			->paginate(100);
	  }else{
		$shop = User::orderBy('id', 'desc')->paginate(100);
	  }
      $free_addons_count = FreeAddon::where('status',1)->count();
      $shop_null = User::whereNull('alladdons_plan')->count();
      $shop_unactive = User::whereNotNull('deleted_at')->count();
      $shop_active = User::whereNull('deleted_at')->count();

      $starterCount = User::where('alladdons_plan', 'Starter')->count();
      $starterMonthly = User::where('alladdons_plan', 'Starter')->where('sub_plan', 'month')->count();
      $starterAnnually = User::where('alladdons_plan', 'Starter')->where('sub_plan', 'yearly')->count();

      $hustlerCount = User::where('alladdons_plan', 'Hustler')->count();
      $hustlerMonthly = User::where('alladdons_plan', 'Hustler')->where('sub_plan', 'month')->count();
      $hustlerAnnually = User::where('alladdons_plan', 'Hustler')->where('sub_plan', 'yearly')->count();;

      $guruCount = (User::where('alladdons_plan', 'Guru')->count())-1;
      $guruMonthly = (User::where('alladdons_plan', 'Guru')->where('sub_plan', 'month')->count())-1;
      $guruAnnually = User::where('alladdons_plan', 'Guru')->where('sub_plan', 'yearly')->count();

      $basicAddonsCount = User::where('alladdons_plan', 'basic')->count();
      $all_addons = User::where('alladdons_plan', 'basic')->get();
      $basicAddonsActive=0;
      foreach ($all_addons as $all_addon) {
        $addon_activated = AddOns::where('user_id',$all_addon->id)->where('status',1)->count();
        $basicAddonsActive += $addon_activated;
      }

      $basicAddonsMonthly = $basicAddonsActive*2;

      $totalPriceMonthly = ($starterMonthly*$starterPriceMonthly)+($hustlerMonthly*$hustlerPriceMonthly)+($guruMonthly*$guruPriceMonthly);
      $totalPriceAnnually = ($starterAnnually*$starterPriceAnnually)+($hustlerAnnually*$hustlerPriceAnnually)+($guruAnnually*$guruPriceAnnually);

      $paid = $starterCount+$hustlerCount+$guruCount+$basicAddonsCount;

      $mrr = ($totalPriceMonthly)+($totalPriceAnnually/12)+($basicAddonsMonthly);
      $arr = (($totalPriceMonthly*12)+$totalPriceAnnually)+($basicAddonsMonthly*12);
      $mrrGoal = 1000;

      $freemium = $shop_active - $paid;

      $basic_addons = 0;

      foreach ($shop as $shops) {
          if($shops->deleted_at){
            $shops->status = 'Unactive';
          } else{
            $shops->status = 'Active';
            try{
              if($shops->email == '' || $shops->email ==null){
                $shop_find = User::where('id', $shops->id)->first();
                $shopData = $shops->api()->request(
                    'GET',
                    '/admin/api/2019-04/shop.json',
                    []
                )['body']['shop'];
                $shop_find->email = $shopData['email'];
                $shop_find->save();
              }
            }catch(\Exception $e){
              logger(json_encode($e));
            }
        }

        if($shops->alladdons_plan == 'Starter'){
          $addon_activated = AddOns::where('user_id',$shops->id)->where('status',1)->count();
          $basic_addons += $addon_activated;
        }

        $already_addon_activated = AddOns::where('user_id',$shops->id)->where('status',1)->count();
        $free_addons = FreeAddon::where('shopify_domain', $shops->name)->where('status',1)->first();

        if($free_addons){
          $free_addons = $free_addons->status;
        }

        $shops->count = $already_addon_activated;
        $shops->referral = $free_addons;
        $allmix[] = $shops;
      }
      // search changes 3

      return view('admin.dashboard', [
        'freemium' => $freemium,
        'starterCount' => $starterCount,
        'starterMonthly' => $starterMonthly,
        'starterAnnually' => $starterAnnually,
        'hustlerCount' => $hustlerCount,
        'hustlerMonthly' => $hustlerMonthly,
        'hustlerAnnually' => $hustlerAnnually,
        'guruCount' => $guruCount,
        'guruMonthly' => $guruMonthly,
        'guruAnnually' => $guruAnnually,
        'starterPriceAnnually' => $starterPriceAnnually,
        'starterPriceMonthly' => $starterPriceMonthly,
        'hustlerPriceAnnually' => $hustlerPriceAnnually,
        'hustlerPriceMonthly' => $hustlerPriceMonthly,
        'guruPriceAnnually' => $guruPriceAnnually,
        'guruPriceMonthly' => $guruPriceMonthly,
        'totalPriceMonthly' => $totalPriceMonthly,
        'totalPriceAnnually' => $totalPriceAnnually,
        'paid' => $paid,
        'shops' => $allmix,
        'shop_pagination' => $shop->appends($request->except('page')),
        'mrr' => $mrr,
        'arr' => $arr,
        'mrrGoal' => $mrrGoal,
        'mrrGoalPercentage' => ($mrr/$mrrGoal) * 100,
        'shopifyreferral' => $free_addons_count,
        'basic_addons' => $basic_addons,
        'already_addon_activated' => $already_addon_activated,
        'basicAddonsCount' => $basicAddonsCount,
        'shop_null' => $shop_null,
        'basicAddonsMonthly' => $basicAddonsMonthly,
        '$basicAddonsCount' => $basicAddonsCount,
        'basicAddonsActive' => $basicAddonsActive,
        'nbShops' => User::count(),
        'keyword' => $keyword,
      ]);
    }

    public function usersSearch(Request $request){
      $shops = User::where(function ($query) use ($request){
              if($request->query('query') != ''){
                  $query->where('name', 'like', '%' . $request->query('query') . '%')
                  ->orWhere('custom_domain', 'like', '%' . $request->query('query') . '%')
                  ->orWhere('email', 'like', '%' . $request->query('query') . '%')
                  ->orWhere('created_at', 'like', '%' . $request->query('query') . '%');
              }
            })->orderBy('id', 'desc')
        ->paginate(10);

      $html = View::make('admin.searched_users',['shops' => $shops]);

      $response = $html->render();
      return response()->json([
          'status' => 'success',
          'html' => $response
      ]);
    }

    public function addtheme(){
        return view('admin.addtheme');
    }

    public function themes(){
      $themes = Themes::orderBy('id', 'desc')->get();
      $themes = Themes::get();
      return view('admin.themes', [
        'themes' => $themes
      ]);
    }


    // courses
    public function courses(Request $request){
      $keyword = '';
      if ($request->isMethod('get') && $request->input('q') != '') {
        $keyword = $request->input('q');
        $courses = Course::where(function ($query) use ($request, $keyword){
                if($request->query('query') != ''){
                        $query->where('title', 'like', '%'.$keyword.'%');
                }
            })->orderBy('id', 'desc')
        ->paginate(50);
      }else{
        $courses = Course::orderBy('id', 'desc')->paginate(50);
      }
      return view('admin.courses', ['courses' => $courses, 'keyword' => $keyword]);
    }

    public function coursesSearch(Request $request){
      $courses = Course::where(function ($query) use ($request){

                if($request->query('query') != ''){
                        $query->where('title', 'like', '%'.$request->query('query').'%');
                }
            })->orderBy('id', 'desc')
        ->paginate(50);

      $html = View::make('admin.searched_courses',['courses' => $courses]);

      $response = $html->render();
      return response()->json([
          'status' => 'success',
          'html' => $response
      ]);
    }

    public function addcourse(Request $request){
      return view('admin.addcourse');
    }

    public function createcourse(Request $request){

      $validator = Validator::make($request->all(), [
            'Title' => 'required',
            'Description' => 'required',
      ]);
      if ($validator->fails())
      {
          return response()->json(['errors'=>$validator->errors()->all()]);
      }

      $modules = $request->get('modules');
      $modules = json_decode($modules, true);

      $course = new Course;
      $course->title = $request->get('Title');
      $course->description = $request->get('Description');
      if($request->has('image')){
        $img_name = 'course_'.time().'.'.$request->get('image_ext');
        $img_content = file_get_contents($request->get('image'));
        Storage::put('public/courses/'.$img_name, $img_content);
        $img_url = env('APP_URL').'/storage/courses/'.$img_name;
        $course->image = $img_url;
      }
      if($request->has('plans')){
        $plans = implode(",", $request->get('plans'));
        $course->plans = $plans;
      }
      $course->save();
      $path= storage_path().'/app/public/videos/course_'.$course->id;
      File::makeDirectory($path, 0777, true, true);

      for($i=0;$i<count($modules);$i++){
        $cur_index = $i+1;
        $module = new Module;
        $module->course_id = $course->id;
        $module->title = $modules[$i]['title'];
        $module->description = $modules[$i]['desc'];
        $module->position = $cur_index;
        $module->save();
        if(isset($modules[$i]['steps'])){
          for($j=0;$j<count($modules[$i]['steps']);$j++){
            $curr_index = $j+1;
            $step = new Step;
            $step->module_id = $module->id;
            $step->title = $modules[$i]['steps'][$j]['title'];
            $step->description = $modules[$i]['steps'][$j]['desc'];
            $step->tags = $modules[$i]['steps'][$j]['tags'];
            $step->position = $curr_index;
            $cur_step = $j+1;

            $url1 = null; $url2 = null;
            if(isset($modules[$i]['steps'][$j]['video1'])){
              // $namefile1 = 'module'.$module->id. '-step'.$cur_step.'-v1_'.time().'.'.$modules[$i]['steps'][$j]['video1_ext'];
              // $content1 = file_get_contents($modules[$i]['steps'][$j]['video1']);
              // Storage::put('public/steps/'.$namefile1, $content1);
              // $url1 = env('APP_URL').'/storage/steps/'.$namefile1;
            }

            if(isset($modules[$i]['steps'][$j]['video2'])){
              // $namefile2 = 'module'.$module->id. '-step'.$cur_step.'-v2_'.time().'.'.$modules[$i]['steps'][$j]['video2_ext'];
              // $content2 = file_get_contents($modules[$i]['steps'][$j]['video2']);
              // Storage::put('public/steps/'.$namefile2, $content2);
              // $url2 = env('APP_URL').'/storage/steps/'.$namefile2;
            }
            $step->video1 = $url1;
            $step->video2 = $url2;
            $step->save();
          }
        }
      }
      $request->session()->flash('status', 'New course created successfully');
      return response()->json([
          'status' => 'ok',
          'redirect'=> route('courses')
      ]);
    }

    public function showcourse(Request $request,$id){
      $course = Course::where('courses.id',$id)->with(['modules.steps' => function ($query) { $query->orderBy('position', 'asc');}])->first();
      $all_plans = ['freemium','starter','hustler','guru'];
      $course->plans = explode(",", $course->plans);
      $course->all_plans = $all_plans;
      $course = json_decode($course);
      $all_files = array();
      $video_files = Storage::files('public/videos/course_'.$course->id);
      foreach ($video_files as $file) {
        $files['url'] = env('APP_URL').'/'.str_replace('public','storage',$file);
        $file_name = explode('public/videos/course_'.$course->id.'/', $file);
        $files['name'] = $file_name[1];
        $all_files[] = $files;
      }

      return view('admin.editcourse', ['course' => $course, 'video_files' => $all_files ]);
    }

    public function editcourse(Request $request,$id){
      $validator = Validator::make($request->all(), [
            'Title' => 'required',
            'Description' => 'required',
      ]);
      if ($validator->fails())
      {
          return response()->json(['errors'=>$validator->errors()->all()]);
      }

      $modules = $request->get('modules');
      $modules = json_decode($modules, true);

      $course = Course::find($id);
      if($course){
        $course->title = $request->get('Title');
        $course->description = $request->get('Description');
        if($request->has('image')){
          $img_name = 'course_'.time().'.'.$request->get('image_ext');
          $img_content = file_get_contents($request->get('image'));
          Storage::put('public/courses/'.$img_name, $img_content);
          $img_url = env('APP_URL').'/storage/courses/'.$img_name;
          $course->image = $img_url;
        }else{
          $course->image = null;
        }
        if($request->has('plans')){
          $plans = implode(",", $request->get('plans'));
          $course->plans = $plans;
        }
        $course->save();

        for($i=0;$i<count($modules);$i++){
          $cur_index = $i+1;
          $module = Module::find($modules[$i]['id']);
          if($module){
            $module->title = $modules[$i]['title'];
            $module->description = $modules[$i]['desc'];
            $module->position = $cur_index;
            $module->save();

            if(isset($modules[$i]['steps'])){
              for($j=0;$j<count($modules[$i]['steps']);$j++){
                $curr_index = $j+1;
                $step = Step::find($modules[$i]['steps'][$j]['id']);
                if($step){
                  $step->position = $curr_index;
                  $step->title = $modules[$i]['steps'][$j]['title'];
                  $step->description = $modules[$i]['steps'][$j]['desc'];
                  $step->tags = $modules[$i]['steps'][$j]['tags'];
                  $cur_step = $j+1;

                  $url1 = null; $url2 = null;
                  if(isset($modules[$i]['steps'][$j]['video1']) && $modules[$i]['steps'][$j]['video1'] != ''){
                  	if( ( $pos = strrpos($modules[$i]['steps'][$j]['video1'] , 'debutify.com' ) ) !== false ) {
                  		$url1 = $modules[$i]['steps'][$j]['video1'];
                  	}else{
                  		// $namefile1 = 'module'.$module->id. '-step'.$cur_step.'-v1_'.time().'.'.$modules[$i]['steps'][$j]['video1_ext'];
	                   //  $content1 = file_get_contents($modules[$i]['steps'][$j]['video1']);
	                   //  Storage::put('public/steps/'.$namefile1, $content1);
	                   //  $url1 = env('APP_URL').'/storage/steps/'.$namefile1;
                      $url1 = env('APP_URL').'/storage/videos/course_'.$course->id.'/'.$modules[$i]['steps'][$j]['video1'];
                  	}
                  }

                  if(isset($modules[$i]['steps'][$j]['video2']) && $modules[$i]['steps'][$j]['video2'] != ''){
                  	if( ( $pos = strrpos( $modules[$i]['steps'][$j]['video2'] , 'debutify.com' ) ) !== false ) {
                  		$url2 = $modules[$i]['steps'][$j]['video2'];
                  	}else{
	                    // $namefile2 = 'module'.$module->id. '-step'.$cur_step.'-v2_'.time().'.'.$modules[$i]['steps'][$j]['video2_ext'];
	                    // $content2 = file_get_contents($modules[$i]['steps'][$j]['video2']);
	                    // Storage::put('public/steps/'.$namefile2, $content2);
	                    // $url2 = env('APP_URL').'/storage/steps/'.$namefile2;
                      $url2 = env('APP_URL').'/storage/videos/course_'.$course->id.'/'.$modules[$i]['steps'][$j]['video2'];
	                  }
                  }

                  $step->video1 = $url1;
                  $step->video2 = $url2;

                }else{
                  $step = new Step;
                  $step->module_id = $module->id;
                  $step->position = $curr_index;
                  $step->title = $modules[$i]['steps'][$j]['title'];
                  $step->description = $modules[$i]['steps'][$j]['desc'];
                  $step->tags = $modules[$i]['steps'][$j]['tags'];
                  $cur_step = $j+1;

                  $url1 = null;$url2 = null;
                  if(isset($modules[$i]['steps'][$j]['video1']) && $modules[$i]['steps'][$j]['video1'] != ''){
                    // $namefile1 = 'module'.$module->id. '-step'.$cur_step.'-v1_'.time().'.'.$modules[$i]['steps'][$j]['video1_ext'];
                    // $content1 = file_get_contents($modules[$i]['steps'][$j]['video1']);
                    // Storage::put('public/steps/'.$namefile1, $content1);
                    // $url1 = env('APP_URL').'/storage/steps/'.$namefile1;
                    $url1 = env('APP_URL').'/storage/videos/course_'.$course->id.'/'.$modules[$i]['steps'][$j]['video1'];
                  }

                  if(isset($modules[$i]['steps'][$j]['video2']) && $modules[$i]['steps'][$j]['video2'] != ''){
                    // $namefile2 = 'module'.$module->id. '-step'.$cur_step.'-v2_'.time().'.'.$modules[$i]['steps'][$j]['video2_ext'];
                    // $content2 = file_get_contents($modules[$i]['steps'][$j]['video2']);
                    // Storage::put('public/steps/'.$namefile2, $content2);
                    // $url2 = env('APP_URL').'/storage/steps/'.$namefile2;
                    $url2 = env('APP_URL').'/storage/videos/course_'.$course->id.'/'.$modules[$i]['steps'][$j]['video2'];
                  }
                  $step->video1 = $url1;
                  $step->video2 = $url2;
                }

                $step->save();
              }
            }
          }else{
            $module = new Module;
            $module->course_id = $course->id;
            $module->position = $cur_index;
            $module->title = $modules[$i]['title'];
            $module->description = $modules[$i]['desc'];
            $module->save();
            if(isset($modules[$i]['steps'])){
              for($j=0;$j<count($modules[$i]['steps']);$j++){
                  $curr_index = $j+1;
                  $step = new Step;
                  $step->module_id = $module->id;
                  $step->position = $curr_index;
                  $step->title = $modules[$i]['steps'][$j]['title'];
                  $step->description = $modules[$i]['steps'][$j]['desc'];
                  $step->tags = $modules[$i]['steps'][$j]['tags'];
                  $cur_step = $j+1;

                  $url1 = null; $url2 = null;
                  if(isset($modules[$i]['steps'][$j]['video1']) && $modules[$i]['steps'][$j]['video1'] != ''){
                    // $namefile1 = 'module'.$module->id. '-step'.$cur_step.'-v1_'.time().'.'.$modules[$i]['steps'][$j]['video1_ext'];
                    // $content1 = file_get_contents($modules[$i]['steps'][$j]['video1']);
                    // Storage::put('public/steps/'.$namefile1, $content1);
                    // $url1 = env('APP_URL').'/storage/steps/'.$namefile1;
                    $url1 = env('APP_URL').'/storage/videos/course_'.$course->id.'/'.$modules[$i]['steps'][$j]['video1'];
                  }

                  if(isset($modules[$i]['steps'][$j]['video2']) && $modules[$i]['steps'][$j]['video2'] != ''){
                    // $namefile2 = 'module'.$module->id. '-step'.$cur_step.'-v2_'.time().'.'.$modules[$i]['steps'][$j]['video2_ext'];
                    // $content2 = file_get_contents($modules[$i]['steps'][$j]['video2']);
                    // Storage::put('public/steps/'.$namefile2, $content2);
                    // $url2 = env('APP_URL').'/storage/steps/'.$namefile2;
                    $url2 = env('APP_URL').'/storage/videos/course_'.$course->id.'/'.$modules[$i]['steps'][$j]['video2'];
                  }
                  $step->video1 = $url1;
                  $step->video2 = $url2;

                  $step->save();
              }
            }
          }
        }
      }
      $request->session()->flash('status', 'Course has been updated successfully');
      return response()->json([
          'status' => 'ok',
          'redirect'=> route('courses')
      ]);
    }

    public function deleteModule(Request $request){
      $module = Module::find($request->get('module_id'));
      if($module){
        $module->delete();
      }
      return response()->json([
          'status' => 'success'
      ]);
    }

    public function deleteStep(Request $request){
      $step = Step::find($request->get('step_id'));
      if($step){
        $step->delete();
      }
      return response()->json([
          'status' => 'success'
      ]);
    }

    public function deleteCourse(Request $request,$id){
      $course = Course::find($id);
      if($course){
        $course->delete();
      }
      $request->session()->flash('status', 'Course deleted successfully');
      return response()->json([
          'status' => 'ok',
          'redirect'=> route('courses')
      ]);
    }

    // products
    public function products(Request $request){
		$keyword = '';
		if ($request->isMethod('get') && $request->input('q') != '') {
		  $keyword = $request->input('q');
		  $products = WinningProduct::where(function ($query) use($keyword) {
				$query->where('name', 'like', '%' . $keyword . '%')
				->orWhere('aliexpresslink', 'like', '%' . $keyword . '%')
				->orWhere('price', 'like', '%' . $keyword . '%');
			})->orderBy('id', 'desc')
			->paginate(50);
		}else{
			$products = WinningProduct::orderBy('id', 'desc')->paginate(50);
		}

		return view('admin.products', ['products' => $products->appends($request->except('page')), 'keyword' => $keyword]);
    }

    public function productsSearch(Request $request){

      $products = WinningProduct::where(function ($query) use($request) {
        if($request->query('q') != ''){
          $query->where('name', 'like', '%' . $request->query('q') . '%')
          ->orWhere('aliexpresslink', 'like', '%' . $request->query('q') . '%')
          ->orWhere('price', 'like', '%' . $request->query('q') . '%');
        }
      })->orderBy('id', 'desc')
      ->paginate(50);

      $html = View::make('admin.searched_products',['products' => $products]);

      $response = $html->render();
      return response()->json([
          'status' => 'success',
          'html' => $response
      ]);
    }

    public function addproduct(){
      return view('admin.addproduct');
    }

    public function newproduct(Request $request){
      // $this->validate($request, [
      //   'name' => 'required',
      //   'price' => 'required',
      //   'cost' => 'required',
      //   'profit' => 'required',
      //   'aliexpresslink' => 'required',
      //   'facebookadslink' => 'required',
      //   'googletrendslink' => 'required',
      //   'youtubelink' => 'required',
      //   'competitorlink' => 'required',
      //   'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      // ]);

      // $this->validate($request, [
      //     'video' => 'max:500000',
      // ]);
      // $video = $request->file('video');
      // if($video){
      //   $filename = 'product_video'.time().'.'.$video->getClientOriginalExtension();
      //   $destinationPath = public_path('product_video');
      //   $video->move($destinationPath,$filename);
      //   $videos= $filename;
      // }
      //

      // $images ='';
      // $image = $request->file('image');
      // if($image){
      //   $input['imagename'] = 'product'.time().'.'.$image->getClientOriginalExtension();
      //   $destinationPath = public_path('images/product');
      //   $img = Image::make($image->getRealPath());

      //   $img->resize(300, 300, function($constraint){
      //     $constraint->aspectRatio();
      //   })->save($destinationPath.'/'.$input['imagename']);
      //   $images = $input['imagename'];
      // }

      $age = implode(",",$request->age);
      $gender = implode(",",$request->gender);
      $placement = implode(",",$request->placement);

      $product = new WinningProduct;
      $product->name = $request->name;
      $product->price = $request->price;
      $product->cost = $request->cost;
      $product->profit = $request->profit;
      $product->aliexpresslink = $request->aliexpresslink;
      $product->facebookadslink = $request->facebookadslink;
      $product->googletrendslink = $request->googletrendslink;
      $product->youtubelink = $request->youtubelink;
      $product->competitorlink = $request->competitorlink;
      $product->age = $age;
      $product->gender = $gender;
      $product->placement = $placement;
      $product->saturationlevel = $request->saturationlevel;
      $product->category = $request->category;
      $product->interesttarget = $request->interesttarget;
      $product->image = $request->image;
      $product->opinion = $request->opinion;
      $product->description = $request->description;
      $product->video = $request->video;
      $product->save();
      $request->session()->flash('status', 'Upload New Product successfully');
      return redirect()->route('products');
    }

    public function showproduct($id){
      $all_ages=['18-24','25-34','35-44','45-54','55-64','65+'];
      $all_genders=['Men','Women'];
      $all_placements=['Mobile','Desktop'];
      $product = WinningProduct::find($id);
      $product->age = explode(",",$product->age);
      $product->gender = explode(",",$product->gender);
      $product->placement = explode(",",$product->placement);
      $age=[];
      $placement =[];
      $gender=[];
      foreach ($all_genders as $keys => $all_gender) {
        $check=false;
        foreach ($product->gender as $key => $genders) {
          if($genders == $all_gender){
            $gender[] = array('gender' =>$genders, 'selected'=>'selected');
            $check = true;
          }
        }
        if($check){}else{
          $gender[] = array('gender' =>$all_gender, 'selected'=>'');
        }
      }
      $product->gender = $gender;
      foreach ($all_placements as $keys => $all_placement) {
        $check=false;
        foreach ($product->placement as $key => $placements) {
          if($placements == $all_placement){
            $placement[] = array('placement' =>$placements, 'selected'=>'selected');
            $check = true;
          }
        }
        if(!$check){
          $placement[] = array('placement' =>$all_placement, 'selected'=>'');
        }
      }
      $product->placement = $placement;
      foreach ($all_ages as $keys => $all_age) {
        $check=false;
        foreach ($product->age as $key => $ages) {
          if($ages == $all_age){
            $age[] = array('age' =>$ages, 'selected'=>'selected');
            $check = true;
          }
        }
        if(!$check){
          $age[] = array('age' =>$all_age, 'selected'=>'');
        }
      }
     $product->age = $age;
      return view('admin.editproduct', ['product' => $product]);
    }

    public function editproduct(Request $request, $id){

      $age = implode(",",$request->age);
      $gender = implode(",",$request->gender);
      $placement = implode(",",$request->placement);

      $product = WinningProduct::find($id);
      $product->name = $request->name;
      $product->price = $request->price;
      $product->cost = $request->cost;
      $product->profit = $request->profit;
      $product->aliexpresslink = $request->aliexpresslink;
      $product->facebookadslink = $request->facebookadslink;
      $product->googletrendslink = $request->googletrendslink;
      $product->youtubelink = $request->youtubelink;
      $product->competitorlink = $request->competitorlink;
      $product->age = $age;
      $product->gender = $gender;
      $product->placement = $placement;
      $product->saturationlevel = $request->saturationlevel;
      $product->category = $request->category;
      $product->interesttarget = $request->interesttarget;
      $product->image = $request->saveimage;
      $product->opinion = $request->opinion;
      $product->description = $request->description;
      $product->video = $request->saveVideo;
      $product->save();
      $request->session()->flash('status', 'Product has been updated successfully');
      return redirect()->route('products');
    }

    public function newtheme(Request $request){
        $file = $request->file('theme_file');
        //$filename = 'debutify-'.$theme_version.'-'. time() . '.' . $file->getClientOriginalExtension();
        $filename = 'debutify-' . time() . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('themes', $filename, 'public');
        $url = env('APP_URL').'/storage/'.$path;

        $product = new WinningProduct;
        $product->version = $request->version;
        $product->size = $request->file('theme_file')->getSize();
        $product->url = $url;
        $product->save();

        return redirect()->route('dashboard');
    }

    public function freeaddon(Request $request){
      if($request->get('status') == 1)
      {
        $status = 0;
      }
      else{
        $status = 1;
      }
      $free_addons = FreeAddon::where('shopify_domain',$request->get('shopify_domain'))->where('email',$request->get('email'))->count();
        if($free_addons==0){
                $freeaddon = new FreeAddon;
                $freeaddon->email = $request->get('email');
                $freeaddon->shopify_domain =$request->get('shopify_domain');
                $freeaddon->status = $status;
                $freeaddon->save();
        }else{
                $freeaddon = FreeAddon::where('shopify_domain',$request->get('shopify_domain'))->where('email',$request->get('email'))->first();
                $freeaddon->status = $status;
                $freeaddon->save();
        }
      //$request->session()->flash('error', 'Invalide coupon code');
      return redirect()->route('dashboard');
    }
    /*--- For referrel --*/
    // public function referral()
    // {
    //   return 'http://example.test/?ref=' . \Hashids::encode(auth()->user()->id);
    // }
    // public function referrer()
    // {
    //     return auth()->user()->referrer;
    // }

    // public function referrals()
    // {
    //     return auth()->user()->referrals;
    // }
    public function deleteproduct(Request $request,$id)
    {
      $product = WinningProduct::find($id);
      if($product){
        $product->delete();
      }
      $request->session()->flash('status', 'Product has been deleted successfully');
      return redirect()->route('products');
    }

    public function addtrialdays(Request $request){
      $shop = User::where('name', $request->get('shopify_domain'))->first();
      $date = new DateTime();
      $new_formats_trial = $date->format('Y-m-d');
      $trial_end_days = date('Y-m-d', strtotime($new_formats_trial. ' + '.$request->get('trial_days').' days'));
      $shop->trial_days = $request->get('trial_days');
      $shop->trial_ends_at = $trial_end_days;
      if($request->get('trial_days') > 0){
        $shop->sub_trial_ends_at = 1;
        $shop->trial_check = 0;
        $license_key = Hash::make(Str::random(12));
        $shop->license_key = $license_key;
        $shopData = $shop->api()->request(
                'GET',
                '/admin/api/shop.json',
                []
            )['body']['shop'];
            $shop_owner = $shopData['shop_owner'];
            $shop_owner_name = explode(" ", $shop_owner);
        // Active Campaign post data
        $post = array(
          'email'                    => $shopData['email'],
          'first_name'               => $shop_owner_name[0],
          'last_name'                => $shop_owner_name[1],
          'phone'                    => $shopData['phone'],
          'field[1,0]'               => 'Trial', //Subscription
          'field[6,0]'               => $shopData['domain'], // Company
          'field[7,0]'               => 'Installed', //App Status
          'field[9,0]'               => $shopData['country_name'], //country
          'field[10,0]'              => $shopData['city'], //city
          'field[11,0]'              => $shopData['zip'], //zip
          'field[12,0]'              => $shopData['address1'], //Address line 1
          'field[13,0]'              => $shopData['address2'], //Address line 2
          'field[14,0]'              => $shopData['province'], //Province
          'field[15,0]'              => $shopData['primary_locale'], //Language
          'field[16,0]'              => $shopData['name'], //Store name
          'p[3]'                     => env('APP_USER'),
        );
        $api_action = 'contact_edit';
        ActiveCampaignJob::dispatch($shop->email,'app', $post, $api_action);

        // $groupsApi = (new \MailerLiteApi\MailerLite(env('MAILERLITE_APIKEY')))->groups();
        //     // print($shopData['email']);
        //   $groupId = env('MAILERLITE_GROUP_ID');
        //       $subscriber = [
        //         'email' => $shop->email,
        //         'fields' => [
        //             'subscription' => 'trial',
        //             'status' => 'active'
        //          ]
        //       ];
        //   $addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);
        //   logger(json_encode($addedSubscriber));
      }else{
         $shop->license_key = null;
      }

      //else{
      //   $shop->sub_trial_ends_at = 0;
      // }
      $shop->save();
      return redirect()->route('dashboard');
    }
} /*---end of controller----*/
