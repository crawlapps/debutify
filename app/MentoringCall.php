<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MentoringCall extends Model
{
    protected $table = 'mentoringcalls';
}
