<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChildStore extends Model
{
    protected $table = 'childstores';
}
