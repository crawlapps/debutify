<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WinningProduct extends Model
{
    protected $table = 'winning_products';
    protected $fillable = [
        'name',
        'price',
        'cost',
        'profit',
        'aliexpresslink',
        'facebookadslink',
        'googletrendslink',
        'youtubelink',
        'competitorlink',
        'age',
        'gender',
        'placement',
        'saturationlevel',
        'image'
    ];
}