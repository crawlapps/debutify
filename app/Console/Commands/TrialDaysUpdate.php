<?php

namespace App\Console\Commands;

use App\User;
use DateTime;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use App\Jobs\ActiveCampaignJob;
use Illuminate\Support\Facades\Hash;
class TrialDaysUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trialdays:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trial days update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        logger("Trail Days update cron");
        $shops = User::where('trial_days', '>', 0)->get();
        foreach ($shops as $key=>$shop) {

           if($shop->trial_ends_at != null && $shop->sub_trial_ends_at == 1){
              $dates = new DateTime();
              $new_formats_trial = $dates->format('Y-m-d');
              $trial_dates = new DateTime($shop->trial_ends_at);
              if($new_formats_trial < $trial_dates->format('Y-m-d')){
                $shop->sub_trial_ends_at = 1;
                $license_key = Hash::make(Str::random(12));
                $shop->license_key = $license_key;
                $diff_trial = strtotime($new_formats_trial) - strtotime($shop->trial_ends_at);
                $days_trial = abs(round($diff_trial / 86400));
                if($shop->email){
                  $this->addactive_campaign($shop,'Trial');
                }
              } else{
                if($shop->sub_trial_ends_at == 1){
                  if($shop->email){
                    $this->addactive_campaign($shop,'Freemium');
                    // $groupsApi = (new \MailerLiteApi\MailerLite(env('MailerLite_APIKEY')))->groups();
                    // $groupId = env('MailerLite_GROUP_ID');
                    //     $subscriber = [
                    //         'email' => $shop->email,
                    //         'fields' => [
                    //             'subscription' => 'freemium',
                    //             'status' => 'active'
                    //         ]
                    //     ];
                    // $addedSubscriber = $groupsApi->addSubscriber($groupId, $subscriber);
                  }
                }
                $shop->license_key = null;
                $shop->sub_trial_ends_at = 0;
                $days_trial = 0;
              }
              $shop->trial_days = $days_trial;
              $shop->save();
           }
        }
    }
    public function addactive_campaign($shop, $Subscription){
      $shopData = $shop->api()->request(
                  'GET',
                  '/admin/api/shop.json',
                  []
              )['body']['shop'];
              $shop_owner = $shopData['shop_owner'];
              $shop_owner_name = explode(" ", $shop_owner);

        $post = array(
          'email'                    => $shopData['email'],
          'first_name'               => $shop_owner_name[0],
          'last_name'                => $shop_owner_name[1],
          'phone'                    => $shopData['phone'],
          'field[1,0]'               => $Subscription, //Subscription
          'field[6,0]'               => $shopData['domain'], // Company
          'field[7,0]'               => 'Installed', //App Status
          'field[9,0]'               => $shopData['country_name'], //country
          'field[10,0]'              => $shopData['city'], //city
          'field[11,0]'              => $shopData['zip'], //zip
          'field[12,0]'              => $shopData['address1'], //Address line 1
          'field[13,0]'              => $shopData['address2'], //Address line 2
          'field[14,0]'              => $shopData['province'], //Province
          'field[15,0]'              => $shopData['primary_locale'], //Language
          'field[16,0]'              => $shopData['name'], //Store name
          'p[3]'                     => env('app_user'),
      );
      $api_action = 'contact_edit';
      ActiveCampaignJob::dispatch($shop->email,'app', $post, $api_action);
    }
}
