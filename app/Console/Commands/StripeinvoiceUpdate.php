<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Subscription;
use App\User;

class StripeinvoiceUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update stripe invoice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        logger("stripe update");
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $shops = User::whereNull('deleted_at')->get();
        foreach ($shops as $key=>$shop) {
            try{
                $subscriptions = Subscription::where('user_id',$shop->id)->whereNull('ends_at')->orderBy('id', 'desc')->first();
                if($subscriptions){
                  $stripe_subs =  \Stripe\Subscription::retrieve($subscriptions->stripe_id);

                  if($stripe_subs->status == 'unpaid'){
                    $shop->license_key = null;
                    $shop->save();
                  }

                  //$invoice = \Stripe\Invoice::retrieve($stripe_subs->latest_invoice);
                  // if($invoice){
                  //   $date = new DateTime();
                  //   $todays_date = $date->format('Y-m-d');
                  //   $date->setTimestamp($invoice->created);
                  //   $dates = $date->format('Y-m-d');
                  //   $last_date = date('Y-m-d', strtotime($dates. ' + 14 days'));
                  //  // logger("shops ". $shop->name);
                  //  // logger($last_date);
                  // //  logger($todays_date);
                  //   if($invoice->status !='paid'){
                  //      // logger($invoice->status);
                  //       $invoice_status = 'pending';
                  //       if ($todays_date >= $last_date) {
                  //         $invoice_status = 'failed';
                  //         $shop->license_key = null;
                  //         $shop->save();
                  //       }
                  //   }
                  // }
                }
            }catch(\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught
                    logger('Since its a decline, \Stripe\Error\Card will be caught');

            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                logger('Too many requests made to the API too quickly');

            } catch (\Stripe\Error\InvalidRequest $e) {
              // Invalid parameters were supplied to Stripe's API
                logger(' Invalid parameters were supplied to Stripes API');

            } catch (\Stripe\Error\Authentication $e) {
              // Authentication with Stripe's API failed
                logger("Authentication with Stripe's API failed");

            } catch (\Stripe\Error\ApiConnection $e) {
              // Network communication with Stripe failed
                logger('Network communication with Stripe failed');

            } catch (\Stripe\Error\Base $e) {
              // Display a very generic error to the user, and maybe send
                logger('Display a very generic error to the user, and maybe send');

            } catch (Exception $e) {
              // Something else happened, completely unrelated to Stripe
                logger('Something else happened, completely unrelated to Stripe');
            }

        }
    }
}
