<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\AddOns;
use App\User;
use Exception;

class InvoiceDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:deleted';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invoice deleted';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
         $addon_inv = AddOns::all()->where('shedule_time', '0');

         foreach ($addon_inv as $key=>$value) {
            $shop = User::whereNull('deleted_at')->where('id', $value->user_id)->first();
             $addon_count = AddOns::where('user_id', $value->user_id)->where('status', 1)->count();
             try{
                if($shop){
                    if($value->invoiceitem)
                    {
                        $getinvoice = \Stripe\InvoiceItem::retrieve($value->invoiceitem);
                        $subscription = \Stripe\Subscription::retrieve($getinvoice->subscription);
                        if($subscription->status !== 'canceled')
                        {
                            $date = new DateTime();
                            $dt = new DateTime();
                            $date->setTimestamp($subscription->current_period_end);
                            $new_format = $date->format('y-m-d');
                             $current_date = $dt->format('y-m-d');
                            if(strtotime($new_format) < strtotime($current_date)){
                                logger('Invoice Item Deleted');
                                $ii = \Stripe\InvoiceItem::retrieve($value->invoiceitem);
                                $ii->delete();
                                $addon = AddOns::where('id', $value->id)->first();
                                $addon->shedule_time = null;
                                $addon->invoiceitem = null;
                                $addon->save();
                                $addon_count = AddOns::where('user_id', $value->user_id)->where('status', 1)->count();
                                if($addon_count == 0)
                                {
                                    if($shop->alladdons_plan == 'basic'){
                                        $shop->alladdons_plan ='freemium';
                                    }
                                    logger('Delete Subscription');
                                    $shop->subscription('main')->cancelNow();
                                   $sub = \Stripe\Subscription::retrieve($getinvoice->subscription);
                                   $sub->cancel();
                                }
                            }
                        }
                    }
                }
              }catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
                logger('Since its a decline, \Stripe\Error\Card will be caught');

        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            logger('Too many requests made to the API too quickly');

        } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API
            logger(' Invalid parameters were supplied to Stripes API');

        } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed
            logger("Authentication with Stripe's API failed");

        } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed
            logger('Network communication with Stripe failed');

        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
            logger('Display a very generic error to the user, and maybe send');

        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            logger('Something else happened, completely unrelated to Stripe');
        }

        }

    }
}
