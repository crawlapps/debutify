<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Subscription;
use App\Taxes;

class UpdateShopSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:subscriptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update canada shop subscriptions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shops = User::whereNull('deleted_at')->get();
        // logger("Updation canada shops subscriptions started from cron");
        foreach ($shops as $key => $shop) {
            try{
                $shopData = $shop->api()->request(
                    'GET',
                    '/admin/api/shop.json',
                    []
                )['body']['shop'];
                $tax_rates = array();
                if($shopData['country_name'] == 'Canada'){
                  // $tax_id = getTaxId($shopData['province']);
                  $tax = Taxes::where('region',$shopData['province'])->first();
                  if($tax){
                    $tax_id = $tax->stripe_taxid;
                  }else{
                    $tax = Taxes::where('region','New-Brunswick')->first();
                    $tax_id = $tax->stripe_taxid;
                  }

                  echo 'id='.$shop->id.' ,domain='.$shop->name.', province='.$shopData['province'].', returning tax='.$tax_id."<br>";
                  logger('id='.$shop->id.' ,domain='.$shop->name.', province='.$shopData['province'].', returning tax='.$tax_id);
                  $tax_rates[] = $tax_id;
                  $subscription = Subscription::where('user_id',$shop->id)->orderBy('id', 'desc')->first();
                  if($subscription){
                    try{
                      \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

                      $subscription_stripe = \Stripe\Subscription::retrieve($subscription->stripe_id);
                      // echo "<pre>";
                      // print_r($subscription_stripe);
                      // echo "</pre>";
                      if (empty($subscription_stripe->default_tax_rates)) {
                        $update_subscription = \Stripe\Subscription::update($subscription->stripe_id, [
                          'cancel_at_period_end' => false,
                          'items' => [
                            [
                              'id' => $subscription_stripe->items->data[0]->id,
                              'plan' => $subscription_stripe->plan->id,
                            ],
                          ],
                          'default_tax_rates' => $tax_rates,
                        ]);
                        echo ' updated subscription with tax, strip id='.$subscription->stripe_id." <br>";
                        logger(' updated subscription with tax, strip id='.$subscription->stripe_id);
                      }else{
                        print_r($subscription_stripe->default_tax_rates);
                        logger('tax already exist='.json_encode($subscription_stripe->default_tax_rates));
                      }
                    }catch(\Stripe\Error\Card $e) {
                       logger("Since it's a decline, \Stripe\Error\Card will be caught, strip id=".$subscription->stripe_id);

                    } catch (\Stripe\Error\RateLimit $e) {
                       logger("Too many requests made to the API too quickly, strip id=".$subscription->stripe_id);

                    } catch (\Stripe\Error\InvalidRequest $e) {
                        logger("Invalid parameters were supplied to Stripe's API, strip id=".$subscription->stripe_id);

                    } catch (\Stripe\Error\Authentication $e) {
                        logger("Authentication with Stripe's API failed(maybe you changed API keys recently), strip id=".$subscription->stripe_id);

                    } catch (\Stripe\Error\ApiConnection $e) {
                        logger("Network communication with Stripe failed, strip id=".$subscription->stripe_id);

                    } catch (\Stripe\Error\Base $e) {
                      logger("Display a very generic error to the user, and maybe send yourself an email, strip id=".$subscription->stripe_id);
                    } catch (Exception $e) {
                      logger("Something else happened, completely unrelated to Stripe, strip id=".$subscription->stripe_id);
                    }
                  }
                }
                // die();
              }catch(\GuzzleHttp\Exception\ClientException $e){
                logger("shop ClientException, shop id=".$shop->id);
              }catch(\GuzzleHttp\Exception\RequestException $e){
                logger("shop RequestException, shop id=".$shop->id);
              }catch (Exception $e) {
                logger("Something else happened, completely unrelated to Stripe, shop id=".$shop->id);
              }
        }
        logger("all canada shops subscriptions updated");
    }
}
