<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// \URL::forceSchema('https');

//landing
Route::get('/','LandingController@landing')->name('landing');
Route::get('/old','LandingController@old')->name('old');
Route::get('/reviews','LandingController@reviews')->name('reviews');
Route::get('/faq','LandingController@faq')->name('faq');
Route::get('/affiliate','LandingController@affiliate')->name('affiliate');
Route::get('/add-ons','LandingController@addons')->name('addons');
Route::get('/pricing','LandingController@pricing')->name('pricing');
Route::get('/theme','LandingController@theme')->name('theme');
Route::get('/about','LandingController@about')->name('about');
Route::get('/contact','LandingController@contact')->name('contact');
Route::post('/contacted','LandingController@contacted')->name('contact');
Route::get('/career','LandingController@career')->name('career');
Route::get('/download','LandingController@download')->name('download');
Route::get('/update_trail','ThemeController@update_trail')->name('update_trail');
Route::get('/update_themecheck','ThemeController@update_themecheck')->name('update_themecheck');

//Policy Pages
Route::get('terms', function () {
    return redirect('/terms-of-use');
});
Route::get('/terms-of-use','LandingController@terms')->name('terms');
Route::get('privacy', function () {
    return redirect('/privacy-policy');
});
Route::get('/privacy-policy','LandingController@privacy')->name('privacy');
Route::get('/terms-of-sales','LandingController@sales_terms')->name('sales_terms');


//activeCampaign
Route::get('/initiate-download','LandingController@initiateDownload')->name('initiate_download');
Route::get('/exit-intent','LandingController@exitIntent')->name('exit_intent');

//app
Route::group(['prefix' => 'app'], function() {
  // views
  Route::get('/','ThemeController@index')->middleware(['auth.shopify', 'billable'])->name('home');
  Route::any('/plans','ThemeController@plans')->middleware(['auth.shopify', 'billable'])->name('plans');
  Route::get('/add_ons','ThemeController@theme_addons')->middleware(['auth.shopify', 'billable'])->name('theme_addons');
  Route::any('/onboarding','ThemeController@onboarding')->middleware(['auth.shopify', 'billable'])->name('onboarding');
  Route::get('/themes','ThemeController@theme_view')->middleware(['auth.shopify', 'billable'])->name('theme_view');
  Route::get('/courses','ThemeController@courses')->middleware(['auth.shopify', 'billable'])->name('app_courses');
  Route::get('/courses/{id}', 'ThemeController@viewCourse')->middleware(['auth.shopify', 'billable'])->name('view_course');
  Route::get('/integrations','ThemeController@integrations')->middleware(['auth.shopify', 'billable'])->name('integrations');
  Route::get('/mentoring','ThemeController@mentoring')->middleware(['auth.shopify', 'billable'])->name('mentoring');
  Route::get('/winning-products','ThemeController@winning_products')->middleware(['auth.shopify', 'billable'])->name('winning_products');
  Route::get('/products/paginate','ThemeController@paginateWinningProducts')->middleware(['auth.shopify', 'billable'])->name('paginate_products');
  Route::get('/products/filter','ThemeController@filterWinningProducts')->middleware(['auth.shopify', 'billable'])->name('filter_products');
  Route::get('/change/saturation','ThemeController@changeSaturation')->name('change_saturation');
  Route::get('/feedback','ThemeController@feedback')->middleware(['auth.shopify', 'billable'])->name('feedback');
  Route::get('/support','ThemeController@support')->middleware(['auth.shopify', 'billable'])->name('support');
  Route::get('/technical-support','ThemeController@technicalSupport')->middleware(['auth.shopify', 'billable'])->name('technical_support');
  Route::get('/affiliate','ThemeController@affiliate')->middleware(['auth.shopify', 'billable'])->name('affiliate');
  Route::get('/changelog','ThemeController@changelog')->middleware(['auth.shopify', 'billable'])->name('changelog');
  Route::get('/thank-you', 'ThemeController@thank_you')->middleware('auth.shopify')->name('thankyou');
  Route::get('/good-bye', 'ThemeController@good_bye')->middleware('auth.shopify')->name('goodbye');
  Route::get('/updatecard', 'ThemeController@update_card')->middleware('auth.shopify')->name('updatecard');

  // deprecated
  Route::get('/free-addons','ThemeController@free_addons')->name('free_addons');
  Route::get('/report_bug_pop', 'ThemeController@report_bug_pop')->name('report_bug_pop');

  // functions
  //Route::get('/admin','ThemeController@index')->middleware(['auth.shopify', 'billable'])->name('home');
  Route::any('/theme','ThemeController@download_theme')->middleware(['auth.shopify', 'billable'])->name('download_theme_post');
  Route::any('/theme/download','ThemeController@theme_download_post')->middleware(['auth.shopify', 'billable'])->name('theme_download_post');
  Route::post('/mail/send', 'ThemeController@sendTestEmail')->name('report_bug');
  Route::post('/addon/cancel_subscription','ThemeController@cancel_subscription')->middleware(['auth.shopify', 'billable'])->name('cancel_subscription');
  Route::post('/addon/create_subscription','ThemeController@create_subscription')->middleware(['auth.shopify', 'billable'])->name('create_subscription');
  Route::any('/update','ThemeController@updatecard')->middleware(['auth.shopify', 'billable'])->name('updatecard');
  /*---update----*/
  Route::post('/addon/update_addons','ThemeController@update_addons')->middleware(['auth.shopify', 'billable'])->name('update_addons');
  Route::post('/addon/all_subscription','ThemeController@all_subscription')->middleware(['auth.shopify', 'billable'])->name('all_subscription');
  Route::post('/addon/update-credit-card','ThemeController@updateCreditCard')->middleware(['auth.shopify', 'billable'])->name('updatecreditCard');
  Route::post('/addon/install_addons','ThemeController@install_addons')->middleware(['auth.shopify', 'billable'])->name('install_addons');
  Route::post('/addon/delete_addons','ThemeController@delete_addons')->middleware(['auth.shopify', 'billable'])->name('delete_addons');
  Route::post('/addon/cancel_all_subscription','ThemeController@cancel_all_subscription')->middleware(['auth.shopify', 'billable'])->name('cancel_all_subscription');
  Route::post('/addon/update_All_subscription','ThemeController@update_All_subscription')->middleware(['auth.shopify', 'billable'])->name('update_All_subscription');
  Route::post('/addon/install_All_addons','ThemeController@install_All_addons')->middleware(['auth.shopify', 'billable'])->name('install_All_addons');
  Route::any('/get_theme_refresh','ThemeController@get_theme_refresh')->middleware(['auth.shopify', 'billable'])->name('get_theme_refresh');
  Route::any('/update_Active_addons','ThemeController@update_Active_addons')->middleware(['auth.shopify', 'billable'])->name('update_Active_addons');
  Route::post('/delete_multipl_addons','ThemeController@delete_multipl_addons')->middleware(['auth.shopify', 'billable'])->name('delete_multipl_addons');
  Route::get('/add_free_trail','ThemeController@add_free_trail')->middleware(['auth.shopify', 'billable'])->name('add_free_trail');
  /*---------*/
  Route::get('/getLicenseKey', 'ThemeController@getLicenseKey')->middleware('cors');
  Route::post('/getcoupon','ThemeController@getcoupon')->middleware('cors')->name('getcoupon');
  Route::post('/applycoupon','ThemeController@applycoupon')->middleware('auth.shopify')->name('applycoupon');
  Route::post('/addon/addchildstore','ThemeController@addchildstore')->name('addchildstore');
  Route::post('/addon/removechildstore','ThemeController@removechildstore')->name('removechildstore');
  /*---------*/
  Route::post('/prorateamount', 'ThemeController@proratedAmount')->middleware('auth.shopify')->name('prorateamount');
  Route::get('/update-all-shops', 'ThemeController@updateAllShopSubscriptions')->name('updateSubscriptions');
});

//admin
Route::group(['prefix' => 'admin'], function() {
  //Auth::routes(['register' => false]);
  //Auth::routes();
  // theme
  Route::get('/themes', 'HomeController@themes')->name('themes');
  Route::get('/addtheme', 'HomeController@addtheme')->name('addtheme');
  Route::post('/new_theme', 'HomeController@newtheme')->name('new_theme');

  // users
  Route::any('/dashboard', 'HomeController@index')->name('dashboard');
  Route::get('/dashboard/search','HomeController@usersSearch')->name('users_search');
  Route::post('/freeaddon', 'HomeController@freeaddon')->name('freeaddon');
  Route::post('/addtrialdays','HomeController@addtrialdays')->name('addtrialdays');

  // products
  Route::any('/products', 'HomeController@products')->name('products');
  Route::get('/addproduct', 'HomeController@addproduct')->name('addproduct');
  Route::post('/new_product', 'HomeController@newproduct')->name('new_product');
  Route::get('/show_product/{id}', 'HomeController@showproduct')->name('show_product');
  Route::post('/edit_product/{id}', 'HomeController@editproduct')->name('edit_product');
  Route::post('/delete_product/{id}', 'HomeController@deleteproduct')->name('delete_product');
  Route::get('/products/search','HomeController@productsSearch')->name('products_search');

  // courses
  Route::get('/courses', 'HomeController@courses')->name('courses');
  Route::get('/addcourse', 'HomeController@addcourse')->name('addcourse');
  Route::post('/create_course/post', 'HomeController@createcourse')->name('createcourse');
  Route::get('/show_course/{id}', 'HomeController@showcourse')->name('show_course');
  Route::post('/edit_course/{id}', 'HomeController@editcourse')->name('edit_course');
  Route::get('/courses/search','HomeController@coursesSearch')->name('courses_search');
  Route::post('/delete_course/{id}', 'HomeController@deleteCourse')->name('delete_course');
  Route::post('/delete_module', 'HomeController@deleteModule')->name('delete_module');
  Route::post('/delete_step', 'HomeController@deleteStep')->name('delete_step');

  Route::get('/search','HomeController@index')->name('search');
});

// Permanent redirects
Route::redirect('/cart/13601325514842:1', '/', 301);
Route::redirect('/pages/contact', '/', 301);
Route::redirect('/pages/changelog', '/', 301);
Route::redirect('/products/debutify-theme', '/', 301);
Route::redirect('/account', '/', 301);
Route::redirect('/account/login', '/', 301);
Route::redirect('/account/register', '/', 301);
Route::redirect('/checkouts', '/', 301);
Route::redirect('/collections', '/', 301);
Route::redirect('/policies/privacy-policy', '/privacy', 301);
Route::redirect('/policies/terms-of-service', '/terms', 301);
Route::redirect('/app/help', '/app/support', 301);
Route::redirect('/app/community', '/app/feedback', 301);
Route::redirect('/testimonials', '/reviews', 301);
Route::redirect('/training', '/', 301);
Route::redirect('/chat', '/', 301);


// Redirect to /app/*
// Route::redirect('/admin', '/app/admin', 302);
// Route::redirect('/theme', '/app/theme', 302);
// Route::redirect('/theme/download', '/app/theme/download', 302);
// Route::redirect('/add_ons', '/app/add_ons', 302);
// Route::redirect('/addon/activate_trust_badge', '/app/addon/activate_trust_badge', 302);
// Route::redirect('/courses', '/app/courses', 302);
// Route::redirect('/free-addons', '/app/free-addons', 302);
// Route::redirect('/plans', '/app/plans', 302);
