<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWinningProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winning_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('price')->nullable();
            $table->string('cost')->nullable();
            $table->string('profit')->nullable();
            $table->string('aliexpresslink')->nullable();
            $table->string('facebookadslink')->nullable();
            $table->string('googletrendslink')->nullable();
            $table->string('youtubelink')->nullable();
            $table->string('competitorlink')->nullable();
            $table->string('age');
            $table->string('gender');
            $table->string('placement');
            $table->string('saturationlevel');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winning_products');
    }
}
